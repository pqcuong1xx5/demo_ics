-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2020 at 06:50 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_sofa_10_5`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access` text CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `user_id`, `access`) VALUES
(1, 11, '{\"product\":[\"products\",\"add\",\"edit\",\"delete\",\"categories\",\"cat_add\",\"cat_edit\",\"cat_delete\"]}'),
(2, 12, '{\"menu\":[\"menulist\",\"add\",\"edit\",\"delete\"]}'),
(3, 2, '{\"product\":[\"products\",\"category_pro\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\",\"delete_once_comment\",\"delete_once_question\"],\"news\":[\"newslist\",\"categories\",\"tagsnews\",\"addnews\",\"delete_once_news\",\"cat_add_news\",\"del_catnews_once\"],\"menu\":[\"addmenu\",\"menulist\",\"delete\"],\"imageupload\":[\"banners\",\"addbanner\",\"delete_Banner_once\"],\"pages\":[\"pagelist\",\"addpage\",\"delete_page_once\"],\"contact\":[\"contacts\",\"delete\"],\"support\":[\"listSuport\",\"add\",\"delete_support_once\"],\"admin\":[\"site_option\",\"maps\",\"store_shopping\"]}'),
(4, 1, '{\"inuser\":[\"categories\",\"cate_add\",\"delete_cat_once\"],\"media\":[\"listAll\",\"categories\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\"],\"video\":[\"listAll\",\"category_video\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\"],\"product\":[\"products\",\"category_pro\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\",\"delete_once_comment\",\"delete_once_question\"],\"order\":[\"orders\",\"listSale\",\"listProvince\",\"delete_once_orders\",\"addSale\",\"del_once_sale\"],\"attribute\":[\"listBrand\",\"listLocale\",\"listColor\",\"listprice\",\"listOption\",\"addbrand\",\"delete_brand_once\",\"addlocale\",\"delete_locale_once\",\"addcolor\",\"delete_color_once\",\"addprice\",\"delete_price_once\",\"addoption\",\"delete_option_once\"],\"news\":[\"newslist\",\"categories\",\"tagsnews\",\"addnews\",\"delete_once_news\",\"cat_add_news\",\"del_catnews_once\"],\"tag\":[\"tag\"],\"menu\":[\"addmenu\",\"menulist\",\"delete\"],\"comment\":[\"comments\",\"questions\"],\"imageupload\":[\"banners\",\"addbanner\",\"delete_Banner_once\"],\"pages\":[\"pagelist\",\"addpage\",\"delete_page_once\"],\"contact\":[\"contacts\",\"delete\"],\"raovat\":[\"listraovat\",\"categories\",\"tagtinrao\",\"add\",\"delete_raovat_once\",\"cat_add\",\"del_cattinrao_once\"],\"email\":[\"emails\",\"delete\"],\"support\":[\"listSuport\",\"add\",\"delete_support_once\"],\"users\":[\"listuser_admin\",\"listusers\",\"add_users\",\"smslist\"],\"admin\":[\"site_option\",\"maps\",\"store_shopping\",\"setup_product\"],\"province\":[\"listDistric\",\"listWard\",\"street\"],\"report\":[\"soldout\",\"bestsellers\"]}'),
(5, 580, '{\"admin\":[\"\",\"site_option\",\"inuser\",\"comment\",\"email\",\"contact\"],\"users\":[\"delete\"],\"order\":[\"orders\",\"Deleteeorder\"],\"support\":[\"add\",\"edit\",\"x\\u00f3a\"],\"product\":[\"products\",\"add\",\"edit\",\"delete\",\"categories\",\"cat_add\",\"cat_edit\",\"listCodeSale\",\"cat_delete\",\"images\"],\"news\":[\"newslist\",\"add\",\"edit\",\"delete\",\"categories\",\"cat_add\",\"cat_edit\",\"delete_cat\"],\"pages\":[\"pagelist\",\"add\",\"edit\",\"delete\",\"action\"],\"menu\":[\"menulist\",\"add\",\"edit\",\"delete\"]}'),
(6, 612, '{\"media\":[\"listAll\",\"categories\"],\"video\":[\"listAll\",\"category_video\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\"]}'),
(7, 636, '{\"product\":[\"products\",\"category_pro\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\",\"delete_once_comment\",\"delete_once_question\"],\"order\":[\"orders\",\"listSale\",\"listProvince\",\"delete_once_orders\",\"addSale\",\"del_once_sale\"],\"news\":[\"newslist\",\"categories\",\"tagsnews\",\"addnews\",\"delete_once_news\",\"cat_add_news\",\"del_catnews_once\"],\"users\":[\"listuser_admin\",\"listusers\",\"delete_users_once\",\"add_users\"],\"admin\":[\"site_option\",\"maps\",\"store_shopping\"]}');

-- --------------------------------------------------------

--
-- Table structure for table `alias`
--

CREATE TABLE `alias` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` char(50) CHARACTER SET utf8 DEFAULT NULL,
  `item_id` int(11) DEFAULT 0,
  `new_cat` int(11) DEFAULT 0,
  `new` int(11) DEFAULT 0,
  `pro_cat` int(11) DEFAULT 0,
  `pro` int(11) DEFAULT 0,
  `page` int(11) DEFAULT 0,
  `m_cat` int(11) DEFAULT 0,
  `media` int(11) DEFAULT 0,
  `locale` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `inuser` int(11) DEFAULT NULL,
  `video_cat` int(11) DEFAULT NULL,
  `video` int(11) DEFAULT NULL,
  `services_cat` int(11) DEFAULT NULL,
  `services` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `alias`
--

INSERT INTO `alias` (`id`, `alias`, `type`, `item_id`, `new_cat`, `new`, `pro_cat`, `pro`, `page`, `m_cat`, `media`, `locale`, `brand`, `inuser`, `video_cat`, `video`, `services_cat`, `services`) VALUES
(2371, 'digital', 'cate-pro', 0, 0, 0, 45, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2373, 'computer2', 'cate-pro', 0, 0, 0, 47, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2372, 'car-electronics', 'cate-pro', 0, 0, 0, 46, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2370, 'amplifier', 'cate-pro', 0, 0, 0, 44, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2369, 'computer', 'cate-pro', 0, 0, 0, 43, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2377, 'man-hinh-may-tinh', 'pro', 0, 0, 0, 0, 89, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2301, 'tin-tuc', 'cate-new', 0, 4, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2378, 'man-hinh-may-tinh2', 'pro', 0, 0, 0, 0, 90, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2379, 'tai-nghe', 'pro', 0, 0, 0, 0, 91, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2380, 'laptop', 'cate-pro', 0, 0, 0, 51, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2381, 'camera', 'cate-pro', 0, 0, 0, 52, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2382, 'fridge', 'cate-pro', 0, 0, 0, 53, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2383, 'hairdryer', 'cate-pro', 0, 0, 0, 54, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2305, 'gioi-thieu', 'page', 0, 0, 0, 0, 0, 31, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2384, 'man-hinh-may-tinh2123', 'pro', 0, 0, 0, 0, 92, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2385, 'man-hinh-may-tinh32121', 'pro', 0, 0, 0, 0, 93, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2386, 'man-hinh-may-tinh42123', 'pro', 0, 0, 0, 0, 94, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2387, 'man-hinh-may-tinh52312', 'pro', 0, 0, 0, 0, 95, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2388, 'ps-vr', 'cate-pro', 0, 0, 0, 55, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2389, 'game-cards', 'cate-pro', 0, 0, 0, 56, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2390, 'playstation', 'cate-pro', 0, 0, 0, 57, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2391, 'headphone', 'cate-pro', 0, 0, 0, 58, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2365, 'luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'new', 0, 0, 11, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2366, 'nhung-luu-y-cuc-quan-trong-khi-chon-sofa-cho-gia-dinh', 'new', 0, 0, 12, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2367, 'chon-sofa-cho-phong-karaoke-sao-cho-dung-cach', 'new', 0, 0, 13, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2368, 'nhung-luu-y-khong-the-bo-qua-khi-ban-muon-mua-sofa-gia-re', 'new', 0, 0, 14, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2374, 'camcorders', 'cate-pro', 0, 0, 0, 48, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2375, 'accessories', 'cate-pro', 0, 0, 0, 49, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2376, 'television-dealers', 'cate-pro', 0, 0, 0, 50, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `code_sale`
--

CREATE TABLE `code_sale` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `code_sale`
--

INSERT INTO `code_sale` (`id`, `name`, `code`, `price`, `active`) VALUES
(10, 'Noel', 'ADCVX', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `comment` text CHARACTER SET utf8 DEFAULT NULL,
  `reply` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `review` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments_binhluan`
--

CREATE TABLE `comments_binhluan` (
  `id` int(11) NOT NULL,
  `id_sanpham` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `giatri` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `date` date NOT NULL,
  `flg` int(1) NOT NULL DEFAULT 0 COMMENT '0: moi binh luan; 1: xac nhan de hien thi',
  `reply` int(11) DEFAULT NULL,
  `review` tinyint(1) DEFAULT 0,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments_binhluan`
--

INSERT INTO `comments_binhluan` (`id`, `id_sanpham`, `comment`, `giatri`, `userid`, `parent_id`, `time`, `date`, `flg`, `reply`, `review`, `user_name`, `user_email`, `lang`) VALUES
(1, 5, 'nội dung đánh giá sản phẩm này rất tốt', 5, 0, 0, 1505698798, '2017-09-18', 0, 0, 1, 'trần mạnh', 'dangtranmanh051187@gmail.com', '1'),
(2, 5, 'nội dung bình luận', 0, 0, 0, 1505698841, '2017-09-18', 0, 0, 1, 'trần mạnh', 'dangtranmanh051187@gmail.com', '1'),
(3, 5, 'noi dung binh luận đánh giá nhận xét', 0, 0, 0, 1505699713, '2017-09-18', 0, 0, 0, 'trần mạnh', 'dangtranmanh051187@gmail.com', '1'),
(4, 5, 'nội dung bình luận tiếp theo', 4, 0, 0, 1505699941, '2017-09-18', 0, 0, 1, 'trần mạnh', 'dangtranmanh051187@gmail.com', '1'),
(5, 5, 'bình luận của vũ', 0, 0, 0, 1505700184, '2017-09-18', 0, 0, 1, 'trần long vũ', 'dangtranmanh051187@gmail.com', '1'),
(6, 5, 'bình luận của vũ', 0, 0, 0, 1505700223, '2017-09-18', 0, 0, 1, 'trần long vũ', 'dangtranmanh051187@gmail.com', '1'),
(7, 5, 'binh luận mới', 2, 0, 0, 1505700317, '2017-09-18', 0, 0, 1, 'tiến đạt', 'nguyentiendat@gmail.com', '1'),
(8, 5, 'noi dung binh luận', 1, 0, 0, 1505702973, '2017-09-18', 0, 0, 1, 'công sáng', 'congsang@gmail.com', '1'),
(9, 5, 'bình luận tiếp theo', 5, 0, 0, 1505703111, '2017-09-18', 0, 0, 1, 'công sáng', 'congsang@gmail.com', '1'),
(10, 5, 'noi trung tra loi binh luan', 4, 0, 0, 1505721191, '0000-00-00', 0, 7, 1, 'cong sangs', 'congsang@gmail.com', '1'),
(11, 4, 'Tốt', 5, 0, 0, 1505981714, '2017-09-21', 0, 0, 1, 'Vân', 'buivananh.th@gmail.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `config_banner`
--

CREATE TABLE `config_banner` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `field` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_banner`
--

INSERT INTO `config_banner` (`id`, `type`, `name`, `field`, `active`) VALUES
(1, '0', 'banner trang chủ', 'banner', '0'),
(2, '0', 'Slide', 'slide', '1'),
(3, '0', 'banner trái', 'left', '0'),
(4, '0', 'Banner phải', 'right', '0'),
(5, '0', 'banner top', 'top', '1'),
(6, '0', 'banner bottom', 'bottom', '0'),
(7, '0', 'Đối tác', 'partners', '1'),
(8, '1', 'colum danh mục', 'danhmuc', '0');

-- --------------------------------------------------------

--
-- Table structure for table `config_checkpro`
--

CREATE TABLE `config_checkpro` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `field` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `color` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_checkpro`
--

INSERT INTO `config_checkpro` (`id`, `type`, `name`, `field`, `color`, `active`) VALUES
(1, 'product', 'sản phẩm mới', 'hot', 'd73925', '0'),
(2, 'product', 'Trang chủ', 'home', '008d4c', '1'),
(3, 'product', 'sp khuyến mại', 'coupon', 'f39c12', '1'),
(4, 'product', 'sản phẩm nổi bật', 'focus', 'd352d4', '1'),
(5, 'product_category', 'Trang chủ', 'home', 'd73925', '1'),
(6, 'product_category', 'giao diện 2', 'hot', '008d4c', '1'),
(7, 'product_category', 'Nổi bật', 'focus', 'c3c3c3', '0'),
(8, 'product_category', 'Đặc biệt', 'coupon', 'd352d4', '0'),
(9, 'news', 'Trang chủ', 'home', 'd73925', '1'),
(10, 'news', 'Tin nổi bật', 'focus', '008d4c', '1'),
(11, 'news', 'Tin tức', 'hot', '4e8e94', '1'),
(12, 'news_category', 'Trang chủ', 'home', 'd73925', '1'),
(13, 'news_category', 'Tin công ty', 'hot', '4e8e94', '1'),
(14, 'news_category', 'Nổi bật', 'focus', 'c3c3c3', '0'),
(15, 'news_category', 'Danh mục bên trái', 'coupon', '0098da', '0'),
(16, 'media', 'Trang chủ', 'home', 'd73925', '1'),
(17, 'media', 'nổi bật', 'focus', '008d4c', '1'),
(18, 'media', 'Đặc biệt', 'hot', 'c3c3c3', '1'),
(19, 'media_category', 'Trang chủ', 'home', 'd73925', '0'),
(20, 'media_category', 'Mới', 'hot', '008d4c', '1'),
(21, 'media_category', 'Nổi bật', 'focus', 'c3c3c3', '0'),
(22, 'media_category', 'Cột trái', 'coupon', 'd352d4', '0'),
(23, 'video', 'Trang chủ', 'home', 'd73925', '0'),
(24, 'video', 'nổi bật', 'focus', '008d4c', '0'),
(25, 'video', 'Đặc biệt', 'hot', 'c3c3c3', '0'),
(26, 'video_category', 'Trang chủ', 'home', 'd73925', '1'),
(27, 'video_category', 'Mới', 'hot', '008d4c', '1'),
(28, 'video_category', 'Nổi bật', 'focus', 'c3c3c3', '0'),
(29, 'staticpage', 'Trang chủ', 'home', 'd73925', '1'),
(30, 'staticpage', 'Liên hệ', 'focus', '008d4c', '0'),
(31, 'staticpage', 'Đặc biệt', 'hot', 'c3c3c3', '0'),
(32, 'raovat', 'Trang chủ', 'home', 'd73925', '1'),
(33, 'raovat', 'nổi bật', 'focus', '008d4c', '1'),
(34, 'raovat', 'Đặc biệt', 'hot', 'c3c3c3', '1'),
(35, 'raovat_category', 'Trang chủ', 'home', 'd73925', '1'),
(36, 'raovat_category', 'Mới', 'hot', '008d4c', '1'),
(37, 'raovat_category', 'Nổi bật', 'focus', 'c3c3c3', '1'),
(38, 'product_category', 'Ảnh đại diện', 'image', '', '1'),
(39, 'news_category', 'ảnh danh mục news', 'image', NULL, '1'),
(40, 'staticpage', 'ảnh nội dung', 'image', NULL, '1'),
(41, 'video_category', 'ảnh danh mục video', 'image', NULL, '1'),
(42, 'media_category', 'ảnh danh mục media', 'image', NULL, '1'),
(43, 'product', 'giá cũ', 'price', NULL, '1'),
(44, 'product', 'giá bán', 'price_sale', NULL, '1'),
(45, 'product', 'thẻ tags', 'tags', NULL, '0'),
(46, 'hotline', 'Hiện thị hotline', 'hotline', '0', '1'),
(47, 'hotline', 'Chát facebook', 'chat_fanpage', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `config_menu`
--

CREATE TABLE `config_menu` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `field` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_menu`
--

INSERT INTO `config_menu` (`id`, `type`, `name`, `field`, `active`) VALUES
(2, 'top', 'menu mobile', NULL, '1'),
(3, 'left', 'menu left', NULL, '0'),
(4, 'right', 'Product Category', NULL, '1'),
(5, 'bottom', 'Menu bottom', NULL, '1'),
(6, 'tag', 'menu tag', NULL, '0'),
(7, 'bottom_2', 'menu bottom 2', NULL, '0'),
(8, 'bottom_3', 'menu bottom 3', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `config_wiget`
--

CREATE TABLE `config_wiget` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `field` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_wiget`
--

INSERT INTO `config_wiget` (`id`, `type`, `name`, `field`, `active`) VALUES
(1, NULL, 'banner trang chủ', 'banner', '1'),
(2, NULL, 'slide', 'slide', '1'),
(3, NULL, 'banner trái', 'left', '0'),
(4, NULL, 'Banner phải', 'right', '0'),
(5, NULL, 'banner top', 'top', '0'),
(6, NULL, 'banner bottom', 'bottom', '0'),
(7, NULL, 'đối tác', 'partners', '1');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `comment` text CHARACTER SET utf8 DEFAULT NULL,
  `mark` tinyint(1) DEFAULT 0,
  `show` tinyint(1) DEFAULT 0,
  `time` int(11) DEFAULT NULL,
  `cat_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `full_name`, `phone`, `email`, `address`, `city`, `country`, `comment`, `mark`, `show`, `time`, `cat_name`, `title`) VALUES
(29, 'Jones', '416-385-3200', 'eric@talkwithcustomer.com', '', NULL, NULL, 'Hello sofabaolam.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website sofabaolam.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website sofabaolam.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you\'d like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=sofabaolam.com\r\n', 0, 0, 1562313579, NULL, NULL),
(30, 'Jones', '416-385-3200', 'eric@talkwithcustomer.com', '', NULL, NULL, 'Hello sofabaolam.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website sofabaolam.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website sofabaolam.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you\'d like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=sofabaolam.com\r\n', 0, 0, 1562408543, NULL, NULL),
(31, 'WilliamJam', '277241778', 'raphaeaccogeondess@gmail.com', 'https://www.google.com', NULL, NULL, 'Good day!  sofabaolam.com \r\n \r\nWe make available \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the Communication partition. Contact form are filled in by our software and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This method increases the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161', 0, 0, 1564187790, NULL, NULL),
(32, 'BusinessCapitalAdvisors', '84458992372', 'noreply@business-capital-advisors.com', 'http://Business-Capital-Advisors.com', NULL, NULL, 'Hi, letting you know that http://Business-Capital-Advisors.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://Business-Capital-Advisors.com \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://Business-Capital-Advisors.com \r\n \r\nHave a great day, \r\nThe Business Capital Advisors Team \r\n \r\nunsubscribe/remove - http://business-capital-advisors.com/r.php?url=sofabaolam.com&id=e164', 0, 0, 1564492387, NULL, NULL),
(33, 'GetBusinessFundingNow', '88129353178', 'noreply@get-business-funding-now.com', 'http://Get-Business-Funding-Now.com', NULL, NULL, 'Hi, letting you know that http://Get-Business-Funding-Now.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://Get-Business-Funding-Now.com \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://Get-Business-Funding-Now.com \r\n \r\nHave a great day, \r\nThe Get Business Funding Now Team \r\n \r\nunsubscribe/remove - http://get-business-funding-now.com/r.php?url=sofabaolam.com&id=e164', 0, 0, 1564658819, NULL, NULL),
(34, 'Chiman', '0345 38 58 55', 'aly1@alychidesigns.com', 'Messedamm 8', NULL, NULL, 'Hello there, My name is Aly and I would like to know if you would have any interest to have your website here at sofabaolam.com  promoted as a resource on our blog alychidesign.com ?\r\n\r\n We are  updating our do-follow broken link resources to include current and up to date resources for our readers. If you may be interested in being included as a resource on our blog, please let me know.\r\n\r\n Thanks, Aly', 0, 0, 1564738400, NULL, NULL),
(35, 'FindBusinessFunding247', '84426721935', 'noreply@findbusinessfunding24-7.com', 'http://FindBusinessFunding24-7.com', NULL, NULL, 'Hi, letting you know that http://FindBusinessFunding24-7.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://FindBusinessFunding24-7.com \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://FindBusinessFunding24-7.com \r\n \r\nHave a great day, \r\nThe Find Business Funding 247 Team \r\n \r\nunsubscribe/remove - http://findbusinessfunding24-7.com/r.php?url=sofabaolam.com&id=e164', 0, 0, 1565054844, NULL, NULL),
(36, 'GetBusinessFundedNow', '87381293234', 'noreply@get-business-funded-now.info', 'http://Get-Business-Funded-Now.info', NULL, NULL, 'Faster and Simpler than the SBA, http://Get-Business-Funded-Now.info can get your business a loan for $2K-350,000 With low-credit and no collateral. \r\n \r\nUse our short form to Find Out exactly how much you can get, No-Cost: \r\n \r\nhttp://Get-Business-Funded-Now.info \r\n \r\nIf you\'ve been in business for at least a year you are already pre-qualified. Our Quick service means funding can be completed within 48 hours. Terms are specific for each business so I suggest applying to find out exactly how much you can get. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. Funds are also Non-Restrictive, allowing you to use the full amount in any way including bills, taxes, hiring, marketing, expansion, or Absolutely Any Other expense. \r\n \r\nThere are limited SBA and private funds available so please apply now if interested, \r\n \r\nClick Here: http://Get-Business-Funded-Now.info \r\n \r\nHave a great day, \r\nThe Get Business Funded Now Team \r\n \r\nunsubscribe here - http://get-business-funded-now.info/r.php?url=sofabaolam.com&id=e165', 0, 0, 1565397220, NULL, NULL),
(37, 'Jones', '416-385-3200', 'eric@talkwithcustomer.com', '', NULL, NULL, 'Hello sofabaolam.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website sofabaolam.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website sofabaolam.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you\'d like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=sofabaolam.com\r\n', 0, 0, 1565578311, NULL, NULL),
(38, 'Martin', '01.84.67.65.56', 'george1@georgemartinjr.com', '93 Rue Joseph Vernet', NULL, NULL, 'Would you be interested in submitting a guest post on georgemartjr.com or possibly allowing us to submit a post to sofabaolam.com ? Maybe you know by now that links are essential\r\nto building a brand online? If you are interested in submitting a post and obtaining a link to sofabaolam.com , let me know and we will get it published in a speedy manner to our blog.\r\n\r\nHope to hear from you soon\r\nGeorge', 0, 0, 1566293430, NULL, NULL),
(39, '228148', '', '', 'http://Get-My-Business-Funded.com', NULL, NULL, 'Quicker and Easier than the SBA, http://Get-My-Business-Funded.com can get your business a loan for $2K-350,000 With low-credit and no collateral. \r\n \r\nUse our quick form to Find Out exactly how much you can get, No-Cost: \r\n \r\nhttp://Get-My-Business-Funded.com \r\n \r\nIf you\'ve been established for at least one year you are already pre-qualified. Our Fast service means funding can be completed within 48 hours. Terms are specific for each business so I suggest applying to find out exactly how much you can get. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. Funds have no Restrictions, allowing you to use the whole amount in any way including bills, taxes, hiring, marketing, expansion, or Absolutely Any Other expense. \r\n \r\nThere are limited SBA and private funds available so please apply now if interested, \r\n \r\nClick Here: http://Get-My-Business-Funded.com \r\n \r\nHave a great day, \r\nThe Get My Business Funded Team \r\n \r\nunsubscribe here - http://get-my-business-funded.com/r.php?url=sofabaolam.com&id=e172', 0, 0, 1567209541, NULL, NULL),
(40, 'Stearns', '0378 1046619', 'noreply@thewordpressclub6460.shop', 'Via Solfatara 89', NULL, NULL, 'Hello,\r\n\r\nAre you utilizing Wordpress/Woocommerce or maybe will you plan to work with it as time goes by ? We currently provide much more than 2500 premium plugins but also themes 100 percent free to get : http://repic.xyz/DTdYB\r\n\r\nRegards,\r\n\r\nKindra', 0, 0, 1567265168, NULL, NULL),
(41, 'JosephHig', '83382598776', 'noreplymonkeydigital@gmail.com', 'https://monkeydigital.co/product/network-traffic-offer/', NULL, NULL, 'Bounce rate. \r\nNetwork traffic to boost ranks and exposure. \r\n \r\nNEW! Now you can choose the Country you want the traffic to come from, as well. \r\n \r\nSupercharge Your SEO And Boost Your Alexa Ranking with 1 Million unique Visitors Traffic sent Within 1 Month. Available only Here. Cheapest Offer On the Internet And Exclusively Available on Monkey Digital \r\n \r\nRead More details about our great offer: \r\nhttps://monkeydigital.co/product/network-traffic-offer/ \r\n \r\n \r\nThanks and regards \r\nMike \r\nMonkey Digital \r\nmonkeydigital.co@gmail.com', 0, 0, 1567586624, NULL, NULL),
(42, 'Ronalddop', '81267245853', 'cribabi01@aol.com', 'http://genbtocanda.tk/re2l', NULL, NULL, 'There is an interestingoffer for you. http://nsecaromoj.tk/wx5n', 0, 0, 1567597425, NULL, NULL),
(43, 'RobertAccix', '86553253621', 'quickchain50@gmail.com', 'https://quickchain.cc/', NULL, NULL, 'Profit +10% after 2 days \r\nThe minimum amount for donation is 0.0025 BTC. \r\nMaximum donation amount is 10 BTC. \r\n \r\nRef bonus 3 levels: 5%,3%,1% paying next day after donation \r\nhttps://quickchain.cc/', 0, 0, 1568116997, NULL, NULL),
(44, 'AveryfaunK', '87142847174', 'raphaeaccogeondess@gmail.com', 'https://www.google.com', NULL, NULL, 'Hello!  sofabaolam.com \r\n \r\nHave you ever heard of sending messages via feedback forms? \r\n \r\nImagine that your message will be readseen by hundreds of thousands of your future customerscustomers. \r\nYour message will not go to the spam folder because people will send the message to themselves. As an example, we have sent you our suggestion  in the same way. \r\n \r\nWe have a database of more than 30 million sites to which we can send your offer. Sites are sorted by country. Unfortunately, you can only select a country when sending a message. \r\n \r\nThe cost of one million messages 49 USD. \r\nThere is a discount program when you purchase  more than two million letter packages. \r\n \r\n \r\nFree trial mailing of 50,000 messages to any country of your selection. \r\n \r\n \r\nThis offer is created automatically. Please use the contact details below to contact us. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - Contact@feedbackmessages.com', 0, 0, 1568195720, NULL, NULL),
(45, 'DorothyUnomi', '84865455217', 'lh@hlaw.at', 'http://facebook.comпјЏmailпјЏ@0X4E18DCC7/Jf32ri', NULL, NULL, 'We would like to inform that you liked a comment ID:35915743 in a social network , January 9, 2019 at 19:48 \r\nThis like has been randomly selected to win the seasonal «Like Of The Year» 2019 award! \r\nhttp://facebook.com+email+@1310252231/vDHVB', 0, 0, 1568431346, NULL, NULL),
(46, 'Robertelupe', '82278595745', 'huyblockchain@gmail.com', 'https://www.google.com', NULL, NULL, 'Tạo, chia sẻ và nhúng các album, catalogue sản phẩm, sách, tạp chí trực tuyến, chuyển đổi các tệp PDF của bạn thành sách lật trình diễn trực tuyến tại website https://zalaa.me/', 0, 0, 1568790288, NULL, NULL),
(47, '112451', '', '', 'http://BusinessLoansFundedNow.com', NULL, NULL, 'Faster and Easier than the SBA, http://BusinessLoansFundedNow.com?url=sofabaolam.com can get your business a loan for $2K-350,000 With low-credit and no collateral. \r\n \r\nUse our fast form to See exactly how much you can get, No-Cost: \r\n \r\nhttp://BusinessLoansFundedNow.com?url=sofabaolam.com \r\n \r\nIf you\'ve been established for at least 12 months you are already pre-qualified. Our Fast service means funding can be completed within 48 hours. Terms are specific for each business so I suggest applying to find out exactly how much you can get. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. Funds have no Restrictions, allowing you to use the full amount in any way including bills, taxes, hiring, marketing, expansion, or Absolutely Any Other expense. \r\n \r\nThere are limited SBA and private funds available so please apply now if interested, \r\n \r\nClick Here: http://BusinessLoansFundedNow.com?url=sofabaolam.com \r\n \r\nHave a great day, \r\nThe Business Loans Funded Now Team \r\n \r\nremove here - http://businessloansfundednow.com/r.php?url=sofabaolam.com&id=pd19', 0, 0, 1568999970, NULL, NULL),
(48, '338795', '', '', 'http://Business-Loans-Funded.com', NULL, NULL, 'Quicker and Simpler than the SBA, http://Business-Loans-Funded.com?url=sofabaolam.com can get your business a loan for $2K-350,000 With low-credit and without collateral. \r\n \r\nUse our 1 minute form to Find Out exactly how much you can get, No-Cost: \r\n \r\nhttp://Business-Loans-Funded.com?url=sofabaolam.com \r\n \r\nIf you\'ve been in business for at least 1 year you are already pre-qualified. Our Quick service means funding can be finished within 48hrs. Terms are specific for each business so I suggest applying to find out exactly how much you can get. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. Funds are also Non-Restrictive, allowing you to use the full amount in any way including bills, taxes, hiring, marketing, expansion, or Absolutely Any Other expense. \r\n \r\nThere are limited SBA and private funds available so please apply now if interested, \r\n \r\nClick Here: http://Business-Loans-Funded.com?url=sofabaolam.com \r\n \r\nHave a great day, \r\nThe Business Loans Funded Team \r\n \r\nremove here - http://business-loans-funded.com/r.php?url=sofabaolam.com&id=e173', 0, 0, 1569465405, NULL, NULL),
(49, 'David Gomez', '81275436795', 'sergiodumass@gmail.com', 'https://www.google.com', NULL, NULL, 'Dearest in mind, \r\n \r\nI would like to introduce myself for the first time. My name is Barrister David Gomez Gonzalez, the personal lawyer to my late client. \r\nWho worked as a private businessman in the international field. In 2012, my client succumbed to an unfortunate car accident. My client was single and childless. \r\nHe left a fortune worth $12,500,000.00 Dollars in a bank in Spain. The bank sent me message that I have to introduce a beneficiary or the money in their bank will be confiscate. My purpose of contacting you is to make you the Next of Kin. \r\nMy late client left no will, I as his personal lawyer, was commissioned by the Spanish Bank to search for relatives to whom the money left behind could be paid to. I have been looking for his relatives for the past 3 months continuously without success. Now I explain why I need your support, I have decided to make a citizen of the same country with my late client the Next of Kin. \r\n \r\nI hereby ask you if you will give me your consent to present you to the Spanish Bank as the next of kin to my deceased client. \r\nI would like to point out that you will receive 45% of the share of this money, 45% then I would be entitled to, 10% percent will be donated to charitable organizations. \r\n \r\nIf you are interested, please contact me at my private contact details by Tel: 0034-604-284-281, Fax: 0034-911-881-353, Email: amucioabogadosl019@gmail.com \r\nI am waiting for your answer \r\nBest regards, \r\n \r\nLawyer: - David Gomez Gonzalez', 0, 0, 1569510378, NULL, NULL),
(50, 'Steveuniof', '81532743244', 'steveKt@gmail.com', 'https://advertisingagencymiami.net/cheap-seo-packages/', NULL, NULL, 'hi there \r\nWe provide best monthly affordable SEO packages & SEO services prices starting $49, Pay for performance based plans & pricing, that’s uniquely tailored to your website, we would be more than happy to create a campaign which accommodates your company’s needs, enabling you to achieve your business goals. Search Engine Optimization is a methodology of strategies, systems and plans to get more traffic on website, increase search engine visibility & ranking. \r\n \r\nCheck out our plans \r\nhttps://advertisingagencymiami.net/cheap-seo-packages/ \r\n \r\nWe know how to get you into top safely, without risking your investment during google updates \r\n \r\nthanks and regards \r\nSteve \r\nstevewebberr@mail.com', 0, 0, 1569634297, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` int(11) UNSIGNED DEFAULT NULL,
  `gender` tinyint(3) UNSIGNED DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province` int(11) UNSIGNED DEFAULT NULL,
  `district` int(11) UNSIGNED DEFAULT NULL,
  `ward` int(10) UNSIGNED DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment` int(11) DEFAULT NULL,
  `date` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `code`, `phone`, `email`, `birthday`, `gender`, `address`, `place`, `province`, `district`, `ward`, `company`, `tax_code`, `note`, `user_id`, `payment`, `date`, `time`, `date_time`) VALUES
(17, 'Hồng Thất Công', 'KH17', '0986083468', 'hongthatcong@gmail.com', 17, 1, '', NULL, NULL, NULL, NULL, 'Cái Bang', '3643bhfsdhfds', '', 2, NULL, NULL, '1526551811', NULL),
(18, 'Tiều Cái', 'KH18', '09647239064', 'tieucai@luongson.com', 17, 1, '108 Lương Sơn', NULL, NULL, NULL, NULL, 'Lương Sơn Bạc', 'DV4364326', '', 2, NULL, NULL, '1526551875', NULL),
(19, 'Tào Tháo', 'KH19', '06949326935', 'taothao@tamquoc.com', 17, 1, '', NULL, NULL, NULL, NULL, 'Tam Quốc Diễn Nghĩa', '634ggdsgsgDG', '', 2, NULL, NULL, '1526551937', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_debt`
--

CREATE TABLE `customer_debt` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_create` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nkd` float(22,0) DEFAULT 0,
  `ghino` float(22,0) DEFAULT 0,
  `ghico` float(22,0) DEFAULT 0,
  `nkc` float(22,0) DEFAULT 0,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` int(11) DEFAULT NULL,
  `time_insert` int(11) DEFAULT NULL,
  `note` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_debt`
--

INSERT INTO `customer_debt` (`id`, `id_customer`, `id_create`, `code`, `nkd`, `ghino`, `ghico`, `nkc`, `type`, `date_time`, `time_insert`, `note`) VALUES
(19, 14, NULL, 'HD38', 0, 760000, 0, -860000, 'Bán hàng', 1526490000, 1526527753, 0),
(20, 14, 580, 'HD38', -860000, 0, 860000, 0, 'Thanh toán tiền hàng', 1526490000, 1526540055, 0),
(21, 13, NULL, 'HD39', 0, 1760000, 0, -1792000, 'Bán hàng', 1526490000, 1526541631, 0),
(22, 18, 2, 'HD40', 0, 358000, 0, -450840, 'Bán hàng', 1526490000, 1526551961, 0);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `pre` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `provinceid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`, `pre`, `provinceid`) VALUES
(106, 'Bến Lức', 'Huyện', 8),
(121, 'Bắc Trà My', 'Huyện', 9),
(139, 'Bà Rịa', 'Thị xã', 10),
(147, 'Buôn Đôn', 'Huyện', 11),
(162, ' Thới Lai', 'Huyện', 12),
(171, 'Bắc Bình', 'Huyện', 13),
(181, 'Bảo Lâm', 'Huyện', 14),
(193, 'A Lưới', 'Huyện', 15),
(202, 'An Biên', 'Huyện', 16),
(217, 'Bắc Ninh', 'Thành phố', 17),
(225, 'Ba Chẽ', 'Huyện', 18),
(239, 'Bá Thước', 'Huyện', 19),
(266, 'Anh Sơn', 'Huyện', 20),
(287, 'Bình Giang', 'Huyện', 21),
(299, 'An Khê', 'Thị xã', 22),
(316, 'Bình Long', 'Thị xã', 23),
(327, 'Ân Thi', 'Huyện', 24),
(337, 'An Lão', 'Huyện', 25),
(348, 'Cái Bè', 'Huyện', 26),
(359, 'Đông Hưng', 'Huyện', 27),
(367, 'Bắc Giang', 'Thành phố', 28),
(377, 'Cao Phong', 'Huyện', 29),
(388, 'An Phú', 'Huyện', 30),
(399, 'Bình Xuyên', 'Huyện', 31),
(408, 'Bến Cầu', 'Huyện', 32),
(417, 'Đại Từ', 'Huyện', 33),
(426, 'Bắc Hà', 'Huyện', 34),
(435, 'Giao Thủy', 'Huyện', 35),
(445, 'Ba Tơ', 'Huyện', 36),
(459, 'Ba Tri', 'Huyện', 37),
(468, 'Cư Jút', 'Huyện', 38),
(476, 'Cà Mau', 'Thành phố', 39),
(485, 'Bình Minh', 'Huyện', 40),
(493, 'Gia Viễn', 'Huyện', 41),
(501, 'Cẩm Khê', 'Huyện', 42),
(514, 'Bác Ái', 'Huyện', 43),
(521, 'Đông Hòa', 'Huyện', 44),
(530, 'Bình Lục', 'Huyện', 45),
(536, 'Cẩm Xuyên', 'Huyện', 46),
(548, 'Cao Lãnh', 'Thành phố', 47),
(560, 'Châu Thành', 'Huyện', 48),
(571, 'Đăk Glei', 'Huyện', 49),
(581, 'Ba Đồn', 'Thị xã', 50),
(589, 'Cam Lộ', 'Huyện', 51),
(599, 'Càng Long', 'Huyện', 52),
(607, 'Châu Thành', 'Huyện', 53),
(614, 'Bắc Yên', 'Huyện', 54),
(626, 'Bạc Liêu', 'Thành phố', 55),
(633, 'Lục Yên', 'Huyện', 56),
(642, 'Chiêm Hóa', 'Huyện', 57),
(649, 'Điện Biên', 'Huyện', 58),
(659, 'Lai Châu', 'Thị xã', 59),
(678, 'Bắc Mê', 'Huyện', 61),
(689, 'Ba Bể', 'Huyện', 62),
(697, 'Bảo Lạc', 'Huyện', 63);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `parent_id` int(11) DEFAULT 0,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(3) DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT '1',
  `active` int(1) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `name`, `parent_id`, `description`, `sort`, `lang`, `active`) VALUES
(1, 'Hướng dẫn tổng quan về quản trị website', 0, '<p><a href=\"http://giaodiendep.vn/huongdan/\">Xem video hướng dẫn</a></p>\r\n', 1, 'vi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `email` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `url` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `target` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `sort` int(3) DEFAULT NULL,
  `cate` int(4) DEFAULT 0,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `content` text CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `type`, `url`, `title`, `target`, `name`, `image`, `id_item`, `sort`, `cate`, `lang`, `content`) VALUES
(200, NULL, NULL, NULL, '_self', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(216, NULL, NULL, 'Video hướng dẫn lắp đặt camera 2', '_self', '12', 'upload/img/media/9f62009350cb11a54f10ffd7c56e1bca.png', 1, 2, 0, NULL, 'https://www.youtube.com/embed/QzqthoS3Xkw'),
(217, NULL, NULL, 'HƯỚNG DẪN LẮP ĐẶT HỆ THỐNG CAMERA QUAN SÁT', '_self', '12', 'upload/img/media/14fca64f4ab55bddda0d89209d9d8c80.png', 1, 3, 0, NULL, 'https://www.youtube.com/embed/JdrNRXs8KqI'),
(218, NULL, NULL, 'Hướng dẫn cấu hình Camera xem qua mạng 100% thành công', '_self', '12', 'upload/img/media/9f62009350cb11a54f10ffd7c56e1bca1.png', 1, 4, 0, NULL, 'https://www.youtube.com/embed/Q27P_jphAXU'),
(219, NULL, NULL, 'Video Clip Hướng dẫn sử dụng Camera IP Wifi không dây thông minh Webvision 6203', '_self', '12', 'upload/img/media/9f62009350cb11a54f10ffd7c56e1bca2.png', 1, 5, 0, NULL, 'https://www.youtube.com/embed/isA3QHA4wOM'),
(291, 'slide', 'https://ics.com.vn/', 'Tablet', '_self', NULL, 'upload/img/banner/banner-product-1.jpg', NULL, NULL, NULL, 'vi', '<p>Up&nbsp;to&nbsp;20%&nbsp;off</p>\r\n'),
(292, 'slide', 'https://ics.com.vn/', 'Laptop', '_self', NULL, 'upload/img/banner/banner-product-21.jpg', NULL, NULL, NULL, 'vi', '<p>NEW&nbsp;PROduct</p>\r\n'),
(294, 'top', 'https://ics.com.vn/', 'on day sale!', '_self', NULL, 'upload/img/banner/banner5.jpg', NULL, NULL, NULL, 'vi', '<p>lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur adipiscing&nbsp;elit</p>\r\n'),
(295, 'top', 'https://ics.com.vn/', 'Smart Watches', '_self', NULL, 'upload/img/banner/banner3.jpg', NULL, NULL, NULL, 'vi', '<p>lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur adipiscing&nbsp;elit</p>\r\n\r\n<div id=\"gtx-trans\" style=\"left:-87px; position:absolute; top:38px\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>\r\n'),
(296, 'partners', '', 'partner1', '_self', NULL, 'upload/img/banner/partner1.png', NULL, NULL, NULL, 'vi', ''),
(297, 'partners', '', 'partner2', '_self', NULL, 'upload/img/banner/partner3.png', NULL, NULL, NULL, 'vi', ''),
(298, 'partners', '', 'partner3', '_self', NULL, 'upload/img/banner/partner2.png', NULL, NULL, NULL, 'vi', ''),
(299, 'partners', '', 'partner4', '_self', NULL, 'upload/img/banner/partner11.png', NULL, NULL, NULL, 'vi', ''),
(300, 'slide', '', 'partner5', '_self', NULL, 'upload/img/banner/partner21.png', NULL, NULL, NULL, 'vi', ''),
(301, 'partners', '', 'partner6', '_self', NULL, 'upload/img/banner/partner22.png', NULL, NULL, NULL, 'vi', ''),
(302, 'partners', '', 'partner7', '_self', NULL, 'upload/img/banner/partner31.png', NULL, NULL, NULL, 'vi', '');

-- --------------------------------------------------------

--
-- Table structure for table `inuser`
--

CREATE TABLE `inuser` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `hot` int(11) DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `tag` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `focus` int(11) DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inuser`
--

INSERT INTO `inuser` (`id`, `title`, `description`, `hot`, `image`, `content`, `alias`, `lang`, `tag`, `time`, `category_id`, `home`, `focus`, `title_seo`, `keyword_seo`, `description_seo`) VALUES
(4, 'Rực Rỡ Mùa Hoa Tây Bắc', 'Tết Nguyên Đán 2015 là thời khắc quan trọng nhất trong năm, là khi mỗi gia đình Việt Nam có thời gian được trở về quây quần bên nhau và tưng bừng du xuân khắp mọi miền đất nước. Trong không khí xuân nồng ấm ấy, Vietravel hân hạnh gửi tới Quý khách hàng ngàn đường tour Việt Nam để gia đình bạn thỏa sức tận hưởng những ngày lễ vui tươi, hạnh phúc, đón chào năm mới An khang Thịnh Vượng. \n', 1, 'upload/img/ava1_hoanhai1.jpg', '<div>&nbsp;</div>\n\n<div>\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 1 : TP.HCM - NỘI B&Agrave;I (H&Agrave; NỘI) &ndash; ĐỀN H&Ugrave;NG - NGHĨA LỘ Số bữa ăn: 3 bữa&nbsp;</strong></p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_141103_mien%20bac%20-%20den%20hung.jpg\" style=\"border:0px; box-sizing:border-box; height:458px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Qu&yacute; kh&aacute;ch tập trung tại cột số 04 ga đi Trong Nước - S&acirc;n bay T&acirc;n Sơn Nhất để hướng dẫn l&agrave;m thủ tục cho Qu&yacute; kh&aacute;ch đ&aacute;p chuyến bay đi H&agrave; Nội. Xe Vietravel đ&oacute;n đo&agrave;n tại s&acirc;n bay Nội B&agrave;i, khởi h&agrave;nh đi Y&ecirc;n B&aacute;i. Tr&ecirc;n đường đi Qu&yacute; kh&aacute;ch gh&eacute; Ph&uacute; Thọ viếng Đền H&ugrave;ng, đến nơi, Qu&yacute; kh&aacute;ch l&agrave;m lễ d&acirc;ng hương đất tổ, tham quan đền Thượng, đền Trung, đền Hạ, Giếng Ngọc, Lăng vua H&ugrave;ng, tự do chụp ảnh mua sắm qu&agrave; lưu niệm. Đo&agrave;n tiếp tục khởi h&agrave;nh đi Nghĩa Lộ, nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi. Buổi tối, Qu&yacute; kh&aacute;ch thưởng thức chương tr&igrave;nh biểu diễn m&uacute;a X&ograve;e, giao lưu v&agrave; t&igrave;m hiểu n&eacute;t văn h&oacute;a đặc sắc của d&acirc;n tộc Th&aacute;i. Nghỉ đ&ecirc;m tại Nghĩa Lộ.</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 2 : NGHĨA LỘ - M&Ugrave; CANG CHẢI - SAPA (Số bữa ăn: 3 bữa)</strong></p>\n\n<p style=\"text-align:center\"><strong><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_141103_sapa%20-%20muong%20hoa%202.jpg\" style=\"border:0px; box-sizing:border-box; height:408px; vertical-align:middle; width:650px\" /></strong></p>\n\n<p style=\"text-align:justify\">Trả ph&ograve;ng kh&aacute;ch sạn, đo&agrave;n khởi h&agrave;nh đi M&ugrave; Cang Chải, ngang qua T&uacute; Lệ, Qu&yacute; kh&aacute;ch sẽ ngửi được m&ugrave;i hương thoang thoảng theo gi&oacute; bảng lảng tr&ecirc;n m&aacute;i nh&agrave; của &ldquo;cơm mới&rdquo;, nơi đ&acirc;y nổi tiếng với x&ocirc;i nếp, cốm kh&ocirc;. Đến Đ&egrave;o Khau Phạ Qu&yacute; kh&aacute;ch dừng chụp ảnh v&agrave; ngắm nh&igrave;n Bản L&igrave;m M&ocirc;ng xinh đẹp tọa lạc dưới ch&acirc;n đ&egrave;o. Đ&acirc;y l&agrave; Bản của d&acirc;n tộc M&ocirc;ng v&agrave; l&agrave; nơi c&oacute; ruộng l&uacute;a đẹp nhất M&ugrave; Cang Chải. Qua đ&egrave;o Khau Phạ v&agrave;o địa phận M&ugrave; Cang Chải, Qu&yacute; kh&aacute;ch sẽ bị m&ecirc; hoặc bởi vẻ đẹp h&uacute;t hồn của cung đường ruộng bậc thang (Nổi tiếng tại 3 x&atilde;: La P&aacute;n Tẩn, Chế Cu Nha v&agrave; Zế Xu Ph&igrave;nh). Đo&agrave;n chi&ecirc;m ngưỡng những thung lũng rộng h&uacute;t tầm mắt, c&aacute;c thửa ruộng tầng tầng lớp lớp lượn s&oacute;ng theo sườn n&uacute;i, ngọn n&uacute;i n&agrave;y nối tiếp ngọn n&uacute;i kh&aacute;c. Qu&yacute; kh&aacute;ch c&oacute; thể tham quan v&agrave; thưởng ngoạn c&aacute;c giai đoạn của m&ugrave;a l&uacute;a: m&ugrave;a nước đổ &oacute;ng &aacute;nh tr&ecirc;n c&aacute;c triền n&uacute;i (th&aacute;ng 2-3), m&ugrave;a cấy l&uacute;a (th&aacute;ng 5), m&ugrave;a l&uacute;a non (th&aacute;ng 6-7) v&agrave; đẹp nhất l&agrave;m m&ugrave;a l&uacute;a ch&iacute;n hay c&ograve;n lại l&agrave; m&ugrave;a v&agrave;ng (th&aacute;ng 9-10). Cũng ch&iacute;nh bởi vẻ đẹp m&ecirc; l&ograve;ng người v&agrave;o m&ugrave;a l&uacute;a ch&iacute;n m&agrave; Ruộng Bậc Thang ở ba x&atilde; n&agrave;y đ&atilde; được xếp hạng Di t&iacute;ch Quốc Gia năm 2007. Đến thị trấn M&ugrave; Cang Chải, Qu&yacute; kh&aacute;ch ăn trưa, nghỉ ngơi. Chiều đo&agrave;n khởi h&agrave;nh đi Sa Pa, tr&ecirc;n đường đi Qu&yacute; kh&aacute;ch dừng ch&acirc;n ngắm to&agrave;n cảnh đồi ch&egrave; T&acirc;n Uy&ecirc;n thơ mộng v&agrave; tiếp tục sẽ được chi&ecirc;m ngưỡng phong cảnh n&uacute;i rừng T&acirc;y Bắc h&ugrave;ng vĩ tr&ecirc;n Đ&egrave;o &Ocirc; Quy Hồ - Ranh giới giữa 2 tỉnh L&agrave;o Cai v&agrave; Lai Ch&acirc;u uốn lượn quanh d&atilde;y Ho&agrave;ng Li&ecirc;n c&ograve;n gọi l&agrave; khu vực Cổng Trời. Đến Sa Pa, nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi. Buổi tối, Qu&yacute; kh&aacute;ch tự do tham quan phố n&uacute;i v&agrave; thưởng thức những m&oacute;n ăn đặc sản tại nơi đ&acirc;y. Nghỉ đ&ecirc;m tại Sa Pa</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 3 : SAPA - LAI CH&Acirc;U (Số bữa ăn: 3 bữa)</strong></p>\n\n<p style=\"text-align:center\"><strong><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_141103_sapa%20trong%20suong%201.jpg\" style=\"border:0px; box-sizing:border-box; height:436px; vertical-align:middle; width:650px\" /></strong></p>\n\n<p style=\"text-align:justify\">Qu&yacute; kh&aacute;ch tham quan v&agrave; chinh phục N&uacute;i H&agrave;m Rồng, thăm Vườn Lan khoe sắc, Vườn Hoa Trung T&acirc;m, ngắm N&uacute;i Phanxipăng h&ugrave;ng vĩ, Cổng Trời, Đầu Rồng Thạch L&acirc;m, S&acirc;n M&acirc;y. Đo&agrave;n tự do ngắm cảnh v&agrave; chụp ảnh thị trấn Sapa trong sương. Trả ph&ograve;ng kh&aacute;ch sạn, ăn trưa. Chiều Qu&yacute; kh&aacute;ch tham quan Th&aacute;c Bạc - D&ograve;ng nước trắng x&oacute;a chảy từ độ cao tr&ecirc;n 200m v&agrave;o d&ograve;ng suối dưới thung lũng &Ocirc; Quy Hồ, tạo n&ecirc;n &acirc;m thanh n&uacute;i rừng đầy ấn tượng, tiếp tục tham quan Lao Chải, Tả Van hoặc Tả Ph&igrave;n (t&ugrave;y điều kiện thực tế). Về đến Lai Ch&acirc;u, Qu&yacute; kh&aacute;ch nhận ph&ograve;ng kh&aacute;ch sạn. Nghỉ đ&ecirc;m tại Lai Ch&acirc;u.</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 4 : LAI CH&Acirc;U - PHONG THỔ - MƯỜNG LAY - ĐIỆN BI&Ecirc;N (Số bữa ăn: 3 bữa)</strong></p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_140929_du-lich-tay-bac.jpg\" style=\"border:0px; box-sizing:border-box; height:432px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Đo&agrave;n trả ph&ograve;ng ăn s&aacute;ng, khởi h&agrave;nh đi Điện Bi&ecirc;n, tr&ecirc;n đường ngắm cảnh rừng n&uacute;i T&acirc;y Bắc dọc theo d&ograve;ng s&ocirc;ng Nậm Na v&agrave; v&ugrave;ng ngập nước do đập nh&agrave; m&aacute;y Thủy điện Sơn La d&acirc;ng l&ecirc;n tại ng&atilde; ba s&ocirc;ng: s&ocirc;ng Đ&agrave;, s&ocirc;ng Nậm Na v&agrave; s&ocirc;ng Nậm Rốm. Đến Mường L&acirc;y ăn trưa. Đo&agrave;n tiếp tục khởi h&agrave;nh đến Điện Bi&ecirc;n, Qu&yacute; kh&aacute;ch tham quan Bảo t&agrave;ng Điện Bi&ecirc;n Phủ - Được x&acirc;y dựng v&agrave;o năm 1984 nh&acirc;n dịp kỷ niệm 30 năm chiến thắng lịch sử Điện Bi&ecirc;n Phủ, viếng Nghĩa trang liệt sĩ đồi A1, thăm Đồi A1, Hầm sở chỉ huy qu&acirc;n đội Ph&aacute;p - Tướng Đờ C&aacute;t (De Castries). Nghỉ đ&ecirc;m tại Điện Bi&ecirc;n. Nhận ph&ograve;ng kh&aacute;ch sạn, ăn tối v&agrave; nghỉ đ&ecirc;m tại Điện Bi&ecirc;n</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 5 : ĐIỆN BI&Ecirc;N - SƠN LA - MỘC CH&Acirc;U (Số bữa ăn: 3 bữa)</strong></p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_140905_Doi%20che%20Moc%20Chau.jpg\" style=\"border:0px; box-sizing:border-box; height:424px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Trả ph&ograve;ng kh&aacute;ch sạn, đo&agrave;n khởi h&agrave;nh về Sơn La. Tr&ecirc;n đường đi, Qu&yacute; kh&aacute;ch sẽ được chi&ecirc;m ngưỡng phong cảnh n&uacute;i rừng T&acirc;y Bắc h&ugrave;ng vĩ tr&ecirc;n đỉnh Đ&egrave;o Pha Đin - Một trong &quot;tứ đại đ&egrave;o&quot; v&ugrave;ng T&acirc;y Bắc v&agrave; được xếp c&ugrave;ng nh&oacute;m 6 con đ&egrave;o g&acirc;y ấn tượng nhất Việt Nam. Đến Sơn La, Qu&yacute; kh&aacute;ch ăn trưa. Sau đ&oacute;, Qu&yacute; kh&aacute;ch khởi h&agrave;nh về Mộc Ch&acirc;u. Đo&agrave;n khởi h&agrave;nh tham quan Th&aacute;c Dải Yếm - C&ograve;n c&oacute; t&ecirc;n gọi l&agrave; Th&aacute;c N&agrave;ng, nhằm v&iacute; vẻ đẹp mềm mại, h&igrave;nh ảnh quyến rũ của th&aacute;c nước như xu&acirc;n sắc của người con g&aacute;i tuổi trăng tr&ograve;n. Sau đ&oacute; tham quan Đồi Ch&egrave; Mộc Ch&acirc;u - Đứng tr&ecirc;n đồi ch&egrave; du kh&aacute;ch sẽ cảm nhận được l&agrave;n kh&ocirc;ng kh&iacute; m&aacute;t lạnh trong l&agrave;nh, tận mắt thấy những l&agrave;n sương bồng bềnh tr&ocirc;i, những đường ch&egrave; chạy v&ograve;ng quanh đồi được sắp đặt th&agrave;nh h&agrave;ng như những thửa ruộng bậc thang xanh ngắt cứ trải d&agrave;i bất tận. Qu&yacute; kh&aacute;ch dừng mua sắm đặc sản nổi tiếng được chế biến từ sữa b&ograve; tươi nổi tiếng của Mộc Ch&acirc;u về l&agrave;m qu&agrave;. Đo&agrave;n về kh&aacute;ch sạn nhận ph&ograve;ng, nghỉ ngơi. Nghỉ đ&ecirc;m tại Mộc Ch&acirc;u.</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 6 : MỘC CH&Acirc;U - MAI CH&Acirc;U - H&Ograve;A B&Igrave;NH - S&Acirc;N BAY NỘI B&Agrave;I (H&Agrave; NỘI) (Số bữa ăn: 2 bữa (s&aacute;ng, trưa))</strong></p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_141103_moc%20chau%20-%20hoa%20cai.jpg\" style=\"border:0px; box-sizing:border-box; height:346px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Ăn s&aacute;ng tại kh&aacute;ch sạn, trả ph&ograve;ng. Đo&agrave;n khởi h&agrave;nh đi Mai Ch&acirc;u - H&ograve;a B&igrave;nh, tham quan Bản L&aacute;c Mai Ch&acirc;u - T&igrave;m hiểu nh&agrave; s&agrave;n, phong tục tập qu&aacute;n, c&aacute;ch kinh doanh du lịch loại h&igrave;nh home stay của b&agrave; con người Th&aacute;i nơi đ&acirc;y. Đo&agrave;n khởi h&agrave;nh về H&ograve;a B&igrave;nh ăn trưa. Đo&agrave;n khởi h&agrave;nh về H&ograve;a B&igrave;nh ăn trưa. Sau đ&oacute;, khởi h&agrave;nh về H&agrave; Nội, xe đưa Qu&yacute; kh&aacute;ch ra s&acirc;n bay Nội B&agrave;i đ&aacute;p chuyến bay về Tp.HCM. Chia tay Qu&yacute; kh&aacute;ch v&agrave; kết th&uacute;c chương tr&igrave;nh du lịch tại s&acirc;n bay T&acirc;n Sơn Nhất</p>\n</div>\n', 'ruc-ro-mua-hoa-tay-bac', '0', '0', 1446786194, 22, 0, 0, '', '', ''),
(5, 'Giấc mộng hoa phương Bắc', 'Đất trời đã vào xuân, non cao miền Bắc bừng sáng trong vẻ đẹp mê đắm của rừng hoa thắm sắc ẩn hiện trong sương khói vấn vương. Những bước chân phiêu du trên núi ngàn cũng rộn rã hơn, chan hòa cùng nét tươi mới giữa đất trời nở hoa. Tour Tết, Trong nước', 1, 'upload/img/mua-hoa-xuan-tay-bac_1.jpg', '<div>&nbsp;</div>\n\n<div>\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_1.jpg\" style=\"border:0px; box-sizing:border-box; height:441px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">nhiều lần lỡ hẹn, t&ocirc;i cũng đặt ch&acirc;n đến miền rẻo cao phương Bắc với thật nhiều h&aacute;o hức. Qu&atilde;ng đường đi qua&nbsp; Sapa, Điện Bi&ecirc;n, Sơn La, Cao Bằng, Lạng Sơn&hellip; dường như ngắn lại bởi ai cũng say sưa ngắm những cung đường bạt ng&agrave;n hoa đ&agrave;o, hoa mận, hoa mơ. Hoa nở tr&agrave;n tr&ecirc;n triền đồi, lấp l&oacute; ven đường, hồn nhi&ecirc;n thả bức r&egrave;m trước s&acirc;n nh&agrave;&hellip; đẹp đến nỗi kh&ocirc;ng một m&aacute;y ảnh &ldquo;khủng&rdquo; n&agrave;o c&oacute; thể ghi lại trọn vẹn.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_6.jpg\" style=\"border:0px; box-sizing:border-box; height:433px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">C&oacute; l&uacute;c hoa phủ hồng cả sườn n&uacute;i, khiến kh&aacute;ch l&atilde;ng du ngất ng&acirc;y chẳng muốn dời ch&acirc;n. Một cơn gi&oacute; thoảng qua, khung cảnh bỗng h&oacute;a th&agrave;nh cơn mưa hoa lất phất. Chắt chiu nhựa sống qua năm d&agrave;i th&aacute;ng rộng, hội tụ đủ tinh t&uacute;y của đất trời để mỗi độ xu&acirc;n về th&acirc;n c&acirc;y x&ugrave; x&igrave; ấy lại nảy lộc đơm hoa sưởi ấm cả n&uacute;i rừng. Những c&aacute;nh đ&agrave;o phai T&acirc;y Bắc hồng phớt, mỏng manh m&agrave; l&agrave;n hương lại dịu d&agrave;ng, thanh tao đến lạ. Đ&ocirc;ng Bắc lại tự h&agrave;o với n&eacute;t ki&ecirc;u sa rực rỡ của rừng hoa đ&agrave;o b&iacute;ch lộng lẫy c&oacute; c&aacute;nh d&agrave;y, to, đủ sắc đỏ, hồng, trắng&hellip;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_4.jpg\" style=\"border:0px; box-sizing:border-box; height:472px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Ven đường đi, h&ograve;a c&ugrave;ng mu&ocirc;n sắc hoa đ&agrave;o b&iacute;ch, đ&agrave;o phai l&agrave; n&eacute;t đẹp hoang d&atilde; của những lo&agrave;i hoa dại t&iacute;m ng&aacute;t, v&agrave;ng rực cả khoảng trời. Đến n&uacute;i N&agrave;ng T&ocirc; Thị, động Tam Thanh, cảm x&uacute;c của t&ocirc;i gần như vỡ &ograve;a khi được chi&ecirc;m ngưỡng những đ&oacute;a hoa đ&agrave;o trắng muốt như tuyết, c&acirc;y đ&agrave;o gh&eacute;p hội tụ đủ ba m&agrave;u trắng - hồng - đỏ rất ấn tượng.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_3_1.jpg\" style=\"border:0px; box-sizing:border-box; height:975px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Hoa kh&ocirc;ng chỉ t&ocirc; điểm cho n&uacute;i rừng m&agrave; c&ograve;n mang cả kh&ocirc;ng gian văn h&oacute;a v&ugrave;ng cao đến với mọi người. T&ocirc;i cứ nhớ m&atilde;i nhịp ch&acirc;n vui của ch&agrave;ng trai bản xuống chợ ng&agrave;y xu&acirc;n m&agrave; tr&ecirc;n vai lắc lư một c&agrave;nh đ&agrave;o thắm. Những c&ocirc; g&aacute;i Dao, M&ocirc;ng v&aacute;y xanh v&aacute;y đỏ tỏa s&aacute;ng dưới hoa xu&acirc;n v&agrave; bọn trẻ con mắt trong veo, n&ocirc; đ&ugrave;a hồn nhi&ecirc;n tr&ecirc;n c&acirc;y mận thật đ&aacute;ng y&ecirc;u l&agrave;m sao!</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_2_1.jpg\" style=\"border:0px; box-sizing:border-box; height:894px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Chỉ cần như thế cũng b&otilde; c&ocirc;ng cho một chuyến ngao du sơn thủy, s&aacute; g&igrave; n&uacute;i cao hay đ&egrave;o vắng, chỉ cần v&aacute;c ba l&ocirc; l&ecirc;n đường, ta lại sở hữu m&ugrave;a xu&acirc;n thi vị cho ri&ecirc;ng m&igrave;nh. Hoa nở khắp đất trời, hoa nở trong l&ograve;ng người để t&ocirc;i m&atilde;i nhung nhớ về miền rẻo cao phương Bắc. Đ&oacute; ch&iacute;nh l&agrave; những x&uacute;c cảm đầu năm thi&ecirc;ng li&ecirc;ng v&agrave; rất đỗi tự h&agrave;o về qu&ecirc; hương m&agrave; kh&ocirc;ng h&agrave;nh tr&igrave;nh n&agrave;o c&oacute; được.</p>\n</div>\n', 'giac-mong-hoa-phuong-bac', '0', '0', 1446792582, 22, 0, 0, '', '', ''),
(6, 'Train Ticket', 'Operated by national carrier Vietnam Railways.Travelling in an air-con sleeping berth and of course, there’s some spectacular scenery to lap up too. There are four main ticket classes: hard seat, soft seat, hard sleeper and soft sleeper. These are also split into air-con and non air-con options. Presently, air-con is only available on the faster express trains. Hard-seat class is usually packed and tolerable for day travel, but expect plenty of cigarette smoke. Ticket prices vary depending on the train; the fastest trains are more expensive. Aside from the main HCMC–Hanoi run, three rail-spur lines link Hanoi with the other parts of northern Vietnam. A third runs northwest to Lao Cai (Sapa).', 0, 'upload/img/ticket.jpg', '', 'train-ticket', '0', '0', 1447426430, 23, 0, 0, '', '', ''),
(7, 'Train North to South', 'Everyday departure with trains number: Trains SE1-SE6: Soft sleepers (4-berth), hard sleepers (6-berth), soft class seats (all air-con). TN3-TN10: Soft sleepers (air-con), hard sleepers (air-con & non-air-con), soft seats (a/c & non-a/c), hard seats (non-air-con).', 0, 'upload/img/tk1.jpg', '<span style=\"color:rgb(85, 85, 85); font-family:arial\">Unit Price: US Dollar (US$); A/C: Air-conditioning.</span><br />\n<span style=\"color:rgb(85, 85, 85); font-family:arial\">Child&#39;s fare: under 5 years: free of charge if sharing bed with parent; 5 years/up: adult rate.</span><br />\n<span style=\"color:rgb(85, 85, 85); font-family:arial\">Please note: 20% of the amount will be charged in case of cancellation for any ticket.</span><br />\n&nbsp;\n<div>&nbsp;</div>\n\n<div>\n<table style=\"border-collapse:collapse; border-spacing:0px; border:1px solid rgb(223, 223, 223); color:rgb(96, 96, 96); font-family:arial; font-size:17.6px; height:105px; line-height:normal; margin:0px auto; padding:0px; vertical-align:baseline; width:800px\">\n	<tbody>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">DEP FROM HANOI</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">SE NO. 1/ TIME TABLE</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">SE NO. 3/&nbsp;TIME TABLE</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">SE NO. 5/&nbsp;TIME TABLE</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">&nbsp;PRICE</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\">HA NOI</td>\n			<td style=\"text-align:center; vertical-align:baseline\">19.35</td>\n			<td style=\"text-align:center; vertical-align:baseline\">22.00</td>\n			<td style=\"text-align:center; vertical-align:baseline\">6.00</td>\n			<td style=\"text-align:center; vertical-align:baseline\">&nbsp;55 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">HUE</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">8.48</td>\n			<td style=\"text-align:center; vertical-align:baseline\">10.27</td>\n			<td style=\"text-align:center; vertical-align:baseline\">19.55</td>\n			<td style=\"text-align:center; vertical-align:baseline\">&nbsp;55 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">DA NANG&nbsp;</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">11.26</td>\n			<td style=\"text-align:center; vertical-align:baseline\">13.00</td>\n			<td style=\"text-align:center; vertical-align:baseline\">22.47</td>\n			<td style=\"text-align:center; vertical-align:baseline\">60 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">&nbsp;NHA TRANG</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">21.14</td>\n			<td style=\"text-align:center; vertical-align:baseline\">22.04</td>\n			<td style=\"text-align:center; vertical-align:baseline\">8.35</td>\n			<td style=\"text-align:center; vertical-align:baseline\">80 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">BINH THUAN&nbsp;</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">1.12</td>\n			<td style=\"text-align:center; vertical-align:baseline\">2.14</td>\n			<td style=\"text-align:center; vertical-align:baseline\">16.14</td>\n			<td style=\"text-align:center; vertical-align:baseline\">85 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">&nbsp;SAI GON</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">4.39</td>\n			<td style=\"text-align:center; vertical-align:baseline\">5.20</td>\n			<td style=\"text-align:center; vertical-align:baseline\">16.05</td>\n			<td style=\"text-align:center; vertical-align:baseline\">100 USD<br />\n			&nbsp;</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n', 'train-north-to-south', '0', '0', 1447426503, 23, 0, 0, '', '', ''),
(8, 'Train to Sapa', 'The Ha Noi-Lao Cai trains runs every evening, departing from Ha Noi Train Station at Tran Quy Cap Street. Three run at night, and one makes a day trip. The following are the trains from Ha Noi to Lao Cai (PM: SP1, SP3 , SP7 ) and vice versa (PM: SP2, SP4 , SP8) daily. The daytime route offers only hard seats, whereas travelers can enjoy soft-sleepers, air-conditioned, four-berth cabins on the night trains. In the SP3 & SP4, there are 2 Victoria Carriages. In SP1 & SP2, there are Orient Express, Tulico Carriages, Friendly Carriages, Ratraco Carriages, and TSC Carriages, King Express Carriages, Royal Carriages. All of these are alternatives for tourists to Sapa from Hanoi.', 0, 'upload/img/tk2.jpg', '<p>Deluxe Train: Fansipan Express (SP1-SP2), Livitrans Express (SP1-SP2), Sapaly Expres (SP3-SP4)</p>\n\n<p>First Class Train: Orient Express (SP1-SP2), TSC Express ( SP1-SP2), Pumpkin Express train (SP1-SP2), VN Express Train ( SP3-SP4)</p>\n\n<p>&nbsp;</p>\n\n<table style=\"border-collapse:collapse; border-spacing:0px; border:1px solid rgb(223, 223, 223); color:rgb(96, 96, 96); font-family:arial; font-size:17.6px; height:105px; line-height:normal; margin:0px auto; padding:0px; vertical-align:baseline; width:800px\">\n	<tbody>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">HANOI - LAO CAI</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">DELUXE CABIN 4 BERTHS</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">\n			<p>FIRST CLASS 4 BERTHS</p>\n			</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">VIP CLASS 2 BERTHS</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">SP1: 21H40 - 5H30</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">30 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">35 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">70 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">SP1: 20H00 - &nbsp;6H10</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">30 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">35 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">70 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">SP1: 20H17 - &nbsp;4H35</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">30 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">35 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">70 USD</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>The prices may change due to exchange rate or season; therefore, please confirm exact price when you make the final booking with payment. Please contact by email to have more information. Email:&nbsp;<a href=\"mailto:info@vietnampremiertravel.com\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-family: Arial; color: rgb(34, 34, 34);\">info@vietnampremiertravel.com</a>&nbsp;/ Tel: (+84 4) 3926 2866</p>\n', 'train-to-sapa', '0', '0', 1447426437, 23, 0, 0, '', '', ''),
(9, 'Tàu Bắc - Nam', 'Khởi hành hàng ngày với tàu số : Xe lửa SE1 - SE6 : tà vẹt mềm ( 4 bến ) , tà vẹt cứng ( 6 bến ) , ghế hạng mềm ( tất cả các máy con) . TN3 - TN10 : tà vẹt mềm ( máy lạnh ) , tà vẹt cứng ( máy lạnh & không khí -con) , ghế ngồi mềm (a / c và phi - a / c ) , ghế ngồi cứng ( không máy lạnh ) .', 0, 'upload/img/tk11.jpg', '<pre>\n<span style=\"font-size:14px\">Đơn gi&aacute; : Dollar Mỹ (US $ ) ; A / C : Điều h&ograve;a kh&ocirc;ng kh&iacute; .\nGi&aacute; v&eacute; cho trẻ em: dưới 5 tuổi: miễn ph&iacute; nếu ngủ chung giường với bố mẹ ; 5 năm / up : tỷ lệ người lớn .\nXin lưu &yacute; : 20 % của số tiền sẽ được t&iacute;nh trong trường hợp hủy cho bất kỳ v&eacute; .</span></pre>\n', 'tau-bac-nam', '0', '0', 1446800384, 22, 0, 0, '', '', ''),
(10, 'teafdsagd', 'gdasgdsg', 0, NULL, 'sagdsagdsagd', 'teafdsagd', 'en', '0', 1453861931, 0, 0, 0, '', '', ''),
(11, 'Dàn xe đời mới - Đa dạng chủng loại', 'Chúng tôi cho thuê xe từ những dòng xe giá rẻ đến những dòng xe cao cấp, từ 4 chỗ đến xe 12 chỗ Dàn xe của chúng tôi luôn có bộ phận theo dõi, quản lý và bảo hành. Để đảm bảo trước khi đến đón khách, Xe luôn trong tình trạng sạch, đẹp và an toàn.', 0, 'upload/img/icon3.png', '', 'dan-xe-doi-moi-da-dang-chung-loai', 'vi', '0', 1453863158, 20, 0, 1, '', '', ''),
(12, 'Tài xế thân thiện và chuyên nghiệp', 'Các tài xế của chúng tôi được tuyển chọn khắt khe theo các tiêu chí. Lái xe an toàn, có kinh nghiệm, thông thạo tuyến đường và được công tu Training các kỹ năng phục vụ khách hàng. Tùy theo mục đích thuê xe và loại xe cũng như yêu cầu của quí khách', 0, 'upload/img/icon2.png', '', 'tai-xe-than-thien-va-chuyen-nghiep', 'vi', '0', 1453863170, 20, 0, 1, '', '', ''),
(13, 'Giá cho thuê xe tốt nhất trên thị trường', 'Qui trình và cách tính giá cũng như báo giá của chúng tôi luôn là mức giá tốt nhất trên thị trường. Chính vì vậy khi quí khách thuê xe của chúng tôi cũng có nghĩa quí khách đã có được mức giá tốt nhât trong những nhà cung cấp.', 0, 'upload/img/icon1.png', '', 'gia-cho-thue-xe-tot-nhat-tren-thi-truong', 'vi', '0', 1453863176, 20, 0, 1, '', '', ''),
(14, 'Hướng dẫn lái xe ô tô an toàn trên đường cao tốc', 'Trên đường cao tốc, người điều khiển phương tiện giao thông được phép lái xe với vận tốc tối đa cao hơn so với lái trên đường phố, đường làng và do đó tiết kiệm thời gian di chuyển hơn nhưng cũng tiềm ẩn nhiều rủi ro xảy ra tai nạn đáng tiếc nếu không tuân thủ đúng luật.', 0, 'upload/img/new1.jpg', '<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">Th&oacute;i quen h&agrave;ng ng&agrave;y khi l&aacute;i xe đ&ocirc;i khi đ&atilde; trở th&agrave;nh nguy&ecirc;n nh&acirc;n của những trường hợp tai nạn đ&aacute;ng tiếc khi tham gia giao th&ocirc;ng tr&ecirc;n đường cao tốc: chạy xe dưới tốc độ tối thiểu, kh&ocirc;ng giữ khoảng c&aacute;ch an to&agrave;n với xe ph&iacute;a trước, dừng/đỗ t&ugrave;y tiện, quay đầu xe&hellip; Nhưng lưu &yacute; sau sẽ gi&uacute;p bạn c&oacute; những chuyến đi an to&agrave;n c&ugrave;ng bạn b&egrave;, người th&acirc;n.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash; Ngo&agrave;i c&aacute;c vấn đề kỹ thuật đảm bảo an to&agrave;n cho xe &ocirc; t&ocirc;, đặc biệt phải lưu &yacute; đến lốp xe bởi khi chạy với tốc độ cao, nhiệt độ ngo&agrave;i trời cao, h&agrave;ng h&oacute;a chở nhiều&hellip;; do đ&oacute;, với những bộ lốp &ldquo;tuổi đời&rdquo; cao, m&ograve;n nhiều cần đặc biệt cẩn trọng (nổ lốp xe khi đang đi tốc độ cao l&agrave; một trong những nguy&ecirc;n nh&acirc;n phổ biến dẫn đến tai nạn).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash; Đảm bảo tốc độ theo hệ thống biển b&aacute;o tr&ecirc;n đường, giảm tốc độ ph&ugrave; hợp ở những đoạn đường cong, c&oacute; nhiều phương tiện (cho d&ugrave; ở l&agrave;n đường kh&aacute;c) hoặc chướng ngại vật&hellip; Tr&aacute;nh nh&igrave;n tập trung v&agrave;o một điểm qu&aacute; l&acirc;u, đặc biệt c&aacute;c đoạn đường cong hay l&ecirc;n/xuống dốc (dễ dẫn đến trường hợp &ldquo;kh&oacute;a mục ti&ecirc;u&rdquo; khiến xe đi thẳng đến điểm đ&oacute;).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&nbsp;&ndash; Giữ khoảng c&aacute;ch an to&agrave;n với quy tắc 3 gi&acirc;y (Bạn nh&igrave;n xe ph&iacute;a trước chạy qua một vật cố định n&agrave;o đ&oacute; ở b&ecirc;n đường: cột đ&egrave;n, biển b&aacute;o&hellip; v&agrave; bắt đầu đếm&nbsp;ước lượng từ 1 đến 3, khoảng thời gian tưởng ứng đủ 3 gi&acirc;y). Nếu trời mưa hoặc tầm quan s&aacute;t bị ảnh hưởng, th&igrave; n&ecirc;n tăng l&ecirc;n 4-5 gi&acirc;y. H&atilde;y ch&uacute; &yacute; c&aacute;c biển chỉ dẫn lưu &yacute; khoảng c&aacute;ch 50 &ndash; 100 &ndash; 200m.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash;<span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span><span style=\"font-family:inherit; font-size:inherit\">Kh&ocirc;ng bao giờ l&ugrave;i xe, quay đầu xe, đi ngược chiều tr&ecirc;n đường cao tốc</span>.<span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span><span style=\"font-family:inherit; font-size:inherit\">Kh&ocirc;ng được cho xe &ocirc; t&ocirc; chạy ở l&agrave;n dừng xe khẩn cấp v&agrave; phần lề đường</span>.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash;<span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span><span style=\"font-family:inherit; font-size:inherit\">Khi v&agrave;o hoặc ra khỏi đường cao tốc phải giảm tốc độ</span><span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span>v&agrave;<span style=\"font-family:inherit; font-size:inherit\"><span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span></span><span style=\"font-family:inherit; font-size:inherit\">nhường đường cho xe đi tr&ecirc;n l&agrave;n đường ch&iacute;nh</span>.<span style=\"font-family:inherit; font-size:inherit\"><span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span></span><span style=\"font-family:inherit; font-size:inherit\">Chỉ được chuyển l&agrave;n đường ở những nơi cho ph&eacute;p</span>,<span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span>khi chuyển l&agrave;n lu&ocirc;n ch&uacute; &yacute; ph&iacute;a sau v&agrave; lu&ocirc;n xi-nhan. Kh&ocirc;ng chuyển l&agrave;n kiểu cắt đầu xe kh&aacute;c v&agrave; chuyển nhiều l&agrave;n đường c&ugrave;ng một thời điểm.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash; Người điều khiển v&agrave; người ngồi trong xe &ocirc;t&ocirc; đều phải thắt d&acirc;y an to&agrave;n. Bởi khi lưu th&ocirc;ng với tốc độ cao (100km/h), th&igrave; thắt d&acirc;y an to&agrave;n cho mọi người tr&ecirc;n xe &ocirc; t&ocirc; l&agrave; việc cần thiết hơn bao giờ hết.</span></p>\r\n', 'huong-dan-lai-xe-o-to-an-toan-tren-duong-cao-toc', 'vi', '0', 1453864782, 22, 1, 0, '', '', ''),
(15, 'Gợi y 8 lộ trình về quê ăn tết tránh kẹt xe ở hà nội', '', 0, 'upload/img/new4.jpg', '', 'goi-y-8-lo-trinh-ve-que-an-tet-tranh-ket-xe-o-ha-noi', 'vi', '0', 1453864774, 22, 1, 0, '', '', ''),
(16, 'Hơn 2000 người tham gia hưởng ứng \"Năm an toàn giao thông\" 2016', '', 0, 'upload/img/new31.jpg', '', 'hon-2000-nguoi-tham-gia-huong-ung-nam-an-toan-giao-thong-2016', 'vi', '0', 1453864761, 22, 1, 0, '', '', ''),
(17, 'Tăng phí trả vé tàu để hạn chế \"cò\" vé chợ đen.', '', 0, 'upload/img/new2.jpg', '', 'tang-phi-tra-ve-tau-de-han-che-co-ve-cho-den', 'vi', '0', 1453864807, 22, 1, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `inuser_category`
--

CREATE TABLE `inuser_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `focus` int(11) DEFAULT NULL,
  `hot` int(11) DEFAULT NULL,
  `tour` int(11) DEFAULT NULL,
  `sort` int(5) DEFAULT 1,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inuser_category`
--

INSERT INTO `inuser_category` (`id`, `name`, `alias`, `description`, `image`, `parent_id`, `home`, `focus`, `hot`, `tour`, `sort`, `lang`, `title`) VALUES
(27, 'Nguyễn công hoan', 'nguyen-cong-hoan', '<p><em>Seaside Resort g&acirc;y ấn tượng với t&ocirc;i nhất nhờ dịch vụ rất ho&agrave;n hảo v&agrave; chuy&ecirc;n nghiệp. Seaside Resort l&agrave; một kh&aacute;ch sạn với đội ngũ nh&acirc;n vi&ecirc;n chuy&ecirc;n nghiệp, năng động, s&aacute;ng tạo, phong c&aacute;ch phục vụ v&agrave; chăm s&oacute;c kh&aacute;ch h&agrave;ng tốt. Hơn nữa ch&iacute;nh s&aacute;ch chăm s&oacute;c kh&aacute;ch h&agrave;ng nhiệt t&igrave;nh, chu đ&aacute;o ngay cả khi đ&atilde; ho&agrave;n th&agrave;nh hợp đồng.</em></p>\r\n', 'upload/img/inuser/avt.png', 0, 1, NULL, NULL, NULL, 9, 'vi', 'Big Boss'),
(28, 'Mrs bin', 'doctor', 'Seaside Resort impresses me most with its excellent service and professionalism. Seaside Resort is a hotel with professional staffs', 'upload/img/traveler_story111.png', 0, 1, 0, 0, 0, 1, 'en', 'doctor'),
(29, 'Nguyễn thành đạt', 'nguyen-thanh-dat', '<p>Thật tuyệt khi sử dụng dịch vụ tại Thăng Long, t&ocirc;i cảm thấy m&igrave;nh được phục vụ v&ocirc; c&ugrave;ng chu đ&aacute;o v&agrave; tận t&igrave;nh.Chắc chắn t&ocirc;i sẽ quay lại mua h&agrave;ng tại Thăng Long lần nữa.Ch&uacute;c Thăng Long ph&aacute;t triển mạnh mẽ hơn nữa, t&ocirc;i tin chắc điều đ&oacute;.</p>\r\n', 'upload/img/inuser/avt.png', 0, 1, NULL, NULL, NULL, 5, 'vi', 'VNPT Technology '),
(30, 'Trưởng phòng HLC Group', 'truong-phong-hlc-group', '<p>&nbsp;</p>\r\n\r\n<p>Y&ecirc;u cầu của ch&uacute;ng t&ocirc;i l&agrave; mỗi ph&ograve;ng h&aacute;t phải l&agrave; một kh&ocirc;ng gian đẹp, một phong c&aacute;ch kh&aacute;ch nhau để những kh&aacute;ch h&agrave;ng muốn kh&aacute;m ph&aacute; khi đến với ch&uacute;ng t&ocirc;i họ lu&ocirc;n lu&ocirc;n thấy thoải m&aacute;i.</p>\r\n', 'upload/img/inuser/avt1.png', 0, 1, NULL, NULL, NULL, 10, 'vi', 'Phạm Minh Quân');

-- --------------------------------------------------------

--
-- Table structure for table `inuser_to_category`
--

CREATE TABLE `inuser_to_category` (
  `id` int(11) NOT NULL,
  `id_inuser` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inuser_to_category`
--

INSERT INTO `inuser_to_category` (`id`, `id_inuser`, `id_category`) VALUES
(25, 1, 20),
(29, 3, 22),
(30, 2, 22),
(32, 4, 22),
(34, 5, 22),
(38, 9, 22),
(39, 6, 23),
(40, 8, 23),
(41, 7, 23),
(50, 11, 20),
(51, 12, 20),
(52, 13, 20),
(53, 16, 22),
(54, 15, 22),
(55, 14, 22),
(56, 17, 22);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datetime` int(11) DEFAULT NULL,
  `timeupdate` int(11) DEFAULT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_ward` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_birthday` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `user_sale` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `customer_pay` int(11) DEFAULT NULL,
  `customer_payted` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `time_buy` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `shipping` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `code`, `date`, `time`, `datetime`, `timeupdate`, `customer`, `customer_id`, `customer_name`, `customer_email`, `customer_phone`, `customer_address`, `customer_place`, `customer_ward`, `customer_birthday`, `user_create`, `user_sale`, `note`, `total_price`, `price_sale`, `customer_pay`, `customer_payted`, `status`, `time_buy`, `type`, `count`, `discount`, `shipping`) VALUES
(40, 'HD40', '17/05/2018', '17:12', 1526490000, 1526551961, NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '', 358000, NULL, 450840, 0, 1, 1526551961, 0, 2, 2, 100000);

-- --------------------------------------------------------

--
-- Table structure for table `invoices_detail`
--

CREATE TABLE `invoices_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `price_imp` int(11) DEFAULT NULL,
  `sale` int(11) DEFAULT NULL,
  `inv_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices_detail`
--

INSERT INTO `invoices_detail` (`id`, `name`, `code`, `quantity`, `price`, `price_sale`, `price_imp`, `sale`, `inv_id`, `product_id`, `note`) VALUES
(116, 'Tinh Chất Dưỡng Da 3 Trong 1 Chống Lão Hoá Innisfree Jeju Bamboo All-In-One Fluid 100ml', NULL, 1, 229000, 229000, NULL, NULL, 40, 95, NULL),
(115, 'Bộ Dưỡng Da Dùng Thử Innisfree Trà Xanh Green Tea Special Kit Set', NULL, 1, 129000, 129000, NULL, NULL, 40, 97, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `alias` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `alias`, `name`) VALUES
(1, 'vi', 'Tiếng Việt'),
(2, 'en', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `map_shopping`
--

CREATE TABLE `map_shopping` (
  `id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `tim_kiem` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `toa_domap` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `toa_dohienthi` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `diachi_shop` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `phone` char(150) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `map_shopping`
--

INSERT INTO `map_shopping` (`id`, `title`, `tim_kiem`, `toa_domap`, `toa_dohienthi`, `diachi_shop`, `phone`, `lang`) VALUES
(2, 'Chi nhánh camera siêu net tại Hà Nội', '168 Nguyễn Tuân - Thanh Xuân Hà Nội', '(20.998863, 105.80291809999994)', '20.998863, 105.80291809999994', '168 Nguyễn Tuân - Thanh Xuân Hà Nội', '0918.041616 - 0987.041616', 'vi'),
(5, 'Chi nhánh camera siêu net tại Hải Phòng', '52 Lê Quang Đạo - Nam Từ Liêm - Hà Nội', '', '', 'Số 66, Trường Chinh, Kiến An, Hải Phòng', '031 3603208', 'vi'),
(6, 'Chi nhánh camera siêu net tại TP. HCM', 'Tp HCM', '(10.7764745, 106.70088310000006)', '10.7764745, 106.70088310000006', '212/58 Thoại Ngọc Hầu, P. Phú Thạnh, Q. Tân Phú, TP. HCM', '08 39722693', 'vi'),
(7, 'Chi nhánh camera siêu net tại Yên Bái', 'Yên Bái', '(21.6837923, 104.4551361)', '21.6837923, 104.4551361', '168 Nguyễn Tuân - Yên Bái', '0918.041616 - 0987.041616', 'vi'),
(11, 'cừa hàng thời trang', 'cua hang so 23 ngo 229 cầu giấy hà nội', '(21.0477839, 105.79456129999994)', '21.0477839, 105.79456129999994', 'cua hang so 23 ngo 229 cầu giấy hà nội', '0988787654', 'vi');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `content` text CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `view` int(11) NOT NULL DEFAULT 0,
  `image` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `name`, `description`, `content`, `title_seo`, `description_seo`, `keyword_seo`, `lang`, `category_id`, `home`, `hot`, `focus`, `sort`, `view`, `image`, `active`, `alias`, `link`) VALUES
(1, 'album hè cắm trại 2018', '<p>nội dung m&ocirc; tả</p>\r\n', '<p>nội dung chi tiết</p>\r\n', 'Không gian nhà hàng', 'Không gian nhà hàng', 'Không gian nhà hàng', 'vi', 11, 1, NULL, NULL, 1, 0, 'upload/img/media/dia-diem-du-lich-54.jpg', 1, 'album-he-cam-trai-2018', ''),
(10, 'album anh  cam trại hè', '', '', '', '', '', 'vi', 11, 1, NULL, NULL, 2, 0, 'upload/img/media/dia-diem-du-lich-4.jpg', 1, 'album-anh-cam-trai-he', ''),
(11, 'up anh jpeg cha le khong duoc-12', '<p>m&ocirc;i tả</p>\r\n', '', '', '', '', 'vi', 1, NULL, NULL, 1, 3, 0, 'upload/img/media/1233.JPEG', 1, 'up-anh-jpeg-cha-le-khong-duoc-12', 't0WFOnwp3MM');

-- --------------------------------------------------------

--
-- Table structure for table `media_category`
--

CREATE TABLE `media_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `coupon` tinyint(1) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` text CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `left_right` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media_category`
--

INSERT INTO `media_category` (`id`, `name`, `alias`, `sort`, `home`, `focus`, `coupon`, `image`, `title_seo`, `description_seo`, `keyword_seo`, `lang`, `description`, `parent_id`, `hot`, `left_right`) VALUES
(1, 'Hình ảnh', 'hinh-anh', 2, NULL, NULL, NULL, 'upload/img/media/anh1.jpg', 'Hình ảnh', 'gdsagds', NULL, 'vi', '<p>noi dung m&ocirc; tả</p>\r\n', 0, NULL, 1),
(11, 'album anh nam 2019', 'album-anh-nam-2019', 5, 1, NULL, NULL, 'upload/img/media/dia-diem-du-lich-5.jpg', '', '', NULL, 'vi', '<p>m&ocirc; tả</p>\r\n', 1, NULL, 1),
(10, 'album nam 2018', 'album-nam-2018', 4, 1, NULL, NULL, 'upload/img/media/anh.jpg', '', '', NULL, 'vi', '<p>noi dung m&ocirc; tả cho album</p>\r\n', 1, NULL, 1),
(12, 'Hình ảnh hội nghị', 'hinh-anh-hoi-nghi', 6, 1, NULL, NULL, 'upload/img/media/dia-diem-du-lich-3.jpg', '', '', NULL, 'vi', '', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `media_images`
--

CREATE TABLE `media_images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` char(200) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media_images`
--

INSERT INTO `media_images` (`id`, `name`, `id_item`, `image`, `url`, `sort`) VALUES
(1, 'anh so 1', 1, 'upload/img/media_multi/ae20248dc61407525e7a96a1b002c72b.jpg', NULL, NULL),
(2, 'anh so 2', 1, 'upload/img/media_multi/67594498cb19b94e98cc1c2095c83c51.jpg', NULL, NULL),
(4, 'anh so 4', 1, 'upload/img/media_multi/44bb59baff034000b0f46258088bf8b8.jpg', NULL, NULL),
(5, 'anh so 5', 1, 'upload/img/media_multi/036d5e089f887f4687e3379500c8256d.jpg', NULL, NULL),
(6, 'anh so 6', 1, 'upload/img/media_multi/fa02a841c335c7566a42548fe1c0083d.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seturl` tinyint(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT 0,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `view_type` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `style` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `image`, `alias`, `position`, `target`, `seturl`, `parent_id`, `description`, `module`, `cat_id`, `sort`, `home`, `lang`, `view_type`, `style`) VALUES
(1, 'Giới thiệu', 'page/gioi-thieu.html', NULL, 'gioi-thieu', 'main', '', NULL, 0, '<p>introduction</p>\r\n', 'pages', 0, 1, 0, 'vi', NULL, NULL),
(7, 'Liên hệ', 'contact', NULL, 'lien-he', 'main', '', NULL, 0, '', '0', 0, 4, 0, 'vi', NULL, NULL),
(39, 'Trang chủ', 'trang-chu', 'upload/img/menus/img_top1.png', 'trang-chu', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(40, 'Giới thiệu', 'gioi-thieu', 'upload/img/menus/img_top2.png', 'gioi-thieu', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(41, 'Thông báo', 'thong-bao', 'upload/img/menus/img_top3.png', 'thong-bao', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(42, 'Thanh toán mua hàng', 'thanh-toan-mua-hang', 'upload/img/menus/img_top4.png', 'thanh-toan-mua-hang', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(43, 'Khuyến mãi', 'khuyen-mai', NULL, 'khuyen-mai', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(44, 'Góp ý', 'gop-y', NULL, 'gop-y', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(45, 'Liên hệ', 'lien-he', NULL, 'lien-he', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(134, 'Watches & Eyewear', 'watches-eyewear', NULL, 'watches-eyewear', 'top', '', NULL, 0, '', '0', 0, 11, 0, 'vi', NULL, NULL),
(135, 'Television Dealers', 'television-dealers', NULL, 'television-dealers', 'top', '', NULL, 0, '', '0', 0, 10, 0, 'vi', NULL, NULL),
(136, 'Digital & Prime Music', 'digital-prime-music', NULL, 'digital-prime-music', 'top', '', NULL, 0, '', '0', 0, 9, 0, 'vi', NULL, NULL),
(58, 'Tư vấn bán lẻ', 'tu-van-ban-le', 'upload/img/menus/img_box_footer4.png', 'tu-van-ban-le', 'bottom', '', NULL, 0, '<p>text demo sản phẩm</p>\r\n', '0', 0, 0, 0, 'vi', NULL, NULL),
(102, 'Sofa nỉ', 'danh-muc/sofa-ni.html', NULL, 'sofa-ni', 'main', '', NULL, 83, '', 'products', 8, 4, 0, 'vi', NULL, NULL),
(71, 'Home', 'home', NULL, 'home', 'main', '', NULL, 0, '', '0', 0, 0, 0, 'en', NULL, NULL),
(72, 'About', 'home-2', NULL, 'about', 'main', '', NULL, 0, '', '0', 0, 1, 0, 'en', NULL, NULL),
(76, 'Hỗ trợ 096 180 3334', 'ho-tro-096-180-3334', NULL, 'ho-tro-096-180-3334', 'bottom', '', NULL, 58, '<p>0961803334</p>\r\n', '0', 0, 3, 0, 'vi', NULL, NULL),
(107, 'Sofa da', 'danh-muc/sofa-da.html', NULL, 'sofa-da', 'main', '', NULL, 83, '', 'products', 13, 9, 0, 'vi', NULL, NULL),
(80, 'tv1', 'huong-dan-mua-tra-gop', NULL, 'tv1', 'bottom', '', NULL, 58, '<p><strong><span style=\"color:#000000\">MrB</span>: <span style=\"color:#c0392b\">091111</span></strong></p>\r\n', '0', 0, 0, 0, 'vi', NULL, NULL),
(83, 'Sản phẩm', 'danh-muc/sofa-vang.html', 'upload/img/menus/img-menu.png', 'san-pham', 'main', '', NULL, 0, '', 'products', 1, 2, 0, 'vi', NULL, NULL),
(90, 'Sofa gia đình', 'danh-muc/sofa-gia-dinh.html', NULL, 'sofa-gia-dinh', 'main', '', NULL, 83, '', 'products', 5, 1, 0, 'vi', NULL, NULL),
(100, 'Sofa khách sạn', 'danh-muc/sofa-khach-san.html', NULL, 'sofa-khach-san', 'main', '', NULL, 83, '', 'products', 6, 2, 0, 'vi', NULL, NULL),
(101, 'Sofa cafe', 'danh-muc/sofa-cafe.html', NULL, 'sofa-cafe', 'main', '', NULL, 83, '', 'products', 7, 3, 0, 'vi', NULL, NULL),
(92, 'Sofa văng', 'danh-muc/sofa-vang.html', NULL, 'sofa-vang', 'main', '', NULL, 83, '', 'products', 0, 0, 0, 'vi', NULL, NULL),
(106, 'Sofa giá rẻ', 'danh-muc/sofa-gia-re.html', NULL, 'sofa-gia-re', 'main', '', NULL, 83, '', 'products', 12, 8, 0, 'vi', NULL, NULL),
(112, 'Trang chủ', 'trang-chu', NULL, 'trang-chu', 'main', '', NULL, 0, '', '0', 0, 0, 0, 'vi', NULL, NULL),
(103, 'Sofa karaoke', 'danh-muc/sofa-karaoke.html', NULL, 'sofa-karaoke', 'main', '', NULL, 83, '', 'products', 9, 5, 0, 'vi', NULL, NULL),
(104, 'Sofa góc', 'danh-muc/sofa-goc.html', NULL, 'sofa-goc', 'main', '', NULL, 83, '', 'products', 10, 6, 0, 'vi', NULL, NULL),
(105, 'Sofa cao cấp', 'danh-muc/sofa-cao-cap.html', NULL, 'sofa-cao-cap', 'main', '', NULL, 83, '', 'products', 11, 7, 0, 'vi', NULL, NULL),
(98, 'Blog', 'danh-muc-tin/tin-tuc.html', NULL, 'blog', 'main', '', NULL, 0, '', 'news', 4, 3, 0, 'vi', NULL, NULL),
(108, 'Hỗ trợ 096 180 3334', 'ho-tro-096-180-3334', NULL, 'ho-tro-096-180-3334', 'bottom', '', NULL, 58, '<p>0961703335 Shinosuke</p>\r\n', '0', 0, 2, 0, 'vi', NULL, NULL),
(119, 'tv2', '0999999980', NULL, 'tv2', 'bottom', '', NULL, 58, '<p><strong><span style=\"font-size:14px\">MrA</span></strong>: <span style=\"font-size:14px\"><strong><span style=\"color:#f1c40f\">0922222</span></strong></span></p>\r\n', '0', 0, 1, 0, 'vi', NULL, NULL),
(118, 'baolong@gmail.com', 'baolonggmailcom', NULL, 'baolonggmailcom', 'bottom', '', NULL, 110, '<p>baolong@gmail.com</p>\r\n', '0', 0, 1, 0, 'vi', NULL, NULL),
(110, ' Email liên hệ', 'email-lien-he', NULL, 'email-lien-he', 'bottom', '', NULL, 0, '', '0', 0, 1, 0, 'vi', NULL, NULL),
(111, 'congtybaolong@gmail.com', 'congtybaolong', NULL, 'congtybaolonggmailcom', 'bottom', '', NULL, 110, '<p>congtybaolong@gmail.com</p>\r\n', '0', 0, 0, 0, 'vi', NULL, NULL),
(120, 'Product', 'product', NULL, 'product', 'main', '', NULL, 0, '', '0', 0, 2, 0, 'en', NULL, NULL),
(121, 'Watches & Eyewear', 'watches-eyewear', NULL, 'watches-eyewear', 'right', '', NULL, 0, '', '0', 0, 12, 0, 'vi', NULL, NULL),
(122, 'Television Dealers', 'television-dealers', NULL, 'television-dealers', 'right', '', NULL, 0, '', '0', 0, 11, 0, 'vi', NULL, NULL),
(123, 'Television Dealers', 'television-dealers', NULL, 'television-dealers', 'right', '', NULL, 0, '', '0', 0, 10, 0, 'vi', NULL, NULL),
(124, 'Digital & Prime Music', 'digital-prime-music', NULL, 'digital-prime-music', 'right', '', NULL, 0, '', '0', 0, 9, 0, 'vi', NULL, NULL),
(125, 'Radiosion', 'radiosion', NULL, 'radiosion', 'right', '', NULL, 0, '', '0', 0, 8, 0, 'vi', NULL, NULL),
(126, 'Accessories', 'accessories', NULL, 'accessories', 'right', '', NULL, 0, '', '0', 0, 7, 0, 'vi', NULL, NULL),
(127, 'Car Electronic & GPS', 'car-electronic-gps', NULL, 'car-electronic-gps', 'right', '', NULL, 0, '', '0', 0, 6, 0, 'vi', NULL, NULL),
(128, 'Gadgets', 'gadgets', NULL, 'gadgets', 'right', '', NULL, 0, '', '0', 0, 5, 0, 'vi', NULL, NULL),
(129, 'TV & Audio', 'tv-audio', NULL, 'tv-audio', 'right', '', NULL, 0, '', '0', 0, 4, 0, 'vi', NULL, NULL),
(130, 'Video Games', 'video-games', NULL, 'video-games', 'right', '', NULL, 0, '', '0', 0, 3, 0, 'vi', NULL, NULL),
(131, 'Smartphones & Tablets', 'smartphones-tablets', NULL, 'smartphones-tablets', 'right', '', NULL, 0, '', '0', 0, 2, 0, 'vi', NULL, NULL),
(132, 'Cameras & Camcorders', 'cameras-camcorders', NULL, 'cameras-camcorders', 'right', '', NULL, 0, '', '0', 0, 1, 0, 'vi', NULL, NULL),
(133, 'Laptops & Computers', 'laptops-computers', NULL, 'laptops-computers', 'right', '', NULL, 0, '', '0', 0, 0, 0, 'vi', NULL, NULL),
(137, 'Radiosion', 'radiosion', NULL, 'radiosion', 'top', '', NULL, 0, '', '0', 0, 8, 0, 'vi', NULL, NULL),
(138, 'Accessories', 'accessories', NULL, 'accessories', 'top', '', NULL, 0, '', '0', 0, 7, 0, 'vi', NULL, NULL),
(139, 'Car Electronic & GPS', 'car-electronic-gps', NULL, 'car-electronic-gps', 'top', '', NULL, 0, '', '0', 0, 6, 0, 'vi', NULL, NULL),
(140, 'Gadgets', 'gadgets', NULL, 'gadgets', 'top', '', NULL, 0, '', '0', 0, 5, 0, 'vi', NULL, NULL),
(141, 'TV & Audio', 'tv-audio', NULL, 'tv-audio', 'top', '', NULL, 0, '', '0', 0, 4, 0, 'vi', NULL, NULL),
(142, 'Video Games', 'video-games', NULL, 'video-games', 'top', '', NULL, 0, '', '0', 0, 3, 0, 'vi', NULL, NULL),
(143, 'Smartphones & Tablets', 'smartphones-tablets', NULL, 'smartphones-tablets', 'top', '', NULL, 0, '', '0', 0, 2, 0, 'vi', NULL, NULL),
(144, 'Cameras & Camcorders', 'cameras-camcorders', NULL, 'cameras-camcorders', 'top', '', NULL, 0, '', '0', 0, 1, 0, 'vi', NULL, NULL),
(145, 'Laptops & Computers', 'laptops-computers', NULL, 'laptops-computers', 'top', '', NULL, 0, '', '0', 0, 0, 0, 'vi', NULL, NULL),
(146, 'Laptops & Computers', 'laptops-computers', NULL, 'laptops-computers', 'top', '', NULL, 134, '', '0', 0, 0, 0, 'vi', NULL, NULL),
(147, 'Laptops & Computers', 'laptops-computers', NULL, 'laptops-computers', 'top', '', NULL, 145, '', '0', 0, 1, 0, 'vi', NULL, NULL),
(148, 'Laptops & Computers', 'laptops-computers', NULL, 'laptops-computers', 'top', '', NULL, 145, '', '0', 0, 0, 0, 'vi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `hot` int(11) DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `tag` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_update` int(8) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `focus` int(11) DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `button_1` int(11) NOT NULL,
  `sort` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `hot`, `image`, `content`, `alias`, `lang`, `tag`, `time_update`, `time`, `category_id`, `home`, `focus`, `title_seo`, `keyword_seo`, `description_seo`, `video`, `view`, `active`, `button_1`, `sort`) VALUES
(11, 'Lưu ý chọn ghế Sofa Cafe cho không gian quán Cafe của bạn', '<p>Qu&aacute;n Cafe với thiết kế v&agrave; c&aacute;ch b&agrave;i tr&iacute; độc đ&aacute;o chắc chắn sẽ tạo n&ecirc;n điểm mới lạ v&agrave; thu h&uacute;t c&agrave;ng nhiều kh&aacute;ch h&agrave;ng. V&agrave; nội thất l&agrave; yếu tố đ&oacute;ng vai tr&ograve; kh&ocirc;ng nhỏ tạo n&ecirc;n vẻ đẹp tổng thể của kh&ocirc;ng gian qu&aacute;n cafe. Ng&agrave;y nay, Sofa được sử dụng ng&agrave;y c&agrave;ng phổ biến khi trang tr&iacute; qu&aacute;n cafe. Bạn muốn l&agrave;m mới kh&ocirc;ng gian qu&aacute;n cafe của m&igrave;nh bằng ghế sofa. Tham khảo một số gợi &yacute; chọn&nbsp;<strong>ghế sofa cafe</strong>&nbsp;sau để c&oacute; chọn lựa đ&uacute;ng đắn nhất.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 'upload/img/news/sofacafe1.jpg', '<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>1. Chọn ghế sofa cafe h&agrave;i h&ograve;a với tổng thể của qu&aacute;n</strong></span></span></h2>\r\n\r\n<p>- Khi chọn ghế sofa cho qu&aacute;n cafe c&aacute;c bạn cần lưu &yacute; đến phong c&aacute;ch m&agrave; bạn hướng đến, nếu qu&aacute;n cafe của bạn c&oacute; phong c&aacute;ch năng động, trẻ trung th&igrave; n&ecirc;n chọn những loại ghế c&oacute; kiểu d&aacute;ng độc đ&aacute;o, m&agrave;u sắc tươi tắn. Hoặc nếu đối tượng kh&aacute;ch h&agrave;ng của bạn l&agrave; những người trưởng th&agrave;nh, thanh lịch th&igrave; những bộ ghế gam m&agrave;u trung t&iacute;nh, thiết kế đơn giản dẽ ph&ugrave; hợp hơn.</p>\r\n\r\n<p><img alt=\"Sofa cafe\" src=\"/upload/images/sofacafe6.jpg\" style=\"height:433px; width:650px\" /></p>\r\n\r\n<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>2. K&iacute;ch thước ghế sofa cafe</strong></span></span></h2>\r\n\r\n<p>- Bạn kh&ocirc;ng thể chọn lựa một mẫu ghế sofa k&iacute;ch thước lớn cho diện t&iacute;ch qu&aacute;n cafe nhỏ ch&iacute;nh v&igrave; thế bạn h&atilde;y lựa chọn những mẫu ghế vừa phải để c&acirc;n đối kh&ocirc;ng gian cho những đồ nội thất quan trọng kh&aacute;c của qu&aacute;n. Bạn n&ecirc;n c&oacute; những t&iacute;nh to&aacute;n , đo đạc cần thiết trước khi đặt mua để mang lại sự hợp l&iacute; nhất cho nội thất qu&aacute;n.</p>\r\n\r\n<p><img alt=\"Sofa2\" src=\"/upload/images/sofacafe3.jpg\" style=\"height:448px; width:746px\" /></p>\r\n\r\n<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>3. Thiết kế ghế sofa cafe</strong></span></span></h2>\r\n\r\n<p>- C&oacute; rất nhiều &yacute; tưởng để bạn lựa chọn với xu hướng thiết kế ghế sofa cho qu&aacute;n cafe hiện đại ng&agrave;y nay. Những yếu tố như chất liệu, m&agrave;u sắc n&ecirc;n được c&acirc;n nhắc kĩ c&agrave;ng để mang tới sự nổi bật cho nội thất qu&aacute;n cafe.</p>\r\n\r\n<p><img alt=\"Sofa cafe\" src=\"/upload/images/sofacafe7.jpg\" style=\"height:704px; width:564px\" /></p>\r\n\r\n<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>4. Ưu ti&ecirc;n chất lượng khi chọn ghế sofa cho qu&aacute;n cafe</strong></span></span></h2>\r\n\r\n<p>- Chọn những đồ nội thất chất lượng cho qu&aacute;n cafe l&agrave; điều rất quan trọng. Đầu ti&ecirc;n, một bộ sofa chất lượng chắc chắn sẽ g&acirc;y ấn tượng tốt với kh&aacute;ch h&agrave;ng v&agrave; sau đ&oacute; những bộ b&agrave;n ghế c&oacute; độ bền cao sẽ gi&uacute;p c&aacute;c bạn tiết kiệm được chi ph&iacute; ph&aacute;t sinh về sau.</p>\r\n\r\n<p><img alt=\"Sofacafe4\" src=\"/upload/images/sofacafe4.jpg\" style=\"height:539px; width:650px\" /></p>\r\n\r\n<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>5. H&agrave;i h&ograve;a với những đồ nội thất kh&aacute;c trong qu&aacute;n</strong></span></span></h2>\r\n\r\n<p>- Kh&aacute;ch h&agrave;ng sẽ v&ocirc; c&ugrave;ng h&agrave;i l&ograve;ng nếu c&aacute;c bạn biết c&aacute;ch kết hợp h&agrave;i h&ograve;a cho tổng thể kh&ocirc;ng gian của m&igrave;nh. C&aacute;c bạn c&oacute; thể chọn những bộ ghế sofa c&oacute; m&agrave;u sắc tươi s&aacute;ng cho những qu&aacute;n cafe c&oacute; kh&ocirc;ng gian mở, kh&ocirc;ng n&ecirc;n chọn những sản phẩm qu&aacute; l&ograve;e loẹt, sặc sỡ v&igrave; n&oacute; kh&ocirc;ng được tự nhi&ecirc;n, gần gũi. Ngo&agrave;i ra, c&aacute;c bạn c&oacute; thể tạo điểm nhấn bằng thảm sofa hoặc gối trang tr&iacute; độc đ&aacute;o, ấn tượng. N&ecirc;n đặc biệt lưu &yacute; phối kết hợp với những đồ nội thất kh&aacute;c để đạt được hiệu quả thẩm mĩ cao nhất.</p>\r\n\r\n<p><img alt=\"Sofacafe9\" src=\"/upload/images/sofacafe8.jpg\" style=\"height:518px; width:800px\" /></p>\r\n\r\n<h4><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></h4>\r\n\r\n<h2><span style=\"color:#e74c3c\"><strong>C&Ocirc;NG TY NỘI THẤT BẢO LONG</strong></span></h2>\r\n\r\n<p>&nbsp;Địa chỉ: Địa chỉ: 130 Ho&agrave;ng C&ocirc;ng Chất, P. Ph&uacute; Diễn, Q. Bắc Từ Li&ecirc;m, Tp. H&agrave; Nội</p>\r\n\r\n<p><a href=\"http://sofabaolam.com/#\" title=\"tel:0986199122\">&nbsp;Hotline: 0963761678</a></p>\r\n\r\n<p>&nbsp;Email: tranlinh.manh@gmail.com</p>\r\n', 'luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'vi', NULL, NULL, 1561111166, 4, 1, 1, 'Lưu ý chọn ghế Sofa Cafe cho không gian quán Cafe của bạn', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Lưu ý chọn ghế Sofa Cafe cho không gian quán Cafe của bạn đẹp và tinh tế', '', 55, 1, 0, 3),
(12, 'Những lưu ý cực quan trọng khi chọn Sofa cho gia đình', '<p><em>Để lựa chọn một bộ sofa đẹp&nbsp;về mẫu m&atilde;, đạt chuẩn chất lượng kh&ocirc;ng phải l&agrave; điều dễ d&agrave;ng. C&oacute; lẽ phải mất kh&aacute; nhiều thời gian t&igrave;m hiểu kĩ c&agrave;ng nhưng chi qua 8 lưu &yacute; cực k&igrave; quan trọng khi mua ghế sofa của Dcor chắc chắn gia đ&igrave;nh c&oacute; được lựa chọn tuyệt vời nhất.</em></p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 'upload/img/news/166sofa111.png', '<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>1. Phần khung</strong></span></span></h4>\r\n\r\n<p>- Khi chọn mua ghế sofa kh&aacute;ch h&agrave;ng cần quan t&acirc;m đầu ti&ecirc;n đến phần khung của bộ ghế. Trong 1 bộ ghế sofa th&igrave; phần khung l&agrave; phần quan trọng nhất. C&aacute;c phần khung chịu lực ch&iacute;nh được l&agrave;m bằng gỗ tự nhi&ecirc;n để tạo kết cấu vững chắc, kh&ocirc;ng biến dạng cong v&ecirc;nh mối mọt trong qu&aacute; tr&igrave;nh sử dụng. Li&ecirc;n kết giữa c&aacute;c kết cấu n&agrave;y bằng mộng, đinh hoặc v&iacute;t đen nhằm tạo n&ecirc;n kết cấu ổn định nhất. Để biết được khung gỗ c&oacute; đạt chất lượng vững chắc hay kh&ocirc;ng, bạn chỉ cần kiểm tra sự ổn định của bộ ghế như khi x&ocirc; vịn kh&ocirc;ng bị ọp ẹp lung lanh, b&ecirc; nặng tay, khung chắc chắn, c&oacute; thể đứng l&ecirc;n những phần tay, lưng để nh&uacute;n m&agrave; kh&ocirc;ng bị v&otilde;ng hay hỏng.</p>\r\n\r\n<p>- Bạn c&oacute; thể t&igrave;m những sản phẩm sofa c&oacute; phần khung l&agrave;m từ c&aacute;c loại gỗ cứng sấy kh&ocirc; như gỗ dầu, th&ocirc;ng, sồi, bạch dương.. kết hợp với c&aacute;c loại v&aacute;n &eacute;p chất lượng cao hoặc v&aacute;n &eacute;p chịu nước.L&ograve; xo dạng xoắn đứng cũng l&agrave; loại tốt, tuy nhi&ecirc;n loại l&ograve; xo dạng d&acirc;y cuộn sẽ thoải m&aacute;i v&agrave; &iacute;t tốn k&eacute;m hơn.</p>\r\n\r\n<p><img alt=\"Sofagiadinh1\" src=\"/upload/images/166sofa21%20(1).jpg\" style=\"height:612px; width:790px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>2. Phần nệm</strong></span></span></h4>\r\n\r\n<p>- Phần nệm ch&iacute;nh l&agrave; yếu tố ch&iacute;nh tạo cảm gi&aacute;c &ecirc;m &aacute;i, thoải m&aacute;i, dễ chịu khi sử dụng. Điều đ&oacute; đạt được khi chất lượng nệm đạt ti&ecirc;u chuẩn. Để thử chất lượng nệm bạn c&oacute; thể ngồi, nh&uacute;n l&ecirc;n xuống ngay khi lựa chọn mua sofa. Nếu cảm gi&aacute;c &ecirc;m &aacute;i, kh&ocirc;ng bị l&uacute;n tụt v&agrave; g&ugrave; lưng th&igrave; đ&oacute; l&agrave; mẫu nệm đạt chuẩn.</p>\r\n\r\n<p>- Phần nệm hầu hết được l&agrave;m từ c&aacute;c chất polyurethane tổng hợp, kh&aacute;c nhau về độ nặng, đặc sẽ ảnh hưởng đến độ mềm, đ&agrave;n hồi của phần ngồi. C&aacute;c d&ograve;ng nội thất gi&aacute; rẻ thường chỉ d&ugrave;ng 1 loại m&uacute;t cố định, hoặc l&agrave; loại rỗng cao (như D25), tạo cảo gi&aacute; mềm nhưng mau xẹp, hoặc l&agrave; loại đặc hẳn (như D40), tạo cảm gi&aacute;c bền nhưng ngồi lại kh&ocirc;ng thoải m&aacute;i. D&ograve;ng nội thất tốt hơn th&igrave; c&oacute; thể bọc th&ecirc;m một &iacute;t g&ograve;n. Loại nệm chất lượng cao l&agrave; loại nệm được kết hợp nhiều loại m&uacute;t với c&aacute;c độ đ&agrave;n hồi kh&aacute;c nhau, kết hợp với c&aacute;c lớp g&ograve;n, hoặc c&oacute; thể l&agrave; l&ocirc;ng vũ, để tạo độ phồng v&agrave; mềm mại - khi tiếp x&uacute;c</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình 2\" src=\"/upload/images/156sofa12445.png\" style=\"height:532px; width:632px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>3. Phần l&ograve; xo đ&agrave;n hồi</strong></span></span></h4>\r\n\r\n<p>- L&ograve; xo l&agrave; phần chịu lực ngồi của sofa n&ecirc;n khi mua ghế sofa cần lưu &yacute; sử dụng l&ograve; xo c&oacute; độ đ&agrave;n hồi tốt v&igrave; n&oacute; l&agrave; kết cấu gắn liền với khung ghế, chịu lực t&aacute;c động trực tiếp từ đệm ngồi xuống ghế. L&ograve; xo được l&agrave;m bằng th&eacute;p l&ograve; xo đặc biệt S60C, chịu đ&agrave;n hồi cao, &ecirc;m &aacute;i khi sử dụng, bền với thời gian. Kh&ocirc;ng bị gi&atilde;n lỏng trong suốt qu&aacute; tr&igrave;nh sử dụng, chịu được lực n&eacute;n lớn.</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/sofavang1.jpg\" style=\"height:534px; width:800px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>4. Ch&acirc;n ghế</strong></span></span></h4>\r\n\r\n<p>- Ch&acirc;n ghế lu&ocirc;n cần lưu &yacute; tới độ cao. Ch&acirc;n ghế cần cao vừa tầm người d&ugrave;ng mang lại cảm gi&aacute;c tiện nghi, c&acirc;n đối trong kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch. Hơn nữa, cũng cần lưu &yacute; tới chất liệu của ch&acirc;n ghế. Nếu ch&acirc;n gỗ kiểm tra độ cứng, chắc của ch&acirc;n tr&aacute;nh bị mối mọt.</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/Picture14-700x441.gif\" style=\"height:441px; width:700px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>5. Kiểu d&aacute;ng của ghế</strong></span></span></h4>\r\n\r\n<p>- Trước khi đi mua ghế sofa bạn h&atilde;y tham khảo một số kiểu d&aacute;ng ghế sofa tr&ecirc;n mạng hoặc tham khảo &yacute; kiến của kiến tr&uacute;c sư hoặc th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh . H&atilde;y chọn ghế sofa bổ sung cho phong c&aacute;ch trang tr&iacute; của cả ng&ocirc;i nh&agrave;. Với kh&ocirc;ng gian nội thất theo phong c&aacute;ch hiện đại việc lựa chọn một mẫu sofa hiện đại hoặc sofa H&agrave;n Quốc l&agrave; kh&ocirc;ng thể bỏ qua. C&aacute;c kiểu sofa hiện đại c&oacute; xu hướng đường n&eacute;t thẳng, r&otilde; r&agrave;ng, ph&oacute;ng kho&aacute;ng, bọc s&aacute;t đất hoặc ch&acirc;n inox cao hẳn.</p>\r\n\r\n<p><img alt=\"Sofa gia đình\" src=\"/upload/images/lua-chon-ban-ghe-sofa-cho-can-ho.jpg\" style=\"height:511px; width:766px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>6. K&iacute;ch cỡ</strong></span></span></h4>\r\n\r\n<p>- Cần phải c&acirc;n đối giữa k&iacute;ch thước bộ b&agrave;n ghế sofa với diện t&iacute;ch kh&ocirc;ng gian của ph&ograve;ng kh&aacute;ch. Với kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch nhỏ hẹp bộ b&agrave;n ghế sofa chữ L vừa gi&uacute;p tiết kiệm diện t&iacute;ch v&ugrave;a mang lại cảm gi&aacute;c gọn g&agrave;ng, sang trọng cho ph&ograve;ng kh&aacute;ch. Nếu c&oacute; lợi thế về diện t&iacute;ch gia đ&igrave;nh c&oacute; nhiều lựa chọn hơn như b&agrave;n ghế sofa văng, sofa chữ U&hellip;</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/156sofa111.png\" style=\"height:577px; width:631px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>7. Chất liệu bọc</strong></span></span></h4>\r\n\r\n<p>- Vải bọc l&agrave; phần b&ecirc;n ngo&agrave;i của chiếc ghế sofa, khi mua ghế sofa kh&aacute;ch h&agrave;ng n&ecirc;n lưu &yacute; đến vỏ bọc n&ecirc;n d&ugrave;ng chất liệu da hay nỉ, vải. Việc chọn chất liệu bọc sofa phụ thuộc v&agrave;o nhu cầu sử dụng v&agrave; sở th&iacute;ch của mỗi gia đ&igrave;nh.</p>\r\n\r\n<p>- Nếu bạn chọn đặt đ&oacute;ng sofa thay v&igrave; mua c&oacute; sẵn, h&atilde;y chắc rằng bạn đ&atilde; tưởng tượng được tổng thể sản phẩm của m&igrave;nh sẽ l&ecirc;n như thế n&agrave;o, c&aacute;c loại vải phối hợp với nhau ra sao. Nếu bạn kh&ocirc;ng tin tưởng về điều đ&oacute;, bạn c&oacute; thể chọn l&agrave;m theo mẫu của catalogue hoặc y&ecirc;u cầu với nh&acirc;n vi&ecirc;n tư vấn của rOsano cho bạn xem mẫu phối tr&ecirc;n m&aacute;y t&iacute;nh.</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/tim-hieu-ve-bao-duong-sofa-da-dung-chuan-nhu-chuyen-gia-1.jpg\" style=\"height:469px; width:660px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>8. Gối trang tr&iacute; sofa</strong></span></span></h4>\r\n\r\n<p>- T&ugrave;y v&agrave;o nhu cầu sử dụng cũng như thẩm mĩ m&agrave; gia chủ lựa chọn gối trang tr&iacute; với họa tiết hoa văn kh&aacute;c nhau. C&aacute;c gối trang tr&iacute; tại Dcor đa dạng về m&agrave;u sắc, chủng loại v&agrave; kiểu c&aacute;ch phục vụ nhu cầu kh&aacute;c nhau của người sử dụng.</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/sofani111.jpg\" style=\"height:480px; width:782px\" /></p>\r\n\r\n<h4><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></h4>\r\n\r\n<h2><span style=\"color:#e74c3c\"><strong>C&Ocirc;NG TY NỘI THẤT BẢO LONG</strong></span></h2>\r\n\r\n<p>&nbsp;Địa chỉ: Địa chỉ: 130 Ho&agrave;ng C&ocirc;ng Chất, P. Ph&uacute; Diễn, Q. Bắc Từ Li&ecirc;m, Tp. H&agrave; Nội</p>\r\n\r\n<p><a href=\"http://sofabaolam.com/#\" title=\"tel:0986199122\">&nbsp;Hotline: 0963761678</a></p>\r\n\r\n<p>&nbsp;Email: tranlinh.manh@gmail.com</p>\r\n', 'nhung-luu-y-cuc-quan-trong-khi-chon-sofa-cho-gia-dinh', 'vi', NULL, NULL, 1561111289, 4, 1, 1, 'Những lưu ý cực quan trọng khi chọn Sofa cho gia đình', 'Những lưu ý cực quan trọng khi chọn Sofa cho gia đình', 'Những lưu ý cực quan trọng khi chọn Sofa cho gia đình', '', 23, 1, 0, 1),
(13, 'Chọn Sofa cho phòng Karaoke sao cho đúng cách??', '<p><em>Để lựa chọn được bộ ghế sofa ph&ugrave; hợp th&igrave; ch&uacute;ng ta n&ecirc;n x&aacute;c định r&otilde; k&iacute;ch thước ph&ograve;ng h&aacute;t, sofa chỉ đẹp khi được &aacute;p dụng ph&ugrave; hợp, một qu&aacute;n karaoke thường c&oacute; nhiều ph&ograve;ng h&aacute;t với diện t&iacute;ch kh&aacute;c nhau n&ecirc;n những thiết kế sofa cũng c&oacute; đ&ocirc;i phần kh&aacute;c nhau về k&iacute;ch thước. Th&ocirc;ng thường, trong ph&ograve;ng h&aacute;t thường sử dụng thiết kế chữ L hay chữ U để c&oacute; sức chứa lớn v&agrave; tạo sự tương t&aacute;c giữa mọi người. Nếu ph&ograve;ng c&oacute; diện t&iacute;ch nhỏ,&nbsp; n&ecirc;n chọn loại sofa chỉ c&oacute; ghế trường kỷ v&agrave; một chiếc b&agrave;n nhỏ, tạo kh&ocirc;ng gian tho&aacute;ng đ&atilde;ng tr&aacute;nh chọn thiết kế sofa lớn cho&aacute;n đầy kh&ocirc;ng gian sẽ g&acirc;y n&ecirc;n cảm gi&aacute;c ngột ngạt, chật chội. Ngược lại, với ph&ograve;ng rộng th&igrave; bạn n&ecirc;n chọn những bộ sofa gồm đầy đủ b&agrave;n, ghế, đ&ocirc;n&hellip; để lấp đầy c&aacute;c khoảng trống, tr&aacute;nh cảm gi&aacute;c bộ sofa đơn độc, lạnh lẽo hay trống trải.</em><br />\r\n&nbsp;</p>\r\n', 1, 'upload/img/news/sofakaoke9.jpg', '<p><span style=\"color:#3498db\"><strong>PH&Ograve;NG&nbsp;KARAOKE THEO PHONG C&Aacute;CH CỔ ĐIỂN</strong></span></p>\r\n\r\n<p>- Đối với ph&ograve;ng h&aacute;t karaoke theo phong c&aacute;ch cổ điển th&igrave; việc lựa chọn một bộ ghế sofa ph&ugrave; hợp cũng cần l&agrave;m nổi bật được sự sang trọng, lịch l&atilde;m đ&uacute;ng như c&aacute;i t&ecirc;n gọi của n&oacute;. V&igrave; vậy n&ecirc;n chọn những bộ sofa cổ điển với kiểu d&aacute;ng cầu kỳ, thiết kế tinh tế sắc sảo, tạo điểm nhấn ở c&aacute;c g&oacute;c cạnh bằng hoa văn uốn lượn tạo cảm gi&aacute;c l&ocirc;i cuốn kh&aacute;ch h&agrave;ng từ c&aacute;i nh&igrave;n đầu ti&ecirc;n.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"color:#e74c3c\"><em>Về chất liệu</em></span></p>\r\n\r\n<p>- Mang đặc trưng cổ điển n&ecirc;n chất liệu được lựa chọn cho ph&ograve;ng h&aacute;t karaoke phải l&agrave; những loại chất liệu tạo n&ecirc;n sự sang trọng, qu&yacute; ph&aacute;i. Hiện nay tr&ecirc;n thị trường, mẫu ghế sofa ng&agrave;y c&agrave;ng đa dạng v&agrave; phong ph&uacute; về kiểu d&aacute;ng như: sofa vải, giả da, nỉ,...với kiểu d&aacute;ng một m&agrave;u đơn thuần, sofa cổ điển với kiểu d&aacute;ng cầu kỳ, thiết kế tinh tế sắc sảo, tạo điểm nhấn ở c&aacute;c g&oacute;c cạnh bằng hoa văn uốn lượn tinh tế kh&oacute; phai, nhằm tạo điểm nhấn cũng như l&ocirc;i cuốn kh&aacute;ch h&agrave;ng ngay từ c&aacute;i nh&igrave;n đầu ti&ecirc;n.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"color:#e74c3c\"><em>Về m&agrave;u sắc</em></span></p>\r\n\r\n<p><br />\r\n- M&agrave;u sắc được xem như l&agrave; linh hồn của bộ ghế sofa cho ph&ograve;ng h&aacute;t karaoke cổ điển. Sofa karaoke&nbsp; phong c&aacute;ch cổ điển thường tập trung v&agrave;o những gam m&agrave;u &aacute;nh v&agrave;ng, bạc, đỏ n&acirc;u, xanh, sữa&hellip;tạo cảm gi&aacute;c quyền lực v&agrave; l&ocirc;i cuốn . Tất cả bảng m&agrave;u kết hợp với nhau tạo th&agrave;nh một tổng thể v&ocirc; c&ugrave;ng ho&agrave;n mỹ. C&oacute; nhiều chủ đầu tư họ lu&ocirc;n chọn m&agrave;u ph&ugrave; hợp với phong thủy, vận, mệnh của m&igrave;nh . Ngo&agrave;i ra khi chọn m&agrave;u bạn n&ecirc;n ch&uacute; &yacute; đến m&agrave;u sơn tường v&agrave; m&agrave;u sắc s&agrave;n nh&agrave; để lựa chọn m&agrave;u cho bộ sofa được ph&ugrave; hợp hơn . Th&ocirc;ng thường sofa karaoke cổ điển sẽ thiết kế theo h&igrave;nh chữ L, chữ U, &hellip; . diện t&iacute;ch ph&ograve;ng karaoke cổ điển sẽ lớn, ch&iacute;nh v&igrave; thế m&agrave; bộ ghế sofa cũng phải ph&ugrave; hợp.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"Sofa Karaoke\" src=\"/upload/images/sofa-karaoke.jpg\" style=\"height:677px; width:1118px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"color:#3498db\"><strong>PH&Ograve;NG KARAOKE PHONG C&Aacute;CH HIỆN ĐẠI&nbsp;&nbsp;</strong></span></p>\r\n\r\n<p>- Kh&aacute;c với ph&ograve;ng h&aacute;t karaoke theo phong c&aacute;ch cổ điển, ph&ograve;ng h&aacute;t theo phong c&aacute;ch hiện đại c&oacute; lượng kh&aacute;ch h&agrave;ng tập trung chủ yếu ở lứa tuổi học sinh, sinh vi&ecirc;n, thanh ni&ecirc;n, người trẻ tuổi,... n&ecirc;n ghế thường được sử dụng l&agrave; loại sofa bọc da, sofa vải đ&iacute;nh hạt cườm hoặc đ&aacute; trang tr&iacute;, tạo n&ecirc;n&nbsp; phong c&aacute;ch mới mẻ, lạ mắt cũng như tạo kh&ocirc;ng gian hiện đại, vui vẻ v&agrave; năng động cho kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p><em>&nbsp;<span style=\"color:#e74c3c\">Về chất liệu</span></em><br />\r\n<br />\r\n- Với thiết kế hiện đại th&igrave; chất liệu được ưu ti&ecirc;n ở đ&acirc;y ch&iacute;nh l&agrave; sofa da v&agrave; sofa vải. Chất liệu ghế sẽ g&oacute;p phần quyết định đến việc kh&aacute;ch h&agrave;ng c&oacute; thoải m&aacute;i hay kh&ocirc;ng. Chắc chắn một điều rằng nếu thoải m&aacute;i th&igrave; họ sẽ n&aacute;n lại l&acirc;u hơn cũng như muốn quay trở lại qu&aacute;n của bạn th&ecirc;m v&agrave;i lần kh&aacute;c nữa. Hơn thế nữa, nếu bạn lựa chọn một chất liệu k&eacute;m hay dễ d&iacute;nh bẩn v&agrave; kh&oacute; vệ sinh th&igrave; việc bạn phải giặt hằng ng&agrave;y l&agrave; điều đương nhi&ecirc;n.V&igrave; vậy, chất liệu da sẽ được sử dụng nhiều nhất bởi ưu điểm kh&ocirc;ng b&aacute;m bụi, dễ lau ch&ugrave;i v&agrave; c&oacute; bẩn hay đổ nước l&ecirc;n th&igrave; cũng dễ lau v&agrave; kh&ocirc;ng mất nhiều c&ocirc;ng sức. C&ograve;n nếu như bạn mua chất nỉ hay vải th&igrave; n&ecirc;n mua th&ecirc;m vỏ bọc b&ecirc;n ngo&agrave;i,&nbsp; vừa tiết kiệm thời gian cũng như c&ocirc;ng sức. Kh&ocirc;ng giống như c&aacute;c loại ghế sofa d&ugrave;ng trong ph&ograve;ng h&aacute;t mang phong c&aacute;ch cổ điển, ghế sofa d&ugrave;ng cho ph&ograve;ng h&aacute;t karaoke hiện đại kh&ocirc;ng cầu k&igrave; về kiểu d&aacute;ng cũng như họa tiết trang tr&iacute;. N&oacute; được thiết kế theo h&igrave;nh thức đơn giản h&oacute;a, nhẹ nh&agrave;ng nhưng trẻ trung, năng động. C&aacute;c mẫu ghế thường được sử dụng đ&oacute; l&agrave; loại ghế chữ U, chữ L, vừa tiết kiệm được kh&ocirc;ng gian, vừa mang lại cảm gi&aacute;c thoải m&aacute;i, tho&aacute;ng m&aacute;t cho căn ph&ograve;ng của bạn.</p>\r\n\r\n<p><em><span style=\"color:#e74c3c\">Về m&agrave;u sắc</span>&nbsp;</em></p>\r\n\r\n<p>- Tr&aacute;i ngược với h&igrave;nh thức trang tr&iacute;, sofa karaoke hiện đại lại mang m&agrave;u sắc nổi bật, bắt mắt. Bạn c&oacute; thể kết hợp nhiều m&agrave;u sắc hay chỉ để c&oacute; một m&agrave;u trơn, nhưng chung quy lại tất cả phải c&oacute; sự h&agrave;i h&ograve;a, g&acirc;y cảm gi&aacute;c ấn tượng cho kh&aacute;ch h&agrave;ng, kh&ocirc;ng bị rối mắt. Những m&agrave;u sắc thường d&ugrave;ng cho ghế sofa karaoke hiện đại sẽ l&agrave; m&agrave;u v&agrave;ng, xanh, cam, t&iacute;m, đỏ.... Một lưu &yacute; nhỏ nữa l&agrave; bạn n&ecirc;n chọn m&agrave;u của ghế ph&ugrave; hợp với &aacute;nh điện, m&agrave;u sơn tường v&agrave; s&agrave;n nh&agrave; để tạo n&ecirc;n kh&ocirc;ng gian thoải m&aacute;i nhất cho người sử dụng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"sofa Karaoke\" src=\"/upload/images/sofakaoke77.jpg\" style=\"height:935px; width:1400px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"color:#3498db\"><strong>P</strong><strong>H&Ograve;NG KARAOKE B&Igrave;NH D&Acirc;N</strong>&nbsp;<strong>&amp; PH&Ograve;NG KARAOKE GIA Đ&Igrave;NH</strong></span></p>\r\n\r\n<p>- Đối với ph&ograve;ng karaoke b&igrave;nh d&acirc;n hay những ph&ograve;ng karaoke gia đ&igrave;nh th&igrave; đơn giản hơn về h&igrave;nh thức, c&oacute; thể lựa chọn những mẫu ghế sofa đơn giản, kh&ocirc;ng cầu k&igrave; về họa tiết trang tr&iacute; nhưng vẫn phải đảm bảo chất lượng, tạo cảm gi&aacute;c thư gi&atilde;n thoải m&aacute;i cho kh&aacute;ch h&agrave;ng cũng như c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh sau mỗi giờ học tập v&agrave; l&agrave;m việc căng thẳng. Đa phần chiếc ghế sofa&nbsp; n&agrave;y mới nh&igrave;n sẽ rất giản dị b&igrave;nh thường kh&ocirc;ng cầu k&igrave; nhưng vẫn to&aacute;t l&ecirc;n sự tinh tế, ấn tượng. Với những ph&ograve;ng karaoke với diện t&iacute;ch hẹp th&igrave; những mẫu ghế sofa b&igrave;nh d&acirc;n với k&iacute;ch thước nhỏ, kh&ocirc;ng cầu k&igrave; sẽ l&agrave; hợp l&iacute; nhất.. Điểm đ&aacute;ng lưu &yacute; ở đ&acirc;y ch&iacute;nh l&agrave; chất liệu, v&igrave; n&oacute; sử dụng ở nhiều nơi kh&aacute;c nhau, trong qu&aacute;n h&aacute;t hay cả ở gia đ&igrave;nh th&igrave; chất liệu n&oacute; cũng vẫn cần được lưu &yacute;. C&oacute; nhiều đơn vị thi c&ocirc;ng họ sẽ d&ugrave;ng chất liệu vải, nỉ, da nhưng v&igrave; chi ph&iacute; đắt hơn n&ecirc;n cũng sẽ được hạn chế. Thường th&igrave; k&iacute;ch thước của những bộ sofa gi&aacute; b&igrave;nh d&acirc;n sẽ nhỏ hơn sofa karaoke hiện đại v&agrave; cổ điển, do diện t&iacute;ch ph&ograve;ng cũng sẽ nhỏ hơn n&ecirc;n thường sẽ sử dụng sofa đ&ocirc;i, sofa ba hay sofa g&oacute;c chữ L với ch&acirc;n gỗ chắc chắn v&agrave; chất lượng tốt kh&ocirc;ng thua k&eacute;m những kiểu sofa kh&aacute;c.&nbsp;</p>\r\n\r\n<p><span style=\"color:#e74c3c\"><em>Về m&agrave;u sắc</em></span></p>\r\n\r\n<p>- Đối với kiểu sofa cho ph&ograve;ng h&aacute;t karaoke b&igrave;nh d&acirc;n sẽ được chọn lựa theo sở th&iacute;ch hay phong thủy nhưng vẫn phải ph&ugrave; hợp với m&agrave;u sơn tường, m&agrave;u s&agrave;n nh&agrave;. C&oacute; thể lựa chọn những m&agrave;u thường tập trung chủ yếu v&agrave;o những gam m&agrave;u n&oacute;ng m&agrave;u trầm như m&agrave;u v&agrave;ng, v&agrave;ng &aacute;nh kim, m&agrave;u bạc, n&acirc;u, đỏ,&hellip;.tạo cảm gi&aacute;c đơn giản m&agrave; l&ocirc;i cuốn. M&agrave;u sắc lu&ocirc;n l&agrave; thứ t&acirc;m điểm được kh&aacute;ch h&agrave;ng ch&uacute; &yacute; v&agrave; c&oacute; ấn tượng nhiều nhất, ch&iacute;nh v&igrave; vậy, để thiết kế được mẫu ph&ograve;ng h&aacute;t b&igrave;nh d&acirc;n, ph&ograve;ng h&aacute;t gia đ&igrave;nh vừa đẹp về thẩm mỹ vừa đẹp về chất lượng, ch&uacute;ng t&ocirc;i lu&ocirc;n nỗ lực t&igrave;m t&ograve;i, nghi&ecirc;n cứu những c&aacute;ch phối hợp ho&agrave;n hảo để mang lại một kh&ocirc;ng gian sinh động, đa sắc m&agrave;u.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"Sofa Karaoke\" src=\"/upload/images/ban-ghe-sofa-karaoke-dep.jpg\" style=\"height:762px; width:1290px\" /></p>\r\n\r\n<h4><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></h4>\r\n\r\n<h2><span style=\"color:#e74c3c\"><strong>C&Ocirc;NG TY NỘI THẤT BẢO LONG</strong></span></h2>\r\n\r\n<p>&nbsp;Địa chỉ: Địa chỉ: 130 Ho&agrave;ng C&ocirc;ng Chất, P. Ph&uacute; Diễn, Q. Bắc Từ Li&ecirc;m, Tp. H&agrave; Nội</p>\r\n\r\n<p><a href=\"http://sofabaolam.com/#\" title=\"tel:0986199122\">&nbsp;Hotline: 0963761678</a></p>\r\n\r\n<p>&nbsp;Email: tranlinh.manh@gmail.com</p>\r\n', 'chon-sofa-cho-phong-karaoke-sao-cho-dung-cach', 'vi', NULL, NULL, 1561110995, 4, 1, 1, 'Chọn Sofa cho phòng Karaoke sao cho đúng cách', 'Chọn Sofa cho phòng Karaoke sao cho đúng cách', 'Việc lựa chọn Sofa cho phòng Karaoke rất quan trọng đến đẳng cấp, thẩm mỹ và chất lượng của phòng Karaoke', '', 16, 1, 0, 2),
(14, 'Những lưu ý không thể bỏ qua khi bạn muốn mua sofa giá rẻ', '<p>&nbsp;</p>\r\n\r\n<p><em>Trước khi chọn mua ghế sofa gi&aacute; rẻ bạn cần phải nắm r&otilde; những ưu v&agrave; khuyết điểm của d&ograve;ng sản phẩm n&agrave;y để c&oacute; thể chọn được sản phẩm chất lượng nhất. Để gi&uacute;p qu&yacute; kh&aacute;ch h&agrave;ng c&oacute; thể tự m&igrave;nh chọn được bộ sofa gi&aacute; rẻ&nbsp;b&agrave;i viết dưới đ&acirc;y Nội thất Bảo L&acirc;m gửi tới qu&yacute; vị 5 lưu &yacute; trước khi chọn mua:</em></p>\r\n', 1, 'upload/img/news/166sofa21-1.jpg', '<p><img alt=\"Sofa Giá rẻ\" src=\"/upload/images/sofavang2.jpg\" style=\"height:720px; width:960px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\">1.&nbsp;<strong>Kiểm tra khung ghế</strong></span></span></p>\r\n\r\n<p>Khi chọn mua ghế sofa gi&aacute; rẻ điều quan trọng nhất m&agrave; bạn kh&ocirc;ng thể bỏ qua đ&oacute; l&agrave; phần khung ghế. Phần khung sofa l&agrave; bộ phận ch&iacute;nh quyết định đến chất lượng, độ bền cũng như kiểu d&aacute;ng của sản phẩm.</p>\r\n\r\n<p>Bạn c&oacute; thể kiểm tra sự chắc chắn của khung ghế c&oacute; được đảm bảo hay kh&ocirc;ng bằng c&aacute;ch ngồi l&ecirc;n ghế, x&ocirc; vịn ghế xem c&oacute; bị lung lay, ọp ẹp hay bị l&uacute;n, v&otilde;ng hay kh&ocirc;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\"><strong>2. Kiểm tra chất lượng đệm m&uacute;t</strong></span></span></p>\r\n\r\n<p>Đệm m&uacute;t l&agrave; phẩn ảnh hưởng trực tiếp đến sự &ecirc;m &aacute;i khi ngồi tr&ecirc;n sofa. Đ&acirc;y cũng l&agrave; phần ch&uacute;ng ta kh&ocirc;ng thể bỏ qua trước khi lựa chọn c&aacute;c sản phẩm sofa, đặc biệt l&agrave; sofa gi&aacute; rẻ. Bởi c&aacute;c sản phẩm gi&aacute; rẻ thường sử dụng c&aacute;c loại đệm m&uacute;t c&oacute; chất lượng thấp, nhanh ch&oacute;ng bị sụt l&uacute;n, biến dạng trong thời gian ngắn sử dụng. C&aacute;ch để bạn kiểm tra chất lượng đệm m&uacute;t c&oacute; được đảm bảo hay kh&ocirc;ng đơn giản l&agrave; bạn ngồi l&ecirc;n ghế trong khoảng thời gian từ 10 &ndash; 15 ph&uacute;t xem phần đệm c&oacute; đ&agrave;n hồi nhanh ch&oacute;ng trở về trạng th&aacute;i cũ hay kh&ocirc;ng. Nếu ch&uacute;ng mất nhiều thời gian để trở về trạng th&aacute;i ban đầu hay lu&ocirc;n hằn vết l&otilde;m th&igrave; chứng tỏ đ&acirc;y l&agrave; sản phẩm c&oacute; chất lượng đệm m&uacute;t k&eacute;m v&agrave; ch&uacute;ng ta kh&ocirc;ng n&ecirc;n chọn mua sản phẩm n&agrave;y.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\"><strong>3. Kiểm tra chất liệu vỏ bọc</strong></span></span></p>\r\n\r\n<p>Bọc vỏ ghế sofa gi&aacute; rẻ được lựa chọn chất liệu nỉ, vải, nhung hoặc chất liệu da simili. T&ugrave;y từng sản phẩm bạn muốn mua l&agrave;&nbsp;<a href=\"http://esofa.vn/sofa-ni\">sofa nỉ</a>, vải hay da m&agrave; n&ecirc;n kỹ c&agrave;ng, bề mặt kh&ocirc;ng được c&oacute; c&aacute;c vết r&aacute;ch, xước, sờn hay c&oacute; c&aacute;c vết bẩn tr&ecirc;n bề mặt.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\"><strong>4. Lựa chọn m&agrave;u sắc</strong></span></span></p>\r\n\r\n<p>Bạn n&ecirc;n lựa chọn m&agrave;u sắc bộ sofa tương đồng, ăn khớp với m&agrave;u sắc của ph&ograve;ng cần b&agrave;i tr&iacute; bộ sofa. Ph&ograve;ng kh&aacute;ch c&oacute; nhiều &aacute;nh s&aacute;ng th&igrave; ch&uacute;ng ta n&ecirc;n chọn c&aacute;c mẫu sofa c&oacute; gam m&agrave;u tươi s&aacute;ng như trắng, xanh, v&agrave;ng, be sẽ l&agrave;m cho kh&ocirc;ng gian ph&ograve;ng trở n&ecirc;n nổi bật v&agrave; ấn tượng hơn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\"><strong>5. Lựa chọn k&iacute;ch thước</strong></span></span></p>\r\n\r\n<p>Bạn cần x&aacute;c định k&iacute;ch thước của ph&ograve;ng đặt ghế sofa (chiều d&agrave;i, chiều rộng) l&agrave; bao nhi&ecirc;u để lựa chọn bộ ghế sofa c&oacute; k&iacute;ch thước ph&ugrave; hợp. Kh&ocirc;ng sử dụng bộ ghế sofa qu&aacute; nhỏ hay qu&aacute; lớn sẽ mang đến sự kh&ocirc;ng h&agrave;i h&ograve;a, lạc l&otilde;ng trong kh&ocirc;ng gian căn ph&ograve;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"Sofa giá rẻ\" src=\"/upload/images/sofacafe3(1).jpg\" style=\"height:448px; width:746px\" /></p>\r\n\r\n<h4><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></h4>\r\n\r\n<h2><span style=\"color:#e74c3c\"><strong>C&Ocirc;NG TY NỘI THẤT BẢO LONG</strong></span></h2>\r\n\r\n<p>&nbsp;<span style=\"color:#1abc9c\">Địa chỉ:</span> Địa chỉ: 130 Ho&agrave;ng C&ocirc;ng Chất, P. Ph&uacute; Diễn, Q. Bắc Từ Li&ecirc;m, Tp. H&agrave; Nội</p>\r\n\r\n<p><a href=\"http://sofabaolam.com/#\" title=\"tel:0986199122\">&nbsp;Hotline: 0963761678</a></p>\r\n\r\n<p><span style=\"color:#e74c3c\">&nbsp;Email</span>: tranlinh.manh@gmail.com</p>\r\n\r\n<p>.</p>\r\n', 'nhung-luu-y-khong-the-bo-qua-khi-ban-muon-mua-sofa-gia-re', 'vi', NULL, NULL, 1561110874, 4, 1, 1, 'Những lưu ý không thể bỏ qua khi bạn muốn mua sofa giá rẻ', 'Những lưu ý không thể bỏ qua khi bạn muốn mua sofa giá rẻ', '5 lưu ý để chọn được Sofa giá rẻ', '', 22, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE `news_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `focus` int(11) DEFAULT NULL,
  `hot` int(11) DEFAULT NULL,
  `coupon` int(11) DEFAULT NULL,
  `time_update` int(11) DEFAULT NULL,
  `time_start` int(8) DEFAULT NULL,
  `sort` int(5) DEFAULT 1,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `title_seo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `button_view_left` int(11) NOT NULL,
  `button_view_right` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news_category`
--

INSERT INTO `news_category` (`id`, `name`, `alias`, `description`, `image`, `parent_id`, `home`, `focus`, `hot`, `coupon`, `time_update`, `time_start`, `sort`, `lang`, `title_seo`, `keyword`, `description_seo`, `button_view_left`, `button_view_right`) VALUES
(4, 'Tin tức', 'tin-tuc', '', NULL, 0, 1, NULL, 1, NULL, NULL, NULL, 1, 'vi', '', NULL, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_to_category`
--

CREATE TABLE `news_to_category` (
  `id` int(11) NOT NULL,
  `id_news` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news_to_category`
--

INSERT INTO `news_to_category` (`id`, `id_news`, `id_category`) VALUES
(15, 1, 4),
(14, 2, 4),
(13, 3, 4),
(12, 4, 4),
(11, 5, 4),
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 2),
(10, 10, 1),
(39, 11, 4),
(40, 12, 4),
(37, 13, 4),
(35, 14, 4);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `note` text CHARACTER SET utf8 DEFAULT NULL,
  `item_order` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `show` tinyint(1) DEFAULT 0,
  `mark` tinyint(1) DEFAULT 0,
  `admin_note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `province` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
  `district` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `ward` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `user_id` decimal(21,0) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `view` tinyint(1) DEFAULT 1,
  `code` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `address2` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `startplaces` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `finishplace` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `mobile` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `startime` char(30) CHARACTER SET utf8 DEFAULT NULL,
  `endtime` char(30) CHARACTER SET utf8 DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `other_note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `code_sale` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `price_sale` int(10) DEFAULT NULL,
  `approved` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `price_ship` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `order_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(50) DEFAULT NULL,
  `price` int(100) DEFAULT NULL,
  `t_option` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `initierary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_start` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hotel` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `room` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tour_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_end` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `pro_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `description`, `lang`) VALUES
(3, 'Osaka', '', NULL),
(2, 'Tokyo', '', NULL),
(4, 'Kanazawa', '', NULL),
(5, 'Shirakawa-go', '', NULL),
(6, 'Nagano', '', NULL),
(7, 'Kobe', '', NULL),
(8, 'Hakuba', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `style` int(11) DEFAULT NULL,
  `id_value` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `coupon` tinyint(1) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `price` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `like` int(11) DEFAULT 0,
  `order` decimal(21,0) DEFAULT 0,
  `category_id` int(11) DEFAULT NULL,
  `caption_1` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption_2` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` int(11) DEFAULT NULL,
  `bought` int(11) DEFAULT 0,
  `dksudung` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `quantity` int(11) DEFAULT 0,
  `counter` int(11) DEFAULT 0,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT 'vi',
  `destination` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(8) DEFAULT NULL,
  `tags` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pro_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multi_image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_dir` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `quaranty` tinyint(3) DEFAULT NULL,
  `tinhtrang` tinyint(1) DEFAULT NULL,
  `group_attribute_id` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `button_color1` int(11) NOT NULL,
  `config_pro` text COLLATE utf8_unicode_ci NOT NULL,
  `config_pro_content` text COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `price_imp` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `style`, `id_value`, `brand`, `name`, `code`, `alias`, `image`, `hot`, `home`, `focus`, `coupon`, `view`, `active`, `price`, `price_sale`, `description`, `description_seo`, `location`, `title_seo`, `keyword_seo`, `detail`, `note`, `like`, `order`, `category_id`, `caption_1`, `caption_2`, `locale`, `bought`, `dksudung`, `sort`, `quantity`, `counter`, `lang`, `destination`, `time`, `tags`, `pro_dir`, `multi_image`, `img_dir`, `status`, `quaranty`, `tinhtrang`, `group_attribute_id`, `color`, `size`, `user_id`, `option_id`, `button_color1`, `config_pro`, `config_pro_content`, `weight`, `price_imp`) VALUES
(89, NULL, NULL, NULL, 'Man hinh may tinh', NULL, 'man-hinh-may-tinh', 'elect-pro2.jpg', NULL, NULL, NULL, NULL, NULL, 1, 6500000, 3200000, '', '', NULL, '', '', NULL, NULL, 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 'vi', NULL, 1601302901, NULL, '28092020', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(90, NULL, NULL, NULL, 'Man hinh may tinh2', NULL, 'man-hinh-may-tinh2', 'sale1.jpg', NULL, NULL, NULL, NULL, NULL, 1, 7500000, 5000000, '', '', NULL, '', '', NULL, NULL, 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 'vi', NULL, 1601304012, NULL, '28092020', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(91, NULL, NULL, NULL, 'Tai nghe', NULL, 'tai-nghe', 'sale31.jpg', NULL, NULL, NULL, NULL, NULL, 1, 9800000, 3200000, '', '', NULL, '', '', NULL, NULL, 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 'vi', NULL, 1601303885, NULL, '28092020', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(92, NULL, NULL, NULL, 'Man hinh may tinh2123', NULL, 'man-hinh-may-tinh2123', 'seller1.jpg', NULL, NULL, NULL, NULL, NULL, 1, 7500000, 3200000, '', '', NULL, '', '', NULL, NULL, 0, '0', 58, NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, 'vi', NULL, 1601568460, NULL, '01102020', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(93, NULL, NULL, NULL, 'Man hinh may tinh32121', NULL, 'man-hinh-may-tinh32121', 'elect-pro3.jpg', NULL, NULL, NULL, NULL, NULL, 1, 7500000, 5000000, '', '', NULL, '', '', NULL, NULL, 0, '0', 58, NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, 'vi', NULL, 1601568452, NULL, '01102020', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(94, NULL, NULL, NULL, 'Man hinh may tinh42123', NULL, 'man-hinh-may-tinh42123', 'elect-pro1.jpg', NULL, NULL, NULL, NULL, NULL, 1, 6500000, 3200000, '', '', NULL, '', '', NULL, NULL, 0, '0', 58, NULL, NULL, NULL, NULL, NULL, 6, 0, NULL, 'vi', NULL, 1601568445, NULL, '01102020', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(95, NULL, NULL, NULL, 'Man hinh may tinh52312', NULL, 'man-hinh-may-tinh52312', 'seller2.jpg', NULL, NULL, NULL, NULL, NULL, 1, 7500000, 5000000, '', '', NULL, '', '', NULL, NULL, 0, '0', 58, NULL, NULL, NULL, NULL, NULL, 7, 0, NULL, 'vi', NULL, 1601568438, NULL, '01102020', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_brand`
--

CREATE TABLE `product_brand` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `women` tinyint(1) DEFAULT NULL,
  `men` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `title_seo` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `description_seo` text CHARACTER SET latin1 DEFAULT NULL,
  `keyword` text CHARACTER SET latin1 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `lang` char(10) CHARACTER SET latin1 DEFAULT NULL,
  `gender` tinyint(1) DEFAULT 1,
  `view` tinyint(1) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_brand`
--

INSERT INTO `product_brand` (`id`, `parent_id`, `name`, `image`, `alias`, `description`, `women`, `men`, `focus`, `title_seo`, `description_seo`, `keyword`, `sort`, `lang`, `gender`, `view`, `home`, `hot`) VALUES
(10, NULL, 'Chanel', 'upload/img/tải_xuống_(1).png', 'chanel', '', 0, 1, 0, NULL, NULL, NULL, 1, 'vi', 1, NULL, NULL, NULL),
(11, NULL, 'puma', 'upload/img/images_(13).jpg', 'puma', '', NULL, NULL, 1, NULL, NULL, NULL, 16, 'vi', 1, NULL, NULL, NULL),
(13, NULL, 'Lanvin', 'upload/img/th17.png', 'lanvin', '', 0, 0, 0, NULL, NULL, NULL, 4, 'vi', 1, NULL, NULL, NULL),
(14, NULL, 'H&M', 'upload/img/tải_xuống_(2).png', 'hm', '', 0, 1, 0, NULL, NULL, NULL, 5, 'vi', 1, NULL, NULL, NULL),
(15, NULL, 'Nike', 'upload/img/tải_xuống_(1).jpg', 'nike', '', 0, 0, 1, NULL, NULL, NULL, 6, 'vi', 1, NULL, NULL, NULL),
(20, NULL, 'Valentino', 'upload/img/tải_xuống_(6).png', 'valentino', '', 0, 0, 0, NULL, NULL, NULL, 7, 'vi', 1, NULL, NULL, NULL),
(21, NULL, 'Zaza', 'upload/img/tải_xuống_(4).png', 'zaza', '', 0, 0, 0, NULL, NULL, NULL, 8, 'vi', 1, NULL, NULL, NULL),
(22, NULL, 'Gucci', 'upload/img/images_(4).jpg', 'gucci', '', NULL, NULL, NULL, NULL, NULL, NULL, 15, 'vi', 1, NULL, NULL, NULL),
(23, NULL, 'Armani', 'upload/img/th4.png', 'armani', '', 0, 0, 0, NULL, NULL, NULL, 1, 'vi', 1, NULL, NULL, NULL),
(24, NULL, 'Bebe', 'upload/img/8307969_orig.jpg', 'bebe', '', 0, 0, 0, NULL, NULL, NULL, 11, 'vi', 1, NULL, NULL, NULL),
(32, NULL, 'Dior', 'upload/img/images_(14).jpg', 'dior', '', 0, 0, 0, NULL, NULL, NULL, 12, 'vi', 1, NULL, NULL, NULL),
(33, NULL, 'Mango', 'upload/img/th7.png', 'mango', '', NULL, NULL, NULL, NULL, NULL, NULL, 14, 'vi', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `user` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` text CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `sort` int(3) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `coupon` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT '1',
  `gender` tinyint(1) DEFAULT NULL,
  `banner` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `user`, `name`, `title_seo`, `keyword_seo`, `description_seo`, `image`, `alias`, `parent_id`, `description`, `home`, `sort`, `hot`, `coupon`, `focus`, `lang`, `gender`, `banner`) VALUES
(45, NULL, 'Digital', '', NULL, '', 'upload/img/category/categories-1.jpg', 'digital', 0, '', NULL, 3, NULL, NULL, NULL, 'vi', NULL, NULL),
(46, NULL, 'Car Electronics', '', NULL, '', 'upload/img/category/digital-banner1.jpg', 'car-electronics', 0, '', NULL, 4, NULL, NULL, NULL, 'vi', NULL, NULL),
(44, NULL, 'Amplifier', '', NULL, '', 'upload/img/category/categories-7.jpg', 'amplifier', 0, '', NULL, 2, NULL, NULL, NULL, 'vi', NULL, NULL),
(43, NULL, 'Computer', '', NULL, '', 'upload/img/category/categories-3.jpg', 'computer', 0, '', NULL, 1, NULL, NULL, NULL, 'vi', NULL, NULL),
(47, NULL, 'Computer2', '', NULL, '', 'upload/img/category/categories-8.jpg', 'computer2', 0, '', NULL, 5, NULL, NULL, NULL, 'vi', NULL, NULL),
(48, NULL, 'Camcorders', '', NULL, '', 'upload/img/category/categories-2.jpg', 'camcorders', 0, '', NULL, 6, NULL, NULL, NULL, 'vi', NULL, NULL),
(49, NULL, 'Accessories', '', NULL, '', 'upload/img/category/categories-4.jpg', 'accessories', 0, '', NULL, 7, NULL, NULL, NULL, 'vi', NULL, NULL),
(50, NULL, 'Television Dealers', '', NULL, '', 'upload/img/category/categories-41.jpg', 'television-dealers', 0, '', NULL, 8, NULL, NULL, NULL, 'vi', NULL, NULL),
(51, NULL, 'Laptop', '', NULL, '', NULL, 'laptop', 0, '', 1, 9, NULL, NULL, NULL, 'vi', NULL, NULL),
(52, NULL, 'Camera', '', NULL, '', NULL, 'camera', 0, '', 1, 10, NULL, NULL, NULL, 'vi', NULL, NULL),
(53, NULL, 'Fridge', '', NULL, '', NULL, 'fridge', 0, '', 1, 11, NULL, NULL, NULL, 'vi', NULL, NULL),
(54, NULL, 'Hairdryer', '', NULL, '', NULL, 'hairdryer', 0, '', 1, 12, NULL, NULL, NULL, 'vi', NULL, NULL),
(55, NULL, 'PS VR', '', NULL, '', NULL, 'ps-vr', 0, '', NULL, 13, 1, NULL, NULL, 'vi', NULL, NULL),
(56, NULL, 'Game Cards', '', NULL, '', NULL, 'game-cards', 0, '', NULL, 14, 1, NULL, NULL, 'vi', NULL, NULL),
(57, NULL, 'Playstation', '', NULL, '', NULL, 'playstation', 0, '', NULL, 15, 1, NULL, NULL, 'vi', NULL, NULL),
(58, NULL, 'Headphone', '', NULL, '', NULL, 'headphone', 0, '', NULL, 16, 1, NULL, NULL, 'vi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE `product_color` (
  `id` int(11) NOT NULL,
  `color` varchar(255) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lang` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_color`
--

INSERT INTO `product_color` (`id`, `color`, `name`, `description`, `lang`, `image`, `sort`, `parent_id`) VALUES
(2, '#31859b', 'Màu xanh lam', 'màu xanh lam', 'vi', NULL, 3, NULL),
(3, '#000000', 'Màu đen', 'màu đen', 'vi', NULL, 4, NULL),
(4, '#ff0000', 'Màu đỏ', 'màu đỏ', 'vi', NULL, 5, NULL),
(5, '#7030a0', 'Màu tím', 'màu tím', 'vi', NULL, 6, NULL),
(6, '#f79646', 'Màu cam', 'màu cam', 'vi', NULL, 7, NULL),
(7, '#ffffff', 'Màu Trắng', 'màu trắng', 'vi', NULL, 8, NULL),
(8, '#d99694', 'màu hồng', '', 'vi', NULL, 9, NULL),
(9, '#7f7f7f', 'màu ghi', 'màu ghi', 'vi', NULL, 10, NULL),
(10, '#ffc000', 'màu ánh vàng', 'màu ánh vàng', 'vi', NULL, 11, NULL),
(11, '#974806', 'màu nâu', 'màu nâu', 'vi', NULL, 12, NULL),
(12, '#4f6128', 'màu xanh xám', 'màu xanh xám', 'vi', NULL, 13, NULL),
(13, '#d8d8d8', 'Màu ánh bạc', 'màu ánh bạc', 'vi', NULL, 14, NULL),
(16, '#5f497a', 'tím', '', 'vi', NULL, 15, NULL),
(17, '#fdeada', 'Màu nude', '<p>m&agrave;u nude</p>\r\n', 'vi', NULL, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_img`
--

CREATE TABLE `product_img` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `multi_image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `img_dir` varchar(255) CHARACTER SET latin1 NOT NULL,
  `id_color` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_locale`
--

CREATE TABLE `product_locale` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(70) CHARACTER SET latin1 DEFAULT NULL,
  `sort` tinyint(1) DEFAULT NULL,
  `description` text CHARACTER SET latin1 DEFAULT NULL,
  `lang` char(10) CHARACTER SET latin1 DEFAULT NULL,
  `alias` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_locale`
--

INSERT INTO `product_locale` (`id`, `name`, `image`, `sort`, `description`, `lang`, `alias`, `title_seo`, `description_seo`, `keyword`, `parent_id`) VALUES
(4, 'Ấn Độ', NULL, 2, '', 'vi', 'an-do', NULL, NULL, NULL, NULL),
(5, 'Thái Lan', NULL, 3, '', 'vi', 'thai-lan', NULL, NULL, NULL, NULL),
(6, 'Đài Loan', NULL, 4, '', 'vi', 'dai-loan', NULL, NULL, NULL, NULL),
(7, 'Trung Quốc', NULL, 5, '', 'vi', 'trung-quoc', NULL, NULL, NULL, NULL),
(8, 'Anh', NULL, 6, '', 'vi', 'anh', NULL, NULL, NULL, NULL),
(9, 'Pháp', NULL, 7, '', 'vi', 'phap', NULL, NULL, NULL, NULL),
(10, 'Mỹ', NULL, 8, '', 'vi', 'my', NULL, NULL, NULL, NULL),
(11, 'Nhật', NULL, 10, '', 'vi', 'nhat', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_old`
--

CREATE TABLE `product_old` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hot` tinyint(1) NOT NULL,
  `home` tinyint(1) NOT NULL,
  `focus` tinyint(1) NOT NULL,
  `coupon` tinyint(1) NOT NULL,
  `mostview` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `price_sale` int(11) NOT NULL,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_seo` text COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `like` int(11) NOT NULL DEFAULT 0,
  `origin` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `color` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `size` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `caption_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `from_price` int(11) DEFAULT NULL,
  `to_price` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `lang` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_price`
--

INSERT INTO `product_price` (`id`, `name`, `from_price`, `to_price`, `sort`, `lang`) VALUES
(1, 'Dưới 100.000 đ', 0, 100000, 1, 'vi'),
(3, '200.000 - 400.000 đ', 200000, 400000, 2, 'vi'),
(4, '400.000 - 500.000 đ', 400000, 500000, 3, 'vi'),
(5, '500.000 - 1000.000 đ', 500000, 1000000, 4, 'vi'),
(6, '1000000 - 2000000đ', 1000000, 2000000, 5, 'vi'),
(9, 'Trên 2000000đ', 2000000, 3000000, 6, 'vi');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `size` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sort` int(11) NOT NULL,
  `lang` varchar(100) CHARACTER SET latin1 NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`id`, `name`, `size`, `sort`, `lang`, `parent_id`) VALUES
(1, 'XL', '', 15, 'vi', NULL),
(2, 'M', '', 13, 'vi', NULL),
(3, 'XS', '', 12, 'vi', NULL),
(4, 'L', '', 14, 'vi', NULL),
(5, 'S', '', 11, 'vi', NULL),
(6, 'XXL', '', 16, 'vi', NULL),
(7, '34', '', 1, 'vi', NULL),
(8, '35', '', 2, 'vi', NULL),
(9, '36', '', 3, 'vi', NULL),
(10, '37', '', 4, 'vi', NULL),
(11, '38', '', 5, 'vi', NULL),
(12, '39', '', 6, 'vi', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_tag`
--

CREATE TABLE `product_tag` (
  `product_tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lang` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tag` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_brand`
--

CREATE TABLE `product_to_brand` (
  `brand_id` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_category`
--

CREATE TABLE `product_to_category` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_to_category`
--

INSERT INTO `product_to_category` (`id`, `id_product`, `id_category`) VALUES
(626, 93, 54),
(623, 94, 54),
(620, 95, 54),
(629, 92, 54),
(619, 95, 53),
(622, 94, 53),
(625, 93, 53),
(630, 92, 58),
(627, 93, 58),
(624, 94, 58),
(621, 95, 58),
(628, 92, 53);

-- --------------------------------------------------------

--
-- Table structure for table `product_to_color`
--

CREATE TABLE `product_to_color` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_color` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_to_color`
--

INSERT INTO `product_to_color` (`id`, `id_product`, `id_color`) VALUES
(67, 5, 2),
(68, 5, 3),
(69, 5, 4),
(70, 5, 5),
(71, 5, 6),
(72, 5, 7),
(75, 3, 2),
(76, 3, 3),
(82, 4, 2),
(81, 8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_to_option`
--

CREATE TABLE `product_to_option` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_season`
--

CREATE TABLE `product_to_season` (
  `id` int(11) NOT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_season` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_size`
--

CREATE TABLE `product_to_size` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_size` int(11) NOT NULL,
  `note` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_to_size`
--

INSERT INTO `product_to_size` (`id`, `id_product`, `id_size`, `note`) VALUES
(1, 120, 1, ''),
(5, 101, 1, ''),
(6, 100, 1, ''),
(7, 99, 1, ''),
(8, 98, 1, ''),
(9, 97, 1, ''),
(10, 96, 1, ''),
(11, 95, 1, ''),
(12, 93, 1, ''),
(13, 94, 1, ''),
(14, 16, 1, ''),
(16, 2, 1, ''),
(17, 3, 1, ''),
(19, 4, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `lat` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `lng` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `lat`, `lng`, `districtid`) VALUES
(1, '13B Conic Phong Phú', '10.71240234375', '106.64177703857', 1),
(2, '13D Asia Phú Mỹ', '10.705533027649', '106.64806365967', 1);

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `price` int(11) DEFAULT 0,
  `sort` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `name`, `code`, `price`, `sort`) VALUES
(1, 'Hồ Chí Minh', 'SG', 20000, NULL),
(2, 'Hà Nội', 'HN', 30000, NULL),
(3, 'Đà Nẵng', 'DDN', 0, NULL),
(4, 'Bình Dương', 'BD', 0, NULL),
(5, 'Đồng Nai', 'DNA', 0, NULL),
(6, 'Khánh Hòa', 'KH', 0, NULL),
(7, 'Hải Phòng', 'HP', 0, NULL),
(8, 'Long An', 'LA', 0, NULL),
(9, 'Quảng Nam', 'QNA', 0, NULL),
(10, 'Bà Rịa Vũng Tàu', 'VT', 0, NULL),
(11, 'Đắk Lắk', 'DDL', 0, NULL),
(12, 'Cần Thơ', 'CT', 0, NULL),
(13, 'Bình Thuận  ', 'BTH', 0, NULL),
(14, 'Lâm Đồng', 'LDD', 0, NULL),
(15, 'Thừa Thiên Huế', 'TTH', 0, NULL),
(16, 'Kiên Giang', 'KG', 0, NULL),
(17, 'Bắc Ninh', 'BN', 0, NULL),
(18, 'Quảng Ninh', 'QNI', 0, NULL),
(19, 'Thanh Hóa', 'TH', 0, NULL),
(20, 'Nghệ An', 'NA', 0, NULL),
(21, 'Hải Dương', 'HD', 0, NULL),
(22, 'Gia Lai', 'GL', 0, NULL),
(23, 'Bình Phước', 'BP', 0, NULL),
(24, 'Hưng Yên', 'HY', 0, NULL),
(25, 'Bình Định', 'BDD', 0, NULL),
(26, 'Tiền Giang', 'TG', 0, NULL),
(27, 'Thái Bình', 'TB', 0, NULL),
(28, 'Bắc Giang', 'BG', 0, NULL),
(29, 'Hòa Bình', 'HB', 0, NULL),
(30, 'An Giang', 'AG', 0, NULL),
(31, 'Vĩnh Phúc', 'VP', 0, NULL),
(32, 'Tây Ninh', 'TNI', 0, NULL),
(33, 'Thái Nguyên', 'TN', 0, NULL),
(34, 'Lào Cai', 'LCA', 0, NULL),
(35, 'Nam Định', 'NDD', 0, NULL),
(36, 'Quảng Ngãi', 'QNG', 0, NULL),
(37, 'Bến Tre', 'BTR', 0, NULL),
(38, 'Đắk Nông', 'DNO', 0, NULL),
(39, 'Cà Mau', 'CM', 120000, NULL),
(40, 'Vĩnh Long', 'VL', 3, NULL),
(41, 'Ninh Bình', 'NB', 320, NULL),
(42, 'Phú Thọ', 'PT', 25, NULL),
(43, 'Ninh Thuận', 'NT', 120000, NULL),
(44, 'Phú Yên', 'PY', 123456, NULL),
(45, 'Hà Nam', 'HNA', 40000, NULL),
(46, 'Hà Tĩnh', 'HT', 12000, NULL),
(47, 'Đồng Tháp', 'DDT', 0, NULL),
(48, 'Sóc Trăng', 'ST', 0, NULL),
(49, 'Kon Tum', 'KT', 0, NULL),
(50, 'Quảng Bình', 'QB', 0, NULL),
(51, 'Quảng Trị', 'QT', 0, NULL),
(52, 'Trà Vinh', 'TV', 0, NULL),
(53, 'Hậu Giang', 'HGI', 0, NULL),
(54, 'Sơn La', 'SL', 0, NULL),
(55, 'Bạc Liêu', 'BL', 0, NULL),
(56, 'Yên Bái', 'YB', 0, NULL),
(57, 'Tuyên Quang', 'TQ', 0, NULL),
(58, 'Điện Biên', 'DDB', 0, NULL),
(59, 'Lai Châu', 'LCH', 0, NULL),
(60, 'Lạng Sơn', 'LS', 0, NULL),
(61, 'Hà Giang', 'HG', 0, NULL),
(62, 'Bắc Kạn', 'BK', 0, NULL),
(63, 'Cao Bằng', 'CB', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pro_size`
--

CREATE TABLE `pro_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pro_values`
--

CREATE TABLE `pro_values` (
  `pro_id` int(11) DEFAULT NULL,
  `attr_id` int(11) DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `p_images`
--

CREATE TABLE `p_images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` char(200) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `p_images`
--

INSERT INTO `p_images` (`id`, `name`, `id_item`, `image`, `url`, `link`, `sort`) VALUES
(35, NULL, 38, 'upload/img/products_multi/13-1.jpg', NULL, NULL, NULL),
(66, NULL, 37, 'upload/img/products_multi/sofa_khách_sạn.jpg', NULL, NULL, NULL),
(16, 'anh so 2', NULL, 'upload/img/products_multi/logo-thep1.jpg', NULL, '', NULL),
(17, 'anh so 2', NULL, NULL, NULL, '', NULL),
(18, 'anh so 123', NULL, 'upload/img/products_multi/logo.png', NULL, '', NULL),
(19, 'anh cho 91', NULL, NULL, NULL, '', NULL),
(20, 'anh cho 91', NULL, 'upload/img/products_multi/logo1.png', NULL, '', NULL),
(22, 'anh so 1', 15, 'upload/img/products_multi/golf.png', NULL, '', NULL),
(24, NULL, 21, 'upload/img/products_multi/01.jpg', NULL, NULL, NULL),
(25, NULL, 21, 'upload/img/products_multi/2.jpg', NULL, NULL, NULL),
(26, NULL, 21, 'upload/img/products_multi/3.jpg', NULL, NULL, NULL),
(68, NULL, 28, 'upload/img/products_multi/kháchan3.jpg', NULL, NULL, NULL),
(60, NULL, 39, 'upload/img/products_multi/tim-hieu-ve-bao-duong-sofa-da-dung-chuan-nhu-chuyen-gia-1.jpg', NULL, NULL, NULL),
(36, NULL, 38, 'upload/img/products_multi/13-2.png', NULL, NULL, NULL),
(54, NULL, 47, 'upload/img/products_multi/sofani.jpg', NULL, NULL, NULL),
(40, NULL, 45, 'upload/img/products_multi/5.jpg', NULL, NULL, NULL),
(41, 'Sofagiadinh1', 46, 'upload/img/products_multi/4.jpg', NULL, NULL, NULL),
(42, NULL, 47, 'upload/img/products_multi/1.png', NULL, NULL, NULL),
(43, NULL, 19, 'upload/img/products_multi/15.png', NULL, NULL, NULL),
(44, NULL, 20, 'upload/img/products_multi/16-11.png', NULL, NULL, NULL),
(80, NULL, 23, 'upload/img/products_multi/156soffa127.png', NULL, NULL, NULL),
(107, NULL, 44, 'upload/img/products_multi/sofakaoke9.jpg', NULL, NULL, NULL),
(52, NULL, 45, 'upload/img/products_multi/sofavang1.jpg', NULL, NULL, NULL),
(53, 'Sofagiadinh1', 46, 'upload/img/products_multi/sofavang11.jpg', NULL, NULL, NULL),
(55, NULL, 48, 'upload/img/products_multi/sofani111.jpg', NULL, NULL, NULL),
(81, NULL, 49, 'upload/img/products_multi/sofada7.jpg', NULL, NULL, NULL),
(57, NULL, 51, 'upload/img/products_multi/sofa-nha-hang-khach-san-tinh-te-sofa-nguyen-a-3.jpg', NULL, NULL, NULL),
(74, NULL, 52, 'upload/img/products_multi/156soffa1233.png', NULL, NULL, NULL),
(59, NULL, 39, 'upload/img/products_multi/sofagd1011.jpg', NULL, NULL, NULL),
(62, NULL, 32, 'upload/img/products_multi/1_(1).jpg', NULL, NULL, NULL),
(63, NULL, 31, 'upload/img/products_multi/sofavang1.jpg', NULL, NULL, NULL),
(67, NULL, 30, 'upload/img/products_multi/sofakhachsan4.jpg', NULL, NULL, NULL),
(70, NULL, 56, 'upload/img/products_multi/156sofa125.png', NULL, NULL, NULL),
(72, NULL, 58, 'upload/img/products_multi/156soffa124.png', NULL, NULL, NULL),
(73, NULL, 40, 'upload/img/products_multi/156sofa12445.png', NULL, NULL, NULL),
(75, NULL, 27, 'upload/img/products_multi/166sofa311.png', NULL, NULL, NULL),
(78, NULL, 61, 'upload/img/products_multi/166sofa411.png', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `id_sanpham` int(11) DEFAULT NULL,
  `comment` text CHARACTER SET utf8 DEFAULT NULL,
  `flg` int(11) DEFAULT NULL,
  `reply` int(11) DEFAULT NULL,
  `review` tinyint(1) DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `id_sanpham`, `comment`, `flg`, `reply`, `review`, `user_name`, `user_email`, `time`, `date`) VALUES
(13, 5, 'hhhhggg', NULL, 0, NULL, 'sieuwebqt', 'dangtranmanh@gmail.com', 1505724581, NULL),
(14, 5, 'hhhhggg', NULL, 0, NULL, 'sieuwebqt', 'dangtranmanh@gmail.com', 1505724675, NULL),
(15, 5, 'noi dung', NULL, 0, NULL, 'nguyen đát', 'dat@gmail.com', 1505725003, NULL),
(16, 5, 'noi dung câu hỏi', NULL, 0, NULL, 'tran manh', 'tranmanh@gmail.com', 1505725440, NULL),
(17, 5, 'noi dung cua toi', NULL, 0, NULL, 'khowebqts', 'tranmanh@gmail.com', 1505725631, NULL),
(18, 5, 'noi dung', NULL, 0, 1, 'tranmanh', 'tranmanh@gmail.com', 1505725689, NULL),
(19, 5, 'noi dung', NULL, 0, 1, 'sieuwebqt', 'tranmanh@gmail.com', 1505725843, NULL),
(20, 5, 'noi dung', NULL, 0, 1, 'sieuwebqt', 'tranmanh@gmail.com', 1505725878, NULL),
(21, 5, 'noi dung', NULL, 0, 1, 'sieuwebqt', 'tranmanh@gmail.com', 1505725928, NULL),
(22, 5, 'noi dung câu hỏi', NULL, 0, 1, 'tranmanh', 'dangtranmanh@gmail.com', 1505726276, NULL),
(23, 5, 'noi dung cau tra loi', NULL, 21, 1, 'van đạt', 'dat@gmail.com', 1505726568, NULL),
(24, 4, 'sâssa', NULL, 0, 1, 'Vân', 'buivananh.th@gmail.com', 1505981779, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `raovat`
--

CREATE TABLE `raovat` (
  `home` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `price` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `caption_1` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption_2` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` int(11) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `quantity` int(11) DEFAULT 0,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT 'vi',
  `caption_3` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(8) DEFAULT NULL,
  `tags` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `raovat_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multi_image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_dir` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `tinhtrang` tinyint(1) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `id` int(11) NOT NULL,
  `style` int(11) DEFAULT NULL,
  `id_value` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `raovat`
--

INSERT INTO `raovat` (`home`, `focus`, `view`, `active`, `price`, `price_sale`, `description`, `description_seo`, `title_seo`, `keyword_seo`, `detail`, `note`, `category_id`, `caption_1`, `caption_2`, `locale`, `sort`, `quantity`, `lang`, `caption_3`, `time`, `tags`, `raovat_dir`, `multi_image`, `img_dir`, `status`, `tinhtrang`, `user_id`, `id`, `style`, `id_value`, `brand`, `name`, `code`, `alias`, `image`, `hot`) VALUES
(1, NULL, 0, 1, 0, 0, '<p>n&ocirc;i dung m&ocirc; ta</p>\r\n', '', '', '', '<p>noi dung chi tiet</p>\r\n', NULL, NULL, NULL, '<p>noi dung phu</p>\r\n', 0, 1, 0, 'vi', NULL, 1504065201, NULL, NULL, NULL, NULL, 0, NULL, 620, 1, NULL, NULL, 0, 'bán nhà tai hà nội', '', 'ban-nha-tai-ha-noi', NULL, NULL),
(1, NULL, 0, 1, 12424334, 12332342, '<p>n&ocirc;i dung m&ocirc; ta</p>\r\n', '', '', '', '<p>noi dung chi tiet</p>\r\n', NULL, 29, NULL, '<p>noi dung phu</p>\r\n', 6, 2, 0, 'vi', NULL, 1504068779, NULL, '30082017', NULL, NULL, 0, NULL, 620, 2, NULL, NULL, 14, 'bán nhà tai hà nội đường số 237', '', 'ban-nha-tai-ha-noi-duong-so-237', 'db652781fa07e94e75c9023c9de373cf.jpg', NULL),
(1, 1, 12, 1, 1234566, 1234333, '<p>n&ocirc;i dung m&ocirc; ta</p>\r\n', '', '', '', '<p>noi dung chi tiet</p>\r\n', NULL, 28, NULL, '<p>noi dung phu</p>\r\n', 5, 3, 0, 'vi', NULL, 1516353599, NULL, '30082017', NULL, NULL, 0, NULL, NULL, 3, NULL, NULL, 10, 'bán nhà tai hà nội viet nam', '', 'ban-nha-tai-ha-noi-viet-nam', '766564be313697c3bdae612b28a89d0a.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `raovat_category`
--

CREATE TABLE `raovat_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `home` tinyint(1) DEFAULT 0,
  `sort` int(3) DEFAULT 0,
  `hot` tinyint(1) DEFAULT 0,
  `focus` tinyint(1) DEFAULT 0,
  `lang` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` text CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `raovat_category`
--

INSERT INTO `raovat_category` (`id`, `name`, `image`, `alias`, `parent_id`, `description`, `home`, `sort`, `hot`, `focus`, `lang`, `title_seo`, `keyword_seo`, `description_seo`) VALUES
(20, 'Điện thoại, viễn thông ', 'upload/img/phone.png', 'dien-thoai-vien-thong', 0, '                                                                                                                                                                                                            ', 0, 1, 0, 0, 'vi', NULL, NULL, NULL),
(27, 'Ô tô, xe máy, xe đạp', 'upload/img/oto.png', 'o-to-xe-may-xe-dap', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(28, 'Xây dựng, công nghiệp', 'upload/img/connghiep.png', 'xay-dung-cong-nghiep', 0, '                                                                                                                                        ', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(29, 'Thời trang, phụ kiện', 'upload/img/thoitrang.png', 'thoi-trang-phu-kien', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(30, 'Mẹ & Bé', 'upload/img/me_be.png', 'me-be', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(31, 'Sức khỏe, sắc đẹp', 'upload/img/suckhoe.png', 'suc-khoe-sac-dep', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(33, 'Nội thất, ngoại thất', 'upload/img/noithat.png', 'noi-that-ngoai-that', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(34, 'Sách, đồ văn phòng', 'upload/img/sach.png', 'sach-do-van-phong', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(35, 'Hoa, quà tặng, đồ chơi', 'upload/img/qua_tang.png', 'hoa-qua-tang-do-choi', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(42, 'Khác', '', 'khac', 0, '', 0, 2, 1, 1, 'vi', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `raovat_images`
--

CREATE TABLE `raovat_images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` char(200) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `raovat_images`
--

INSERT INTO `raovat_images` (`id`, `name`, `id_item`, `image`, `url`, `link`, `sort`) VALUES
(1, NULL, 3, 'upload/img/raovats_multi/f46482c87ab814e5d5ea59819e568564.jpg', NULL, NULL, NULL),
(2, NULL, 3, 'upload/img/raovats_multi/f4b467b6d383eb5d6062b2fa9c9c0708.jpg', NULL, NULL, NULL),
(3, NULL, 3, 'upload/img/raovats_multi/e86f742e7d986de26413443600fa8535.jpg', NULL, NULL, NULL),
(4, NULL, 3, 'upload/img/raovats_multi/d640c2db815fbba330306bdbdc9e9326.jpg', NULL, NULL, NULL),
(5, NULL, 2, 'upload/img/raovats_multi/3915f302b19fa28fc4001d6a66238681.jpg', NULL, NULL, NULL),
(6, NULL, 2, 'upload/img/raovats_multi/866917b6bab0b8c3eeb0f52f45efd867.jpg', NULL, NULL, NULL),
(7, NULL, 2, 'upload/img/raovats_multi/a8f9dbaa6c627b3a47a0f442cbe0c1ab.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `raovat_tag`
--

CREATE TABLE `raovat_tag` (
  `raovat_tag_id` int(11) NOT NULL,
  `raovat_id` int(11) NOT NULL,
  `lang` varchar(11) CHARACTER SET utf8 NOT NULL,
  `tag` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raovat_to_category`
--

CREATE TABLE `raovat_to_category` (
  `id` int(11) NOT NULL,
  `id_raovat` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `raovat_to_category`
--

INSERT INTO `raovat_to_category` (`id`, `id_raovat`, `id_category`) VALUES
(18, 3, 27),
(19, 3, 28),
(26, 2, 27),
(27, 2, 28),
(28, 2, 29);

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT 0,
  `resource` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT 0,
  `icon` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active` int(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `parent_id`, `resource`, `name`, `description`, `sort`, `icon`, `alias`, `active`) VALUES
(10, 0, 'product', 'Quản lý sản phẩm', NULL, 3, 'fa-bars', '', 1),
(11, 10, 'products', 'Danh sách sản phẩm', NULL, 1, 'fa-files-o', 'vnadmin/product/products', 1),
(13, 116, 'listColor', 'Màu sắc', NULL, 3, 'fa-circle-o', 'vnadmin/attribute/listColor', 0),
(14, 116, 'listprice', 'Khoảng giá', NULL, 4, 'fa-circle-o', 'vnadmin/attribute/listprice', 0),
(15, 116, 'listOption', 'Kích thước', NULL, 5, 'fa-circle-o', 'vnadmin/attribute/listOption', 0),
(17, 0, 'menu', 'Quản lý menu', NULL, 7, 'fa-bars', 'vnadmin/menu/menulist', 1),
(18, 10, 'category_pro', 'Danh mục sản phẩm', NULL, 2, 'fa-files-o', 'vnadmin/product/categories', 1),
(19, 173, 'comments', 'Đánh giá bình luận', NULL, 3, 'fa-comments-o', 'vnadmin/comment/comments', 0),
(20, 173, 'questions', 'Danh sách hỏi đáp', NULL, 4, 'fa-question-circle', 'vnadmin/comment/questions', 0),
(22, 174, 'tag', 'Thẻ tags sản phẩm', NULL, 5, 'fa-tag', 'vnadmin/tag/listtagpro', 0),
(25, 0, 'news', 'Quản lý tin bài', NULL, 6, 'fa-newspaper-o', '', 1),
(26, 25, 'newslist', 'Danh sách tin', NULL, 1, 'fa-file-text-o', 'vnadmin/news/newslist', 1),
(28, 25, 'tagsnews', 'Tags tin tức', NULL, 3, 'fa fa-tag', '', 0),
(29, 0, 'media', 'Quản lý media', NULL, 1, 'fa-picture-o', '', 0),
(30, 29, 'listAll', 'Danh sách media', NULL, 1, 'fa-file-image-o', 'vnadmin/media/listAll', 1),
(31, 25, 'categories', 'Danh mục tin', NULL, 2, 'fa-newspaper-o', 'vnadmin/news/categories', 1),
(32, 29, 'categories', 'Danh mục media', NULL, 2, 'fa-file-image-o', 'vnadmin/media/categories', 1),
(33, 0, 'users', 'Quản lý thành viên', NULL, 16, 'fa-users', '', 1),
(34, 33, 'smslist', 'Tin Nhắn SMS', NULL, 6, 'fa-commenting-o', 'vnadmin/users/smslist', 1),
(39, 0, 'pages', 'Quản lý nội dung', NULL, 9, 'fa-file-o', 'vnadmin/pages/pagelist', 1),
(40, 0, 'video', 'Quản Lý Video', NULL, 2, 'fa-video-camera', '', 1),
(42, 40, 'listAll', 'Danh sách video', NULL, 1, 'fa-file-video-o', 'vnadmin/video/listAll', 1),
(43, 40, 'category_video', 'Danh mục video', NULL, 2, 'fa-video-camera', 'vnadmin/video/categories', 1),
(44, 107, 'listraovat', 'Danh sách rao vặt', NULL, 1, 'fa-files-o', 'vnadmin/raovat/listraovat', 1),
(49, 107, 'categories', 'Danh mục rao vặt', NULL, 2, 'fa-files-o', 'vnadmin/raovat/categories', 1),
(53, 0, 'imageupload', 'Quản lý banner', NULL, 8, 'fa-file-image-o', 'vnadmin/imageupload/banners', 1),
(54, 162, 'listWard', 'Quản lý phường xã', NULL, 3, 'fa-map-signs', 'vnadmin/province/listWard', 1),
(56, 162, 'listDistric', 'Quản lý quận huyện', NULL, 2, 'fa-map-marker', 'vnadmin/province/listDistric', 1),
(57, 162, 'street', 'Quản lý đường phố', NULL, 4, 'fa-road', 'vnadmin/province/listStreet', 1),
(58, 97, 'soldout', 'danh sách hết hàng', NULL, 1, 'fa-circle-o', 'admin/report/soldout', 0),
(63, 10, 'cat_add', 'Thêm - Sửa danh mục sp', NULL, 9, '', '', 0),
(64, 95, 'maps', 'Cấu hình bản đồ Maps', NULL, 1, ' fa-map-o', 'vnadmin/admin/bando_map', 1),
(65, 10, 'add', 'Thêm -Sửa sản phẩm', NULL, 7, '', '', 0),
(66, 17, 'delete', 'Xóa menu', NULL, 2, '', '', 0),
(67, 10, 'delete_once', 'Xóa sản phẩm', NULL, 8, '', '', 0),
(95, 0, 'admin', 'Hệ thống', NULL, 17, 'fa-gears text-red', '', 1),
(96, 95, 'site_option', 'Cấu hình hệ thống', NULL, 0, 'fa-circle-o text-red', 'vnadmin/admin/site_option', 1),
(97, 0, 'report', 'Báo cáo-Thống kê', NULL, 19, '', '', 0),
(98, 104, 'listProvince', 'Phí vận chuyển', NULL, 3, 'fa-truck', 'vnadmin/order/listProvince', 0),
(99, 90, 'categories', 'Danh mục share', NULL, 0, NULL, NULL, NULL),
(100, 90, 'cat_add', 'Thêm danh mục share', NULL, 0, NULL, NULL, NULL),
(101, 90, 'cat_edit', 'Sửa danh mục share', NULL, 0, NULL, NULL, NULL),
(102, 90, 'delete_cat', 'Xóa danh mục share', NULL, 0, NULL, NULL, NULL),
(103, 97, 'bestsellers', 'Hàng bán chạy', NULL, 2, 'fa-circle-o', 'admin/report/bestsellers', 0),
(104, 0, 'order', 'Quản lý giỏ hàng', NULL, 4, 'fa-shopping-cart', '', 0),
(105, 104, 'orders', 'Danh sách đặt hàng', NULL, 1, 'fa-cart-arrow-down', 'vnadmin/order/orders', 1),
(106, 104, 'listSale', 'Mã giảm giá', NULL, 2, 'fa-files-o', 'vnadmin/order/listSale', 0),
(107, 0, 'raovat', 'Quản lý rao vặt', NULL, 13, 'fa-bars', '', 1),
(108, 0, 'inuser', 'Ý kiến khách hàng', NULL, 0, 'fa-user-plus', 'vnadmin/inuser/categories', 1),
(109, 107, 'tagtinrao', 'Tags tin rao', NULL, 3, 'fa-tag', '', 0),
(110, 0, 'email', 'Quản lý email', NULL, 14, ' fa-envelope-o ', 'vnadmin/email/emails', 0),
(111, 0, 'support', 'Hỗ trợ  trực tuyến', NULL, 15, 'fa-life-ring', 'vnadmin/support/listSuport', 1),
(112, 95, 'store_shopping', 'Chuỗi cửa hàng', NULL, 5, 'fa-files-o', 'vnadmin/store/Ds_shopping', 0),
(113, 116, 'listBrand', 'Thương hiệu', NULL, 1, 'fa-circle-o', 'vnadmin/attribute/listBrand', 0),
(114, 116, 'listLocale', 'Xuất sứ', NULL, 2, 'fa-circle-o', 'vnadmin/attribute/listLocale', 0),
(115, 0, 'contact', 'Quản lý liên hệ', NULL, 10, 'fa-bars', 'vnadmin/contact/contacts', 1),
(116, 0, 'attribute', 'Thuộc tính sản phẩm', NULL, 5, 'fa-bars', '', 0),
(117, 108, 'cate_add', 'Thêm và Sửa', NULL, 2, '', '', 1),
(118, 108, 'delete_cat_once', 'Xóa', NULL, 3, '', '', 0),
(119, 108, 'categories', 'ý kiến khách hàng', NULL, 1, 'fa-files-o', 'vnadmin/inuser/categories', 1),
(120, 17, 'addmenu', 'Thêm - Sửa menu', NULL, 0, '', '', 0),
(121, 10, 'del_cat_once', 'Xóa danh mục sp', NULL, 10, '', '', 0),
(122, 29, 'add', 'Thêm -Sửa media', NULL, 3, '', '', 0),
(123, 29, 'delete_once', 'Xóa media', NULL, 4, '', '', 0),
(124, 29, 'cat_add', 'Thêm - Sửa danh mục media', NULL, 5, '', '', 0),
(125, 29, 'del_cat_once', 'Xóa danh mục media', NULL, 6, '', '', 0),
(126, 40, 'add', 'Thêm sửa video', NULL, 3, '', '', 0),
(127, 40, 'delete_once', 'Xóa video', NULL, 4, '', '', 0),
(128, 40, 'cat_add', 'Thêm danh mục video', NULL, 5, '', '', 0),
(129, 40, 'del_cat_once', 'Xóa danh mục video', NULL, 6, '', '', 0),
(130, 10, 'delete_once_question', 'Xóa hỏi đáp', NULL, 12, '', '', 0),
(131, 10, 'delete_once_comment', 'Xóa bình luận', NULL, 11, '', '', 0),
(132, 104, 'delete_once_orders', 'Xóa đơn hàng', NULL, 4, '', '', 0),
(133, 104, 'addSale', 'Thêm - Sửa mã giảm giá', NULL, 5, '', '', 0),
(134, 104, 'del_once_sale', 'Xóa mã giảm giá', NULL, 6, '', '', 0),
(135, 116, 'addbrand', 'Thêm - Sửa thương hiệu', NULL, 6, '', '', 0),
(136, 116, 'delete_brand_once', 'Xóa thương hiệu', NULL, 7, '', '', 0),
(137, 116, 'addlocale', 'Thêm - Sửa xuất sứ', NULL, 7, '', '', 0),
(138, 116, 'delete_locale_once', 'Xóa xuất sứ', NULL, 8, '', '', 0),
(139, 116, 'addcolor', 'Thêm - Sửa màu sắc', NULL, 9, '', '', 0),
(140, 116, 'delete_color_once', 'Xóa màu sắc', NULL, 10, '', '', 0),
(141, 116, 'addprice', 'Thêm - Sửa khoản giá', NULL, 11, '', '', 0),
(142, 116, 'delete_price_once', 'Xóa khoảng giá', NULL, 12, '', '', 0),
(143, 116, 'addoption', 'Thêm - Sửa kích thước', NULL, 12, '', '', 0),
(144, 116, 'delete_option_once', 'Xóa kích thước', NULL, 13, '', '', 0),
(145, 25, 'addnews', 'Thêm - Sửa tin tức', NULL, 4, '', '', 0),
(146, 25, 'delete_once_news', 'Xóa tin tức', NULL, 5, '', '', 0),
(147, 25, 'cat_add_news', 'Thêm - Sửa danh mục tin', NULL, 6, '', '', 0),
(148, 25, 'del_catnews_once', 'Xóa danh mục tin', NULL, 7, '', '', 0),
(149, 53, 'addbanner', 'Thêm - Sửa banner', NULL, 1, '', '', 0),
(150, 53, 'delete_Banner_once', 'Xóa banner', NULL, 2, '', '', 0),
(151, 39, 'addpage', 'Thêm - Sửa nội dung', NULL, 1, '', '', 0),
(152, 39, 'delete_page_once', 'Xóa nội dung', NULL, 2, '', '', 0),
(153, 115, 'delete', 'Xóa liên hệ', NULL, 1, '', '', 0),
(154, 107, 'add', 'Thêm - Sửa rao vặt', NULL, 4, '', '', 0),
(155, 107, 'delete_raovat_once', 'Xóa tin rao', NULL, 5, '', '', 0),
(156, 107, 'cat_add', 'Thêm - Sửa danh mục tin rao', NULL, 6, '', '', 0),
(157, 107, 'del_cattinrao_once', 'Xóa danh mục tin rao', NULL, 7, '', '', 0),
(158, 110, 'delete', 'Xóa email', NULL, 1, '', '', 0),
(159, 111, 'add', 'Thêm - Sửa hỗ trợ trực tuyến', NULL, 1, '', '', 0),
(160, 111, 'delete_support_once', 'Xóa hỗ trợ trực tuyến', NULL, 2, '', '', 0),
(161, 33, 'delete_users_once', 'Xóa thành viên', NULL, 1, '', '', 1),
(162, 0, 'province', 'Danh sách quan huyện', NULL, 18, '', '', 1),
(163, 33, 'add_users', 'Thêm thành viên quan trị', NULL, 1, '', 'vnadmin/users/add_users', 1),
(164, 33, 'delete_users_once', 'Xóa thành viên quản trị', NULL, 10, '', '', 1),
(165, 33, 'listuser_admin', 'Danh sách tài khoản quản trị', NULL, 0, '', 'vnadmin/users/listuser_admin', 1),
(166, 33, 'listusers', 'Danh sách thành viên', NULL, 0, '', 'vnadmin/users/listusers', 1),
(167, 17, 'menulist', 'Danh sách menu', NULL, 1, '', 'vnadmin/menu/menulist', 0),
(168, 53, 'banners', 'Danh sách banner', NULL, 0, '', 'vnadmin/imageupload/banners', 0),
(169, 39, 'pagelist', 'Danh sách nội dung', NULL, 0, '', 'vnadmin/pages/pagelist', 0),
(170, 110, 'emails', 'Danh sách email', NULL, 0, '', 'vnadmin/email/emails', 0),
(171, 115, 'contacts', 'Danh sách liên hệ', NULL, 0, '', 'vnadmin/contact/contacts', 0),
(172, 111, 'listSuport', 'Danh sách support', NULL, 0, '', 'vnadmin/support/listSuport', 1),
(173, 0, 'comment', 'Quản lý bình luận', NULL, 7, 'fa-comments-o', '', 0),
(174, 0, 'tag', 'Quản lý thẻ tag', NULL, 6, 'fa-tags', '', 0),
(175, 174, 'tag', 'Thẻ tags tin tức', NULL, 2, 'fa-tag', 'vnadmin/tag/listnew', 0),
(177, 95, 'setup_product', ' Cấu hình sản phẩm', NULL, 20, 'fa-gears', 'vnadmin/admin/setup_product', 1),
(178, 107, 'raovat_bds', 'Danh mục rao vặt BĐS', NULL, 1, 'fa-user-plus', 'vnadmin/raovat_bds/cat_raovat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_log`
--

CREATE TABLE `site_log` (
  `site_log_id` int(10) UNSIGNED NOT NULL,
  `no_of_visits` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `requested_url` tinytext CHARACTER SET utf8 DEFAULT NULL,
  `referer_page` tinytext CHARACTER SET utf8 DEFAULT NULL,
  `page_name` tinytext CHARACTER SET utf8 DEFAULT NULL,
  `query_string` tinytext CHARACTER SET utf8 DEFAULT NULL,
  `user_agent` tinytext CHARACTER SET utf8 DEFAULT NULL,
  `is_unique` tinyint(1) DEFAULT 0,
  `access_date` timestamp NULL DEFAULT current_timestamp(),
  `visits_count` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_log`
--

INSERT INTO `site_log` (`site_log_id`, `no_of_visits`, `ip_address`, `requested_url`, `referer_page`, `page_name`, `query_string`, `user_agent`, `is_unique`, `access_date`, `visits_count`) VALUES
(63916, 1, '::1', '/demo_sofa_10_5/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:50:03', 0),
(63915, 1, '::1', '/demo_ics/vnadmin', 'http://localhost:81/demo_ics/vnadmin/product/edit/95', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:39:18', 0),
(63913, 1, '::1', '/demo_ics/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:09:18', 0),
(63914, 1, '::1', '/demo_ics//vnadmin/logout/index', 'http://localhost:81/demo_ics/vnadmin/product/edit/95', 'logout/index', 't/index', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:39:18', 0),
(63912, 1, '::1', '/demo_ics/vnadmin/product/edit/95', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/95', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:08:26', 0),
(63911, 1, '::1', '/demo_ics/', 'http://localhost:81/demo_ics/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:42', 0),
(63910, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/edit/92', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:40', 0),
(63909, 1, '::1', '/demo_ics/vnadmin/product/edit/92', 'http://localhost:81/demo_ics/vnadmin/product/edit/92', 'product/edit', 'ct/edit/92', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:40', 0),
(63908, 1, '::1', '/demo_ics/vnadmin/alias/checkEdit', 'http://localhost:81/demo_ics/vnadmin/product/edit/92', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:40', 0),
(63907, 1, '::1', '/demo_ics/vnadmin/product/edit/92', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/92', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:36', 0),
(63906, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/edit/93', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:33', 0),
(63905, 1, '::1', '/demo_ics/vnadmin/product/edit/93', 'http://localhost:81/demo_ics/vnadmin/product/edit/93', 'product/edit', 'ct/edit/93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:32', 0),
(63904, 1, '::1', '/demo_ics/vnadmin/alias/checkEdit', 'http://localhost:81/demo_ics/vnadmin/product/edit/93', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:32', 0),
(63903, 1, '::1', '/demo_ics/vnadmin/product/edit/93', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:27', 0),
(63902, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/edit/94', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:25', 0),
(63901, 1, '::1', '/demo_ics/vnadmin/product/edit/94', 'http://localhost:81/demo_ics/vnadmin/product/edit/94', 'product/edit', 'ct/edit/94', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:25', 0),
(63900, 1, '::1', '/demo_ics/vnadmin/alias/checkEdit', 'http://localhost:81/demo_ics/vnadmin/product/edit/94', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:25', 0),
(63899, 1, '::1', '/demo_ics/vnadmin/product/edit/94', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/94', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:20', 0),
(63898, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/edit/95', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:18', 0),
(63897, 1, '::1', '/demo_ics/vnadmin/product/edit/95', 'http://localhost:81/demo_ics/vnadmin/product/edit/95', 'product/edit', 'ct/edit/95', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:18', 0),
(63896, 1, '::1', '/demo_ics/vnadmin/alias/checkEdit', 'http://localhost:81/demo_ics/vnadmin/product/edit/95', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:18', 0),
(63895, 1, '::1', '/demo_ics/vnadmin/product/edit/95', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/95', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:07:06', 0),
(63894, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 16:06:59', 0),
(63893, 1, '::1', '/demo_ics/', 'http://localhost:81/demo_ics/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:59:19', 0),
(63892, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:59:05', 0),
(63891, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:59:05', 0),
(63890, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:59:05', 0),
(63889, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:59:02', 0),
(63888, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:59', 0),
(63887, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:58', 0),
(63886, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:58', 0),
(63885, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:55', 0),
(63884, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:50', 0),
(63883, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:50', 0),
(63882, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:50', 0),
(63881, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:42', 0),
(63880, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:41', 0),
(63879, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:41', 0),
(63878, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:41', 0),
(63877, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:09', 0),
(63876, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:58:08', 0),
(63875, 1, '::1', '/demo_ics/', 'http://localhost:81/demo_ics/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:27', 0),
(63874, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/edit/92', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:26', 0),
(63873, 1, '::1', '/demo_ics/vnadmin/product/edit/92', 'http://localhost:81/demo_ics/vnadmin/product/edit/92', 'product/edit', 'ct/edit/92', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:25', 0),
(63872, 1, '::1', '/demo_ics/vnadmin/alias/checkEdit', 'http://localhost:81/demo_ics/vnadmin/product/edit/92', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:25', 0),
(63871, 1, '::1', '/demo_ics/vnadmin/product/edit/92', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/92', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:22', 0),
(63870, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/edit/93', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:19', 0),
(63869, 1, '::1', '/demo_ics/vnadmin/product/edit/93', 'http://localhost:81/demo_ics/vnadmin/product/edit/93', 'product/edit', 'ct/edit/93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:19', 0),
(63868, 1, '::1', '/demo_ics/vnadmin/alias/checkEdit', 'http://localhost:81/demo_ics/vnadmin/product/edit/93', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:19', 0),
(63867, 1, '::1', '/demo_ics/vnadmin/product/edit/93', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:14', 0),
(63866, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/edit/94', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:12', 0),
(63865, 1, '::1', '/demo_ics/vnadmin/product/edit/94', 'http://localhost:81/demo_ics/vnadmin/product/edit/94', 'product/edit', 'ct/edit/94', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:12', 0),
(63864, 1, '::1', '/demo_ics/vnadmin/alias/checkEdit', 'http://localhost:81/demo_ics/vnadmin/product/edit/94', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:12', 0),
(63863, 1, '::1', '/demo_ics/vnadmin/product/edit/94', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/94', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:08', 0),
(63862, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/edit/95', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:05', 0),
(63861, 1, '::1', '/demo_ics/vnadmin/product/edit/95', 'http://localhost:81/demo_ics/vnadmin/product/edit/95', 'product/edit', 'ct/edit/95', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:05', 0),
(63860, 1, '::1', '/demo_ics/vnadmin/alias/checkEdit', 'http://localhost:81/demo_ics/vnadmin/product/edit/95', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:57:05', 0),
(63859, 1, '::1', '/demo_ics/vnadmin/product/edit/95', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/edit', 'ct/edit/95', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:56', 0),
(63858, 1, '::1', '/demo_ics/', 'http://localhost:81/demo_ics/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:46', 0),
(63857, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/add', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:43', 0),
(63856, 1, '::1', '/demo_ics/vnadmin/product/add', 'http://localhost:81/demo_ics/vnadmin/product/add', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:43', 0),
(63855, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:42', 0),
(63854, 1, '::1', '/demo_ics/vnadmin/product/add', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:26', 0),
(63853, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/add', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:23', 0),
(63852, 1, '::1', '/demo_ics/vnadmin/product/add', 'http://localhost:81/demo_ics/vnadmin/product/add', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:23', 0),
(63851, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:23', 0),
(63850, 1, '::1', '/demo_ics/vnadmin/product/add', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:03', 0),
(63849, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/add', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:00', 0),
(63848, 1, '::1', '/demo_ics/vnadmin/product/add', 'http://localhost:81/demo_ics/vnadmin/product/add', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:00', 0),
(63847, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:56:00', 0),
(63846, 1, '::1', '/demo_ics/vnadmin/product/add', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:55:41', 0),
(63845, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/add', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:55:38', 0),
(63844, 1, '::1', '/demo_ics/vnadmin/product/add', 'http://localhost:81/demo_ics/vnadmin/product/add', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:55:38', 0),
(63843, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:55:31', 0),
(63842, 1, '::1', '/demo_ics/vnadmin/product/add', 'http://localhost:81/demo_ics/vnadmin/product/products', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:55:05', 0),
(63841, 1, '::1', '/demo_ics/vnadmin/product/products', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:46', 0),
(63840, 1, '::1', '/demo_ics/', 'http://localhost:81/demo_ics/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:18', 0),
(63839, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:16', 0),
(63838, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:16', 0),
(63837, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:16', 0),
(63836, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:12', 0),
(63835, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:09', 0),
(63834, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:09', 0),
(63833, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:08', 0),
(63832, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:54:01', 0),
(63831, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:53:59', 0),
(63830, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:53:59', 0),
(63829, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:53:59', 0),
(63828, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:53:50', 0),
(63827, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:53:49', 0),
(63826, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:53:48', 0),
(63825, 1, '::1', '/demo_ics/vnadmin/alias/checkAdd', 'http://localhost:81/demo_ics/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:53:48', 0),
(63824, 1, '::1', '/demo_ics/vnadmin/product/cat_add', 'http://localhost:81/demo_ics/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:53:16', 0),
(63823, 1, '::1', '/demo_ics/vnadmin/product/categories', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:52:45', 0),
(63822, 1, '::1', '/demo_ics/', 'http://localhost:81/demo_ics/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:33:19', 0),
(63821, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/addbanner', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:30:25', 0),
(63820, 1, '::1', '/demo_ics/vnadmin/imageupload/addbanner', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/addbanner', 'dbanner', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:30:14', 0),
(63819, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/addbanner', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:30:13', 0),
(63818, 1, '::1', '/demo_ics/vnadmin/imageupload/addbanner', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/addbanner', 'dbanner', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:30:03', 0),
(63817, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/addbanner', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:30:02', 0),
(63816, 1, '::1', '/demo_ics/vnadmin/imageupload/addbanner', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/addbanner', 'dbanner', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:54', 0),
(63815, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/edit/299', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:53', 0),
(63814, 1, '::1', '/demo_ics/vnadmin/imageupload/edit/299', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/edit', 'ad/edit/299', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:49', 0),
(63813, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/addbanner', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:46', 0),
(63812, 1, '::1', '/demo_ics/vnadmin/imageupload/addbanner', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/addbanner', 'dbanner', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:38', 0),
(63811, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/edit/296', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:36', 0),
(63810, 1, '::1', '/demo_ics/vnadmin/imageupload/edit/296', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/edit', 'ad/edit/296', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:33', 0),
(63809, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/edit/297', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:30', 0),
(63808, 1, '::1', '/demo_ics/vnadmin/imageupload/edit/297', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/edit', 'ad/edit/297', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:26', 0),
(63807, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/edit/298', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:23', 0),
(63806, 1, '::1', '/demo_ics/vnadmin/imageupload/edit/298', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/edit', 'ad/edit/298', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:18', 0),
(63805, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/addbanner', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:16', 0),
(63804, 1, '::1', '/demo_ics/vnadmin/imageupload/addbanner', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/addbanner', 'dbanner', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:07', 0),
(63803, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/addbanner', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:29:05', 0),
(63802, 1, '::1', '/demo_ics/vnadmin/imageupload/addbanner', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/addbanner', 'dbanner', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:28:52', 0),
(63801, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/imageupload/addbanner', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:28:48', 0),
(63800, 1, '::1', '/demo_ics/vnadmin/imageupload/addbanner', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/addbanner', 'dbanner', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:28:24', 0),
(63799, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/pages/pagelist', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:25:04', 0),
(63798, 1, '::1', '/demo_ics/vnadmin/imageupload/addbanner', 'http://localhost:81/demo_ics/vnadmin/imageupload/banners', 'imageupload/addbanner', 'dbanner', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:25:01', 0),
(63797, 1, '::1', '/demo_ics/vnadmin/imageupload/banners', 'http://localhost:81/demo_ics/vnadmin/pages/pagelist', 'imageupload/banners', 'banners', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:24:55', 0),
(63796, 1, '::1', '/demo_ics/vnadmin/pages/pagelist', 'http://localhost:81/demo_ics/vnadmin', 'pages/pagelist', 'agelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:24:51', 0),
(63795, 1, '::1', '/demo_ics/', 'http://localhost:81/demo_ics/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:24:09', 0),
(63794, 1, '::1', '/demo_ics/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:14:51', 0),
(63793, 1, '::1', '/demo_ics/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-10-01 15:13:20', 0),
(63792, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 15:10:12', 0),
(63791, 1, '::1', '/demo_sofa_10_5//vnadmin/logout/index', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'logout/index', 't/index', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 15:10:12', 0),
(63790, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:40:13', 0),
(63789, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/90', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:40:12', 0),
(63788, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/90', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/90', 'product/edit', 'ct/edit/90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:40:12', 0),
(63787, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkEdit', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/90', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:40:12', 0),
(63786, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/90', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/edit', 'ct/edit/90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:39:59', 0),
(63785, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:38:08', 0),
(63784, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/91', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:38:05', 0),
(63783, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/91', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/91', 'product/edit', 'ct/edit/91', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:38:05', 0),
(63782, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkEdit', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/91', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:38:04', 0),
(63781, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/91', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/edit', 'ct/edit/91', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:37:59', 0),
(63780, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/91', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:36:05', 0),
(63779, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/91', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/91', 'product/edit', 'ct/edit/91', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:36:05', 0),
(63778, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkEdit', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/91', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:36:05', 0),
(63777, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/91', '', 'product/edit', 'ct/edit/91', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:35:53', 0),
(63776, 1, '::1', '/demo_sofa_10_5/ajax/ajax/deleteimage', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/91', 'ajax/deleteimage', 'mage', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:35:48', 0),
(63775, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/91', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/edit', 'ct/edit/91', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:35:40', 0),
(63774, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:22:45', 0),
(63773, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:22:38', 0),
(63772, 1, '::1', '/demo_sofa_10_5/vnadmin/product/add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:22:38', 0),
(63771, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:22:38', 0),
(63770, 1, '::1', '/demo_sofa_10_5/vnadmin/product/add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:21:59', 0),
(63769, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/89', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:21:41', 0),
(63768, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/89', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/89', 'product/edit', 'ct/edit/89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:21:41', 0),
(63767, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkEdit', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/edit/89', 'alias/checkEdit', 'eckEdit', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:21:41', 0),
(63766, 1, '::1', '/demo_sofa_10_5/vnadmin/product/edit/89', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/edit', 'ct/edit/89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:21:17', 0),
(63765, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:21:12', 0),
(63764, 1, '::1', '/demo_sofa_10_5/vnadmin/product/add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:21:12', 0),
(63763, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:21:11', 0),
(63762, 1, '::1', '/demo_sofa_10_5/vnadmin/product/add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:36', 0),
(63761, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:34', 0),
(63760, 1, '::1', '/demo_sofa_10_5/vnadmin/product/add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:34', 0),
(63759, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:34', 0),
(63758, 1, '::1', '/demo_sofa_10_5/vnadmin/product/add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/add', 'uct/add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:09', 0),
(63757, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:07', 0),
(63756, 1, '::1', '/demo_sofa_10_5/vnadmin/product/deletes', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/deletes', 'deletes', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:07', 0),
(63755, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:04', 0),
(63754, 1, '::1', '/demo_sofa_10_5/vnadmin/product/deletes', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/deletes', 'deletes', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:04', 0),
(63753, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:01', 0),
(63752, 1, '::1', '/demo_sofa_10_5/vnadmin/product/deletes', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'product/deletes', 'deletes', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:18:01', 0),
(63751, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:17:55', 0),
(63750, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:17:45', 0),
(63749, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:10:35', 0),
(63748, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:08:44', 0),
(63746, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:38:47', 0),
(63747, 1, '::1', '/demo_sofa_10_5//vnadmin/logout/index', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'logout/index', 't/index', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 14:08:44', 0),
(63745, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:38:44', 0),
(63744, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:38:44', 0),
(63743, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:38:44', 0);
INSERT INTO `site_log` (`site_log_id`, `no_of_visits`, `ip_address`, `requested_url`, `referer_page`, `page_name`, `query_string`, `user_agent`, `is_unique`, `access_date`, `visits_count`) VALUES
(63742, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:38:23', 0),
(63741, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:30:40', 0),
(63740, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:27:04', 0),
(63739, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:27:04', 0),
(63738, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:27:04', 0),
(63737, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:50', 0),
(63736, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:46', 0),
(63735, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:46', 0),
(63734, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:46', 0),
(63733, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:27', 0),
(63732, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:24', 0),
(63731, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:24', 0),
(63730, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:20', 0),
(63729, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:14', 0),
(63728, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:10', 0),
(63727, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:10', 0),
(63726, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:26:10', 0),
(63725, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:53', 0),
(63724, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:51', 0),
(63723, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:51', 0),
(63722, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:51', 0),
(63721, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:36', 0),
(63720, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:35', 0),
(63719, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:34', 0),
(63718, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:34', 0),
(63717, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:20', 0),
(63716, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:19', 0),
(63715, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:19', 0),
(63714, 1, '::1', '/demo_sofa_10_5/vnadmin/alias/checkAdd', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'alias/checkAdd', 'heckAdd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:25:19', 0),
(63713, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:23:50', 0),
(63712, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:23:49', 0),
(63711, 1, '::1', '/demo_sofa_10_5/vnadmin/product/deletes_category', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/deletes_category', 'ategory', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:23:49', 0),
(63710, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:23:41', 0),
(63709, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-28 13:23:30', 0),
(63708, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 23:23:09', 0),
(63707, 1, '::1', '/demo_sofa_10_5//vnadmin/logout/index', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'logout/index', 't/index', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 23:23:09', 0),
(63706, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:53:09', 0),
(63705, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:53:09', 0),
(63704, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:53:03', 0),
(63703, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:53:03', 0),
(63702, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:53:01', 0),
(63701, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:53:01', 0),
(63700, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:52:59', 0),
(63699, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:52:59', 0),
(63698, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:48:55', 0),
(63697, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:48:55', 0),
(63696, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:48:54', 0),
(63695, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:48:53', 0),
(63694, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:48:52', 0),
(63693, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/cat_add', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:48:52', 0),
(63692, 1, '::1', '/demo_sofa_10_5/vnadmin/product/cat_add', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/categories', 'product/cat_add', 'cat_add', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:47:10', 0),
(63691, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:45:29', 0),
(63690, 1, '::1', '/demo_sofa_10_5/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:45:20', 0),
(63689, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:43:09', 0),
(63688, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:42:10', 0),
(63687, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:42:10', 0),
(63686, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:41:29', 0),
(63685, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:41:29', 0),
(63684, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:41:03', 0),
(63683, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:41:03', 0),
(63535, 1, '::1', '/demo_sofa_10_5/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:08:20', 0),
(63682, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:40:50', 0),
(63681, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:40:50', 0),
(63679, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:40:47', 0),
(63680, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:40:47', 0),
(63678, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:18', 0),
(63677, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:18', 0),
(63675, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:16', 0),
(63676, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:17', 0),
(63674, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:15', 0),
(63673, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:15', 0),
(63672, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:02', 0),
(63671, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:02', 0),
(63669, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:00', 0),
(63670, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:38:00', 0),
(63668, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:37:57', 0),
(63667, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:37:57', 0),
(63666, 1, '::1', '/demo_sofa_10_5/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:37:45', 0),
(63664, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:37:03', 0),
(63665, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:37:33', 0),
(63663, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:37:03', 0),
(63662, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:37:00', 0),
(63661, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:37:00', 0),
(63659, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:36:59', 0),
(63660, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:36:59', 0),
(63658, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:29:22', 0),
(63657, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:29:22', 0),
(63656, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:29:19', 0),
(63655, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:29:19', 0),
(63653, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', '', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:25:00', 0),
(63654, 1, '::1', '/demo_sofa_10_5/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:25:00', 0),
(63652, 1, '::1', '/demo_sofa_10_5/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:56', 0),
(63651, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', '', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:56', 0),
(63650, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/news/newslist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:45', 0),
(63648, 1, '::1', '/demo_sofa_10_5/vnadmin/news/newslist', 'http://localhost:81/demo_sofa_10_5/vnadmin/news/categories', 'news/newslist', 'ewslist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:42', 0),
(63649, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/news/newslist', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:45', 0),
(63647, 1, '::1', '/demo_sofa_10_5/vnadmin/news/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin/product/products', 'news/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:41', 0),
(63645, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:17', 0),
(63646, 1, '::1', '/demo_sofa_10_5/vnadmin/product/products', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/products', 'roducts', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:19', 0),
(63644, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:17', 0),
(63643, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:11', 0),
(63641, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:22:32', 0),
(63642, 1, '::1', '/demo_sofa_10_5/vnadmin/product/categories', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'product/categories', 'egories', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 22:24:11', 0),
(63640, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 09:03:15', 0),
(63638, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 08:33:17', 0),
(63639, 1, '::1', '/demo_sofa_10_5//vnadmin/logout/index', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'logout/index', 't/index', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 09:03:15', 0),
(63636, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1', 0, '2020-09-27 04:17:47', 0),
(63637, 1, '::1', '/demo_sofa_10_5/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 08:33:14', 0),
(63635, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:16:01', 0),
(63634, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:16:00', 0),
(63633, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:16:00', 0),
(63632, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:51', 0),
(63631, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:50', 0),
(63630, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:49', 0),
(63629, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:49', 0),
(63628, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:36', 0),
(63627, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:35', 0),
(63626, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:34', 0),
(63625, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:33', 0),
(63624, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/edit/146?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/edit', 'nu/edit/146', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:33', 0),
(63623, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:29', 0),
(63622, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:28', 0),
(63621, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:28', 0),
(63620, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:13', 0),
(63619, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:04', 0),
(63618, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:02', 0),
(63617, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:02', 0),
(63616, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:15:00', 0),
(63615, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:59', 0),
(63614, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:54', 0),
(63613, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:54', 0),
(63612, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:52', 0),
(63611, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:50', 0),
(63610, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:44', 0),
(63609, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:44', 0),
(63608, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:39', 0),
(63607, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:38', 0),
(63606, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:37', 0),
(63605, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:37', 0),
(63603, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:34', 0),
(63604, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:35', 0),
(63602, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:29', 0),
(63601, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:29', 0),
(63599, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:25', 0),
(63600, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:27', 0),
(63598, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:20', 0),
(63596, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:17', 0),
(63597, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:20', 0),
(63595, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:17', 0),
(63594, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:14', 0),
(63593, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:13', 0),
(63592, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:11', 0),
(63591, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:05', 0),
(63590, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:04', 0),
(63589, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:03', 0),
(63587, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:58', 0),
(63588, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:14:01', 0),
(63586, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:56', 0),
(63584, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:54', 0),
(63585, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:56', 0),
(63583, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:51', 0),
(63582, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:50', 0),
(63581, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:50', 0),
(63580, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:46', 0),
(63579, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/edit/112?p=main', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/edit', 'nu/edit/112', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:46', 0),
(63578, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:44', 0),
(63577, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:43', 0),
(63576, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:43', 0),
(63575, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:41', 0),
(63574, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/edit/134?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/edit', 'nu/edit/134', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:41', 0),
(63573, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:39', 0),
(63572, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:35', 0),
(63571, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:35', 0),
(63570, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:31', 0);
INSERT INTO `site_log` (`site_log_id`, `no_of_visits`, `ip_address`, `requested_url`, `referer_page`, `page_name`, `query_string`, `user_agent`, `is_unique`, `access_date`, `visits_count`) VALUES
(63569, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:30', 0),
(63568, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:29', 0),
(63567, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:29', 0),
(63566, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:23', 0),
(63565, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:22', 0),
(63564, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:20', 0),
(63563, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:20', 0),
(63562, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/addmenu?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/addmenu', 'addmenu', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:13', 0),
(63561, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:12', 0),
(63560, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:12', 0),
(63559, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/delete/46', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/delete', '/delete/46', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:12', 0),
(63558, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:10', 0),
(63556, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/delete/47', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/delete', '/delete/47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:10', 0),
(63557, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:10', 0),
(63555, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:07', 0),
(63554, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:07', 0),
(63553, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/delete/48', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/delete', '/delete/48', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:07', 0),
(63552, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:04', 0),
(63550, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:01', 0),
(63551, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:03', 0),
(63549, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/edit/48?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/edit', 'nu/edit/48', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:13:01', 0),
(63548, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:59', 0),
(63547, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:59', 0),
(63546, 1, '::1', '/demo_sofa_10_5/vnadmin', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:51', 0),
(63545, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/edit/48?p=top', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/edit', 'nu/edit/48', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:51', 0),
(63544, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:42', 0),
(63543, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:38', 0),
(63542, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin/admin/setup_product', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:38', 0),
(63541, 1, '::1', '/demo_sofa_10_5/ajax/ajax/update_value', 'http://localhost:81/demo_sofa_10_5/vnadmin/admin/setup_product', 'ajax/update_value', 'alue', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:33', 0),
(63540, 1, '::1', '/demo_sofa_10_5/ajax/ajax/update_fill', 'http://localhost:81/demo_sofa_10_5/vnadmin/admin/setup_product', 'ajax/update_fill', 'fill', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:30', 0),
(63539, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/active_tab', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:16', 0),
(63537, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/menulist', 'http://localhost:81/demo_sofa_10_5/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:13', 0),
(63538, 1, '::1', '/demo_sofa_10_5/vnadmin/menu/savelist', 'http://localhost:81/demo_sofa_10_5/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:12:13', 0),
(63536, 1, '::1', '/demo_sofa_10_5/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 0, '2020-09-27 04:08:33', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_option`
--

CREATE TABLE `site_option` (
  `id` int(11) NOT NULL,
  `coppy_right` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `site_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `slogan` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `link_instagram` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `site_logo` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `site_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name_language` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `site_keyword` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_keyword_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link_sky` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link_printer` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `link_linkedin` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `site_email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `site_fanpage` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_video` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `WM_text` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `WM_color` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `WM_size` int(10) DEFAULT NULL,
  `hotline1` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `hotline2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `hotline3` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8 DEFAULT NULL,
  `link_tt` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `favicon` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `company_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `shipping` text CHARACTER SET utf8 DEFAULT NULL,
  `site_promo` text CHARACTER SET utf8 DEFAULT NULL,
  `thanhtoan_tienmat` text CHARACTER SET utf8 DEFAULT NULL,
  `thanhtoan_chuyenkhoan` text CHARACTER SET utf8 DEFAULT NULL,
  `hdfMap` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `map_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `map_adrdress` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `map_phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `dia_chi_timkiem` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT '1',
  `link_gg` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_youtube` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `face_id` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timeopen` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `chat` text CHARACTER SET utf8 DEFAULT NULL,
  `site_logo_footer` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `map_iframe` text CHARACTER SET utf8 DEFAULT NULL,
  `input_text_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_footer` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `config_pro` text COLLATE utf8_unicode_ci NOT NULL,
  `config_pro_content` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(3) UNSIGNED DEFAULT 1,
  `bando` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `watermark` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_option`
--

INSERT INTO `site_option` (`id`, `coppy_right`, `site_name`, `slogan`, `link_instagram`, `site_logo`, `site_title`, `name_language`, `site_keyword`, `site_keyword_en`, `site_description`, `link_sky`, `link_printer`, `link_linkedin`, `site_email`, `site_fanpage`, `site_video`, `WM_text`, `WM_color`, `WM_size`, `hotline1`, `hotline2`, `hotline3`, `address`, `link_tt`, `favicon`, `company_name`, `shipping`, `site_promo`, `thanhtoan_tienmat`, `thanhtoan_chuyenkhoan`, `hdfMap`, `map_title`, `map_adrdress`, `map_phone`, `dia_chi_timkiem`, `lang`, `link_gg`, `link_youtube`, `face_id`, `timeopen`, `chat`, `site_logo_footer`, `map_iframe`, `input_text_1`, `domain`, `map_footer`, `icon_language`, `config_pro`, `config_pro_content`, `active`, `bando`, `watermark`) VALUES
(1, NULL, 'CÔNG TY TNHH THƯƠNG MẠI ICS', 'Sự hài lòng của khách hàng là niềm vui của chúng tôi', 'pqcuong1995', 'upload/img/logo/logo7.png', 'CÔNG TY TNHH THƯƠNG MẠI ICS', 'Việt Nam', '', '0', '', '', NULL, NULL, 'tranlinh.manh@gmail.com', 'BaoLongDesigner', 'lU02XJYXgK8', NULL, NULL, NULL, '0963761678', '0972050202', NULL, ' Địa chỉ: 130 Hoàng Công Chất, P. Phú Diễn, Q. Bắc Từ Liêm, Tp. Hà Nội', '', 'upload/img/logo/logo8.png', NULL, '<p>&nbsp;</p>\r\n\r\n<p>Tế b&agrave;o gốc l&agrave; tế b&agrave;o mầm hay tế b&agrave;o nền m&oacute;ng m&agrave; từ đ&oacute; c&aacute;c loại tế b&agrave;o của cơ thể con người được tạo ra. Mọi tế b&agrave;o trong cơ thể người đều được tạo ra từ tế b&agrave;o nền m&oacute;ng m&agrave; từ đ&oacute; c&aacute;c loại tế b&agrave;o của cơ thể con người được tạo ra.</p>\r\n', NULL, NULL, NULL, '(,)', 'Công ty nội thất Bảo Lâm', '130 Hoàng Công Chất, Cầu Diễn, Bắc Từ Liêm, Hà Nội', '0963761678', '130 Hoàng Công Chất, Cầu Diễn, Bắc Từ Liêm, Hà Nội', 'vi', '', NULL, NULL, NULL, NULL, 'upload/img/logo/logo1.png', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.4672661068253!2d105.76712161501895!3d21.013981586006356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3134535816bfae5d%3A0xc352638ab10225d4!2zQ-G7lSBwaOG6p24gQ8O0bmcgbmdo4buHIFFUUw!5e0!3m2!1svi!2s!4v1529901490962\" width=\"100%\" height=\"200\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', NULL, '', NULL, 'img/vi.gif', '[]', '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', 1, NULL, '0'),
(2, NULL, 'JSC polygon media', '', NULL, 'upload/img/logo4.png', '', 'English', '', '0', '', '0', NULL, NULL, 'hanhnh@polygonmedia.vn', '', 'uI2wcf05wq0', '', '', 0, '', '', '0', '', '', '0', NULL, '', '', NULL, NULL, '(21.0218044, 105.79087200000004)', 'Công ty', '', '', 'Yên hòa', 'en', '', '', '', '', '', NULL, '', NULL, '', '', 'img/en.gif', '[]', '', 1, NULL, '0'),
(3, '0', '1', '1', '1', '1', '1', '', '1', '1', '1', '1', '0', '0', '1', '1', '1', '0', '0', 0, '1', '1', '0', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', 'config', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '', '1', 0, NULL, '0'),
(4, 'Coppy right', 'tên đơn vị', 'Slogan', 'Zalo', NULL, 'Tiêu đề website', NULL, NULL, NULL, NULL, 'Link skype', 'link printer', 'Link linkedin', NULL, 'Fanpage Facebook', 'Video (Youtube)', 'Chữ Nổi Warter Mark', 'Màu Chữ (Hex Color VD : #ed1c2', 1, 'Hotline', 'Hotline 2', 'điên thoại bàn', 'Địa chỉ', 'Link twitter', NULL, NULL, 'Thông tin chân trang', 'khuyến mại', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'conf_text', 'Link google', 'Link youtube', 'id ap facebook', 'Thời gian mở cửa', 'mã chát online', 'logo chân trang', 'Mã nhúng bản đồ chân trang', 'padding text', 'Tên miền', 'mã nhúng javascript', '', '', '', 0, NULL, 'Đóng dấu logo'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, 'japanese', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ja', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', '', 'upload/img/logo/ja4.jpg', '[]', '', 0, NULL, '0'),
(6, NULL, NULL, NULL, NULL, NULL, NULL, 'Korean', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ko', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', '', 'upload/img/logo/lag21.png', '[]', '', 0, NULL, '0'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, 'hungary', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hu', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', '', 'upload/img/logo/hungary.png', '[]', '', 0, NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `staticpage`
--

CREATE TABLE `staticpage` (
  `id` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `home` tinyint(1) DEFAULT 0,
  `hot` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `contact_page` tinyint(1) DEFAULT 0,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT 0,
  `page_footer` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staticpage`
--

INSERT INTO `staticpage` (`id`, `name`, `alias`, `description`, `content`, `image`, `lang`, `home`, `hot`, `focus`, `contact_page`, `title_seo`, `keyword_seo`, `description_seo`, `parent_id`, `page_footer`) VALUES
(31, 'Giới Thiệu', 'gioi-thieu', '<p style=\"text-align:justify\">Lấy cảm hứng từ nguồn năng lượng &aacute;nh s&aacute;ng của vũ trụ. BẢO LONG đ&atilde; thổi v&agrave;o c&aacute;c thiết kế sự huyền diệu, lung linh bằng hệ thống LED,MOVING hiện đại, kỹ xảo ưu việt, cho thượng đế cảm gi&aacute;c mỗi kh&aacute;n ph&ograve;ng như l&agrave; 1 s&acirc;n khấu &aacute;nh s&aacute;ng thực sự.&nbsp;</p>\r\n', '<p>Lấy cảm hứng từ nguồn năng lượng &aacute;nh s&aacute;ng của vũ trụ. BẢO LONG đ&atilde; thổi v&agrave;o c&aacute;c thiết kế sự huyền diệu, lung linh bằng hệ thống LED,MOVING hiện đại, kỹ xảo ưu việt, cho thượng đế cảm gi&aacute;c mỗi kh&aacute;n ph&ograve;ng như l&agrave; 1 s&acirc;n khấu &aacute;nh s&aacute;ng thực sự.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>L&agrave; một trong những đơn vị h&agrave;ng đầu trong ngh&agrave;nh&nbsp;SẢN XUẤT, GIA C&Ocirc;NG SẢN PHẨM INOX&nbsp;về quy m&ocirc;, nh&acirc;n sự, trang thiết bị m&aacute;y m&oacute;c hiện đại. Inox Bảo Long c&oacute; khả năng đ&aacute;p ứng đa dạng nhu cầu của kh&aacute;ch h&agrave;ng, tại Tp. Hồ Ch&iacute; Minh v&agrave; c&aacute;c tỉnh l&acirc;n cận khu vực ph&iacute;a Nam!&nbsp;<br />\r\nSản phẩm ti&ecirc;u biểu:<br />\r\n✎ B&agrave;n ghế inox<br />\r\n✎ Thiết bị bếp c&ocirc;ng nghiệp inox<br />\r\n✎ Thiết bị y tế inox<br />\r\n✎ Đồ gia dụng inox,..<br />\r\nVới cam kết: Thẩm mỹ đẹp, độ bền cao, gi&aacute; th&agrave;nh tốt.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\nĐặc biệt, ch&uacute;ng t&ocirc;i&nbsp;Nhận Gia C&ocirc;ng Sản Phẩm Inox Theo Y&ecirc;u Cầu.<br />\r\nH&atilde;y đến với Inox Bảo Long để sở hữu những sản phẩm chất lượng!</p>\r\n', 'upload/img/pages/1.jpg', 'vi', 1, 1, 1, 0, '', '', '', 0, 1),
(40, 'Công nghệ thầm mỹ', 'cong-nghe-tham-my', '<p>Ti&ecirc;n tiến h&agrave;ng đầu</p>\r\n', '', 'upload/img/pages/gt.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(42, 'Dịch vụ khách hàng', 'dich-vu-khach-hang', '<p>Ch&uacute;ng t&ocirc;i sử dụng c&aacute;c c&ocirc;ng nghệ in ấn v&agrave; th&aacute;nh phẩm ti&ecirc;n tiến nhất Việt Nam để cho ra sản phẩm chất lượng&nbsp;</p>\r\n', '', 'upload/img/pages/img_dv_kh.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(43, 'In mẫu miễn phí', 'in-mau-mien-phi', '<p>Ch&uacute;ng t&ocirc;i sử dụng c&aacute;c c&ocirc;ng nghệ in ấn v&agrave; th&aacute;nh phẩm ti&ecirc;n tiến nhất Việt Nam để cho ra sản phẩm chất lượng&nbsp;</p>\r\n', '', 'upload/img/pages/img_im_mp.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(44, 'Giao hàng tận nơi', 'giao-hang-tan-noi', '<p>Ch&uacute;ng t&ocirc;i sử dụng c&aacute;c c&ocirc;ng nghệ in ấn v&agrave; th&aacute;nh phẩm ti&ecirc;n tiến nhất Việt Nam để cho ra sản phẩm chất lượng&nbsp;</p>\r\n', '', 'upload/img/pages/img_gh_tn.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(45, 'Giá luôn ổn định', 'gia-luon-on-dinh', '<p>Ch&uacute;ng t&ocirc;i sử dụng c&aacute;c c&ocirc;ng nghệ in ấn v&agrave; th&aacute;nh phẩm ti&ecirc;n tiến nhất Việt Nam để cho ra sản phẩm chất lượng&nbsp;</p>\r\n', '', 'upload/img/pages/img_gh_tn1.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(47, 'Lịch sử hình thành', 'lich-su-hinh-thanh', '', '', 'upload/img/pages/img_box.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `street`
--

CREATE TABLE `street` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `pre` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `districtid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `support_online`
--

CREATE TABLE `support_online` (
  `id` int(11) NOT NULL,
  `yahoo` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `skype` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `image` varchar(70) CHARACTER SET utf8 DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `support_online`
--

INSERT INTO `support_online` (`id`, `yahoo`, `phone`, `skype`, `email`, `name`, `active`, `image`, `type`, `address`) VALUES
(19, 'https://id.zalo.me/account/login?continue=https%3A', '0963.671.678', '', 'tranlinh.manh@gmail.com', 'Mr. LINH', 1, NULL, 2, '130 Hoàng Công Chất, P. Phú Diễn, Q. Bắc Từ Liêm, Tp. Hà Nội'),
(22, '', '0972.050.202', '', 'sofabaolam1@gmail.com', 'Ms. Gấm', 1, NULL, 0, '130 Hoàng Công Chất, Từ Liêm Hà Nội');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `alias`, `lang`, `time`, `title_seo`, `keyword_seo`, `description_seo`, `sort`) VALUES
(1, 'manh', 'manh', 'vi', 1526529820, NULL, NULL, NULL, 0),
(2, 'tuyen', 'tuyen', 'vi', 1526529820, NULL, NULL, NULL, 0),
(3, 'tin tức', 'tin-tuc', 'vi', 1526530190, '', '', '', 1),
(4, 'Kem bb', 'kem-bb', 'vi', 1526530223, '', '', '', 2),
(5, 'kem', 'kem', 'vi', 1526530670, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tags_news`
--

CREATE TABLE `tags_news` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags_to_news`
--

CREATE TABLE `tags_to_news` (
  `id` int(11) NOT NULL,
  `id_raovat` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags_to_product`
--

CREATE TABLE `tags_to_product` (
  `id` int(11) NOT NULL,
  `id_tags` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags_to_product`
--

INSERT INTO `tags_to_product` (`id`, `id_tags`, `id_product`) VALUES
(1, 1, 101),
(2, 2, 101),
(3, 5, 100),
(4, 4, 100),
(5, 3, 100),
(6, 1, 100),
(7, 2, 100);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_xnt`
--

CREATE TABLE `tbl_xnt` (
  `id` int(11) NOT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` int(11) DEFAULT NULL,
  `mahh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sltd` int(11) DEFAULT NULL COMMENT 'Số lượng tồn đầu ngày',
  `sln` int(11) DEFAULT NULL COMMENT 'Số lượng hàng nhập trong ngày',
  `slx` int(11) DEFAULT NULL COMMENT 'Số lượng hàng xuất trong ngày',
  `sltc` int(11) DEFAULT NULL COMMENT 'Số lượng hàng tồn cuối ngày',
  `sltt` int(11) DEFAULT NULL COMMENT 'Số lượng hàng tồn tối thiểu',
  `product_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL COMMENT 'Giá Hiện Tại',
  `price_export` int(11) DEFAULT NULL COMMENT 'Giá Xuất Hàng',
  `price_import` int(11) DEFAULT NULL COMMENT 'Giá Nhập Hàng'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_xnt`
--

INSERT INTO `tbl_xnt` (`id`, `date`, `date_time`, `mahh`, `sltd`, `sln`, `slx`, `sltc`, `sltt`, `product_id`, `price`, `price_export`, `price_import`) VALUES
(356, '1526490000', 1526551961, NULL, 0, 0, 1, 0, 1, 97, 129000, NULL, NULL),
(357, '1526490000', 1526551961, NULL, 0, 0, 1, 0, 1, 95, 229000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `thong_ke_online`
--

CREATE TABLE `thong_ke_online` (
  `id` int(11) NOT NULL,
  `access_date` int(11) NOT NULL,
  `today` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thong_ke_online`
--

INSERT INTO `thong_ke_online` (`id`, `access_date`, `today`) VALUES
(37, 1517850000, 0),
(38, 1517936400, 1),
(39, 1518022800, 29),
(40, 1518109200, 20),
(41, 1519578000, 9),
(42, 1519664400, 15),
(43, 1519750800, 5),
(44, 1521046800, 55),
(45, 1521133200, 9),
(46, 1521219600, 233),
(47, 1526317200, 332),
(48, 1526403600, 258),
(49, 1526490000, 541),
(50, 1526835600, 251),
(51, 1526922000, 245),
(52, 1527008400, 95),
(53, 1527094800, 114),
(54, 1527181200, 51),
(55, 1527267600, 378),
(56, 1527440400, 265),
(57, 1527699600, 427),
(58, 1527786000, 20),
(59, 1528045200, 41),
(60, 1528131600, 43),
(61, 1528218000, 120),
(62, 1528390800, 182),
(63, 1528477200, 37),
(64, 1528736400, 336),
(65, 1528822800, 128),
(66, 1528995600, 435),
(67, 1529341200, 61),
(68, 1529427600, 161),
(69, 1529600400, 80),
(70, 1529686800, 64),
(71, 1529859600, 3391),
(72, 1529946000, 1457),
(73, 1530032400, 746),
(74, 1530118800, 522),
(75, 1530205200, 792),
(76, 1530464400, 800),
(77, 1530550800, 603),
(78, 1530637200, 519),
(79, 1530723600, 95),
(80, 1530810000, 29),
(81, 1530896400, 217),
(82, 1531069200, 83),
(83, 1531155600, 8),
(84, 1531242000, 6),
(85, 1531328400, 5),
(86, 1531414800, 113),
(87, 1531760400, 40),
(88, 1531846800, 10),
(89, 1531933200, 60),
(90, 1532019600, 69),
(91, 1532106000, 299),
(92, 1532278800, 346),
(93, 1532365200, 555),
(94, 1532451600, 122),
(95, 1532538000, 219),
(96, 1532624400, 41),
(97, 1532883600, 463),
(98, 1532970000, 172),
(99, 1533056400, 96),
(100, 1533142800, 46),
(101, 1533229200, 8),
(102, 1533315600, 142),
(103, 1533488400, 567),
(104, 1533574800, 24),
(105, 1533661200, 851),
(106, 1533747600, 460),
(107, 1533834000, 217),
(108, 1534179600, 276),
(109, 1534266000, 389),
(110, 1534352400, 68),
(111, 1534784400, 18),
(112, 1534870800, 64),
(113, 1534957200, 6),
(114, 1535302800, 9),
(115, 1535389200, 36),
(116, 1535475600, 21),
(117, 1535562000, 718),
(118, 1535994000, 49),
(119, 1536166800, 8),
(120, 1536253200, 26),
(121, 1536339600, 166),
(122, 1536512400, 40),
(123, 1536685200, 185),
(124, 1536771600, 11),
(125, 1536858000, 74),
(126, 1536944400, 9),
(127, 1537117200, 17),
(128, 1537203600, 99),
(129, 1537290000, 202),
(130, 1537376400, 22),
(131, 1537549200, 31),
(132, 1537722000, 9),
(133, 1537894800, 42),
(134, 1538067600, 0),
(135, 1538154000, 0),
(136, 1538413200, 21),
(137, 1538499600, 204),
(138, 1538672400, 237),
(139, 1538931600, 4),
(140, 1539104400, 0),
(141, 1539190800, 0),
(142, 1539277200, 1),
(143, 1539363600, 1),
(144, 1539709200, 0),
(145, 1539882000, 0),
(146, 1540141200, 0),
(147, 1540227600, 8),
(148, 1540314000, 0),
(149, 1540573200, 0),
(150, 1540832400, 127),
(151, 1540918800, 74),
(152, 1541005200, 0),
(153, 1199120400, 407),
(154, 1199552400, 823),
(155, 1199638800, 46),
(156, 1199725200, 127),
(157, 1541782800, 102),
(158, 1542042000, 1),
(159, 1542128400, 13),
(160, 1542646800, 180),
(161, 1543338000, 60),
(162, 1543424400, 62),
(163, 1548694800, 18),
(164, 1550422800, 66),
(165, 1550509200, 118),
(166, 1550595600, 596),
(167, 1550682000, 2096),
(168, 1550768400, 1666),
(169, 1551027600, 2011),
(170, 1551114000, 60),
(171, 1551286800, 16),
(172, 1551978000, 238),
(173, 1552237200, 74),
(174, 1554138000, 9),
(175, 1555347600, 102),
(176, 1555434000, 8),
(177, 1556125200, 4),
(178, 1556730000, 10),
(179, 1557680400, 71),
(180, 1557853200, 10),
(181, 1558026000, 109),
(182, 1558890000, 1282),
(183, 1558976400, 1428),
(184, 1559062800, 807),
(185, 1559149200, 203),
(186, 1559235600, 99),
(187, 1559494800, 2),
(188, 1559581200, 189),
(189, 1559840400, 84),
(190, 1559926800, 492),
(191, 1560099600, 387),
(192, 1560186000, 214),
(193, 1560358800, 162),
(194, 1560445200, 8),
(195, 1560531600, 239),
(196, 1560704400, 621),
(197, 1560790800, 124),
(198, 1560877200, 73),
(199, 1560963600, 555),
(200, 1561050000, 20),
(201, 1562173200, 3),
(202, 1563814800, 3),
(203, 1600275600, 2),
(204, 1600880400, 1),
(205, 1600966800, 18),
(206, 1601053200, 46),
(207, 1601139600, 103),
(208, 1601226000, 110),
(209, 1601485200, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `md5_id` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `username` varchar(35) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(35) CHARACTER SET utf8 NOT NULL,
  `fullname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `use_salt` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `shop_name` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `avt_dir` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `avatar` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `use_logo` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `block` tinyint(3) UNSIGNED DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` int(1) UNSIGNED DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address_province` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `address_district` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `address_ward` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `use_mobile` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `use_face` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `use_yahoo` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `use_skype` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `use_group` int(3) UNSIGNED DEFAULT NULL,
  `active` int(1) UNSIGNED DEFAULT NULL,
  `use_key` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `smskey` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `token` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `deleted` tinyint(3) UNSIGNED DEFAULT NULL,
  `use_regisdate` int(11) UNSIGNED DEFAULT NULL,
  `use_enddate` int(11) UNSIGNED DEFAULT NULL,
  `lastest_login` int(11) UNSIGNED DEFAULT NULL,
  `signup_date` int(11) DEFAULT NULL,
  `lever` tinyint(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `md5_id`, `username`, `phone`, `email`, `password`, `fullname`, `use_salt`, `shop_name`, `avt_dir`, `avatar`, `use_logo`, `block`, `birthday`, `sex`, `address`, `address_province`, `address_district`, `address_ward`, `use_mobile`, `use_face`, `use_yahoo`, `use_skype`, `use_group`, `active`, `use_key`, `smskey`, `token`, `deleted`, `use_regisdate`, `use_enddate`, `lastest_login`, `signup_date`, `lever`) VALUES
(2, NULL, 'admin', 'admin', 'daibkz@gmail.com', 'c26be8aaf53b15054896983b43eb6a65', 'Admin', 'Wm8KT06E', NULL, NULL, NULL, NULL, NULL, '0000-00-00', 1, 'Ninh Binh', '66', NULL, NULL, '0986839102', NULL, 'dainguyen', '', 4, 1, '9671508f22c9982fbac60ffc130f9b7811ec2b4d7f6e9f253679a3b950a3f5c8', NULL, NULL, NULL, 1498496400, 1814029200, 1540432578, NULL, 3),
(617, '5d44ee6f2c3f71b73125876103c8f6c4', 'taikhoan', '01649962597', 'cauhai.1297@gmail.com', 'ab77a83b110f3517f746938bf49d0ae3', 'Nguyễn Văn Hải', NULL, NULL, '04072017', '986bc2226881542276ecf99e72443fc7.jpg', NULL, 0, NULL, NULL, 'Số 38 - Đường Dương Khuê ', '01', '005', '00163', NULL, NULL, NULL, NULL, NULL, 1, NULL, '595ae9294eb32', '2d9228de1d6c18ad3ab56b2a0c6d2def', 0, 1499130153, NULL, 1500969769, NULL, 0),
(620, 'b73dfe25b4b8714c029b37a6ad3006fa', 'taikhoan', '0986126561', 'hungvu258@gmail.com', 'a9f1ea798b9bcdcf0573dad7af97cbe0', 'Vũ Văn Hùng', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '595f3520d9e2e', '86054560b15b889346283a906596eaa6', 0, 1499411744, NULL, 1499411806, NULL, 0),
(612, 'f76a89f0cb91bc419542ce9fa43902dc', 'ĐẶNG VĂN ĐIỀN', '0965986385', 'cauvan1995@gmail.com', 'c26be8aaf53b15054896983b43eb6a65', 'ĐẶNG VĂN ĐIỀN', '-h01K8w3', NULL, '03072017', 'ad29f13d8e28e7cabeaf257192385ba6.png', NULL, 0, NULL, 1, 'Số 36 Dương Khuê', '01', '005', '00163', NULL, NULL, NULL, NULL, 4, 1, 'c51519f1ba3de1da58ef5bd2850861e5bf233a4b55eec27fdef32357a98b7205', '5954b39739ebb', '36fb0bab89277945551398212d0c1d8e', 0, 1499619600, 2067613200, 1504604112, 2017, 1),
(619, 'cdc0d6e63aa8e41c89689f54970bb35f', 'taikhoan', '0985088848', 'ngoc.dbsk@gmail.com', 'acb4798109c61257851f53f7521d8a4f', 'Đỗ Thị Ngọc', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '595c4381c1481', 'cd3c498d71a8889eebe96ed5946df7a3', 0, 1499218817, NULL, 1499503366, NULL, 0),
(616, '7750ca3559e5b8e1f44210283368fc16', 'taikhoan', '0915460000', 'ktviet.com.vn@gmail.com', '6140c8871dd9df0c091760c83d3562a7', 'Kỹ thuật việt', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Số 38 Đường Dương Khuê ', '01', '005', '00163', NULL, NULL, NULL, NULL, NULL, 1, NULL, '595a22e73caf4', 'd04eedd402adbee246d22bd05a16d82f', 0, 1499079399, NULL, 1501031009, NULL, 0),
(621, '85fc37b18c57097425b52fc7afbb6969', 'taikhoan', '0987999947', 'ktviet.com.vn@gmail.com', '6140c8871dd9df0c091760c83d3562a7', 'aalo.vn', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '5960999273327', '9652a76f8510d397d571651a98234986', 0, 1499502994, NULL, 1500945384, NULL, 0),
(622, '3871bd64012152bfb53fdf04b401193f', 'taikhoan', '0869118060', 'Sales@maytinhtruongson.com.vn', '29ac98cd17193f4ce1fe80017bff7cb8', 'Phan Văn Trường', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '59638b308df68', 'f082409b697ee95fbd373f4078ade2e3', 0, 1499695920, NULL, NULL, NULL, 0),
(623, 'a733fa9b25f33689e2adbe72199f0e62', 'taikhoan', '0983003484', 'cunhue@gmail.com', '3c31d5cf8058f39ef8ed267658fcae11', 'Nguyễn Trọng Hiền', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '59661988955c0', 'd89f5465c4496ea3cfe6a7f3b57c365a', 0, 1499863432, NULL, 1499863576, NULL, 0),
(629, '051e4e127b92f5d98d3c79b195f2b291', 'taikhoan', '0975279573', 'vietbk193@gmail.com', 'f1160b722eceefca344715db03d1c66b', 'Ma Thế Việt', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '5972f6b2ed53b', '4cd25c877db0de884d1dcf85f1211fc6', 0, 1500706482, NULL, 1500706576, NULL, 0),
(628, '42e77b63637ab381e8be5f8318cc28a2', 'taikhoan', '0964278201', 'nguyenvantrisahara@gmail.com', 'ef9468922149cf75765bab2d348d64aa', 'Nguyễn Văn Trí', NULL, NULL, '21072017', '6c92927ea9071ce920efcc34f6f732c2.jpg', NULL, 0, NULL, NULL, '52 Đường Lê Quang Đạo Quận Nam Từ Liêm', '01', '019', '00592', NULL, NULL, NULL, NULL, NULL, 0, NULL, '5969ae9b73f4e', '878cbe26fbc949c65aaf15d3ba3019b9', 0, 1500098203, NULL, 1500686349, NULL, 0),
(638, '4c27cea8526af8cfee3be5e183ac9605', 'taikhoan', '0982255552', 'buivananh.th@gmail.com', '01b080fe7398c4c669be0be9cd78792d', 'Vân', '9SZDFmt3', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '09c7375321d2ce9a405e4c1606850ccdb7413aed9db60ec941a374a31c42f129', NULL, '553048f16cca9be3bbd6cf0ea897dd39', NULL, 1505926800, NULL, 1506331171, NULL, 0),
(639, '0f96613235062963ccde717b18f97592', 'taikhoan', '0982255552', 'Van@gmail.com', 'c26be8aaf53b15054896983b43eb6a65', 'Vân anh', 'S3phkf4r', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '6760ca72cfe94cd737b7a804b6f415f2d28ed2339429656e2fb086e47312517d', NULL, 'aec76ec422606554a14edd7ff28cee3f', NULL, 1505926800, NULL, NULL, NULL, 0),
(646, NULL, 'huyen1', '0384932462', 'danghuyen6297@gmail.com', 'c26be8aaf53b15054896983b43eb6a65', 'Đặng Thị Huyền', 'aH0rJ8uE', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, '7d4da39237a0759ce86dc9e59c2010ace331ce980fb3dead5233a072c7105417', NULL, '3ca0ab12f6a1a634fa7f53cea8a91ff5', NULL, 1559149200, 1559149200, 1559149200, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_sms`
--

CREATE TABLE `user_sms` (
  `id` int(11) NOT NULL,
  `smsid` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 DEFAULT NULL,
  `result` int(11) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `error` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `comment` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `create_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_sms`
--

INSERT INTO `user_sms` (`id`, `smsid`, `userid`, `phone`, `content`, `result`, `count`, `error`, `comment`, `create_at`) VALUES
(5, '1130b1db-ffcb-477b-862b-040e60174a888', 76, '0974901590', 'Mã Kích Hoạt SMS : 5950ac70440c6', 100, 0, NULL, NULL, '2017-06-26 13:40:48'),
(6, '3141f19d-e25d-46fb-9cff-9c1cdd3371fb8', 76, '0974901590', 'abc test gửi lại', 100, 0, NULL, 'gửi lại', '2017-06-26 13:41:39'),
(7, NULL, 610, '0986839102', 'Mã Kích Hoạt SMS : 5954a8af5779f', 103, 0, 'Balance not enough to send message', NULL, '2017-06-29 14:13:53'),
(8, NULL, 611, '0986839102', 'Mã Kích Hoạt SMS : 5954a9ed7f497', 103, 0, 'Balance not enough to send message', NULL, '2017-06-29 14:19:09'),
(9, NULL, 612, '0965986385', 'Mã Kích Hoạt SMS : 5954b39739ebb', 103, 0, 'Balance not enough to send message', NULL, '2017-06-29 15:00:23'),
(10, NULL, 613, '01649962597', 'Mã Kích Hoạt SMS : 5955bbaedda8d', 103, 0, 'Balance not enough to send message', NULL, '2017-06-30 09:47:11'),
(11, NULL, 614, '987654321', 'Mã Kích Hoạt SMS : 595606e747183', 103, 0, 'Balance not enough to send message', NULL, '2017-06-30 15:08:07'),
(12, NULL, 615, '324234234', 'Mã Kích Hoạt SMS : 5956074367a46', 99, 0, 'Phone not valid:324234234', NULL, '2017-06-30 15:09:39'),
(13, NULL, 616, '0915460000', 'Mã Kích Hoạt SMS : 595a22e73caf4', 103, 0, 'Balance not enough to send message', NULL, '2017-07-03 17:56:39'),
(14, NULL, 617, '01649962597', 'Mã Kích Hoạt SMS : 595ae9294eb32', 103, 0, 'Balance not enough to send message', NULL, '2017-07-04 08:02:33'),
(15, NULL, 618, '0985088848', 'Mã Kích Hoạt SMS : 595b3b0287471', 103, 0, 'Balance not enough to send message', NULL, '2017-07-04 13:51:46'),
(16, NULL, 619, '0985088848', 'Mã Kích Hoạt SMS : 595c4381c1481', 103, 0, 'Balance not enough to send message', NULL, '2017-07-05 08:40:19'),
(17, NULL, 620, '0986126561', 'Mã Kích Hoạt SMS : 595f3520d9e2e', 103, 0, 'Balance not enough to send message', NULL, '2017-07-07 14:15:45'),
(18, NULL, 621, '0987999947', 'Mã Kích Hoạt SMS : 5960999273327', 103, 0, 'Balance not enough to send message', NULL, '2017-07-08 15:36:34'),
(19, NULL, 622, '0869118060', 'Mã Kích Hoạt SMS : 59638b308df68', 103, 0, 'Balance not enough to send message', NULL, '2017-07-10 21:12:00'),
(20, NULL, 623, '0983003484', 'Mã Kích Hoạt SMS : 59661988955c0', 103, 0, 'Balance not enough to send message', NULL, '2017-07-12 19:43:52'),
(21, NULL, 624, '01652724972', 'Mã Kích Hoạt SMS : 5966e56f21617', 103, 0, 'Balance not enough to send message', NULL, '2017-07-13 10:13:51'),
(22, NULL, 625, '09164278201', 'Mã Kích Hoạt SMS : 59697ab70dbfb', 99, 0, 'Phone not valid:09164278201', NULL, '2017-07-15 09:15:19'),
(23, NULL, 626, '0964278201', 'Mã Kích Hoạt SMS : 59697b7e356e4', 103, 0, 'Balance not enough to send message', NULL, '2017-07-15 09:18:38'),
(24, NULL, 627, '09642728201', 'Mã Kích Hoạt SMS : 59697cba3fe16', 99, 0, 'Phone not valid:09642728201', NULL, '2017-07-15 09:23:54'),
(25, NULL, 628, '0964278201', 'Mã Kích Hoạt SMS : 5969ae9b73f4e', 103, 0, 'Balance not enough to send message', NULL, '2017-07-15 12:56:43'),
(26, NULL, 629, '0975279573', 'Mã Kích Hoạt SMS : 5972f6b2ed53b', 103, 0, 'Balance not enough to send message', NULL, '2017-07-22 13:54:43'),
(27, NULL, 630, '01648464081', 'Mã Kích Hoạt SMS : 5974f19ddd13a', 103, 0, 'Balance not enough to send message', NULL, '2017-07-24 01:57:34');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link_video` text CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `image` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `name`, `description`, `link_video`, `title_seo`, `description_seo`, `keyword_seo`, `lang`, `category_id`, `home`, `hot`, `focus`, `sort`, `image`, `active`, `alias`) VALUES
(1, 'SKIN5 - OCEANIA', '<p>n&ocirc;i dung video h&aacute;t rất hay nh&eacute;</p>\r\n', 'gf22lsI2oT0', '', '', '', 'vi', 3, 1, NULL, NULL, 1, 'upload/img/video/img_news_video1.png', 1, 'skin5-oceania'),
(3, 'IVE500 - OCEANIA', '', '3GaoeXv2T90', '', '', '', 'vi', 3, NULL, NULL, NULL, 3, 'upload/img/video/img_news_video2.png', 0, 'ive500-oceania'),
(2, 'Raffine - Auto MTS - OCEANIA', '<p>nội dung m&ocirc; tả</p>\r\n', '1mx44MNB4M4', '', '', '', 'vi', 3, 1, NULL, 1, 2, 'upload/img/video/img_news_video.png', 1, 'raffine-auto-mts-oceania'),
(4, 'R1 Face - OCEANIA', '', 'mDZGJWIifW8', '', '', '', 'vi', 3, NULL, NULL, NULL, 4, 'upload/img/video/img_news_video3.png', 0, 'r1-face-oceania'),
(5, 'Smart Sonic - OCEANIA', '', 'Bm6u9zHpp0w', '', '', '', 'vi', 3, NULL, NULL, NULL, 5, 'upload/img/video/img_news_video4.png', 0, 'smart-sonic-oceania');

-- --------------------------------------------------------

--
-- Table structure for table `video_category`
--

CREATE TABLE `video_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` text CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video_category`
--

INSERT INTO `video_category` (`id`, `name`, `alias`, `sort`, `home`, `hot`, `focus`, `image`, `title_seo`, `description_seo`, `keyword_seo`, `lang`, `description`, `parent_id`) VALUES
(3, 'Video', 'video', 1, 1, NULL, NULL, 'upload/img/video/dia-diem-du-lich-4.jpg', '', '', NULL, 'vi', '<p>nội dung m&ocirc; tả</p>\r\n', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ward`
--

CREATE TABLE `ward` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `pre` varchar(50) CHARACTER SET utf8 NOT NULL,
  `districtid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `pro_id` decimal(21,0) DEFAULT NULL,
  `user_id` decimal(21,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alias`
--
ALTER TABLE `alias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `code_sale`
--
ALTER TABLE `code_sale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments_binhluan`
--
ALTER TABLE `comments_binhluan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_banner`
--
ALTER TABLE `config_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_checkpro`
--
ALTER TABLE `config_checkpro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_menu`
--
ALTER TABLE `config_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_wiget`
--
ALTER TABLE `config_wiget`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_debt`
--
ALTER TABLE `customer_debt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inuser`
--
ALTER TABLE `inuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inuser_category`
--
ALTER TABLE `inuser_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inuser_to_category`
--
ALTER TABLE `inuser_to_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices_detail`
--
ALTER TABLE `invoices_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_shopping`
--
ALTER TABLE `map_shopping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_category`
--
ALTER TABLE `media_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_images`
--
ALTER TABLE `media_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_to_category`
--
ALTER TABLE `news_to_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_brand`
--
ALTER TABLE `product_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_img`
--
ALTER TABLE `product_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_locale`
--
ALTER TABLE `product_locale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_old`
--
ALTER TABLE `product_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tag`
--
ALTER TABLE `product_tag`
  ADD PRIMARY KEY (`product_tag_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `language_id` (`lang`),
  ADD KEY `tag` (`tag`);

--
-- Indexes for table `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_to_color`
--
ALTER TABLE `product_to_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_to_option`
--
ALTER TABLE `product_to_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_to_season`
--
ALTER TABLE `product_to_season`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_to_size`
--
ALTER TABLE `product_to_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pro_size`
--
ALTER TABLE `pro_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_images`
--
ALTER TABLE `p_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raovat`
--
ALTER TABLE `raovat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raovat_category`
--
ALTER TABLE `raovat_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raovat_images`
--
ALTER TABLE `raovat_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raovat_tag`
--
ALTER TABLE `raovat_tag`
  ADD PRIMARY KEY (`raovat_tag_id`);

--
-- Indexes for table `raovat_to_category`
--
ALTER TABLE `raovat_to_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_log`
--
ALTER TABLE `site_log`
  ADD PRIMARY KEY (`site_log_id`);

--
-- Indexes for table `site_option`
--
ALTER TABLE `site_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staticpage`
--
ALTER TABLE `staticpage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `street`
--
ALTER TABLE `street`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_online`
--
ALTER TABLE `support_online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags_news`
--
ALTER TABLE `tags_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags_to_news`
--
ALTER TABLE `tags_to_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags_to_product`
--
ALTER TABLE `tags_to_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_xnt`
--
ALTER TABLE `tbl_xnt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thong_ke_online`
--
ALTER TABLE `thong_ke_online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sms`
--
ALTER TABLE `user_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_category`
--
ALTER TABLE `video_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ward`
--
ALTER TABLE `ward`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `alias`
--
ALTER TABLE `alias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2392;

--
-- AUTO_INCREMENT for table `code_sale`
--
ALTER TABLE `code_sale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `comments_binhluan`
--
ALTER TABLE `comments_binhluan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `config_banner`
--
ALTER TABLE `config_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `config_checkpro`
--
ALTER TABLE `config_checkpro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `config_menu`
--
ALTER TABLE `config_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `config_wiget`
--
ALTER TABLE `config_wiget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `customer_debt`
--
ALTER TABLE `customer_debt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=698;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;

--
-- AUTO_INCREMENT for table `inuser`
--
ALTER TABLE `inuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `inuser_category`
--
ALTER TABLE `inuser_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `inuser_to_category`
--
ALTER TABLE `inuser_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `invoices_detail`
--
ALTER TABLE `invoices_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `map_shopping`
--
ALTER TABLE `map_shopping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `media_category`
--
ALTER TABLE `media_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `media_images`
--
ALTER TABLE `media_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `news_to_category`
--
ALTER TABLE `news_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `product_brand`
--
ALTER TABLE `product_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `product_img`
--
ALTER TABLE `product_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_locale`
--
ALTER TABLE `product_locale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_old`
--
ALTER TABLE `product_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_price`
--
ALTER TABLE `product_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_tag`
--
ALTER TABLE `product_tag`
  MODIFY `product_tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_to_category`
--
ALTER TABLE `product_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=631;

--
-- AUTO_INCREMENT for table `product_to_color`
--
ALTER TABLE `product_to_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `product_to_option`
--
ALTER TABLE `product_to_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_to_season`
--
ALTER TABLE `product_to_season`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_to_size`
--
ALTER TABLE `product_to_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `pro_size`
--
ALTER TABLE `pro_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `p_images`
--
ALTER TABLE `p_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `raovat`
--
ALTER TABLE `raovat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `raovat_category`
--
ALTER TABLE `raovat_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `raovat_images`
--
ALTER TABLE `raovat_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `raovat_tag`
--
ALTER TABLE `raovat_tag`
  MODIFY `raovat_tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `raovat_to_category`
--
ALTER TABLE `raovat_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `site_log`
--
ALTER TABLE `site_log`
  MODIFY `site_log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63917;

--
-- AUTO_INCREMENT for table `site_option`
--
ALTER TABLE `site_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staticpage`
--
ALTER TABLE `staticpage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `street`
--
ALTER TABLE `street`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `support_online`
--
ALTER TABLE `support_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tags_news`
--
ALTER TABLE `tags_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags_to_news`
--
ALTER TABLE `tags_to_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags_to_product`
--
ALTER TABLE `tags_to_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_xnt`
--
ALTER TABLE `tbl_xnt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=358;

--
-- AUTO_INCREMENT for table `thong_ke_online`
--
ALTER TABLE `thong_ke_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=647;

--
-- AUTO_INCREMENT for table `user_sms`
--
ALTER TABLE `user_sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `video_category`
--
ALTER TABLE `video_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ward`
--
ALTER TABLE `ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
