<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
    <title><?= isset($seo['title']) && $seo['title'] != '' ? $seo['title'] : @$this->option->site_name; ?></title>
    <link rel="shortcut icon" href="<?= base_url(@$this->option->favicon ) ?>"/>
    <meta name='description'
          content='<?= isset($seo['description']) ? $seo['description'] : @$this->option->site_description; ?>'/>
    <meta name='keywords'
          content='<?= isset($seo['keyword']) && $seo['keyword'] != '' ? $seo['keyword'] : $this->option->site_keyword; ?>'/>
    <meta name='robots' content='index,follow'/>
    <meta name='revisit-after' content='1 days'/>
    <meta http-equiv='content-language' content='vi'/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="fb:app_id" content="126821687974504" />
    <meta property="fb:admins" content="100006472503973"/>

    <link rel="canonical" href="<?=current_url();?>" />
    <!--    for facebook-->
    <meta property="og:title"
          content="<?= isset($seo['title']) && $seo['title'] != '' ? $seo['title'] : @$this->option->site_name; ?>"/>
    <meta property="og:site_name" content="<?= @$this->option->site_name; ?>"/>
    <meta property="og:url" content="<?= current_url(); ?>"/>
    <meta property="og:description"
          content="<?= isset($seo['description']) && $seo['description'] != '' ? $seo['description'] : @$this->option->site_description; ?>"/>
    <meta property="og:type" content="<?= @$seo['type']; ?>"/>
    <meta property="og:image" content="<?= isset($seo['image']) && @$seo['image'] != '' ? base_url(@$seo['image']) : @$this->option->site_logo ; ?>"/>

    <meta property="og:locale" content="vi_VN"/>

    <!-- for Twitter -->
    <meta name="twitter:card"
          content="<?= isset($seo['description']) && $seo['description'] != '' ? $seo['description'] : @$this->option->site_description; ?>"/>
    <meta name="twitter:title"
          content="<?= isset($seo['title']) && $seo['title'] != '' ? $seo['title'] : @$this->option->site_name; ?>"/>
    <meta name="twitter:description"
          content="<?= isset($seo['description']) && $seo['description'] != '' ? $seo['description'] : @$this->option->site_description; ?>"/>
    <meta name="twitter:image"
          content="<?= isset($seo['image']) && $seo['image'] != '' ? base_url($seo['image']) : base_url(@$this->option->site_logo); ?>"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/bootstrap-reboot.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/owl.carousel.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/owl.theme.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/slick.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/slick-theme.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/all.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/animate.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/fontawesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/main.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/front_end/media.css" />
    <script src="<?=base_url()?>assets/js/front_end/jquery-3.5.1.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/wow.min.js"></script>
		<script>
        new WOW().init();
    </script>

  <input type="hidden" value="<?= base_url()?>" id="base_url" name="">

</head>

<body id="homepage">
<style>

.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url("<?=base_url();?>/img/Preloader_3.gif") center no-repeat #fff;
}
</style>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");;
	});
</script>
<div class="se-pre-con" id="loading-page"></div>



<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=126821687974504";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<!-- begin container_header --><!-- end container_header -->
<header>
	<div class="container-ics">
			
			<div class="row header-row">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<div class="header-logo">
						<a href="<?= base_url()?>" title=""><img class="img-thumbnail" src="<?= base_url(@$this->option->site_logo)?>" alt="" /></a>
					</div>
				</div>
				
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class="header-info">
						<h2 class="company-name"><?=@$this->option->site_title?></h2>
						<div class="company-slogan"><?=@$this->option->slogan?></div>
						<div class="company-email">Email: <?=@$this->option->site_email?></div>
						<div class="company-phone">Phone: <?=@$this->option->hotline1?></div>
						<div class="company-address">Address: <?=@$this->option->address?></div>
					</div>
				</div>
				
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<img class="img-thumbnail" src="<?= base_url(@$this->option->site_logo)?>" alt="" />
				</div>
			</div>
			
	</div>
</header>
<div class="menu-header">
	<div class="container-ics">
		<div class="menu-content">
			<div class="menu-bar-left">
				<?=$menu_right;?>
				<?=$menu_main;?>
				
			</div>
			<div class="cart-menu-right">
				<div class="cart-menu-content" id="quant-cart">
					<i class="fal fa-shopping-cart"></i>
					<a href="<?=base_url('gio-hang')?>" class="cart-menu-number" ><span ><?=$cart ? count($cart) : '0';?></span> items</a>
					<span class="hyphen">-</span>
					<span class="cart-menu-total"><?=number_format($money);?> VND</span>
				</div>
			</div>
		</div>
	</div>
</div>

<?=$menu_mobile?>
