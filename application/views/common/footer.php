<footer>
	<div class="container-ics">
		<div class="footer-content">
			<div class="row row-footer">
				<div class="col-md-4 col-sm-4 col-xs-4 col-footer">
					<div class="footer-info-company">
						<div class="footer-logo">
							<a href="<?= base_url()?>" title="" class="footer-logo-link"><img src="<?= base_url(@$this->option->site_logo_footer)?>" alt=""></a>
							<div class="footer-slogan"><?=@$this->option->shipping?></div>
						</div>

						<div class="footer-signup">
							<h3 class="footer-signup-title">NEWSLETTER SIGN UP</h3>
							<div class="footer-signup-form">
								<form action="">
									<input type="text" name="email" placeholder="Enter your email address" required>
									<button type="button" class="btn btn-primary"><i class="far fa-paper-plane"></i></button>
								</form>
							</div>
						</div>
						<div class="footer-social">
							<ul class="list-social-icon">
								<li class="facebook-social"><a href="<?=@$this->option->site_fanpage?>" target="_blank" title=""><i class="fab fa-facebook-f"></i><span class="ts-tooltip social-tooltip">Facebook</span></a></li>
								<li class="flickr-social"><a href="<?=@$this->option->link_printer?>" target="_blank" title=""><i class="fab fa-flickr"></i><span class="ts-tooltip social-tooltip">Flickr</span></a></li>
								<li class="vimeo-social"><a href="<?=@$this->option->link_linkedin?>" target="_blank" title=""><i class="fab fa-vimeo-square"></i><span class="ts-tooltip social-tooltip">Vimeo</span></a></li>
								<li class="youtube-social"><a href="<?=@$this->option->link_youtube?>" target="_blank" title=""><i class="fab fa-youtube-square"></i><span class="ts-tooltip social-tooltip">Youtube</span></a></li>
							</ul>
						</div>
					</div>
				</div>
				<?=$menu_bottom;?>
			</div>
		</div>
	</div>
</footer>
<div class="copyright">
	<div class="container-ics">
		<div class="copyright-content">
			<div class="copyright-left">
			<?=@$this->option->coppy_right;?> 
			</div>
			<div class="copyright-right">
				<img src="<?= base_url()?>/img/payment.png" alt="">
			</div>
		</div>
	</div>
</div>
<div class="ketqua">
	<h2 class='messenter_popup'>Cập nhập giỏ thành công</h2>
</div>

	<script src="<?=base_url()?>assets/js/front_end/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/all.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/slick.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/owl.carousel.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/fontawesome.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/main.js"></script>

<!-- end footer -->
<!-- begin container_close_footer --><!-- end container_close_footer -->
<?php if(isset($this->config_hotline)){?>
<?=$this->load->view('temp/hotline');?>
<?php } ?>


<?php if(isset($this->config_chatfanpage)){?>
<?=$this->load->view('temp/chat_fanpage');?><?php } ?>
<div id="show_success_mss" style="position: fixed; top: 40px; right: 20px;z-index:9999;">
    <?php if($this->session->flashdata('mess')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-success"></i><?=$this->session->flashdata('mess'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('mess_err')): ?>
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-warning"></i><?=$this->session->flashdata('mess_err'); ?>
    </div>
    <?php endif; ?>
</div>
<link href="<?= base_url()?>assets/plugin/ValidationEngine/css/validationEngine.jquery.css" rel="stylesheet"/>
<script src="<?= base_url()?>assets/plugin/ValidationEngine/js/jquery.validationEngine-vi.js"
            charset="utf-8"></script>
<script src="<?= base_url()?>assets/plugin/ValidationEngine/js/jquery.validationEngine.js"></script>
<script src="<?= base_url()?>assets/js/front_end/script.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
		$('#loading-page').removeClass('se-pre-con');
        $('.validate ').validationEngine();
    });
     setTimeout(function(){
        $('#show_success_mss').fadeOut().empty();
    }, 9000);
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/front_end/owl.carousel2.min.js"></script>
</body>
</html>
