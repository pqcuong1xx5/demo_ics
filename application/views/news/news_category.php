<style>
.row-item-new {
    padding: 30px 0;
    border-bottom: solid 1px #ebebeb;
}
.row-item-new:last-child{
	border: none;
}
.product-similar-item .product-similar-image {
    display: inline-block;
    width: 100%;
    border: solid 1px #ebebeb;
    padding: 5px;
	height: 200px;
	
}

.product-similar-item .product-similar-image img {
    display: inline-block;
    width: 100%;
	object-fit:cover;
	height: 100%;
}


.product-category-content .row-filter .horizontal-list,
.product-category-content .row-filter .vetical-list {
    width: 50px;
    height: 50px;
    text-align: center;
    border: solid 1px #afafaf;
    padding: 10px 0;
    font-size: 20px;
    color: #777777;
    margin: 0 5px;
    cursor: pointer;
}

.product-category-content .row-filter .horizontal-list,
.product-category-content .row-filter .vetical-list:hover {
    color: #0083c4;
    border: solid 1px #0083c4;
}

 .menu-category-title{
	font-size: 16px;
	padding: 13px 0 13px 0;
	font-family: 'Roboto-bold';
	margin-bottom: 0;
	position: relative;
}
.menu-category-title:before{
	content : '';
	background: #ebebeb;
	left: -20px;
	right: -20px;
	position: absolute;
	bottom: 0;
	height: 1px;
}
.menu-category-left {
	min-height: 400px;
	padding: 0 20px;
    border: solid 1px #ebebeb;
    border-top: solid 2px #0083c4;
}
.menu-category-left .menu-category-list{
	padding: 8px 0 20px 0;
}
.menu-category-left .menu-category-list .menu-list-item {
	position: relative;
	padding: 8px 0;
}
.menu-category-left .menu-category-list .menu-list-item .btn-dropdown{
	position: absolute;
    top: 0;
    right: 0;
    width: 20px;
    height: 20px;
    cursor: pointer;
    padding: 5px 10px;
}
.menu-category-left .menu-category-list .menu-list-item  .menu-list-link{
	transition: .3s;
}
.menu-category-left .menu-category-list .menu-list-item.active .menu-list-link,.menu-category-left .menu-category-list .menu-list-item:hover  .menu-list-link{
	color: #0083c4;
}
.menu-category-child{
	margin: 5px 0 0 15px;
	height: 0;
	opacity: 0;
	
}
.menu-child-active .menu-category-child{
	height: auto;
	opacity: 1;
	transition: .5s;
}
.menu-category-child .category-child-item{
	padding: 5px 0;
}
.menu-category-child .category-child-item .menu-child-link{
	color: #848484;
}
div.pagination{
	margin: 40px 0;

}
.phantrang{
	margin: 0 auto;
}
.phantrang li  a{
	display: inline-block;
    padding: 5px 15px;
    border: solid 1px #afafaf;
    margin: 0 5px;
}
.name-new-page{
	overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    display: -webkit-box;
}
.new-description{
	overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    display: -webkit-box;
}
.name-new-page a{
    font-size: 21px;
    font-family: 'Roboto-Medium';
}
.product-category-content .row-filter {
    display: flex;
    margin-bottom: 20px;
}
.dcs-new-page{
	height: 100%;
	position: relative;
}
.view-pros-page{
	position: absolute;
	bottom: 0;
	width: 100%;
}
</style>

<div class="product-category">
    <div class="container-ics">
        <div class="back-link">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"> <i class="fa fa-home" aria-hidden="true"></i><a href="#">Trang chủ</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page"><?= $cate_current->name?></li>
                </ol>
            </nav>
        </div>
        <div class="product-category-content">

            <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="menu-category-left">
						<h3 class="menu-category-title">CATEGORIES</h3>
						<div class="menu-category-list">
							<?php if (isset($all_cate)) { ?>
								<ul class="category-list">
									<?php foreach ($all_cate as $cate_parent) { ?>
										<li class="menu-list-item ">
											<a href="<?= base_url('danh-muc-tin/'.$cate_current->alias.'.html')?>" title="<?=$cate_parent->name?>" class="menu-list-link"><?=$cate_parent->name?> (<?=$cate_parent->count_pro?>)</a>
											<?php if (isset($cate_parent->cate_child) && count($cate_parent->cate_child)>0) { ?>
												<div class="btn-dropdown">
													<i class="fas fa-sort-down btn-dropdown-icon"></i>
												</div>
												<ul class="menu-category-child">
													<?php foreach ($cate_parent->cate_child as $key => $n) { ?>
														<li class="category-child-item"><a href="<?=base_url('danh-muc/'.$n->alias.'.html')?>" title="<?=$n->name?>" class="menu-child-link"><?=$n->name?> (<?=$n->count_pro?>)</a></li>
													<?php } ?>
													
												</ul>
											<?php } ?>
										</li>
									<?php } ?>
								</ul>
							<?php } ?>
						</div>
                    </div>
					
                </div>

                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                    <div class="row-filter">

                        <div class="vetical-list">
                            <i class="fas fa-th-list"></i>
                        </div>
                        <div class="horizontal-list">
                            <i class="fas fa-th"></i>
                        </div>
                    </div>
                    <div class="row-list-product">
						<?php if(isset($lists)){ ?>
                            <?php foreach($lists as $new) : ?>
                        <div class="row row-item-new">
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <div class="product-similar-item">
                                    <a href="<?= base_url('new/'.$new->alias.'.html') ?>"
                                        class="product-similar-image"><img
                                            src="<?= base_url($new->image)?>"
                                            alt=""></a>
                                    
                                </div>
                            </div>
                            
							<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
								<div class="dcs-new-page">
                                        <h3 class="name-new-page">
                                            <a href="<?= base_url('new/'.$new->alias.'.html')?>" title=""><?= $new->title?></a>
                                        </h3>
                                        <div class="new-description">
											<?= $new->description?>
										</div>
                                        <div class="view-pros-page clearfix">
                                            <span class="date-time pull-left"><?= date('d',$new->time)?> Tháng <?= date('m',$new->time)?>, <?= date('Y',$new->time)?></span>
                                            <a href="<?= base_url('new/'.$new->alias.'.html')?>" title="" class="btn btn-view-page pull-right">Xem thêm <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                        </div>
                                </div>
							</div>
							
                        </div>
						<?php endforeach;?>
						<?php }?>
                    </div>
                </div>

            </div>

        </div>
		<div class="pagination">
			<?php
				echo $this->pagination->create_links(); // tạo link phân trang
				?>
		</div>
    </div>
</div>
 



<div class="clearfix"></div>
