
<style>
	.product-detail{
		margin: 40px 0;
	}
	.slider-wrapper{
		height: 548px;
	}
	.slider-wrapper img{
		height: 100%;
		width: 100%;
	}
	.slider-for{
		
	}
	.slider-wrapper .slider-nav,.slider-for{
		height: 100%;
	}
	.slider-nav .slider-nav__item{
		padding: 5px;
		border: solid 1px  #ddd;
	}
	.slider-for .slider-for__item{
		height: 548px;
		padding: 5px;
	}
	.slider-nav img{
		height: 111px;
		width: 100%;
		object-fit: cover;
		
	}
	.product-detail-rating{
		font-size: 14px !important;
	}
	.product-detail-description{
		margin: 15px 0;
	}
	.product-info-cart{
		border-top: solid 1px #ddd;
	}
	.product-info-cart .price-demo{
		font-size: 36px;
		margin: 20px 0;
	}
	.product-info-cart .price-demo .price-old{
		font-size: 20px;
	}
	.product-quantity{
		width : 120px;
		position: relative;
		display: inline-block;
		margin: 0 5px;
	}
	.product-quantity .value-quantity{
		height: 50px;
		border: solid 1px #ddd;
		width: 100%;
		text-align: center;
		padding: 16px 45px 15px 15px;
	}
	.product-quantity .value-quantity:focus{
		outline: none;
	}
	.product-quantity .btn-quantity-up{
		height: 25px;
		width: 35px;
		position: absolute;
		top: 0;
		right: 0;
		padding: 5px;
		cursor: pointer;
		border: solid 1px #ddd;
	}
	.product-quantity .btn-quantity-down{
		height: 25px;
		width: 35px;
		position: absolute;
		bottom: 0;
		right: 0;
		padding: 5px;
		cursor: pointer;
		border: solid 1px #ddd;
	}
	.product-add-cart{
		width: 170px;
		margin: 0 5px;
		height: 50px;
		background-color: #0083c4;
		border-color: #0083c4;
		color: #ffffff;
		padding: 12px 0;
		text-align: center;
		font-size: 16px;
		cursor: pointer;
		text-transform: uppercase;
		transition: .5s;
		display: inline-block;
	}
	.product-add-cart:hover{
		background-color: #3f3f3f;
    color: #ffffff;
    border-color: #3f3f3f;
	}
	.product-add-cart .fa-shopping-cart{
		margin-right: 5px;
	}
	.product-detail-name{
		font-family: 'Roboto-Bold';
	}
	.product-info-tab{
		border: solid 1px #ebebeb;
		margin: 50px 0;
	}
	.product-info-tab .nav-tabs{
		padding: 10px;
	}
	.product-info-tab .nav-tabs li{
		margin: 0 10px 0 0;
	}
	.product-info-tab .nav-tabs li a{
		font-size: 16px;
		text-transform: uppercase;
		padding: 8px 30px;
		border: solid 1px #ebebeb;
		width: 190px;
		text-align: center;
		position: relative;
		display: inline-block;
		color: #000;
	}
	.product-info-tab .nav-tabs li a:before{
		content: "\f0dd";
		font-family: 'FontAwesome';
		position: absolute;
		bottom: -4px;
		top: auto;
		right: auto;
		left: 50%;
		margin-left: -5px;
		font-size: 20px;
		line-height: 10px;
		color: transparent;
		border: 0;
		width: auto;
		height: auto;
		display: inline-block;
		transition: all 200ms ease 0s;
		-webkit-transition: all 200ms ease 0s;
		-moz-transition: all 200ms ease 0s;
		opacity: 0;
	}
	.product-info-tab .nav-tabs li a:hover{
		color: #fff;
		background: #000000;
		border-color: #000000;
	}
	.product-info-tab .nav-tabs li a.active:before{
		color: #000;
		opacity: 1;
	}
	.product-info-tab .nav-tabs li a.active{
		color: #fff;
		opacity: 1;
		background: #000;
		border-color: #000000;
	}

	.product-info-tab .nav-tabs li a svg{
		margin-right: 5px;
	}
	.product-detail .product-tab-content{
		padding: 20px;
	}
	.product-info-share{
		margin: 40px 0;
		border-top: solid 1px #ebebeb;
		padding: 40px 0;
	}
	.product-share-button{
		margin-top: 20px;
	}
	.product-similar h3{
		border-bottom: solid 1px #ebebeb;
		padding: 10px 0;
	}
	.product-similar-item {
		padding: 5px;
		
		display: inline-block;
	}
	.product-similar-item .product-similar-image{
		display: inline-block;
		width: 100%;
		height: 250px;
		border: solid 1px #ebebeb;
	}
	.product-similar-item .product-similar-image img{
		width: 100%;
		height: 100%;
		object-fit: cover;
	}
	.product-similar-info{
		text-align: center;
	}
	.slider-image-zoom{
		border: solid 1px #ddd;
		width: 100%;
    	height: 100%;
	}
	
</style>

<?php if(isset($item)){ ?>
<div class="product-detail">
	<div class="container-ics">
		<div class="back-link">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"> <i class="fa fa-home" aria-hidden="true"></i><a href="#">Trang chủ</a></li>
					<?php if(isset($cate_current)){ ?>
					<li class="breadcrumb-item"><a href="<?=base_url('danh-muc/'.$cate_current->alias.'.html') ?>"><?= $cate_current->name?></a></li>
					<?php } ?>
					<li class="breadcrumb-item active" aria-current="page"><?= $item->name?></li>
				</ol>
			</nav>
		</div>
		<div class="product-detail-content">
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class="slider-wrapper">
						<div class="row">
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<div class="slider-nav">
								<?php if(!empty($item->image)){ ?>
                                    <div class="slider-nav__item">
                                        <img src="<?= base_url('upload/img/products/' . $item->pro_dir . '/' . @$item->image) ?>" alt="">
                                    </div>
								<?php } ?>
								<?php if (isset($p_images)): ?>
                                  <?php foreach ($p_images as $key => $v): ?>
                                    <div class="slider-nav__item">
                                        <img src="<?= base_url(@$v->image) ?>" alt="">
                                    </div>
									<?php endforeach ?>
								<?php endif ?>
								</div>
							</div>
							<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
								<div class="slider-for ">
								<?php if(!empty($item->image)){ ?>
									<div class="slider-for__item ex1" data-src="https://unsplash.it/870/870">
										<div class="slider-image-zoom">
										<img src="<?= base_url('upload/img/products/' . $item->pro_dir . '/' . @$item->image) ?>" alt="">
										</div>
									</div>
								<?php } ?>
								<?php if (isset($p_images)): ?>
                                  <?php foreach ($p_images as $key => $v): ?>
									<div class="slider-for__item ex1" data-src="https://unsplash.it/871/870">
										<div class="slider-image-zoom">
											<img src="<?= base_url(@$v->image) ?>" alt="">
										</div>
									</div>
									<?php endforeach ?>
								<?php endif ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class="product-detail-info">
						<h3 class="product-detail-name"><?=$item->name; ?></h3>
						<div class="product-info-des">
							<div class="product-detail-rating rating-demo">
							<?php if (isset($thuoctinh)) {
								foreach ($thuoctinh as $key=>$v) { 
									if($v->type=='int'){
									?>
										<?php if(@$v->content && @$v->content > 0){ ?>
											<?php for( $i=0; $i<5; $i++ ){ ?>
												<?php if( $i <= @$v->content) { ?>
													<?php if( $i == floor(@$v->content) &&  @$v->content-$i !=0 ) { ?>
														<span class="fas fa-star-half-alt"></span>
													<?php }else{ ?>
														<span class="fas fa-star checked"></span>
													<?php } ?>
												<?php }else{ ?>
													<span class="far fa-star"></span>
												<?php } ?>
											<?php } ?>
										<?php }else{ ?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
								<?php  } } }else{?>
									<div class="no-rating">
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star" aria-hidden="true"></span>
										<span class="far fa-star" aria-hidden="true"></span>
									</div>
								<?php } ?>
							</div>
							<div class="product-detail-description">
								<?=$item->description; ?>							
							</div>
						</div>
						<div class="product-info-cart">
							<div class="price-demo">
								<?php if($item->price && $item->price>0){ ?><span class="price-old"><?=number_format($item->price)?></span><?php } ?><?=number_format($item->price_sale)?>VND
							</div>
							<div class="product-info-cart-button">
								<div class="product-quantity">
									<input type="number" name="qty" class="value-quantity" id="quantity-pro" value="1">
									<input type="hidden" name="product" id="id-pro" value="<?=$item->id; ?>">
									<i class="fas fa-plus btn-quantity-up"></i>
									<i class="fas fa-minus btn-quantity-down"></i>
								</div>
								<div class="product-add-cart">
								<!-- <input type="submit" name="id" value="submit"> -->
								<i class="fas fa-shopping-cart"></i>Add to cart
								</div>
							</div>
							
						</div>
						<div class="product-info-share">
							<div class="share-category">
							Categories: Laptops, Video Games
							</div>
							<div class="share-tag">
							Tags: boxshop, theme-sky, woocommerce, wordpress
							</div>
							<div class="product-share-button">
								<div id="fb-root"></div>
								<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0&appId=357555714875122&autoLogAppEvents=1" nonce="N2STVkeQ"></script>
								<div class="fb-share-button" data-href="<?=base_url('san-pham/'.$item->alias)?>" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="product-info-tab">
				<ul class="nav nav-tabs">
						<li><a data-toggle="tab" class="active" href="#description"><i class="fas fa-pencil-alt"></i>Description</a></li>
						<li><a data-toggle="tab"  href="#review"><i class="fas fa-search"></i>review</a></li>
				</ul>
				<div class="product-tab-content tab-content">
					<div id="description" class="product-tab-item tab-pane fade in active show">
					<?php if (isset($thuoctinh)) {
						foreach ($thuoctinh as $key=>$v) { 
							if($v->type=='textarea'){
							?>
							<?=@$v->content;?>
						<?php }  } }?>
					</div>
					<div id="review" class="product-tab-item tab-pane fade">

					</div>
				</div>
			</div>
			<div class="product-similar">
				<h3>Sản phẩm tương tự</h3>
				<div class="product-similar-list">
					<?php if(isset($list_item)):
					foreach($list_item as $pro):  ?>
					<div class="product-similar-item">
						<a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" class="product-similar-image"><img src="<?=base_url('upload/img/products/'.$pro->pro_dir.'/thumbnail_2_'.@$pro->image)?>" alt=""></a>
						<div class="product-similar-info">
							<a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" class="product-similar-link name-product-current"><?php echo $pro->name; ?></a>
							<div class="product-similar-rating rating-demo">
								<span class="fas fa-star checked"></span>
								<span class="fas fa-star checked"></span>
								<span class="fas fa-star-half-alt"></span>
								<span class="far fa-star" aria-hidden="true"></span>
								<span class="far fa-star" aria-hidden="true"></span>
							</div>
							<div class="product-similar-price price-demo">
							<?=number_format($pro->price_sale)?>VND
							</div>
						</div>
					</div>
					<?php endforeach;endif;?> 
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script>
	$(document).ready(function () {
		$('.product-add-cart').click(function (e) { 
			var quantity = document.getElementById('quantity-pro').value;
			var id = document.getElementById('id-pro').value;

			$.ajax({
		      		type: "POST",
		      		url: "<?php echo base_url('add-to-cart'); ?>",
		      		data: { product: id,qty: quantity },
		      		success: function(result){
						$('.messenter_popup').addClass('messager_active');
		      			setTimeout(function(){
		    				$('.messenter_popup').removeClass('messager_active');
						}, 2000);
						$('#quant-cart').html(result);
		      		}
			})
		});
		$('.btn-quantity-up').click(function (e) { 
			
			var quantity = document.getElementById('quantity-pro').value;
			quantity++;
			document.getElementById('quantity-pro').value = quantity;
			
		});
		$('.btn-quantity-down').click(function (e) { 
			
			var quantity = document.getElementById('quantity-pro').value;
			if(quantity<=1){
				return;
			}
			quantity--;
			document.getElementById('quantity-pro').value = quantity;
			
		});
		
		
	});
	
</script>
