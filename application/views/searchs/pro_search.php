
<div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3 paright-0">
                    <div class="banner-menu">
                        <ul>
                        <?=$danhmuc?>
                            
                        </ul>
                    </div>
                    <?=$product_noibat?>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9 paddleft0">
                    <?=$search?>
                    <div class="back-link">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"> <i class="fa fa-home" aria-hidden="true"></i><a href="<?=base_url()?>" title="">Trang chủ</a></li>
                                <li class="breadcrumb-item"><a href="<?=base_url('danh-muc/sofa-vang')?>" title="">Sản phẩm</a></li>
                                <li class="breadcrumb-item active" aria-current="page">tìm kiếm</li>
                            </ol>
                        </nav>
                    </div>
                    
                    <div class="procate-list">
                        
                        <div class="procategory-content">
                            <h2 class="procate-name">Kết quả tìm kiếm</h2>
                        </div>
                        <?php if(count($lists)>0){ ?>
                        <div class="row margleft0" style="padding-right: 10px;">
                                <?php if (count($lists)) { foreach ($lists as $key => $pro) { ?>
                            <div class="col-md-3 con-sm-3 col-xs-6 paddleft5 paddright5">
                                <div class="product-list">
                                    <div class="pr-img">
                                        <a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" title=""><img src="<?=base_url('upload/img/products/'.$pro->pro_dir.'/thumbnail_2_'.@$pro->image)?>" alt=""></a>
                                        <?php if($pro->price>$pro->price_sale){ ?>
                                        <div class="pr-img-sale">
                                            <div class="sale-before">
                                                <p>SALE</p>
                                            </div>

                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="pr-title">
                                        <a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" title="" class="pr-title-name"><?php echo $pro->name; ?></a>
                                        <p class="pr-title-price">Giá: <?php if(!empty($pro->price_sale)){echo $pro->price_sale;}else{?>Liên hệ<?php }  ?></p>
                                        <a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" title="" class="pr-title-btn">Chi tiết</a>
                                    </div>
                                </div>
                            </div>
 
                                 <?php }}?>


                        </div>
                                               <? }else{ ?>
                            <div class="search-fail">
                                <h3 style="font-size: 18px; font-weight: bold; font-style: italic;">!Không tìm thấy kết quả nào.....</h3>
                            </div>
                        <? } ?>
                    </div>
                     <div class="phantrang_prod">
                        <?php echo $this->pagination->create_links();?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>



