<style>
	body .menu-header .container-ics .menu-content .menu-bar-left .vertical-menu-left .vertical-menu-list {
    display: inline-block;
  
}
	body .menu-header .container-ics .menu-content .menu-bar-left .vertical-menu-left:hover .vertical-menu-list {
		animation-name: none;
    animation-duration: 0;
}
</style>

<div class="banner">
	<div class="container-ics">
		<div class="row-banner">
							<?=$banner?>
							<?=$quangcao_right?>
			
		</div>
	</div>
</div>
<?=$category_slide?>	
<?=$product_sale?>	
<?=$product_noibat;?>
<?=$doitac;?>
<?=$product_cat_home;?>
<?=$quangcao_left;?>
<?=$product_noibat_home;?>

<div class="policy">
	<div class="container-ics">
		<div class="policy-content">
			<div class="policy-item ">
				<div class="policy-item-return">
					<div class="policy-item-content">
						<a href="" title="" class="policy-item-img"><i class="fas fa-sync-alt icon-policy"></i></a>
						<div class="policy-item-description">
							<a href="" title="" class="policy-item-title">FREE RETURNS</a>
							<div class="policy-feature-excerpt">
								Only for all orders over $100.00
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="policy-item ">
				<div class="policy-item-ship">
					<div class="policy-item-content">
						<a href="" title="" class="policy-item-img"><i class="fas fa-plane icon-policy"></i></a>
						<div class="policy-item-description">
							<a href="" title="" class="policy-item-title">FREE SHIPPING</a>
							<div class="policy-feature-excerpt">
								on all orders over $50.00
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="policy-item ">
				<div class="policy-item-support">
					<div class="policy-item-content">
						<a href="" title="" class="policy-item-img"><i class="fas fa-comments icon-policy"></i></a>
						<div class="policy-item-description">
							<a href="" title="" class="policy-item-title">FREE SUPPORT</a>
							<div class="policy-feature-excerpt">
								24 / 7 from Monday to Friday
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

