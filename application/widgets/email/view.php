<div class="clearfix-30"></div>
<section id="order">
    <h2>Tất cả chỉ mất 1 phút để đặt hàng</h2>
    <p><a href="" data-toggle="modal" data-target="#myModal">Đặt mua ngay</a></p>
</section>
<div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title tit_dh">Đặt hàng</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="choose clearfix">
                            <form action="">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember"> LB-101</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember"> LB-201</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember"> LB-202</label>
                                </div>
                            </form>
                        </div>
                        <form action="" class="form_order clearfix">
                            <div class="form-group">
                                <label for="quality"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></label>
                                <input type="text" class="form-control" id="soluong" placeholder="Số lượng đặt mua" name="soluong">
                            </div>
                            <div class="form-group">
                                <label for="name"><i class="fa fa-user" aria-hidden="true"></i></label>
                                <input type="text" class="form-control" id="name" placeholder="Tên của bạn" name="name">
                            </div>
                            <div class="form-group">
                                <label for="pwd"><i class="fa fa-user" aria-hidden="true"></i></label>
                                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
                            </div>
                            <div class="form-group">
                                <label for="phone"><i class="fa fa-phone" aria-hidden="true"></i></label>
                                <input type="text" class="form-control" id="phone" placeholder="Số điện thoại" name="phone">
                            </div>
                            <div class="form-group">
                                <label for="address"><i class="fa fa-map-marker" aria-hidden="true"></i></label>
                                <input type="text" class="form-control" id="address" placeholder="Địa chỉ nhận hàng" name="address">
                            </div>
                            <div class="form-group">
                                <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i></label>
                                <input type="text" class="form-control" id="email" placeholder="Email nhận thông báo" name="email">
                            </div>
                            <div class="form-group">
                                <label for="add"><i class="fa fa-plus-square-o" aria-hidden="true"></i></label>
                                <input type="text" class="form-control" id="add" placeholder="Ghi chú thêm" name="add">
                            </div>
                            <button type="submit" class="btn btn-default submit_dh">Hoàn tất</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>