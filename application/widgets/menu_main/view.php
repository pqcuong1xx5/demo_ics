<?php if(count($menu_root)) : ?>
<div class="horizontal-menu-center">
	<div class="horizontal-menu-list">
			<ul class="horizontal-menu-parent">
				<?php foreach ($menu_root as $key_r => $mr) : ?>
				<li class="horizontal-menu-item">
				<a href="<?=base_url($mr->url);?>" class="parent-menu-link <?php if(!empty($mr->menu_sub)){ echo 'show-drop-down' ;} ?>" title=""><?=@$mr->name;?></a>
					<?php if(!empty($mr->menu_sub)): ?>
					<ul class="horizontal-menu-child">
						<?php $i=0; foreach($mr->menu_sub as $menu_sub) : $i++; ?>
							<li class="child-menu-item">
								<a href="<?=base_url($menu_sub->url);?>" title=""><?=@$menu_sub->name;?></a>
							</li>
						<?php endforeach;?>
					</ul>
					<?php endif;?>
				</li>
				<?php endforeach;?>
			</ul>
	</div>
</div>
<?php endif;?>

