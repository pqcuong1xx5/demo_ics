
<?php if(isset($slides)) : $i=0; ?>
<div class="banner-slider">
	<?php foreach ($slides as $k => $v) { $i++;?>
	<?php if($i==1){?>
	<div class="banner-slider-item">
		<img alt="" class="banner-slider-image" src="<?=base_url(@$v->image);?>">
		<div class="banner-content ">
			<div class="banner-content-hero">
				<h2 class="banner-content-title"><?=@$v->title;?></h2>

			</div>
			<div class="banner-description">
				<span class="banner-content-description"><?=@$v->content;?></span>
			</div>
			<div class="banner-button">
				<a class="banner-btn-link" href="<?=@$v->url;?>" tabindex="0">SHOP NOW</a>
			</div>
		</div>
	</div>
	<?php }?>
	<?php if($i==2){ ?>
	<div class="banner-slider-item banner-slider-item2">
		<img alt="" class="banner-slider-image" src="<?=base_url(@$v->image);?>">
		<div class="banner-content2 ">
			<div class="banner-content-hero2">
				<h2 class="banner-content-title2"><?=@$v->title;?></h2>
			</div>
			<div class="banner-description2">
				<span class="banner-content-description2"><?=@$v->content;?></span>
			</div>
			<div class="banner-button2">
				<a class="banner-btn-link" href="<?=@$v->url;?>" tabindex="0">SHOP NOW</a>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php }?>  
</div>
    
<?php endif;?>

                      
                        



