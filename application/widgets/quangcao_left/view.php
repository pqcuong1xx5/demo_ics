<?php if(isset($slides)) { ?>
<div class="partner">
	<div class="container-ics">
		<div class="partner-content">
			<div class="partner-slide">
			<?php foreach($slides as $slide) :  ?>
				<div class="partner-item">
					<a href="<?=base_url(@$slide->url)?>" title="" class="partner-link"><img src="<?=base_url(@$slide->image)?>" alt="<?=$slide->title?>"></a>
				</div>
				<?php endforeach;?>  
				
			</div>
		</div>
	</div>
</div>
<?php } ?>
