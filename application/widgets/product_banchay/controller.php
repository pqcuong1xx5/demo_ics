<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_banchay_widget extends MY_Widget
{
    // Nhận 2 biến truyền vào
    function index($name){
        
		$data['pros_title'] = $name;
		$data['pros'] = $this->system_model->get_data('product',array(
            'coupon' => 1,
            'lang' => $this->language,
            ),array('sort' => 'desc'),false,3,0);
		//var_dump($data['pros']);die;
	    $this->load->view('view',$data);
    }
}