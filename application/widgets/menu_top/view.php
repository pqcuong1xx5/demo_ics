<?php if(isset($menu_mobile)){ ?>
<div class="mobile-button" id="mobile-button-visible">
	<div class="mobile-button-click" onClick="onToggleMenu(this)">
		<div class="mobile-button-line1"></div>
		<div class="mobile-button-line2"></div>
		<div class="mobile-button-line3"></div>
	</div>
</div>
<div class="mobile-menu-list" id="mobile-menu-list">
	<div class="mobile-logo">
		<img src="<?= base_url($this->option->site_logo)?>" alt="">
		<div class="mobile-button-close">
			<div class="mobile-button-click">
				<div class="mobile-button-line1"></div>
				<div class="mobile-button-line2"></div>
				<div class="mobile-button-line3"></div>
			</div>
		</div>
	</div>
	<ul class="mobile-menu-parent">
		<?php foreach ($menu_mobile as $key_r => $mr) : ?>
		<li class="mobile-menu-item">
			<a href="<?=@$mr->url;?>" class="mobile-menu-link" title=""><?=@$mr->name;?></a>
			<?php if(!empty($mr->menu_sub)): ?>
			<div class="btn-show-chid">
				<i class="fas fa-chevron-down toggle-icon"></i>
			</div>
			<ul class="mobile-menu-child">
				<?php $i=0; foreach($mr->menu_sub as $menu_sub) : $i++; ?>
				<li class="mobile-child-item">
					<a href="<?= $menu_sub->url?>" title=""><?= $menu_sub->name?></a>
				</li>
				<?php endforeach;?>
			</ul>
			<?php endif;?>
		</li>
		<?php endforeach;?>
	</ul>
</div>
<?php } ?>
