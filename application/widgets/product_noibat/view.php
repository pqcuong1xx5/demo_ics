

<?php if(isset($cate_hot)) { $i=0;$j=0; ?>
	<?php foreach ($cate_hot as $k => $cate_parent) { ?>
		<div class="electronic">
			<div class="container-ics">
				<div class="electtronic-content">
					<div class="electronic-header">
						<div class="electronic-header-left">
							<div class="electronic-header-icon">
								<img src="<?= base_url()?>/img/icon-electronic.png" alt="">
							</div>
							<div class="electronic-header-name">
								<h3><?=$cate_parent->name;?></h3>
							</div>
						</div>
						<div class="electronic-header-right">
							<div class="electronic-tab">
								<ul class="nav nav-tabs">
								<?php if(isset($cate_parent->cat_child)) { ?>
									<?php foreach ($cate_parent->cat_child as $k => $cate) { $i++; ?>
										<li><a data-toggle="tab" 
										class="<?php if($i==1){ echo 'active' ;}?>"
										href="#electronic<?=$i;?>"><?=$cate->name;?></a></li>
									<?php } ?>
								<?php } ?>
									
								</ul>
							</div>
						</div>
					</div>
					<div class="electronic-detail">
						<div class="electronic-banner">
							<a href="<?= base_url($cate_parent->description) ?>" title="<?=$cate_parent->name;?>" class="electronic-banner-link">
								<img src="<?= base_url($cate_parent->image) ?>" alt="">
							</a>
						</div>
						<div class="electronic-product tab-content">
							<?php if(isset($cate_parent->cat_child)) { ?>
								<?php foreach ($cate_parent->cat_child as $k => $cate) { $j++; ?>
									<div class="electronic-tab tab-pane fade <?php if($j==1){echo 'in active show';}?> " id="electronic<?=$j;?>">
										<div class="electronic-product-slide custom-btn-slide  ">
										<?php if(isset($cate->pro_cate_child)) { ?>
											<?php foreach ($cate->pro_cate_child as $k => $pro) { ?>
												<div class="electronic-product-item">
													<a href="<?=base_url('san-pham/'.$pro->alias);?>" title="" class="product-electronic-image">
														<img src="<?= base_url('upload/img/products/'.$pro->pro_dir.'/'.$pro->image) ?>" alt="">
														<div class="seller-sale">
															<span class="seller-cap onsale">Sale</span>
														</div>
													</a>
													<div class="product-electronic-description">
														<a href="<?=base_url('san-pham/'.$pro->alias);?>" title="" class="product-electronic-name name-product-current"><?=$pro->name;?></a>
														<div class="product-electronic-rating rating-demo">
															<?php if (isset($pro->rating)) { ?>
																	<?php if(@$pro->rating > 0){ ?>
																		<?php for( $i=0; $i<5; $i++ ){ ?>
																			<?php if( $i <= @$pro->rating) { ?>
																				<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																					<span class="fas fa-star-half-alt"></span>
																				<?php }else{ ?>
																					<span class="fas fa-star checked"></span>
																				<?php } ?>
																			<?php }else{ ?>
																				<span class="far fa-star"></span>
																			<?php } ?>
																		<?php } ?>
																	<?php }else{ ?>
																		<div class="no-rating">
																			<span class="far fa-star"></span>
																			<span class="far fa-star"></span>
																			<span class="far fa-star"></span>
																			<span class="far fa-star" aria-hidden="true"></span>
																			<span class="far fa-star" aria-hidden="true"></span>
																		</div>
																	<?php } ?>
															<?php  }else{?>
																<div class="no-rating">
																	<span class="far fa-star"></span>
																	<span class="far fa-star"></span>
																	<span class="far fa-star"></span>
																	<span class="far fa-star" aria-hidden="true"></span>
																	<span class="far fa-star" aria-hidden="true"></span>
																</div>
															<?php } ?>
														</div>
														<div class="product-electronic-price price-demo">
														<?php if($pro->price >0 ) {?><span class="price-old"><?=number_format($pro->price);?></span><?php } ?><?=number_format($pro->price_sale);?>VND
														</div>
													</div>
												</div>
											<?php }?>
										<?php }?>
											
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
<?php } ?>
