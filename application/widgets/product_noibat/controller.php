<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_noibat_widget extends MY_Widget
{
    // Nhận 2 biến truyền vào
    function index(){
        
		//$this->load->model('f_homemodel');
		
		//san pham mới	
		$data['cate_hot'] = $this->system_model->get_data('product_category',array(
            'lang' => $this->language,
            'parent_id'=>0,
            'home'=>1
        ),array('sort' => 'desc'),null,1,0);
        if(isset($data['cate_hot'])){
            foreach ($data['cate_hot'] as $k => $cat) {
				$data['cate_hot'][$k]->cat_child = $this->system_model->get_data('product_category',array(
					'lang' => $this->language,
					'parent_id'=>$cat->id,
				),array('sort' => 'desc'),null,3,0);
				if(isset($data['cate_hot'][$k]->cat_child)){
					foreach ($data['cate_hot'][$k]->cat_child as $key => $item_child) {
						$data['cate_hot'][$k]->cat_child[$key]->pro_cate_child = $this->f_homemodel->get_products(array(
							'lang' => $this->language,
							'product_to_category.id_category'=> $item_child->id,
							 ),array('sort' => 'desc'),25,0);
						if(isset($item_child->pro_cate_child)){
							foreach ($item_child->pro_cate_child as $keyPro => $value) {
								$data['array_json'] = json_decode($value->config_pro);
								if(!empty($data['array_json'])){
									foreach ($data['array_json'] as $keyJson => $json) {
										if (isset($json->name) && $json->name=="Vope star") {
											$item_child->pro_cate_child[$keyPro]->rating =  $json->content;
										}
										if (isset($json->name) && $json->name=="Chi tiết sản phẩm") {
											$item_child->pro_cate_child[$keyPro]->content =  $json->content;
										}
									}
								}
							}
						
						}
					}
				}
                $data['cate_hot'][$k]->pro_hot = $this->f_homemodel->get_products(array(
                'lang' => $this->language,
                'product_to_category.id_category'=> $cat->id,
				 ),array('sort' => 'desc'),25,0);

				 if(isset($data['cate_hot'][$k]->pro_hot)){
					foreach ($data['cate_hot'][$k]->pro_hot as $keyPro => $value) {
						$data['array_json'] = json_decode($value->config_pro);
						if(!empty($data['array_json'])){
							foreach ($data['array_json'] as $keyJson => $json) {
								if (isset($json->name) && $json->name=="Vope star") {
									$data['cate_hot'][$k]->pro_hot[$keyPro]->rating =  $json->content;
								}
								if (isset($json->name) && $json->name=="Chi tiết sản phẩm") {
									$data['cate_hot'][$k]->pro_hot[$keyPro]->content =  $json->content;
								}
							}
						}
					}
				
				}
			}
		}   
		
	    $this->load->view('view',$data);
    }
}
