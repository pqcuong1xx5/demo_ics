<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_noibat_home_widget extends MY_Widget
{
    // Nhận 2 biến truyền vào
    function index(){
        
		//$this->load->model('f_homemodel');
		
		//san pham mới	
		$data['all_pros'] = $this->system_model->get_data('product',array(
            'hot' => 1,
            'lang' => $this->language,
			),array('sort' => 'desc'));
			$data['pros']=[];
		if(isset($data['all_pros']) && count($data['all_pros'])!=0){
			$count = count($data['all_pros']);
			$i=-1;
			while($count > 0){
				
				$data['pros']['column_'.$i] = array();
				if(isset( $data['all_pros'][$i+1])){
					array_push($data['pros']['column_'.$i], $data['all_pros'][$i+1]);
				}
				if(isset( $data['all_pros'][$i+2])){
					array_push($data['pros']['column_'.$i], $data['all_pros'][$i+2]);
				}
				$i+=2;
				$count-=2;
				
			}
		}

	    $this->load->view('view',$data);
    }
}
