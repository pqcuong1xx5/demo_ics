<?php if(isset($pros)){ $i=0 ;?>
<div class="recently-viewed">
	<div class="container-ics">
		<div class="recently-content">
			<div class="recently-header">
				<h3 class="recently-viewed-name" tooltip="test">Sản phẩm Hot</h3>
			</div>
			<div class="recently-product">
				<div class="recently-slide custom-btn-slide">
					<?php foreach ($pros as $k => $pro_column) { ?>
					<div class="recently-product-column">
						<?php if(isset($pro_column)){ $i=0 ;?>
						<?php foreach ($pro_column as $k => $pro) { ?>
						<div class="recently-product-item">
							<div class="recently-product-image">
								<a href="<?=base_url('san-pham/'.$pro->alias)?>" title="" class="product-recently-link"><img src="<?= base_url('upload/img/products/'.$pro->pro_dir.'/'.$pro->image) ?>" alt=""></a>
							</div>
							<div class="product-recently-description">
								<a href="<?=base_url('san-pham/'.$pro->alias)?>" title="" class="product-recently-name name-product-current"><?=$pro->name?></a>
								<div class="product-recently-rating rating-demo">
									<span class="fas fa-star checked"></span>
									<span class="fas fa-star checked"></span>
									<span class="fas fa-star-half-alt"></span>
									<span class="far fa-star" aria-hidden="true"></span>
									<span class="far fa-star" aria-hidden="true"></span>
								</div>
								<div class="product-recently-price price-demo">
								<?=number_format($pro->price_sale);?>VND
								</div>
							</div>
						</div>
						<?php } ?>
						<?php } ?>
						
					</div>
					<?php $i++; } ?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
