<article id="body_home">
   <div class="container">
      <div class="row_pc">
         <div class="row">
            <div class="col-md-3 col-sm-12 hidden-xs hidden-sm">
               <div class="qts_left_content">
                  <?php if (count($cate_all)) : ?>
                  <div class="content_right">
                     <h2 class="title"> 
                        danh mục sản phẩm 
                     </h2>
                     <ul class="list-cate">
                        <?php foreach ($cate_all as $cate) : ?>
                        <li class="ul-item">
                           <h3 class="cate-lv1"> 
                              <a href="<?=base_url('danh-muc/'.$cate->alias.'.html')?>" title=""><?=$cate->name?></a>
                           </h3>
                           <?php if (count($cate->cat_sub)) : ?>
                           <ul class="box_sub">
                              <?php foreach ($cate->cat_sub as $sub) : ?>
                                 <li><a href="<?=base_url('danh-muc/'.$sub->alias.'.html')?>" title=""><?=$sub->name?></a></li>
                              <?php endforeach ?>
                           </ul>
                           <?php endif ?>
                        </li>
                        <?php endforeach ?>
                     </ul>
                  </div>
                  <div class="clearfix"></div>
                  <?php endif ?>

                  <?php if (count($pros)) : ?>
                  <div class="full_box_sp_news ">
                     <div class="content_right">
                        <h2 class="title">
                           <span>Sản phẩm nổi bật</span>
                        </h2>
                        <div class="box_sp_news">
                           <ul class="product_list_widget">
                              <?php $i=0; foreach ($pros as $pro) : $i++; ?>
                              <li>
                                 <a href="<?= base_url('san-pham/' . $pro->alias . '.html') ?>" title="KF-541V">
                                 <img src="<?= base_url('upload/img/products/' . $pro->pro_dir . '/thumbnail_2_' . @$pro->image) ?>"
                                    alt="<?= $pro->name ?>">
                                 <span class="product-title"><?= $pro->name ?></span>
                                 </a>
                                 <span class="woocommerce-Price-amount amount">
                                 <?php if ($pro->price_sale != 0) {
                                   echo number_format($pro->price_sale).'đ';
                                  } else {echo '<span class="woocommerce-Price-currencySymbol">Giá: liên hệ</span>
                                 </span>'; } ?>
                              </li>
                              <?php endforeach; ?>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <?php endif; ?>

                  <div class="content_right">
                     <div class="cleafix-20"></div>
                     <div class="title">
                        Fanpage FACEBOOK
                     </div>
                     
                        <?=@$this->load->widget('fanpage2')?>
                     
                  </div>
                  <div class="clearfix-15"></div>
                  <?php if (count($news)) : ?>
                  <div class="full_box_sp_news">
                     <div class="content_right">
                        <h2 class="title">
                           <span>Tin tức nổi bật</span>
                        </h2>
                        <div class="box_sp_news">
                           <ul class="product_list_widget">
                              <?php foreach ($news as $n) : ?>
                              <li>
                                 <a href="<?=base_url('new/'.$n->alias.'.html')?>">
                                 <img src="<?=base_url($n->image)?>">
                                 <span class="product-title"> <?=$n->title?></span>
                                 </a>
                              </li>
                              <?php endforeach ?>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <?php endif; ?>
                  <?php if (count($supports)) : ?>
                  <div class="support_onl">
                     <div class="content_right">
                        <h2 class="title">hỗ trợ trực tuyến</h2>
                     </div>
                     <div class="box_support">
                        <div class="img_tt_supp"><img src="<?=base_url()?>img/img_sup.png" class="w_100" title="" alt=""></div>
                        <p class="text-center phone_sup">Hotline: <a href="tel:<?=@$this->option->hotline1?>" title="" class=""><?=@$this->option->hotline1?></a></p>
                        <div class="info_connect clearfix">
                           <?php foreach ($supports as $support) : ?>
                           <div class="line_1 clearfix">
                              <a href="<?=$support->skype?>" class="btn_sup pull-left" title=""><img src="<?=base_url()?>img/i_skype.png" title="" class="w_100" alt=""></a>
                              <a href="<?=$support->yahoo?>" class="btn_sup pull-left" title=""><img src="<?=base_url()?>img/i_zalo.png" title="" class="w_100" alt=""></a>
                              <div class="support_num pull-left">
                                 <span><?= $support->name;?></span>
                                 <p>SĐT:<a href="tel:<?= $support->phone;?>" class="" title=""><?= $support->phone;?></a></p>
                              </div>
                           </div>
                           <?php endforeach; ?>
                        </div>
                     </div>
                  </div>
                  <?php endif ?>
                  <div class="content_right">
                     <div class="cleafix-20"></div>
                     <div class="title">
                        thống kê truy cập
                     </div>
                     <div class="cover_connect">
                        <div class="clearfix-15 clearfix"></div>
                        <div class="text_connect">
                           <div><span> Hôm nay: <?=today()?></span></div>
                           <div><span> Hôm qua: <?=yesterday()?></span></div>
                           <div><span> Tổng lượt truy cập: <?=total()?></span></div>
                        </div>
                        <div class="clearfix-10 clearfix"></div>
                        <div class="text-center">
                           <p>Số người đang online: <?=online()?></p>
                           <p><span>Online Counter</span></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class=" col-md-9 col-sm-12">
               <?php if (count($cate_home)) : ?>
               <?php foreach ($cate_home as $cate) : ?>
               <div class="qts_content_home qts_box_product">
                  <h2 class="title_home"><a href="<?=base_url('danh-muc/'.$cate->alias.'.html')?>"><?=$cate->name?></a></h2>
                  <div class="clearfix"></div>
                  <div class="row_2">
                     <?php if (count($cate->pro)) : ?>
                     <?php foreach ($cate->pro as $pro) : ?>
                     <!-- begin tem pro home -->
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                        <div class="row_13">
                           <!-- begin sub temp pro -->
                           <div class="full_box_sp">
                              <div class="img_box_sp h_7488">
                                 <a href="<?=base_url('san-pham/'.$pro->alias.'.html')?>"><img 
                                    src="<?=base_url('upload/img/products/'.$pro->pro_dir.'/thumbnail_2_'.@$pro->image)?>"
                                    class="w_100" alt="<?=$pro->name?>"/></a>
                              </div>
                              <div class="text_box_sp">
                                 <h3 class="name_sp">
                                    <a href="<?=base_url('san-pham/'.$pro->alias.'.html')?>"><?=$pro->name?></a>
                                 </h3>
                                 <p>
                                    <?php if ($pro->price_sale != 0) {
                                      echo number_format($pro->price_sale).'đ';
                                    
                                     } else {echo "Liên hệ"; } ?>
                                 </p>
                                 <a title="" href="<?=base_url('addCart_now?id='.$pro->id)?>" class="btn buy-prod" >Mua hàng</a>
                              </div>
                           </div>
                           <!-- end sub temp pro -->
                        </div>
                     </div>
                     <!-- end tem pro home -->
                     <?php endforeach; ?>
                     <?php endif ?>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <?php endforeach; ?>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </div>
</article>
<div class="clearfix-20"></div>