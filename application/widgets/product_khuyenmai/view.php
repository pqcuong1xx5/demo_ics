<div class="product-hot">
	<div class="container-ics">
		<div class="product-hot-content">
				<?php if(isset($cate_hot)){ ?>
					<?php foreach ($cate_hot as $key => $cate_parent) { ?>
						<div class="product-sale-left">
							<div class="product-sale-left-detail">
								<div class="product-sale-title">
										<h3 class="product-sale-title-name"><?=$cate_parent->name;?></h3>
								</div>
								<div class="product-sale-content">
									<div class="product-sale-slide">
										<?php if(isset($cate_parent->pro_hot)){ ?>
											<?php foreach ($cate_parent->pro_hot as $keys => $pro_parent) { ?>
											<div class="product-sale-item">
												<a href="<?= base_url('san-pham/'.$pro_parent->alias)?>" title="" class="product-sale-img">
														<img src="<?= base_url('upload/img/products/'.$pro_parent->pro_dir.'/'.$pro_parent->image) ?>" alt="">

												</a>
												<div class="product-sale-description">
														<a href="<?= base_url('san-pham/'.$pro_parent->alias)?>" title="" class="product-sale-name name-product-current"><?=$pro_parent->name;?></a>
														<div class="product-sale-rating rating-demo">
														<?php if (isset($thuoctinh)) {
															foreach ($thuoctinh as $key=>$v) { 
																if($v->type=='int'){
																?>
																	<?php if(@$v->content && @$v->content > 0){ ?>
																		<?php for( $i=0; $i<5; $i++ ){ ?>
																			<?php if( $i <= @$v->content) { ?>
																				<?php if( $i == floor(@$v->content) &&  @$v->content-$i !=0 ) { ?>
																					<span class="fas fa-star-half-alt"></span>
																				<?php }else{ ?>
																					<span class="fas fa-star checked"></span>
																				<?php } ?>
																			<?php }else{ ?>
																				<span class="far fa-star"></span>
																			<?php } ?>
																		<?php } ?>
																	<?php }else{ ?>
																		<div class="no-rating">
																			<span class="far fa-star"></span>
																			<span class="far fa-star"></span>
																			<span class="far fa-star"></span>
																			<span class="far fa-star" aria-hidden="true"></span>
																			<span class="far fa-star" aria-hidden="true"></span>
																		</div>
																	<?php } ?>
															<?php  } } }?>
														</div>
														<div class="product-sale-price price-demo">
															<span class="price-old"><?=number_format($pro_parent->price);?>VND</span> <?=number_format($pro_parent->price_sale);?>VND
														</div>
														<div class="product-sale-time">
															<div class="time-day">736 <br><span>Days</span> </div>
															<div class="time-day">14 <br><span>HOURS</span></div>
															<div class="time-day">12 <br><span>MINS</span></div>
															<div class="time-day">36 <br><span>SECS</span></div>
														</div>
												</div>
											</div>
											<?php } ?>
										<?php } ?>
										
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
				<?php if(isset($cate_hot)) { $i=0;$j=0; ?>
					<?php foreach ($cate_hot as $k => $cate_parent) {?>
						<div class="product-hot-category">
							<div class="product-hot-header">
								<div class="product-category-tab">
										<ul class="nav nav-tabs ">
											<?php if(isset($cate_parent->cat_child)) {  ?>
												<?php foreach ($cate_parent->cat_child as $k => $cate) { $i++;?>
													<li><a data-toggle="tab" class=" <?php if($i==1){ echo 'active';} ?>" href="#hot-cate<?=$i;?>"><?=$cate->name;?></a></li>
												<?php } ?>
											<?php } ?>
										</ul>
								</div>
								<div class="tab-content product-tab-content">
										<?php if(isset($cate_parent->cat_child)) {  ?>
											<?php foreach ($cate_parent->cat_child as $k => $cate) { $j++;?>
												<div id="hot-cate<?=$j;?>" class="tab-pane fade <?php if($j==1){echo 'in active show';} ?>">
													<?php if(isset($cate->pro_cate_child)){ ?>
													<div class=" product-best-seller custom-btn-slide">
														<?php foreach ($cate->pro_cate_child as $key => $pros) { ?>
														<div class="product-seller-item">
																<a href="<?= base_url('san-pham/'.$pros->alias)?>" title="" class="product-seller-image">
																	<img src="<?= base_url('upload/img/products/'.$pros->pro_dir.'/'.$pros->image) ?>" alt="">
																	<?php if($pros->price - $pros->price_sale >0) { ?>
																	<div class="seller-sale">
																		<span class="seller-cap onsale">Sale</span>
																	</div>
																	<?php } ?>
																</a>
																<div class="product-seller-description">
																	<a href="<?= base_url('san-pham/'.$pros->alias)?>" title="" class="product-seller-name name-product-current"><?=$pros->name;?></a>
																	<div class="product-seller-rating rating-demo">
																		<?php if (isset($pros->rating)) { ?>
																			<?php if(@$pros->rating > 0){ ?>
																				<?php for( $i=0; $i<5; $i++ ){ ?>
																					<?php if( $i <= @$pros->rating) { ?>
																						<?php if( $i == floor(@$pros->rating) &&  @$pros->rating-$i !=0 ) { ?>
																							<span class="fas fa-star-half-alt"></span>
																						<?php }else{ ?>
																							<span class="fas fa-star checked"></span>
																						<?php } ?>
																					<?php }else{ ?>
																						<span class="far fa-star"></span>
																					<?php } ?>
																				<?php } ?>
																			<?php }else{ ?>
																				<div class="no-rating">
																					<span class="far fa-star"></span>
																					<span class="far fa-star"></span>
																					<span class="far fa-star"></span>
																					<span class="far fa-star" aria-hidden="true"></span>
																					<span class="far fa-star" aria-hidden="true"></span>
																				</div>
																			<?php } ?>
																		<?php  }else{?>
																			<div class="no-rating">
																				<span class="far fa-star"></span>
																				<span class="far fa-star"></span>
																				<span class="far fa-star"></span>
																				<span class="far fa-star" aria-hidden="true"></span>
																				<span class="far fa-star" aria-hidden="true"></span>
																			</div>
																		<?php } ?>
																	</div>
																	<div class="product-seller-price price-demo">
																		<?php if($pros->price >0 ) {?><span class="price-old"><?=number_format($pros->price);?></span><?php } ?><?=number_format($pros->price_sale);?>VND
																	</div>
																</div>
														</div>
														<?php } ?>
														
													</div>
													<?php } ?>
												</div>
											<?php } ?>
										<?php } ?>
								</div>
							</div>
							<div class="product-hot-bottom">
								<div class="product-hot-banner">
										<a href="" title="" class="hot-banner-link"><img src="<?= base_url()?>/img/seller-banner1.jpg" alt=""></a>
								</div>
								<div class="product-hot-banner">
										<a href="" title="" class="hot-banner-link"><img src="<?= base_url()?>/img/seller-banner2.jpg" alt=""></a>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
		</div>
	</div>
</div>
