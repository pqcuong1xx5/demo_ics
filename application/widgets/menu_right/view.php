<div class="vertical-menu-left">
	<div class="vertical-menu-heading"><i class="fas fa-th-list"></i><span>DANH MUC SAN PHAM</span>
	</div>
	<?php if(isset($menu_category)) : ?>
	<div class="vertical-menu-list">
		<ul class="vertical-menu-parent">
			<?php foreach ($menu_category as $key_r => $mr) : ?>
			<li class="vertical-menu-item">
				<a href="<?=base_url($mr->url);?>" title=""><?=$mr->name;?></a>
			</li>
			<?php endforeach;?>
		</ul>
	</div>

	<?php endif;?>
</div>
			 