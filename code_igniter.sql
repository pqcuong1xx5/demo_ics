-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 03, 2019 at 10:25 PM
-- Server version: 5.5.31
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sofabl0619_sofa`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access` text CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `user_id`, `access`) VALUES
(1, 11, '{\"product\":[\"products\",\"add\",\"edit\",\"delete\",\"categories\",\"cat_add\",\"cat_edit\",\"cat_delete\"]}'),
(2, 12, '{\"menu\":[\"menulist\",\"add\",\"edit\",\"delete\"]}'),
(3, 2, '{\"product\":[\"products\",\"category_pro\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\",\"delete_once_comment\",\"delete_once_question\"],\"news\":[\"newslist\",\"categories\",\"tagsnews\",\"addnews\",\"delete_once_news\",\"cat_add_news\",\"del_catnews_once\"],\"menu\":[\"addmenu\",\"menulist\",\"delete\"],\"imageupload\":[\"banners\",\"addbanner\",\"delete_Banner_once\"],\"pages\":[\"pagelist\",\"addpage\",\"delete_page_once\"],\"contact\":[\"contacts\",\"delete\"],\"support\":[\"listSuport\",\"add\",\"delete_support_once\"],\"admin\":[\"site_option\",\"maps\",\"store_shopping\"]}'),
(4, 1, '{\"inuser\":[\"categories\",\"cate_add\",\"delete_cat_once\"],\"media\":[\"listAll\",\"categories\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\"],\"video\":[\"listAll\",\"category_video\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\"],\"product\":[\"products\",\"category_pro\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\",\"delete_once_comment\",\"delete_once_question\"],\"order\":[\"orders\",\"listSale\",\"listProvince\",\"delete_once_orders\",\"addSale\",\"del_once_sale\"],\"attribute\":[\"listBrand\",\"listLocale\",\"listColor\",\"listprice\",\"listOption\",\"addbrand\",\"delete_brand_once\",\"addlocale\",\"delete_locale_once\",\"addcolor\",\"delete_color_once\",\"addprice\",\"delete_price_once\",\"addoption\",\"delete_option_once\"],\"news\":[\"newslist\",\"categories\",\"tagsnews\",\"addnews\",\"delete_once_news\",\"cat_add_news\",\"del_catnews_once\"],\"tag\":[\"tag\"],\"menu\":[\"addmenu\",\"menulist\",\"delete\"],\"comment\":[\"comments\",\"questions\"],\"imageupload\":[\"banners\",\"addbanner\",\"delete_Banner_once\"],\"pages\":[\"pagelist\",\"addpage\",\"delete_page_once\"],\"contact\":[\"contacts\",\"delete\"],\"raovat\":[\"listraovat\",\"categories\",\"tagtinrao\",\"add\",\"delete_raovat_once\",\"cat_add\",\"del_cattinrao_once\"],\"email\":[\"emails\",\"delete\"],\"support\":[\"listSuport\",\"add\",\"delete_support_once\"],\"users\":[\"listuser_admin\",\"listusers\",\"add_users\",\"smslist\"],\"admin\":[\"site_option\",\"maps\",\"store_shopping\",\"setup_product\"],\"province\":[\"listDistric\",\"listWard\",\"street\"],\"report\":[\"soldout\",\"bestsellers\"]}'),
(5, 580, '{\"admin\":[\"\",\"site_option\",\"inuser\",\"comment\",\"email\",\"contact\"],\"users\":[\"delete\"],\"order\":[\"orders\",\"Deleteeorder\"],\"support\":[\"add\",\"edit\",\"x\\u00f3a\"],\"product\":[\"products\",\"add\",\"edit\",\"delete\",\"categories\",\"cat_add\",\"cat_edit\",\"listCodeSale\",\"cat_delete\",\"images\"],\"news\":[\"newslist\",\"add\",\"edit\",\"delete\",\"categories\",\"cat_add\",\"cat_edit\",\"delete_cat\"],\"pages\":[\"pagelist\",\"add\",\"edit\",\"delete\",\"action\"],\"menu\":[\"menulist\",\"add\",\"edit\",\"delete\"]}'),
(6, 612, '{\"media\":[\"listAll\",\"categories\"],\"video\":[\"listAll\",\"category_video\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\"]}'),
(7, 636, '{\"product\":[\"products\",\"category_pro\",\"add\",\"delete_once\",\"cat_add\",\"del_cat_once\",\"delete_once_comment\",\"delete_once_question\"],\"order\":[\"orders\",\"listSale\",\"listProvince\",\"delete_once_orders\",\"addSale\",\"del_once_sale\"],\"news\":[\"newslist\",\"categories\",\"tagsnews\",\"addnews\",\"delete_once_news\",\"cat_add_news\",\"del_catnews_once\"],\"users\":[\"listuser_admin\",\"listusers\",\"delete_users_once\",\"add_users\"],\"admin\":[\"site_option\",\"maps\",\"store_shopping\"]}');

-- --------------------------------------------------------

--
-- Table structure for table `alias`
--

CREATE TABLE `alias` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` char(50) CHARACTER SET utf8 DEFAULT NULL,
  `item_id` int(11) DEFAULT '0',
  `new_cat` int(11) DEFAULT '0',
  `new` int(11) DEFAULT '0',
  `pro_cat` int(11) DEFAULT '0',
  `pro` int(11) DEFAULT '0',
  `page` int(11) DEFAULT '0',
  `m_cat` int(11) DEFAULT '0',
  `media` int(11) DEFAULT '0',
  `locale` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `inuser` int(11) DEFAULT NULL,
  `video_cat` int(11) DEFAULT NULL,
  `video` int(11) DEFAULT NULL,
  `services_cat` int(11) DEFAULT NULL,
  `services` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `alias`
--

INSERT INTO `alias` (`id`, `alias`, `type`, `item_id`, `new_cat`, `new`, `pro_cat`, `pro`, `page`, `m_cat`, `media`, `locale`, `brand`, `inuser`, `video_cat`, `video`, `services_cat`, `services`) VALUES
(2247, 'sofa-khach-san', 'cate-pro', 0, 0, 0, 6, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2356, 'sofa-karaoke-ma-sk812', 'pro', 0, 0, 0, 0, 80, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2355, 'sofa-karaoke-ma-sk811', 'pro', 0, 0, 0, 0, 79, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2246, 'sofa-gia-dinh', 'cate-pro', 0, 0, 0, 5, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2248, 'sofa-cafe', 'cate-pro', 0, 0, 0, 7, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2250, 'thanh-ly-sofa-karaoke', 'cate-pro', 0, 0, 0, 9, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2332, 'sofa-cao-cap-ma-cp511', 'pro', 0, 0, 0, 0, 56, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2252, 'sofa-cao-cap', 'cate-pro', 0, 0, 0, 11, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2253, 'sofa-gia-re', 'cate-pro', 0, 0, 0, 12, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2254, 'sofa-da', 'cate-pro', 0, 0, 0, 13, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2285, 'sofa-gia-dinh-ma-gd215', 'pro', 0, 0, 0, 0, 23, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2301, 'tin-tuc', 'cate-new', 0, 4, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2327, 'sofa-khach-san-ma-ks202', 'pro', 0, 0, 0, 0, 51, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2328, 'sofa-gia-dinh-ma-gd101', 'pro', 0, 0, 0, 0, 52, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2357, 'sofa-karaoke-ma-sk813', 'pro', 0, 0, 0, 0, 81, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2289, 'sofa-cao-cap-ma-cp515', 'pro', 0, 0, 0, 0, 27, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2290, 'sofa-khach-san-ma-ks207', 'pro', 0, 0, 0, 0, 28, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2291, 'sofa-khach-san-ma-ks206', 'pro', 0, 0, 0, 0, 29, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2292, 'sofa-khach-san-ma-ks205', 'pro', 0, 0, 0, 0, 30, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2293, 'sofa-gia-dinh-ma-gd111', 'pro', 0, 0, 0, 0, 31, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2294, 'sofa-gia-dinh-ma-gd109', 'pro', 0, 0, 0, 0, 32, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2358, 'sofa-karaoke-ma-sk814', 'pro', 0, 0, 0, 0, 82, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2305, 'gioi-thieu', 'page', 0, 0, 0, 0, 0, 31, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2326, 'sofa-karaoke-ma-sk822', 'pro', 0, 0, 0, 0, 50, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2307, 'sofia', 'pro', 0, 0, 0, 0, 36, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2325, 'sofa-da-ma-sd311', 'pro', 0, 0, 0, 0, 49, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2308, 'sofa-khach-san-ma-ks203', 'pro', 0, 0, 0, 0, 37, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2324, 'sofa-ni-gia-dinh-ma-gd102', 'pro', 0, 0, 0, 0, 48, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2322, 'sofa-gia-dinh-ma-gd104', 'pro', 0, 0, 0, 0, 46, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2323, 'sofa-ni-gia-dinh-ma-gd103', 'pro', 0, 0, 0, 0, 47, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2312, 'sofa-gia-dinhs', 'pro', 0, 0, 0, 0, 38, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2313, 'sofa-gia-dinh-ma-gd106', 'pro', 0, 0, 0, 0, 39, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2314, 'sofa-gia-dinh-ma-gd105', 'pro', 0, 0, 0, 0, 40, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2315, 'sofa-khach-san-ma-ks201', 'pro', 0, 0, 0, 0, 41, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2317, 'sofa-vang-7', 'pro', 0, 0, 0, 0, 42, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2319, 'sofa-karaoke-ma-sk823', 'pro', 0, 0, 0, 0, 44, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2329, 'sofa-gia-dinh-ma-gd-112', 'pro', 0, 0, 0, 0, 53, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2330, 'sofa-gia-dinh-ma-gd108', 'pro', 0, 0, 0, 0, 54, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2331, 'sofa-khach-san-ma-ks208', 'pro', 0, 0, 0, 0, 55, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2333, 'sofa-cao-cap-ma-cp512', 'pro', 0, 0, 0, 0, 57, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2334, 'sofa-cao-cap-ma-cp-513', 'pro', 0, 0, 0, 0, 58, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2335, 'sofa-cao-cap-ma-cp516', 'pro', 0, 0, 0, 0, 59, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2336, 'sofa', 'pro', 0, 0, 0, 0, 60, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2337, 'sofa-cao-cap-ma-cp518', 'pro', 0, 0, 0, 0, 61, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2338, 'sofa-cao-cap-ma-cp519', 'pro', 0, 0, 0, 0, 62, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2340, 'sofa-da-ma-sd312', 'pro', 0, 0, 0, 0, 64, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2341, 'sofa-da-ma-sd313', 'pro', 0, 0, 0, 0, 65, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2342, 'sofa-da-ma-sd314', 'pro', 0, 0, 0, 0, 66, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2343, 'sofa-da-ma-sd315', 'pro', 0, 0, 0, 0, 67, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2344, 'sofa-da-ma-sd316', 'pro', 0, 0, 0, 0, 68, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2345, 'sofa-cafe-ma-sc611', 'pro', 0, 0, 0, 0, 69, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2346, 'sofa-cafe-ma-sc612', 'pro', 0, 0, 0, 0, 70, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2347, 'sofa-cafe-ma-sd613', 'pro', 0, 0, 0, 0, 71, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2348, 'sofa-cafe-ma-sc614', 'pro', 0, 0, 0, 0, 72, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2349, 'sofa-cafe-ma-sc615', 'pro', 0, 0, 0, 0, 73, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2350, 'sofa-cafe-ma-sc616', 'pro', 0, 0, 0, 0, 74, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2351, 'sofa-cafe-ma-sc617', 'pro', 0, 0, 0, 0, 75, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2352, 'sofa-cafe-ma-sc618', 'pro', 0, 0, 0, 0, 76, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2353, 'sofa-cafe-ma-sc619', 'pro', 0, 0, 0, 0, 77, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2354, 'sofa-cafe-ma-sc621', 'pro', 0, 0, 0, 0, 78, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2359, 'sofa-karaoke-ma-sk815', 'pro', 0, 0, 0, 0, 83, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2360, 'sofa-karaoke-ma-sk816', 'pro', 0, 0, 0, 0, 84, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2361, 'sofa-karaoke-ma-sk817', 'pro', 0, 0, 0, 0, 85, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2362, 'sofa-karaoke-ma-sk818', 'pro', 0, 0, 0, 0, 86, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2363, 'sofa-karaoke-ma-sk819', 'pro', 0, 0, 0, 0, 87, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2364, 'sofa-karaoke-ma-sk821', 'pro', 0, 0, 0, 0, 88, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2365, 'luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'new', 0, 0, 11, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2366, 'nhung-luu-y-cuc-quan-trong-khi-chon-sofa-cho-gia-dinh', 'new', 0, 0, 12, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2367, 'chon-sofa-cho-phong-karaoke-sao-cho-dung-cach', 'new', 0, 0, 13, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2368, 'nhung-luu-y-khong-the-bo-qua-khi-ban-muon-mua-sofa-gia-re', 'new', 0, 0, 14, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `code_sale`
--

CREATE TABLE `code_sale` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `code_sale`
--

INSERT INTO `code_sale` (`id`, `name`, `code`, `price`, `active`) VALUES
(10, 'Noel', 'ADCVX', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `comment` text CHARACTER SET utf8,
  `reply` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `review` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments_binhluan`
--

CREATE TABLE `comments_binhluan` (
  `id` int(11) NOT NULL,
  `id_sanpham` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `giatri` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `date` date NOT NULL,
  `flg` int(1) NOT NULL DEFAULT '0' COMMENT '0: moi binh luan; 1: xac nhan de hien thi',
  `reply` int(11) DEFAULT NULL,
  `review` tinyint(1) DEFAULT '0',
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments_binhluan`
--

INSERT INTO `comments_binhluan` (`id`, `id_sanpham`, `comment`, `giatri`, `userid`, `parent_id`, `time`, `date`, `flg`, `reply`, `review`, `user_name`, `user_email`, `lang`) VALUES
(1, 5, 'nội dung đánh giá sản phẩm này rất tốt', 5, 0, 0, 1505698798, '2017-09-18', 0, 0, 1, 'trần mạnh', 'dangtranmanh051187@gmail.com', '1'),
(2, 5, 'nội dung bình luận', 0, 0, 0, 1505698841, '2017-09-18', 0, 0, 1, 'trần mạnh', 'dangtranmanh051187@gmail.com', '1'),
(3, 5, 'noi dung binh luận đánh giá nhận xét', 0, 0, 0, 1505699713, '2017-09-18', 0, 0, 0, 'trần mạnh', 'dangtranmanh051187@gmail.com', '1'),
(4, 5, 'nội dung bình luận tiếp theo', 4, 0, 0, 1505699941, '2017-09-18', 0, 0, 1, 'trần mạnh', 'dangtranmanh051187@gmail.com', '1'),
(5, 5, 'bình luận của vũ', 0, 0, 0, 1505700184, '2017-09-18', 0, 0, 1, 'trần long vũ', 'dangtranmanh051187@gmail.com', '1'),
(6, 5, 'bình luận của vũ', 0, 0, 0, 1505700223, '2017-09-18', 0, 0, 1, 'trần long vũ', 'dangtranmanh051187@gmail.com', '1'),
(7, 5, 'binh luận mới', 2, 0, 0, 1505700317, '2017-09-18', 0, 0, 1, 'tiến đạt', 'nguyentiendat@gmail.com', '1'),
(8, 5, 'noi dung binh luận', 1, 0, 0, 1505702973, '2017-09-18', 0, 0, 1, 'công sáng', 'congsang@gmail.com', '1'),
(9, 5, 'bình luận tiếp theo', 5, 0, 0, 1505703111, '2017-09-18', 0, 0, 1, 'công sáng', 'congsang@gmail.com', '1'),
(10, 5, 'noi trung tra loi binh luan', 4, 0, 0, 1505721191, '0000-00-00', 0, 7, 1, 'cong sangs', 'congsang@gmail.com', '1'),
(11, 4, 'Tốt', 5, 0, 0, 1505981714, '2017-09-21', 0, 0, 1, 'Vân', 'buivananh.th@gmail.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `config_banner`
--

CREATE TABLE `config_banner` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `field` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_banner`
--

INSERT INTO `config_banner` (`id`, `type`, `name`, `field`, `active`) VALUES
(1, '0', 'banner trang chủ', 'banner', '0'),
(2, '0', 'Slide', 'slide', '1'),
(3, '0', 'banner trái', 'left', '0'),
(4, '0', 'Banner phải', 'right', '0'),
(5, '0', 'banner top', 'top', '1'),
(6, '0', 'banner bottom', 'bottom', '0'),
(7, '0', 'Đối tác', 'partners', '1'),
(8, '1', 'colum danh mục', 'danhmuc', '0');

-- --------------------------------------------------------

--
-- Table structure for table `config_checkpro`
--

CREATE TABLE `config_checkpro` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `field` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `color` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_checkpro`
--

INSERT INTO `config_checkpro` (`id`, `type`, `name`, `field`, `color`, `active`) VALUES
(1, 'product', 'sản phẩm mới', 'hot', 'd73925', '0'),
(2, 'product', 'Trang chủ', 'home', '008d4c', '1'),
(3, 'product', 'sp khuyến mại', 'coupon', 'f39c12', '1'),
(4, 'product', 'sản phẩm nổi bật', 'focus', 'd352d4', '1'),
(5, 'product_category', 'Trang chủ', 'home', 'd73925', '1'),
(6, 'product_category', 'giao diện 2', 'hot', '008d4c', '1'),
(7, 'product_category', 'Nổi bật', 'focus', 'c3c3c3', '0'),
(8, 'product_category', 'Đặc biệt', 'coupon', 'd352d4', '0'),
(9, 'news', 'Trang chủ', 'home', 'd73925', '1'),
(10, 'news', 'Tin nổi bật', 'focus', '008d4c', '1'),
(11, 'news', 'Tin tức', 'hot', '4e8e94', '1'),
(12, 'news_category', 'Trang chủ', 'home', 'd73925', '1'),
(13, 'news_category', 'Tin công ty', 'hot', '4e8e94', '1'),
(14, 'news_category', 'Nổi bật', 'focus', 'c3c3c3', '0'),
(15, 'news_category', 'Danh mục bên trái', 'coupon', '0098da', '0'),
(16, 'media', 'Trang chủ', 'home', 'd73925', '1'),
(17, 'media', 'nổi bật', 'focus', '008d4c', '1'),
(18, 'media', 'Đặc biệt', 'hot', 'c3c3c3', '1'),
(19, 'media_category', 'Trang chủ', 'home', 'd73925', '0'),
(20, 'media_category', 'Mới', 'hot', '008d4c', '1'),
(21, 'media_category', 'Nổi bật', 'focus', 'c3c3c3', '0'),
(22, 'media_category', 'Cột trái', 'coupon', 'd352d4', '0'),
(23, 'video', 'Trang chủ', 'home', 'd73925', '0'),
(24, 'video', 'nổi bật', 'focus', '008d4c', '0'),
(25, 'video', 'Đặc biệt', 'hot', 'c3c3c3', '0'),
(26, 'video_category', 'Trang chủ', 'home', 'd73925', '1'),
(27, 'video_category', 'Mới', 'hot', '008d4c', '1'),
(28, 'video_category', 'Nổi bật', 'focus', 'c3c3c3', '0'),
(29, 'staticpage', 'Trang chủ', 'home', 'd73925', '1'),
(30, 'staticpage', 'Liên hệ', 'focus', '008d4c', '0'),
(31, 'staticpage', 'Đặc biệt', 'hot', 'c3c3c3', '0'),
(32, 'raovat', 'Trang chủ', 'home', 'd73925', '1'),
(33, 'raovat', 'nổi bật', 'focus', '008d4c', '1'),
(34, 'raovat', 'Đặc biệt', 'hot', 'c3c3c3', '1'),
(35, 'raovat_category', 'Trang chủ', 'home', 'd73925', '1'),
(36, 'raovat_category', 'Mới', 'hot', '008d4c', '1'),
(37, 'raovat_category', 'Nổi bật', 'focus', 'c3c3c3', '1'),
(38, 'product_category', 'Ảnh đại diện', 'image', '', '1'),
(39, 'news_category', 'ảnh danh mục news', 'image', NULL, '1'),
(40, 'staticpage', 'ảnh nội dung', 'image', NULL, '1'),
(41, 'video_category', 'ảnh danh mục video', 'image', NULL, '1'),
(42, 'media_category', 'ảnh danh mục media', 'image', NULL, '1'),
(43, 'product', 'giá cũ', 'price', NULL, '1'),
(44, 'product', 'giá bán', 'price_sale', NULL, '1'),
(45, 'product', 'thẻ tags', 'tags', NULL, '0'),
(46, 'hotline', 'Hiện thị hotline', 'hotline', '0', '1'),
(47, 'hotline', 'Chát facebook', 'chat_fanpage', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `config_menu`
--

CREATE TABLE `config_menu` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `field` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_menu`
--

INSERT INTO `config_menu` (`id`, `type`, `name`, `field`, `active`) VALUES
(2, 'top', 'menu đại', NULL, '0'),
(3, 'left', 'menu left', NULL, '0'),
(4, 'right', 'menu right', NULL, '0'),
(5, 'bottom', 'Menu bottom', NULL, '1'),
(6, 'tag', 'menu tag', NULL, '0'),
(7, 'bottom_2', 'menu bottom 2', NULL, '0'),
(8, 'bottom_3', 'menu bottom 3', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `config_wiget`
--

CREATE TABLE `config_wiget` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `field` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_wiget`
--

INSERT INTO `config_wiget` (`id`, `type`, `name`, `field`, `active`) VALUES
(1, NULL, 'banner trang chủ', 'banner', '1'),
(2, NULL, 'slide', 'slide', '1'),
(3, NULL, 'banner trái', 'left', '0'),
(4, NULL, 'Banner phải', 'right', '0'),
(5, NULL, 'banner top', 'top', '0'),
(6, NULL, 'banner bottom', 'bottom', '0'),
(7, NULL, 'đối tác', 'partners', '1');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `comment` text CHARACTER SET utf8,
  `mark` tinyint(1) DEFAULT '0',
  `show` tinyint(1) DEFAULT '0',
  `time` int(11) DEFAULT NULL,
  `cat_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `full_name`, `phone`, `email`, `address`, `city`, `country`, `comment`, `mark`, `show`, `time`, `cat_name`, `title`) VALUES
(29, 'Jones', '416-385-3200', 'eric@talkwithcustomer.com', '', NULL, NULL, 'Hello sofabaolam.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website sofabaolam.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website sofabaolam.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you\'d like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=sofabaolam.com\r\n', 0, 0, 1562313579, NULL, NULL),
(30, 'Jones', '416-385-3200', 'eric@talkwithcustomer.com', '', NULL, NULL, 'Hello sofabaolam.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website sofabaolam.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website sofabaolam.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you\'d like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=sofabaolam.com\r\n', 0, 0, 1562408543, NULL, NULL),
(31, 'WilliamJam', '277241778', 'raphaeaccogeondess@gmail.com', 'https://www.google.com', NULL, NULL, 'Good day!  sofabaolam.com \r\n \r\nWe make available \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the Communication partition. Contact form are filled in by our software and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This method increases the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161', 0, 0, 1564187790, NULL, NULL),
(32, 'BusinessCapitalAdvisors', '84458992372', 'noreply@business-capital-advisors.com', 'http://Business-Capital-Advisors.com', NULL, NULL, 'Hi, letting you know that http://Business-Capital-Advisors.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://Business-Capital-Advisors.com \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://Business-Capital-Advisors.com \r\n \r\nHave a great day, \r\nThe Business Capital Advisors Team \r\n \r\nunsubscribe/remove - http://business-capital-advisors.com/r.php?url=sofabaolam.com&id=e164', 0, 0, 1564492387, NULL, NULL),
(33, 'GetBusinessFundingNow', '88129353178', 'noreply@get-business-funding-now.com', 'http://Get-Business-Funding-Now.com', NULL, NULL, 'Hi, letting you know that http://Get-Business-Funding-Now.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://Get-Business-Funding-Now.com \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://Get-Business-Funding-Now.com \r\n \r\nHave a great day, \r\nThe Get Business Funding Now Team \r\n \r\nunsubscribe/remove - http://get-business-funding-now.com/r.php?url=sofabaolam.com&id=e164', 0, 0, 1564658819, NULL, NULL),
(34, 'Chiman', '0345 38 58 55', 'aly1@alychidesigns.com', 'Messedamm 8', NULL, NULL, 'Hello there, My name is Aly and I would like to know if you would have any interest to have your website here at sofabaolam.com  promoted as a resource on our blog alychidesign.com ?\r\n\r\n We are  updating our do-follow broken link resources to include current and up to date resources for our readers. If you may be interested in being included as a resource on our blog, please let me know.\r\n\r\n Thanks, Aly', 0, 0, 1564738400, NULL, NULL),
(35, 'FindBusinessFunding247', '84426721935', 'noreply@findbusinessfunding24-7.com', 'http://FindBusinessFunding24-7.com', NULL, NULL, 'Hi, letting you know that http://FindBusinessFunding24-7.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://FindBusinessFunding24-7.com \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://FindBusinessFunding24-7.com \r\n \r\nHave a great day, \r\nThe Find Business Funding 247 Team \r\n \r\nunsubscribe/remove - http://findbusinessfunding24-7.com/r.php?url=sofabaolam.com&id=e164', 0, 0, 1565054844, NULL, NULL),
(36, 'GetBusinessFundedNow', '87381293234', 'noreply@get-business-funded-now.info', 'http://Get-Business-Funded-Now.info', NULL, NULL, 'Faster and Simpler than the SBA, http://Get-Business-Funded-Now.info can get your business a loan for $2K-350,000 With low-credit and no collateral. \r\n \r\nUse our short form to Find Out exactly how much you can get, No-Cost: \r\n \r\nhttp://Get-Business-Funded-Now.info \r\n \r\nIf you\'ve been in business for at least a year you are already pre-qualified. Our Quick service means funding can be completed within 48 hours. Terms are specific for each business so I suggest applying to find out exactly how much you can get. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. Funds are also Non-Restrictive, allowing you to use the full amount in any way including bills, taxes, hiring, marketing, expansion, or Absolutely Any Other expense. \r\n \r\nThere are limited SBA and private funds available so please apply now if interested, \r\n \r\nClick Here: http://Get-Business-Funded-Now.info \r\n \r\nHave a great day, \r\nThe Get Business Funded Now Team \r\n \r\nunsubscribe here - http://get-business-funded-now.info/r.php?url=sofabaolam.com&id=e165', 0, 0, 1565397220, NULL, NULL),
(37, 'Jones', '416-385-3200', 'eric@talkwithcustomer.com', '', NULL, NULL, 'Hello sofabaolam.com,\r\n\r\nPeople ask, “why does TalkWithCustomer work so well?”\r\n\r\nIt’s simple.\r\n\r\nTalkWithCustomer enables you to connect with a prospective customer at EXACTLY the Perfect Time.\r\n\r\n- NOT one week, two weeks, three weeks after they’ve checked out your website sofabaolam.com.\r\n- NOT with a form letter style email that looks like it was written by a bot.\r\n- NOT with a robocall that could come at any time out of the blue.\r\n\r\nTalkWithCustomer connects you to that person within seconds of THEM asking to hear from YOU.\r\n\r\nThey kick off the conversation.\r\n\r\nThey take that first step.\r\n\r\nThey ask to hear from you regarding what you have to offer and how it can make their life better. \r\n\r\nAnd it happens almost immediately. In real time. While they’re still looking over your website sofabaolam.com, trying to make up their mind whether you are right for them.\r\n\r\nWhen you connect with them at that very moment it’s the ultimate in Perfect Timing – as one famous marketer put it, “you’re entering the conversation already going on in their mind.”\r\n\r\nYou can’t find a better opportunity than that.\r\n\r\nAnd you can’t find an easier way to seize that chance than TalkWithCustomer. \r\n\r\nCLICK HERE http://www.talkwithcustomer.com now to take a free, 14-day test drive and see what a difference “Perfect Timing” can make to your business.\r\n\r\nSincerely,\r\nEric\r\n\r\nPS:  If you’re wondering whether NOW is the perfect time to try TalkWithCustomer, ask yourself this:\r\n“Will doing what I’m already doing now produce up to 100X more leads?”\r\nBecause those are the kinds of results we know TalkWithCustomer can deliver.  \r\nIt shouldn’t even be a question, especially since it will cost you ZERO to give it a try. \r\nCLICK HERE http://www.talkwithcustomer.com to start your free 14-day test drive today.\r\n\r\nIf you\'d like to unsubscribe click here http://liveserveronline.com/talkwithcustomer.aspx?d=sofabaolam.com\r\n', 0, 0, 1565578311, NULL, NULL),
(38, 'Martin', '01.84.67.65.56', 'george1@georgemartinjr.com', '93 Rue Joseph Vernet', NULL, NULL, 'Would you be interested in submitting a guest post on georgemartjr.com or possibly allowing us to submit a post to sofabaolam.com ? Maybe you know by now that links are essential\r\nto building a brand online? If you are interested in submitting a post and obtaining a link to sofabaolam.com , let me know and we will get it published in a speedy manner to our blog.\r\n\r\nHope to hear from you soon\r\nGeorge', 0, 0, 1566293430, NULL, NULL),
(39, '228148', '', '', 'http://Get-My-Business-Funded.com', NULL, NULL, 'Quicker and Easier than the SBA, http://Get-My-Business-Funded.com can get your business a loan for $2K-350,000 With low-credit and no collateral. \r\n \r\nUse our quick form to Find Out exactly how much you can get, No-Cost: \r\n \r\nhttp://Get-My-Business-Funded.com \r\n \r\nIf you\'ve been established for at least one year you are already pre-qualified. Our Fast service means funding can be completed within 48 hours. Terms are specific for each business so I suggest applying to find out exactly how much you can get. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. Funds have no Restrictions, allowing you to use the whole amount in any way including bills, taxes, hiring, marketing, expansion, or Absolutely Any Other expense. \r\n \r\nThere are limited SBA and private funds available so please apply now if interested, \r\n \r\nClick Here: http://Get-My-Business-Funded.com \r\n \r\nHave a great day, \r\nThe Get My Business Funded Team \r\n \r\nunsubscribe here - http://get-my-business-funded.com/r.php?url=sofabaolam.com&id=e172', 0, 0, 1567209541, NULL, NULL),
(40, 'Stearns', '0378 1046619', 'noreply@thewordpressclub6460.shop', 'Via Solfatara 89', NULL, NULL, 'Hello,\r\n\r\nAre you utilizing Wordpress/Woocommerce or maybe will you plan to work with it as time goes by ? We currently provide much more than 2500 premium plugins but also themes 100 percent free to get : http://repic.xyz/DTdYB\r\n\r\nRegards,\r\n\r\nKindra', 0, 0, 1567265168, NULL, NULL),
(41, 'JosephHig', '83382598776', 'noreplymonkeydigital@gmail.com', 'https://monkeydigital.co/product/network-traffic-offer/', NULL, NULL, 'Bounce rate. \r\nNetwork traffic to boost ranks and exposure. \r\n \r\nNEW! Now you can choose the Country you want the traffic to come from, as well. \r\n \r\nSupercharge Your SEO And Boost Your Alexa Ranking with 1 Million unique Visitors Traffic sent Within 1 Month. Available only Here. Cheapest Offer On the Internet And Exclusively Available on Monkey Digital \r\n \r\nRead More details about our great offer: \r\nhttps://monkeydigital.co/product/network-traffic-offer/ \r\n \r\n \r\nThanks and regards \r\nMike \r\nMonkey Digital \r\nmonkeydigital.co@gmail.com', 0, 0, 1567586624, NULL, NULL),
(42, 'Ronalddop', '81267245853', 'cribabi01@aol.com', 'http://genbtocanda.tk/re2l', NULL, NULL, 'There is an interestingoffer for you. http://nsecaromoj.tk/wx5n', 0, 0, 1567597425, NULL, NULL),
(43, 'RobertAccix', '86553253621', 'quickchain50@gmail.com', 'https://quickchain.cc/', NULL, NULL, 'Profit +10% after 2 days \r\nThe minimum amount for donation is 0.0025 BTC. \r\nMaximum donation amount is 10 BTC. \r\n \r\nRef bonus 3 levels: 5%,3%,1% paying next day after donation \r\nhttps://quickchain.cc/', 0, 0, 1568116997, NULL, NULL),
(44, 'AveryfaunK', '87142847174', 'raphaeaccogeondess@gmail.com', 'https://www.google.com', NULL, NULL, 'Hello!  sofabaolam.com \r\n \r\nHave you ever heard of sending messages via feedback forms? \r\n \r\nImagine that your message will be readseen by hundreds of thousands of your future customerscustomers. \r\nYour message will not go to the spam folder because people will send the message to themselves. As an example, we have sent you our suggestion  in the same way. \r\n \r\nWe have a database of more than 30 million sites to which we can send your offer. Sites are sorted by country. Unfortunately, you can only select a country when sending a message. \r\n \r\nThe cost of one million messages 49 USD. \r\nThere is a discount program when you purchase  more than two million letter packages. \r\n \r\n \r\nFree trial mailing of 50,000 messages to any country of your selection. \r\n \r\n \r\nThis offer is created automatically. Please use the contact details below to contact us. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - Contact@feedbackmessages.com', 0, 0, 1568195720, NULL, NULL),
(45, 'DorothyUnomi', '84865455217', 'lh@hlaw.at', 'http://facebook.comпјЏmailпјЏ@0X4E18DCC7/Jf32ri', NULL, NULL, 'We would like to inform that you liked a comment ID:35915743 in a social network , January 9, 2019 at 19:48 \r\nThis like has been randomly selected to win the seasonal «Like Of The Year» 2019 award! \r\nhttp://facebook.com+email+@1310252231/vDHVB', 0, 0, 1568431346, NULL, NULL),
(46, 'Robertelupe', '82278595745', 'huyblockchain@gmail.com', 'https://www.google.com', NULL, NULL, 'Tạo, chia sẻ và nhúng các album, catalogue sản phẩm, sách, tạp chí trực tuyến, chuyển đổi các tệp PDF của bạn thành sách lật trình diễn trực tuyến tại website https://zalaa.me/', 0, 0, 1568790288, NULL, NULL),
(47, '112451', '', '', 'http://BusinessLoansFundedNow.com', NULL, NULL, 'Faster and Easier than the SBA, http://BusinessLoansFundedNow.com?url=sofabaolam.com can get your business a loan for $2K-350,000 With low-credit and no collateral. \r\n \r\nUse our fast form to See exactly how much you can get, No-Cost: \r\n \r\nhttp://BusinessLoansFundedNow.com?url=sofabaolam.com \r\n \r\nIf you\'ve been established for at least 12 months you are already pre-qualified. Our Fast service means funding can be completed within 48 hours. Terms are specific for each business so I suggest applying to find out exactly how much you can get. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. Funds have no Restrictions, allowing you to use the full amount in any way including bills, taxes, hiring, marketing, expansion, or Absolutely Any Other expense. \r\n \r\nThere are limited SBA and private funds available so please apply now if interested, \r\n \r\nClick Here: http://BusinessLoansFundedNow.com?url=sofabaolam.com \r\n \r\nHave a great day, \r\nThe Business Loans Funded Now Team \r\n \r\nremove here - http://businessloansfundednow.com/r.php?url=sofabaolam.com&id=pd19', 0, 0, 1568999970, NULL, NULL),
(48, '338795', '', '', 'http://Business-Loans-Funded.com', NULL, NULL, 'Quicker and Simpler than the SBA, http://Business-Loans-Funded.com?url=sofabaolam.com can get your business a loan for $2K-350,000 With low-credit and without collateral. \r\n \r\nUse our 1 minute form to Find Out exactly how much you can get, No-Cost: \r\n \r\nhttp://Business-Loans-Funded.com?url=sofabaolam.com \r\n \r\nIf you\'ve been in business for at least 1 year you are already pre-qualified. Our Quick service means funding can be finished within 48hrs. Terms are specific for each business so I suggest applying to find out exactly how much you can get. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. Funds are also Non-Restrictive, allowing you to use the full amount in any way including bills, taxes, hiring, marketing, expansion, or Absolutely Any Other expense. \r\n \r\nThere are limited SBA and private funds available so please apply now if interested, \r\n \r\nClick Here: http://Business-Loans-Funded.com?url=sofabaolam.com \r\n \r\nHave a great day, \r\nThe Business Loans Funded Team \r\n \r\nremove here - http://business-loans-funded.com/r.php?url=sofabaolam.com&id=e173', 0, 0, 1569465405, NULL, NULL),
(49, 'David Gomez', '81275436795', 'sergiodumass@gmail.com', 'https://www.google.com', NULL, NULL, 'Dearest in mind, \r\n \r\nI would like to introduce myself for the first time. My name is Barrister David Gomez Gonzalez, the personal lawyer to my late client. \r\nWho worked as a private businessman in the international field. In 2012, my client succumbed to an unfortunate car accident. My client was single and childless. \r\nHe left a fortune worth $12,500,000.00 Dollars in a bank in Spain. The bank sent me message that I have to introduce a beneficiary or the money in their bank will be confiscate. My purpose of contacting you is to make you the Next of Kin. \r\nMy late client left no will, I as his personal lawyer, was commissioned by the Spanish Bank to search for relatives to whom the money left behind could be paid to. I have been looking for his relatives for the past 3 months continuously without success. Now I explain why I need your support, I have decided to make a citizen of the same country with my late client the Next of Kin. \r\n \r\nI hereby ask you if you will give me your consent to present you to the Spanish Bank as the next of kin to my deceased client. \r\nI would like to point out that you will receive 45% of the share of this money, 45% then I would be entitled to, 10% percent will be donated to charitable organizations. \r\n \r\nIf you are interested, please contact me at my private contact details by Tel: 0034-604-284-281, Fax: 0034-911-881-353, Email: amucioabogadosl019@gmail.com \r\nI am waiting for your answer \r\nBest regards, \r\n \r\nLawyer: - David Gomez Gonzalez', 0, 0, 1569510378, NULL, NULL),
(50, 'Steveuniof', '81532743244', 'steveKt@gmail.com', 'https://advertisingagencymiami.net/cheap-seo-packages/', NULL, NULL, 'hi there \r\nWe provide best monthly affordable SEO packages & SEO services prices starting $49, Pay for performance based plans & pricing, that’s uniquely tailored to your website, we would be more than happy to create a campaign which accommodates your company’s needs, enabling you to achieve your business goals. Search Engine Optimization is a methodology of strategies, systems and plans to get more traffic on website, increase search engine visibility & ranking. \r\n \r\nCheck out our plans \r\nhttps://advertisingagencymiami.net/cheap-seo-packages/ \r\n \r\nWe know how to get you into top safely, without risking your investment during google updates \r\n \r\nthanks and regards \r\nSteve \r\nstevewebberr@mail.com', 0, 0, 1569634297, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` int(11) UNSIGNED DEFAULT NULL,
  `gender` tinyint(3) UNSIGNED DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province` int(11) UNSIGNED DEFAULT NULL,
  `district` int(11) UNSIGNED DEFAULT NULL,
  `ward` int(10) UNSIGNED DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `payment` int(11) DEFAULT NULL,
  `date` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `code`, `phone`, `email`, `birthday`, `gender`, `address`, `place`, `province`, `district`, `ward`, `company`, `tax_code`, `note`, `user_id`, `payment`, `date`, `time`, `date_time`) VALUES
(17, 'Hồng Thất Công', 'KH17', '0986083468', 'hongthatcong@gmail.com', 17, 1, '', NULL, NULL, NULL, NULL, 'Cái Bang', '3643bhfsdhfds', '', 2, NULL, NULL, '1526551811', NULL),
(18, 'Tiều Cái', 'KH18', '09647239064', 'tieucai@luongson.com', 17, 1, '108 Lương Sơn', NULL, NULL, NULL, NULL, 'Lương Sơn Bạc', 'DV4364326', '', 2, NULL, NULL, '1526551875', NULL),
(19, 'Tào Tháo', 'KH19', '06949326935', 'taothao@tamquoc.com', 17, 1, '', NULL, NULL, NULL, NULL, 'Tam Quốc Diễn Nghĩa', '634ggdsgsgDG', '', 2, NULL, NULL, '1526551937', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_debt`
--

CREATE TABLE `customer_debt` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_create` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nkd` float(22,0) DEFAULT '0',
  `ghino` float(22,0) DEFAULT '0',
  `ghico` float(22,0) DEFAULT '0',
  `nkc` float(22,0) DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` int(11) DEFAULT NULL,
  `time_insert` int(11) DEFAULT NULL,
  `note` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_debt`
--

INSERT INTO `customer_debt` (`id`, `id_customer`, `id_create`, `code`, `nkd`, `ghino`, `ghico`, `nkc`, `type`, `date_time`, `time_insert`, `note`) VALUES
(19, 14, NULL, 'HD38', 0, 760000, 0, -860000, 'Bán hàng', 1526490000, 1526527753, 0),
(20, 14, 580, 'HD38', -860000, 0, 860000, 0, 'Thanh toán tiền hàng', 1526490000, 1526540055, 0),
(21, 13, NULL, 'HD39', 0, 1760000, 0, -1792000, 'Bán hàng', 1526490000, 1526541631, 0),
(22, 18, 2, 'HD40', 0, 358000, 0, -450840, 'Bán hàng', 1526490000, 1526551961, 0);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `pre` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `provinceid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`, `pre`, `provinceid`) VALUES
(106, 'Bến Lức', 'Huyện', 8),
(121, 'Bắc Trà My', 'Huyện', 9),
(139, 'Bà Rịa', 'Thị xã', 10),
(147, 'Buôn Đôn', 'Huyện', 11),
(162, ' Thới Lai', 'Huyện', 12),
(171, 'Bắc Bình', 'Huyện', 13),
(181, 'Bảo Lâm', 'Huyện', 14),
(193, 'A Lưới', 'Huyện', 15),
(202, 'An Biên', 'Huyện', 16),
(217, 'Bắc Ninh', 'Thành phố', 17),
(225, 'Ba Chẽ', 'Huyện', 18),
(239, 'Bá Thước', 'Huyện', 19),
(266, 'Anh Sơn', 'Huyện', 20),
(287, 'Bình Giang', 'Huyện', 21),
(299, 'An Khê', 'Thị xã', 22),
(316, 'Bình Long', 'Thị xã', 23),
(327, 'Ân Thi', 'Huyện', 24),
(337, 'An Lão', 'Huyện', 25),
(348, 'Cái Bè', 'Huyện', 26),
(359, 'Đông Hưng', 'Huyện', 27),
(367, 'Bắc Giang', 'Thành phố', 28),
(377, 'Cao Phong', 'Huyện', 29),
(388, 'An Phú', 'Huyện', 30),
(399, 'Bình Xuyên', 'Huyện', 31),
(408, 'Bến Cầu', 'Huyện', 32),
(417, 'Đại Từ', 'Huyện', 33),
(426, 'Bắc Hà', 'Huyện', 34),
(435, 'Giao Thủy', 'Huyện', 35),
(445, 'Ba Tơ', 'Huyện', 36),
(459, 'Ba Tri', 'Huyện', 37),
(468, 'Cư Jút', 'Huyện', 38),
(476, 'Cà Mau', 'Thành phố', 39),
(485, 'Bình Minh', 'Huyện', 40),
(493, 'Gia Viễn', 'Huyện', 41),
(501, 'Cẩm Khê', 'Huyện', 42),
(514, 'Bác Ái', 'Huyện', 43),
(521, 'Đông Hòa', 'Huyện', 44),
(530, 'Bình Lục', 'Huyện', 45),
(536, 'Cẩm Xuyên', 'Huyện', 46),
(548, 'Cao Lãnh', 'Thành phố', 47),
(560, 'Châu Thành', 'Huyện', 48),
(571, 'Đăk Glei', 'Huyện', 49),
(581, 'Ba Đồn', 'Thị xã', 50),
(589, 'Cam Lộ', 'Huyện', 51),
(599, 'Càng Long', 'Huyện', 52),
(607, 'Châu Thành', 'Huyện', 53),
(614, 'Bắc Yên', 'Huyện', 54),
(626, 'Bạc Liêu', 'Thành phố', 55),
(633, 'Lục Yên', 'Huyện', 56),
(642, 'Chiêm Hóa', 'Huyện', 57),
(649, 'Điện Biên', 'Huyện', 58),
(659, 'Lai Châu', 'Thị xã', 59),
(678, 'Bắc Mê', 'Huyện', 61),
(689, 'Ba Bể', 'Huyện', 62),
(697, 'Bảo Lạc', 'Huyện', 63);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `description` text CHARACTER SET utf8,
  `sort` int(3) DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT '1',
  `active` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `name`, `parent_id`, `description`, `sort`, `lang`, `active`) VALUES
(1, 'Hướng dẫn tổng quan về quản trị website', 0, '<p><a href=\"http://giaodiendep.vn/huongdan/\">Xem video hướng dẫn</a></p>\r\n', 1, 'vi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `email` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `url` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `target` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `sort` int(3) DEFAULT NULL,
  `cate` int(4) DEFAULT '0',
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `content` text CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `type`, `url`, `title`, `target`, `name`, `image`, `id_item`, `sort`, `cate`, `lang`, `content`) VALUES
(200, NULL, NULL, NULL, '_self', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(216, NULL, NULL, 'Video hướng dẫn lắp đặt camera 2', '_self', '12', 'upload/img/media/9f62009350cb11a54f10ffd7c56e1bca.png', 1, 2, 0, NULL, 'https://www.youtube.com/embed/QzqthoS3Xkw'),
(217, NULL, NULL, 'HƯỚNG DẪN LẮP ĐẶT HỆ THỐNG CAMERA QUAN SÁT', '_self', '12', 'upload/img/media/14fca64f4ab55bddda0d89209d9d8c80.png', 1, 3, 0, NULL, 'https://www.youtube.com/embed/JdrNRXs8KqI'),
(218, NULL, NULL, 'Hướng dẫn cấu hình Camera xem qua mạng 100% thành công', '_self', '12', 'upload/img/media/9f62009350cb11a54f10ffd7c56e1bca1.png', 1, 4, 0, NULL, 'https://www.youtube.com/embed/Q27P_jphAXU'),
(219, NULL, NULL, 'Video Clip Hướng dẫn sử dụng Camera IP Wifi không dây thông minh Webvision 6203', '_self', '12', 'upload/img/media/9f62009350cb11a54f10ffd7c56e1bca2.png', 1, 5, 0, NULL, 'https://www.youtube.com/embed/isA3QHA4wOM'),
(291, 'slide', '', 'banner2', '_self', NULL, 'upload/img/banner/untitled.png', NULL, NULL, NULL, 'vi', ''),
(292, 'partners', '', 'quảng cáo 1', '_self', NULL, 'upload/img/banner/banner-tan-co.jpg', NULL, NULL, NULL, 'vi', ''),
(294, 'top', '', 'www', '_self', NULL, 'upload/img/banner/qc2.jpg', NULL, NULL, NULL, 'vi', '');

-- --------------------------------------------------------

--
-- Table structure for table `inuser`
--

CREATE TABLE `inuser` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `hot` int(11) DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `tag` text COLLATE utf8_unicode_ci,
  `time` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `focus` int(11) DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inuser`
--

INSERT INTO `inuser` (`id`, `title`, `description`, `hot`, `image`, `content`, `alias`, `lang`, `tag`, `time`, `category_id`, `home`, `focus`, `title_seo`, `keyword_seo`, `description_seo`) VALUES
(4, 'Rực Rỡ Mùa Hoa Tây Bắc', 'Tết Nguyên Đán 2015 là thời khắc quan trọng nhất trong năm, là khi mỗi gia đình Việt Nam có thời gian được trở về quây quần bên nhau và tưng bừng du xuân khắp mọi miền đất nước. Trong không khí xuân nồng ấm ấy, Vietravel hân hạnh gửi tới Quý khách hàng ngàn đường tour Việt Nam để gia đình bạn thỏa sức tận hưởng những ngày lễ vui tươi, hạnh phúc, đón chào năm mới An khang Thịnh Vượng. \n', 1, 'upload/img/ava1_hoanhai1.jpg', '<div>&nbsp;</div>\n\n<div>\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 1 : TP.HCM - NỘI B&Agrave;I (H&Agrave; NỘI) &ndash; ĐỀN H&Ugrave;NG - NGHĨA LỘ Số bữa ăn: 3 bữa&nbsp;</strong></p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_141103_mien%20bac%20-%20den%20hung.jpg\" style=\"border:0px; box-sizing:border-box; height:458px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Qu&yacute; kh&aacute;ch tập trung tại cột số 04 ga đi Trong Nước - S&acirc;n bay T&acirc;n Sơn Nhất để hướng dẫn l&agrave;m thủ tục cho Qu&yacute; kh&aacute;ch đ&aacute;p chuyến bay đi H&agrave; Nội. Xe Vietravel đ&oacute;n đo&agrave;n tại s&acirc;n bay Nội B&agrave;i, khởi h&agrave;nh đi Y&ecirc;n B&aacute;i. Tr&ecirc;n đường đi Qu&yacute; kh&aacute;ch gh&eacute; Ph&uacute; Thọ viếng Đền H&ugrave;ng, đến nơi, Qu&yacute; kh&aacute;ch l&agrave;m lễ d&acirc;ng hương đất tổ, tham quan đền Thượng, đền Trung, đền Hạ, Giếng Ngọc, Lăng vua H&ugrave;ng, tự do chụp ảnh mua sắm qu&agrave; lưu niệm. Đo&agrave;n tiếp tục khởi h&agrave;nh đi Nghĩa Lộ, nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi. Buổi tối, Qu&yacute; kh&aacute;ch thưởng thức chương tr&igrave;nh biểu diễn m&uacute;a X&ograve;e, giao lưu v&agrave; t&igrave;m hiểu n&eacute;t văn h&oacute;a đặc sắc của d&acirc;n tộc Th&aacute;i. Nghỉ đ&ecirc;m tại Nghĩa Lộ.</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 2 : NGHĨA LỘ - M&Ugrave; CANG CHẢI - SAPA (Số bữa ăn: 3 bữa)</strong></p>\n\n<p style=\"text-align:center\"><strong><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_141103_sapa%20-%20muong%20hoa%202.jpg\" style=\"border:0px; box-sizing:border-box; height:408px; vertical-align:middle; width:650px\" /></strong></p>\n\n<p style=\"text-align:justify\">Trả ph&ograve;ng kh&aacute;ch sạn, đo&agrave;n khởi h&agrave;nh đi M&ugrave; Cang Chải, ngang qua T&uacute; Lệ, Qu&yacute; kh&aacute;ch sẽ ngửi được m&ugrave;i hương thoang thoảng theo gi&oacute; bảng lảng tr&ecirc;n m&aacute;i nh&agrave; của &ldquo;cơm mới&rdquo;, nơi đ&acirc;y nổi tiếng với x&ocirc;i nếp, cốm kh&ocirc;. Đến Đ&egrave;o Khau Phạ Qu&yacute; kh&aacute;ch dừng chụp ảnh v&agrave; ngắm nh&igrave;n Bản L&igrave;m M&ocirc;ng xinh đẹp tọa lạc dưới ch&acirc;n đ&egrave;o. Đ&acirc;y l&agrave; Bản của d&acirc;n tộc M&ocirc;ng v&agrave; l&agrave; nơi c&oacute; ruộng l&uacute;a đẹp nhất M&ugrave; Cang Chải. Qua đ&egrave;o Khau Phạ v&agrave;o địa phận M&ugrave; Cang Chải, Qu&yacute; kh&aacute;ch sẽ bị m&ecirc; hoặc bởi vẻ đẹp h&uacute;t hồn của cung đường ruộng bậc thang (Nổi tiếng tại 3 x&atilde;: La P&aacute;n Tẩn, Chế Cu Nha v&agrave; Zế Xu Ph&igrave;nh). Đo&agrave;n chi&ecirc;m ngưỡng những thung lũng rộng h&uacute;t tầm mắt, c&aacute;c thửa ruộng tầng tầng lớp lớp lượn s&oacute;ng theo sườn n&uacute;i, ngọn n&uacute;i n&agrave;y nối tiếp ngọn n&uacute;i kh&aacute;c. Qu&yacute; kh&aacute;ch c&oacute; thể tham quan v&agrave; thưởng ngoạn c&aacute;c giai đoạn của m&ugrave;a l&uacute;a: m&ugrave;a nước đổ &oacute;ng &aacute;nh tr&ecirc;n c&aacute;c triền n&uacute;i (th&aacute;ng 2-3), m&ugrave;a cấy l&uacute;a (th&aacute;ng 5), m&ugrave;a l&uacute;a non (th&aacute;ng 6-7) v&agrave; đẹp nhất l&agrave;m m&ugrave;a l&uacute;a ch&iacute;n hay c&ograve;n lại l&agrave; m&ugrave;a v&agrave;ng (th&aacute;ng 9-10). Cũng ch&iacute;nh bởi vẻ đẹp m&ecirc; l&ograve;ng người v&agrave;o m&ugrave;a l&uacute;a ch&iacute;n m&agrave; Ruộng Bậc Thang ở ba x&atilde; n&agrave;y đ&atilde; được xếp hạng Di t&iacute;ch Quốc Gia năm 2007. Đến thị trấn M&ugrave; Cang Chải, Qu&yacute; kh&aacute;ch ăn trưa, nghỉ ngơi. Chiều đo&agrave;n khởi h&agrave;nh đi Sa Pa, tr&ecirc;n đường đi Qu&yacute; kh&aacute;ch dừng ch&acirc;n ngắm to&agrave;n cảnh đồi ch&egrave; T&acirc;n Uy&ecirc;n thơ mộng v&agrave; tiếp tục sẽ được chi&ecirc;m ngưỡng phong cảnh n&uacute;i rừng T&acirc;y Bắc h&ugrave;ng vĩ tr&ecirc;n Đ&egrave;o &Ocirc; Quy Hồ - Ranh giới giữa 2 tỉnh L&agrave;o Cai v&agrave; Lai Ch&acirc;u uốn lượn quanh d&atilde;y Ho&agrave;ng Li&ecirc;n c&ograve;n gọi l&agrave; khu vực Cổng Trời. Đến Sa Pa, nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi. Buổi tối, Qu&yacute; kh&aacute;ch tự do tham quan phố n&uacute;i v&agrave; thưởng thức những m&oacute;n ăn đặc sản tại nơi đ&acirc;y. Nghỉ đ&ecirc;m tại Sa Pa</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 3 : SAPA - LAI CH&Acirc;U (Số bữa ăn: 3 bữa)</strong></p>\n\n<p style=\"text-align:center\"><strong><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_141103_sapa%20trong%20suong%201.jpg\" style=\"border:0px; box-sizing:border-box; height:436px; vertical-align:middle; width:650px\" /></strong></p>\n\n<p style=\"text-align:justify\">Qu&yacute; kh&aacute;ch tham quan v&agrave; chinh phục N&uacute;i H&agrave;m Rồng, thăm Vườn Lan khoe sắc, Vườn Hoa Trung T&acirc;m, ngắm N&uacute;i Phanxipăng h&ugrave;ng vĩ, Cổng Trời, Đầu Rồng Thạch L&acirc;m, S&acirc;n M&acirc;y. Đo&agrave;n tự do ngắm cảnh v&agrave; chụp ảnh thị trấn Sapa trong sương. Trả ph&ograve;ng kh&aacute;ch sạn, ăn trưa. Chiều Qu&yacute; kh&aacute;ch tham quan Th&aacute;c Bạc - D&ograve;ng nước trắng x&oacute;a chảy từ độ cao tr&ecirc;n 200m v&agrave;o d&ograve;ng suối dưới thung lũng &Ocirc; Quy Hồ, tạo n&ecirc;n &acirc;m thanh n&uacute;i rừng đầy ấn tượng, tiếp tục tham quan Lao Chải, Tả Van hoặc Tả Ph&igrave;n (t&ugrave;y điều kiện thực tế). Về đến Lai Ch&acirc;u, Qu&yacute; kh&aacute;ch nhận ph&ograve;ng kh&aacute;ch sạn. Nghỉ đ&ecirc;m tại Lai Ch&acirc;u.</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 4 : LAI CH&Acirc;U - PHONG THỔ - MƯỜNG LAY - ĐIỆN BI&Ecirc;N (Số bữa ăn: 3 bữa)</strong></p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_140929_du-lich-tay-bac.jpg\" style=\"border:0px; box-sizing:border-box; height:432px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Đo&agrave;n trả ph&ograve;ng ăn s&aacute;ng, khởi h&agrave;nh đi Điện Bi&ecirc;n, tr&ecirc;n đường ngắm cảnh rừng n&uacute;i T&acirc;y Bắc dọc theo d&ograve;ng s&ocirc;ng Nậm Na v&agrave; v&ugrave;ng ngập nước do đập nh&agrave; m&aacute;y Thủy điện Sơn La d&acirc;ng l&ecirc;n tại ng&atilde; ba s&ocirc;ng: s&ocirc;ng Đ&agrave;, s&ocirc;ng Nậm Na v&agrave; s&ocirc;ng Nậm Rốm. Đến Mường L&acirc;y ăn trưa. Đo&agrave;n tiếp tục khởi h&agrave;nh đến Điện Bi&ecirc;n, Qu&yacute; kh&aacute;ch tham quan Bảo t&agrave;ng Điện Bi&ecirc;n Phủ - Được x&acirc;y dựng v&agrave;o năm 1984 nh&acirc;n dịp kỷ niệm 30 năm chiến thắng lịch sử Điện Bi&ecirc;n Phủ, viếng Nghĩa trang liệt sĩ đồi A1, thăm Đồi A1, Hầm sở chỉ huy qu&acirc;n đội Ph&aacute;p - Tướng Đờ C&aacute;t (De Castries). Nghỉ đ&ecirc;m tại Điện Bi&ecirc;n. Nhận ph&ograve;ng kh&aacute;ch sạn, ăn tối v&agrave; nghỉ đ&ecirc;m tại Điện Bi&ecirc;n</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 5 : ĐIỆN BI&Ecirc;N - SƠN LA - MỘC CH&Acirc;U (Số bữa ăn: 3 bữa)</strong></p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_140905_Doi%20che%20Moc%20Chau.jpg\" style=\"border:0px; box-sizing:border-box; height:424px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Trả ph&ograve;ng kh&aacute;ch sạn, đo&agrave;n khởi h&agrave;nh về Sơn La. Tr&ecirc;n đường đi, Qu&yacute; kh&aacute;ch sẽ được chi&ecirc;m ngưỡng phong cảnh n&uacute;i rừng T&acirc;y Bắc h&ugrave;ng vĩ tr&ecirc;n đỉnh Đ&egrave;o Pha Đin - Một trong &quot;tứ đại đ&egrave;o&quot; v&ugrave;ng T&acirc;y Bắc v&agrave; được xếp c&ugrave;ng nh&oacute;m 6 con đ&egrave;o g&acirc;y ấn tượng nhất Việt Nam. Đến Sơn La, Qu&yacute; kh&aacute;ch ăn trưa. Sau đ&oacute;, Qu&yacute; kh&aacute;ch khởi h&agrave;nh về Mộc Ch&acirc;u. Đo&agrave;n khởi h&agrave;nh tham quan Th&aacute;c Dải Yếm - C&ograve;n c&oacute; t&ecirc;n gọi l&agrave; Th&aacute;c N&agrave;ng, nhằm v&iacute; vẻ đẹp mềm mại, h&igrave;nh ảnh quyến rũ của th&aacute;c nước như xu&acirc;n sắc của người con g&aacute;i tuổi trăng tr&ograve;n. Sau đ&oacute; tham quan Đồi Ch&egrave; Mộc Ch&acirc;u - Đứng tr&ecirc;n đồi ch&egrave; du kh&aacute;ch sẽ cảm nhận được l&agrave;n kh&ocirc;ng kh&iacute; m&aacute;t lạnh trong l&agrave;nh, tận mắt thấy những l&agrave;n sương bồng bềnh tr&ocirc;i, những đường ch&egrave; chạy v&ograve;ng quanh đồi được sắp đặt th&agrave;nh h&agrave;ng như những thửa ruộng bậc thang xanh ngắt cứ trải d&agrave;i bất tận. Qu&yacute; kh&aacute;ch dừng mua sắm đặc sản nổi tiếng được chế biến từ sữa b&ograve; tươi nổi tiếng của Mộc Ch&acirc;u về l&agrave;m qu&agrave;. Đo&agrave;n về kh&aacute;ch sạn nhận ph&ograve;ng, nghỉ ngơi. Nghỉ đ&ecirc;m tại Mộc Ch&acirc;u.</p>\n\n<p style=\"text-align:justify\"><strong>Ng&agrave;y 6 : MỘC CH&Acirc;U - MAI CH&Acirc;U - H&Ograve;A B&Igrave;NH - S&Acirc;N BAY NỘI B&Agrave;I (H&Agrave; NỘI) (Số bữa ăn: 2 bữa (s&aacute;ng, trưa))</strong></p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"https://travel.com.vn/Images/tour/tfd_141103_moc%20chau%20-%20hoa%20cai.jpg\" style=\"border:0px; box-sizing:border-box; height:346px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Ăn s&aacute;ng tại kh&aacute;ch sạn, trả ph&ograve;ng. Đo&agrave;n khởi h&agrave;nh đi Mai Ch&acirc;u - H&ograve;a B&igrave;nh, tham quan Bản L&aacute;c Mai Ch&acirc;u - T&igrave;m hiểu nh&agrave; s&agrave;n, phong tục tập qu&aacute;n, c&aacute;ch kinh doanh du lịch loại h&igrave;nh home stay của b&agrave; con người Th&aacute;i nơi đ&acirc;y. Đo&agrave;n khởi h&agrave;nh về H&ograve;a B&igrave;nh ăn trưa. Đo&agrave;n khởi h&agrave;nh về H&ograve;a B&igrave;nh ăn trưa. Sau đ&oacute;, khởi h&agrave;nh về H&agrave; Nội, xe đưa Qu&yacute; kh&aacute;ch ra s&acirc;n bay Nội B&agrave;i đ&aacute;p chuyến bay về Tp.HCM. Chia tay Qu&yacute; kh&aacute;ch v&agrave; kết th&uacute;c chương tr&igrave;nh du lịch tại s&acirc;n bay T&acirc;n Sơn Nhất</p>\n</div>\n', 'ruc-ro-mua-hoa-tay-bac', '0', '0', 1446786194, 22, 0, 0, '', '', ''),
(5, 'Giấc mộng hoa phương Bắc', 'Đất trời đã vào xuân, non cao miền Bắc bừng sáng trong vẻ đẹp mê đắm của rừng hoa thắm sắc ẩn hiện trong sương khói vấn vương. Những bước chân phiêu du trên núi ngàn cũng rộn rã hơn, chan hòa cùng nét tươi mới giữa đất trời nở hoa. Tour Tết, Trong nước', 1, 'upload/img/mua-hoa-xuan-tay-bac_1.jpg', '<div>&nbsp;</div>\n\n<div>\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_1.jpg\" style=\"border:0px; box-sizing:border-box; height:441px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">nhiều lần lỡ hẹn, t&ocirc;i cũng đặt ch&acirc;n đến miền rẻo cao phương Bắc với thật nhiều h&aacute;o hức. Qu&atilde;ng đường đi qua&nbsp; Sapa, Điện Bi&ecirc;n, Sơn La, Cao Bằng, Lạng Sơn&hellip; dường như ngắn lại bởi ai cũng say sưa ngắm những cung đường bạt ng&agrave;n hoa đ&agrave;o, hoa mận, hoa mơ. Hoa nở tr&agrave;n tr&ecirc;n triền đồi, lấp l&oacute; ven đường, hồn nhi&ecirc;n thả bức r&egrave;m trước s&acirc;n nh&agrave;&hellip; đẹp đến nỗi kh&ocirc;ng một m&aacute;y ảnh &ldquo;khủng&rdquo; n&agrave;o c&oacute; thể ghi lại trọn vẹn.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_6.jpg\" style=\"border:0px; box-sizing:border-box; height:433px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">C&oacute; l&uacute;c hoa phủ hồng cả sườn n&uacute;i, khiến kh&aacute;ch l&atilde;ng du ngất ng&acirc;y chẳng muốn dời ch&acirc;n. Một cơn gi&oacute; thoảng qua, khung cảnh bỗng h&oacute;a th&agrave;nh cơn mưa hoa lất phất. Chắt chiu nhựa sống qua năm d&agrave;i th&aacute;ng rộng, hội tụ đủ tinh t&uacute;y của đất trời để mỗi độ xu&acirc;n về th&acirc;n c&acirc;y x&ugrave; x&igrave; ấy lại nảy lộc đơm hoa sưởi ấm cả n&uacute;i rừng. Những c&aacute;nh đ&agrave;o phai T&acirc;y Bắc hồng phớt, mỏng manh m&agrave; l&agrave;n hương lại dịu d&agrave;ng, thanh tao đến lạ. Đ&ocirc;ng Bắc lại tự h&agrave;o với n&eacute;t ki&ecirc;u sa rực rỡ của rừng hoa đ&agrave;o b&iacute;ch lộng lẫy c&oacute; c&aacute;nh d&agrave;y, to, đủ sắc đỏ, hồng, trắng&hellip;</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_4.jpg\" style=\"border:0px; box-sizing:border-box; height:472px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Ven đường đi, h&ograve;a c&ugrave;ng mu&ocirc;n sắc hoa đ&agrave;o b&iacute;ch, đ&agrave;o phai l&agrave; n&eacute;t đẹp hoang d&atilde; của những lo&agrave;i hoa dại t&iacute;m ng&aacute;t, v&agrave;ng rực cả khoảng trời. Đến n&uacute;i N&agrave;ng T&ocirc; Thị, động Tam Thanh, cảm x&uacute;c của t&ocirc;i gần như vỡ &ograve;a khi được chi&ecirc;m ngưỡng những đ&oacute;a hoa đ&agrave;o trắng muốt như tuyết, c&acirc;y đ&agrave;o gh&eacute;p hội tụ đủ ba m&agrave;u trắng - hồng - đỏ rất ấn tượng.</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_3_1.jpg\" style=\"border:0px; box-sizing:border-box; height:975px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Hoa kh&ocirc;ng chỉ t&ocirc; điểm cho n&uacute;i rừng m&agrave; c&ograve;n mang cả kh&ocirc;ng gian văn h&oacute;a v&ugrave;ng cao đến với mọi người. T&ocirc;i cứ nhớ m&atilde;i nhịp ch&acirc;n vui của ch&agrave;ng trai bản xuống chợ ng&agrave;y xu&acirc;n m&agrave; tr&ecirc;n vai lắc lư một c&agrave;nh đ&agrave;o thắm. Những c&ocirc; g&aacute;i Dao, M&ocirc;ng v&aacute;y xanh v&aacute;y đỏ tỏa s&aacute;ng dưới hoa xu&acirc;n v&agrave; bọn trẻ con mắt trong veo, n&ocirc; đ&ugrave;a hồn nhi&ecirc;n tr&ecirc;n c&acirc;y mận thật đ&aacute;ng y&ecirc;u l&agrave;m sao!</p>\n\n<p style=\"text-align: center;\"><img alt=\"\" src=\"http://www.vietravel.com.vn/Images/inuserPicture/mua-hoa-xuan-tay-bac_2_1.jpg\" style=\"border:0px; box-sizing:border-box; height:894px; vertical-align:middle; width:650px\" /></p>\n\n<p style=\"text-align:justify\">Chỉ cần như thế cũng b&otilde; c&ocirc;ng cho một chuyến ngao du sơn thủy, s&aacute; g&igrave; n&uacute;i cao hay đ&egrave;o vắng, chỉ cần v&aacute;c ba l&ocirc; l&ecirc;n đường, ta lại sở hữu m&ugrave;a xu&acirc;n thi vị cho ri&ecirc;ng m&igrave;nh. Hoa nở khắp đất trời, hoa nở trong l&ograve;ng người để t&ocirc;i m&atilde;i nhung nhớ về miền rẻo cao phương Bắc. Đ&oacute; ch&iacute;nh l&agrave; những x&uacute;c cảm đầu năm thi&ecirc;ng li&ecirc;ng v&agrave; rất đỗi tự h&agrave;o về qu&ecirc; hương m&agrave; kh&ocirc;ng h&agrave;nh tr&igrave;nh n&agrave;o c&oacute; được.</p>\n</div>\n', 'giac-mong-hoa-phuong-bac', '0', '0', 1446792582, 22, 0, 0, '', '', ''),
(6, 'Train Ticket', 'Operated by national carrier Vietnam Railways.Travelling in an air-con sleeping berth and of course, there’s some spectacular scenery to lap up too. There are four main ticket classes: hard seat, soft seat, hard sleeper and soft sleeper. These are also split into air-con and non air-con options. Presently, air-con is only available on the faster express trains. Hard-seat class is usually packed and tolerable for day travel, but expect plenty of cigarette smoke. Ticket prices vary depending on the train; the fastest trains are more expensive. Aside from the main HCMC–Hanoi run, three rail-spur lines link Hanoi with the other parts of northern Vietnam. A third runs northwest to Lao Cai (Sapa).', 0, 'upload/img/ticket.jpg', '', 'train-ticket', '0', '0', 1447426430, 23, 0, 0, '', '', ''),
(7, 'Train North to South', 'Everyday departure with trains number: Trains SE1-SE6: Soft sleepers (4-berth), hard sleepers (6-berth), soft class seats (all air-con). TN3-TN10: Soft sleepers (air-con), hard sleepers (air-con & non-air-con), soft seats (a/c & non-a/c), hard seats (non-air-con).', 0, 'upload/img/tk1.jpg', '<span style=\"color:rgb(85, 85, 85); font-family:arial\">Unit Price: US Dollar (US$); A/C: Air-conditioning.</span><br />\n<span style=\"color:rgb(85, 85, 85); font-family:arial\">Child&#39;s fare: under 5 years: free of charge if sharing bed with parent; 5 years/up: adult rate.</span><br />\n<span style=\"color:rgb(85, 85, 85); font-family:arial\">Please note: 20% of the amount will be charged in case of cancellation for any ticket.</span><br />\n&nbsp;\n<div>&nbsp;</div>\n\n<div>\n<table style=\"border-collapse:collapse; border-spacing:0px; border:1px solid rgb(223, 223, 223); color:rgb(96, 96, 96); font-family:arial; font-size:17.6px; height:105px; line-height:normal; margin:0px auto; padding:0px; vertical-align:baseline; width:800px\">\n	<tbody>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">DEP FROM HANOI</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">SE NO. 1/ TIME TABLE</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">SE NO. 3/&nbsp;TIME TABLE</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">SE NO. 5/&nbsp;TIME TABLE</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">&nbsp;PRICE</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\">HA NOI</td>\n			<td style=\"text-align:center; vertical-align:baseline\">19.35</td>\n			<td style=\"text-align:center; vertical-align:baseline\">22.00</td>\n			<td style=\"text-align:center; vertical-align:baseline\">6.00</td>\n			<td style=\"text-align:center; vertical-align:baseline\">&nbsp;55 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">HUE</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">8.48</td>\n			<td style=\"text-align:center; vertical-align:baseline\">10.27</td>\n			<td style=\"text-align:center; vertical-align:baseline\">19.55</td>\n			<td style=\"text-align:center; vertical-align:baseline\">&nbsp;55 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">DA NANG&nbsp;</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">11.26</td>\n			<td style=\"text-align:center; vertical-align:baseline\">13.00</td>\n			<td style=\"text-align:center; vertical-align:baseline\">22.47</td>\n			<td style=\"text-align:center; vertical-align:baseline\">60 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">&nbsp;NHA TRANG</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">21.14</td>\n			<td style=\"text-align:center; vertical-align:baseline\">22.04</td>\n			<td style=\"text-align:center; vertical-align:baseline\">8.35</td>\n			<td style=\"text-align:center; vertical-align:baseline\">80 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">BINH THUAN&nbsp;</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">1.12</td>\n			<td style=\"text-align:center; vertical-align:baseline\">2.14</td>\n			<td style=\"text-align:center; vertical-align:baseline\">16.14</td>\n			<td style=\"text-align:center; vertical-align:baseline\">85 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">&nbsp;SAI GON</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">4.39</td>\n			<td style=\"text-align:center; vertical-align:baseline\">5.20</td>\n			<td style=\"text-align:center; vertical-align:baseline\">16.05</td>\n			<td style=\"text-align:center; vertical-align:baseline\">100 USD<br />\n			&nbsp;</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n', 'train-north-to-south', '0', '0', 1447426503, 23, 0, 0, '', '', ''),
(8, 'Train to Sapa', 'The Ha Noi-Lao Cai trains runs every evening, departing from Ha Noi Train Station at Tran Quy Cap Street. Three run at night, and one makes a day trip. The following are the trains from Ha Noi to Lao Cai (PM: SP1, SP3 , SP7 ) and vice versa (PM: SP2, SP4 , SP8) daily. The daytime route offers only hard seats, whereas travelers can enjoy soft-sleepers, air-conditioned, four-berth cabins on the night trains. In the SP3 & SP4, there are 2 Victoria Carriages. In SP1 & SP2, there are Orient Express, Tulico Carriages, Friendly Carriages, Ratraco Carriages, and TSC Carriages, King Express Carriages, Royal Carriages. All of these are alternatives for tourists to Sapa from Hanoi.', 0, 'upload/img/tk2.jpg', '<p>Deluxe Train: Fansipan Express (SP1-SP2), Livitrans Express (SP1-SP2), Sapaly Expres (SP3-SP4)</p>\n\n<p>First Class Train: Orient Express (SP1-SP2), TSC Express ( SP1-SP2), Pumpkin Express train (SP1-SP2), VN Express Train ( SP3-SP4)</p>\n\n<p>&nbsp;</p>\n\n<table style=\"border-collapse:collapse; border-spacing:0px; border:1px solid rgb(223, 223, 223); color:rgb(96, 96, 96); font-family:arial; font-size:17.6px; height:105px; line-height:normal; margin:0px auto; padding:0px; vertical-align:baseline; width:800px\">\n	<tbody>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">HANOI - LAO CAI</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">DELUXE CABIN 4 BERTHS</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">\n			<p>FIRST CLASS 4 BERTHS</p>\n			</td>\n			<td style=\"text-align:center; vertical-align:baseline; width:250px\">VIP CLASS 2 BERTHS</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">SP1: 21H40 - 5H30</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">30 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">35 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">70 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">SP1: 20H00 - &nbsp;6H10</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">30 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">35 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">70 USD</td>\n		</tr>\n		<tr>\n			<td style=\"text-align:center; vertical-align:baseline\"><span style=\"color:rgb(128, 128, 128)\">SP1: 20H17 - &nbsp;4H35</span></td>\n			<td style=\"text-align:center; vertical-align:baseline\">30 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">35 USD</td>\n			<td style=\"text-align:center; vertical-align:baseline\">70 USD</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>The prices may change due to exchange rate or season; therefore, please confirm exact price when you make the final booking with payment. Please contact by email to have more information. Email:&nbsp;<a href=\"mailto:info@vietnampremiertravel.com\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-family: Arial; color: rgb(34, 34, 34);\">info@vietnampremiertravel.com</a>&nbsp;/ Tel: (+84 4) 3926 2866</p>\n', 'train-to-sapa', '0', '0', 1447426437, 23, 0, 0, '', '', ''),
(9, 'Tàu Bắc - Nam', 'Khởi hành hàng ngày với tàu số : Xe lửa SE1 - SE6 : tà vẹt mềm ( 4 bến ) , tà vẹt cứng ( 6 bến ) , ghế hạng mềm ( tất cả các máy con) . TN3 - TN10 : tà vẹt mềm ( máy lạnh ) , tà vẹt cứng ( máy lạnh & không khí -con) , ghế ngồi mềm (a / c và phi - a / c ) , ghế ngồi cứng ( không máy lạnh ) .', 0, 'upload/img/tk11.jpg', '<pre>\n<span style=\"font-size:14px\">Đơn gi&aacute; : Dollar Mỹ (US $ ) ; A / C : Điều h&ograve;a kh&ocirc;ng kh&iacute; .\nGi&aacute; v&eacute; cho trẻ em: dưới 5 tuổi: miễn ph&iacute; nếu ngủ chung giường với bố mẹ ; 5 năm / up : tỷ lệ người lớn .\nXin lưu &yacute; : 20 % của số tiền sẽ được t&iacute;nh trong trường hợp hủy cho bất kỳ v&eacute; .</span></pre>\n', 'tau-bac-nam', '0', '0', 1446800384, 22, 0, 0, '', '', ''),
(10, 'teafdsagd', 'gdasgdsg', 0, NULL, 'sagdsagdsagd', 'teafdsagd', 'en', '0', 1453861931, 0, 0, 0, '', '', ''),
(11, 'Dàn xe đời mới - Đa dạng chủng loại', 'Chúng tôi cho thuê xe từ những dòng xe giá rẻ đến những dòng xe cao cấp, từ 4 chỗ đến xe 12 chỗ Dàn xe của chúng tôi luôn có bộ phận theo dõi, quản lý và bảo hành. Để đảm bảo trước khi đến đón khách, Xe luôn trong tình trạng sạch, đẹp và an toàn.', 0, 'upload/img/icon3.png', '', 'dan-xe-doi-moi-da-dang-chung-loai', 'vi', '0', 1453863158, 20, 0, 1, '', '', ''),
(12, 'Tài xế thân thiện và chuyên nghiệp', 'Các tài xế của chúng tôi được tuyển chọn khắt khe theo các tiêu chí. Lái xe an toàn, có kinh nghiệm, thông thạo tuyến đường và được công tu Training các kỹ năng phục vụ khách hàng. Tùy theo mục đích thuê xe và loại xe cũng như yêu cầu của quí khách', 0, 'upload/img/icon2.png', '', 'tai-xe-than-thien-va-chuyen-nghiep', 'vi', '0', 1453863170, 20, 0, 1, '', '', ''),
(13, 'Giá cho thuê xe tốt nhất trên thị trường', 'Qui trình và cách tính giá cũng như báo giá của chúng tôi luôn là mức giá tốt nhất trên thị trường. Chính vì vậy khi quí khách thuê xe của chúng tôi cũng có nghĩa quí khách đã có được mức giá tốt nhât trong những nhà cung cấp.', 0, 'upload/img/icon1.png', '', 'gia-cho-thue-xe-tot-nhat-tren-thi-truong', 'vi', '0', 1453863176, 20, 0, 1, '', '', ''),
(14, 'Hướng dẫn lái xe ô tô an toàn trên đường cao tốc', 'Trên đường cao tốc, người điều khiển phương tiện giao thông được phép lái xe với vận tốc tối đa cao hơn so với lái trên đường phố, đường làng và do đó tiết kiệm thời gian di chuyển hơn nhưng cũng tiềm ẩn nhiều rủi ro xảy ra tai nạn đáng tiếc nếu không tuân thủ đúng luật.', 0, 'upload/img/new1.jpg', '<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">Th&oacute;i quen h&agrave;ng ng&agrave;y khi l&aacute;i xe đ&ocirc;i khi đ&atilde; trở th&agrave;nh nguy&ecirc;n nh&acirc;n của những trường hợp tai nạn đ&aacute;ng tiếc khi tham gia giao th&ocirc;ng tr&ecirc;n đường cao tốc: chạy xe dưới tốc độ tối thiểu, kh&ocirc;ng giữ khoảng c&aacute;ch an to&agrave;n với xe ph&iacute;a trước, dừng/đỗ t&ugrave;y tiện, quay đầu xe&hellip; Nhưng lưu &yacute; sau sẽ gi&uacute;p bạn c&oacute; những chuyến đi an to&agrave;n c&ugrave;ng bạn b&egrave;, người th&acirc;n.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash; Ngo&agrave;i c&aacute;c vấn đề kỹ thuật đảm bảo an to&agrave;n cho xe &ocirc; t&ocirc;, đặc biệt phải lưu &yacute; đến lốp xe bởi khi chạy với tốc độ cao, nhiệt độ ngo&agrave;i trời cao, h&agrave;ng h&oacute;a chở nhiều&hellip;; do đ&oacute;, với những bộ lốp &ldquo;tuổi đời&rdquo; cao, m&ograve;n nhiều cần đặc biệt cẩn trọng (nổ lốp xe khi đang đi tốc độ cao l&agrave; một trong những nguy&ecirc;n nh&acirc;n phổ biến dẫn đến tai nạn).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash; Đảm bảo tốc độ theo hệ thống biển b&aacute;o tr&ecirc;n đường, giảm tốc độ ph&ugrave; hợp ở những đoạn đường cong, c&oacute; nhiều phương tiện (cho d&ugrave; ở l&agrave;n đường kh&aacute;c) hoặc chướng ngại vật&hellip; Tr&aacute;nh nh&igrave;n tập trung v&agrave;o một điểm qu&aacute; l&acirc;u, đặc biệt c&aacute;c đoạn đường cong hay l&ecirc;n/xuống dốc (dễ dẫn đến trường hợp &ldquo;kh&oacute;a mục ti&ecirc;u&rdquo; khiến xe đi thẳng đến điểm đ&oacute;).</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&nbsp;&ndash; Giữ khoảng c&aacute;ch an to&agrave;n với quy tắc 3 gi&acirc;y (Bạn nh&igrave;n xe ph&iacute;a trước chạy qua một vật cố định n&agrave;o đ&oacute; ở b&ecirc;n đường: cột đ&egrave;n, biển b&aacute;o&hellip; v&agrave; bắt đầu đếm&nbsp;ước lượng từ 1 đến 3, khoảng thời gian tưởng ứng đủ 3 gi&acirc;y). Nếu trời mưa hoặc tầm quan s&aacute;t bị ảnh hưởng, th&igrave; n&ecirc;n tăng l&ecirc;n 4-5 gi&acirc;y. H&atilde;y ch&uacute; &yacute; c&aacute;c biển chỉ dẫn lưu &yacute; khoảng c&aacute;ch 50 &ndash; 100 &ndash; 200m.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash;<span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span><span style=\"font-family:inherit; font-size:inherit\">Kh&ocirc;ng bao giờ l&ugrave;i xe, quay đầu xe, đi ngược chiều tr&ecirc;n đường cao tốc</span>.<span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span><span style=\"font-family:inherit; font-size:inherit\">Kh&ocirc;ng được cho xe &ocirc; t&ocirc; chạy ở l&agrave;n dừng xe khẩn cấp v&agrave; phần lề đường</span>.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash;<span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span><span style=\"font-family:inherit; font-size:inherit\">Khi v&agrave;o hoặc ra khỏi đường cao tốc phải giảm tốc độ</span><span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span>v&agrave;<span style=\"font-family:inherit; font-size:inherit\"><span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span></span><span style=\"font-family:inherit; font-size:inherit\">nhường đường cho xe đi tr&ecirc;n l&agrave;n đường ch&iacute;nh</span>.<span style=\"font-family:inherit; font-size:inherit\"><span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span></span><span style=\"font-family:inherit; font-size:inherit\">Chỉ được chuyển l&agrave;n đường ở những nơi cho ph&eacute;p</span>,<span style=\"font-family:inherit; font-size:inherit\">&nbsp;</span>khi chuyển l&agrave;n lu&ocirc;n ch&uacute; &yacute; ph&iacute;a sau v&agrave; lu&ocirc;n xi-nhan. Kh&ocirc;ng chuyển l&agrave;n kiểu cắt đầu xe kh&aacute;c v&agrave; chuyển nhiều l&agrave;n đường c&ugrave;ng một thời điểm.</span></p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:rgb(0, 0, 0); font-family:inherit; font-size:inherit\">&ndash; Người điều khiển v&agrave; người ngồi trong xe &ocirc;t&ocirc; đều phải thắt d&acirc;y an to&agrave;n. Bởi khi lưu th&ocirc;ng với tốc độ cao (100km/h), th&igrave; thắt d&acirc;y an to&agrave;n cho mọi người tr&ecirc;n xe &ocirc; t&ocirc; l&agrave; việc cần thiết hơn bao giờ hết.</span></p>\r\n', 'huong-dan-lai-xe-o-to-an-toan-tren-duong-cao-toc', 'vi', '0', 1453864782, 22, 1, 0, '', '', ''),
(15, 'Gợi y 8 lộ trình về quê ăn tết tránh kẹt xe ở hà nội', '', 0, 'upload/img/new4.jpg', '', 'goi-y-8-lo-trinh-ve-que-an-tet-tranh-ket-xe-o-ha-noi', 'vi', '0', 1453864774, 22, 1, 0, '', '', ''),
(16, 'Hơn 2000 người tham gia hưởng ứng \"Năm an toàn giao thông\" 2016', '', 0, 'upload/img/new31.jpg', '', 'hon-2000-nguoi-tham-gia-huong-ung-nam-an-toan-giao-thong-2016', 'vi', '0', 1453864761, 22, 1, 0, '', '', ''),
(17, 'Tăng phí trả vé tàu để hạn chế \"cò\" vé chợ đen.', '', 0, 'upload/img/new2.jpg', '', 'tang-phi-tra-ve-tau-de-han-che-co-ve-cho-den', 'vi', '0', 1453864807, 22, 1, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `inuser_category`
--

CREATE TABLE `inuser_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `focus` int(11) DEFAULT NULL,
  `hot` int(11) DEFAULT NULL,
  `tour` int(11) DEFAULT NULL,
  `sort` int(5) DEFAULT '1',
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inuser_category`
--

INSERT INTO `inuser_category` (`id`, `name`, `alias`, `description`, `image`, `parent_id`, `home`, `focus`, `hot`, `tour`, `sort`, `lang`, `title`) VALUES
(27, 'Nguyễn công hoan', 'nguyen-cong-hoan', '<p><em>Seaside Resort g&acirc;y ấn tượng với t&ocirc;i nhất nhờ dịch vụ rất ho&agrave;n hảo v&agrave; chuy&ecirc;n nghiệp. Seaside Resort l&agrave; một kh&aacute;ch sạn với đội ngũ nh&acirc;n vi&ecirc;n chuy&ecirc;n nghiệp, năng động, s&aacute;ng tạo, phong c&aacute;ch phục vụ v&agrave; chăm s&oacute;c kh&aacute;ch h&agrave;ng tốt. Hơn nữa ch&iacute;nh s&aacute;ch chăm s&oacute;c kh&aacute;ch h&agrave;ng nhiệt t&igrave;nh, chu đ&aacute;o ngay cả khi đ&atilde; ho&agrave;n th&agrave;nh hợp đồng.</em></p>\r\n', 'upload/img/inuser/avt.png', 0, 1, NULL, NULL, NULL, 9, 'vi', 'Big Boss'),
(28, 'Mrs bin', 'doctor', 'Seaside Resort impresses me most with its excellent service and professionalism. Seaside Resort is a hotel with professional staffs', 'upload/img/traveler_story111.png', 0, 1, 0, 0, 0, 1, 'en', 'doctor'),
(29, 'Nguyễn thành đạt', 'nguyen-thanh-dat', '<p>Thật tuyệt khi sử dụng dịch vụ tại Thăng Long, t&ocirc;i cảm thấy m&igrave;nh được phục vụ v&ocirc; c&ugrave;ng chu đ&aacute;o v&agrave; tận t&igrave;nh.Chắc chắn t&ocirc;i sẽ quay lại mua h&agrave;ng tại Thăng Long lần nữa.Ch&uacute;c Thăng Long ph&aacute;t triển mạnh mẽ hơn nữa, t&ocirc;i tin chắc điều đ&oacute;.</p>\r\n', 'upload/img/inuser/avt.png', 0, 1, NULL, NULL, NULL, 5, 'vi', 'VNPT Technology '),
(30, 'Trưởng phòng HLC Group', 'truong-phong-hlc-group', '<p>&nbsp;</p>\r\n\r\n<p>Y&ecirc;u cầu của ch&uacute;ng t&ocirc;i l&agrave; mỗi ph&ograve;ng h&aacute;t phải l&agrave; một kh&ocirc;ng gian đẹp, một phong c&aacute;ch kh&aacute;ch nhau để những kh&aacute;ch h&agrave;ng muốn kh&aacute;m ph&aacute; khi đến với ch&uacute;ng t&ocirc;i họ lu&ocirc;n lu&ocirc;n thấy thoải m&aacute;i.</p>\r\n', 'upload/img/inuser/avt1.png', 0, 1, NULL, NULL, NULL, 10, 'vi', 'Phạm Minh Quân');

-- --------------------------------------------------------

--
-- Table structure for table `inuser_to_category`
--

CREATE TABLE `inuser_to_category` (
  `id` int(11) NOT NULL,
  `id_inuser` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inuser_to_category`
--

INSERT INTO `inuser_to_category` (`id`, `id_inuser`, `id_category`) VALUES
(25, 1, 20),
(29, 3, 22),
(30, 2, 22),
(32, 4, 22),
(34, 5, 22),
(38, 9, 22),
(39, 6, 23),
(40, 8, 23),
(41, 7, 23),
(50, 11, 20),
(51, 12, 20),
(52, 13, 20),
(53, 16, 22),
(54, 15, 22),
(55, 14, 22),
(56, 17, 22);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datetime` int(11) DEFAULT NULL,
  `timeupdate` int(11) DEFAULT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_ward` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_birthday` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `user_sale` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `total_price` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `customer_pay` int(11) DEFAULT NULL,
  `customer_payted` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `time_buy` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `shipping` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `code`, `date`, `time`, `datetime`, `timeupdate`, `customer`, `customer_id`, `customer_name`, `customer_email`, `customer_phone`, `customer_address`, `customer_place`, `customer_ward`, `customer_birthday`, `user_create`, `user_sale`, `note`, `total_price`, `price_sale`, `customer_pay`, `customer_payted`, `status`, `time_buy`, `type`, `count`, `discount`, `shipping`) VALUES
(40, 'HD40', '17/05/2018', '17:12', 1526490000, 1526551961, NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '', 358000, NULL, 450840, 0, 1, 1526551961, 0, 2, 2, 100000);

-- --------------------------------------------------------

--
-- Table structure for table `invoices_detail`
--

CREATE TABLE `invoices_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `price_imp` int(11) DEFAULT NULL,
  `sale` int(11) DEFAULT NULL,
  `inv_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices_detail`
--

INSERT INTO `invoices_detail` (`id`, `name`, `code`, `quantity`, `price`, `price_sale`, `price_imp`, `sale`, `inv_id`, `product_id`, `note`) VALUES
(116, 'Tinh Chất Dưỡng Da 3 Trong 1 Chống Lão Hoá Innisfree Jeju Bamboo All-In-One Fluid 100ml', NULL, 1, 229000, 229000, NULL, NULL, 40, 95, NULL),
(115, 'Bộ Dưỡng Da Dùng Thử Innisfree Trà Xanh Green Tea Special Kit Set', NULL, 1, 129000, 129000, NULL, NULL, 40, 97, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `alias` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `alias`, `name`) VALUES
(1, 'vi', 'Tiếng Việt'),
(2, 'en', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `map_shopping`
--

CREATE TABLE `map_shopping` (
  `id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `tim_kiem` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `toa_domap` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `toa_dohienthi` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `diachi_shop` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `phone` char(150) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `map_shopping`
--

INSERT INTO `map_shopping` (`id`, `title`, `tim_kiem`, `toa_domap`, `toa_dohienthi`, `diachi_shop`, `phone`, `lang`) VALUES
(2, 'Chi nhánh camera siêu net tại Hà Nội', '168 Nguyễn Tuân - Thanh Xuân Hà Nội', '(20.998863, 105.80291809999994)', '20.998863, 105.80291809999994', '168 Nguyễn Tuân - Thanh Xuân Hà Nội', '0918.041616 - 0987.041616', 'vi'),
(5, 'Chi nhánh camera siêu net tại Hải Phòng', '52 Lê Quang Đạo - Nam Từ Liêm - Hà Nội', '', '', 'Số 66, Trường Chinh, Kiến An, Hải Phòng', '031 3603208', 'vi'),
(6, 'Chi nhánh camera siêu net tại TP. HCM', 'Tp HCM', '(10.7764745, 106.70088310000006)', '10.7764745, 106.70088310000006', '212/58 Thoại Ngọc Hầu, P. Phú Thạnh, Q. Tân Phú, TP. HCM', '08 39722693', 'vi'),
(7, 'Chi nhánh camera siêu net tại Yên Bái', 'Yên Bái', '(21.6837923, 104.4551361)', '21.6837923, 104.4551361', '168 Nguyễn Tuân - Yên Bái', '0918.041616 - 0987.041616', 'vi'),
(11, 'cừa hàng thời trang', 'cua hang so 23 ngo 229 cầu giấy hà nội', '(21.0477839, 105.79456129999994)', '21.0477839, 105.79456129999994', 'cua hang so 23 ngo 229 cầu giấy hà nội', '0988787654', 'vi');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `content` text CHARACTER SET utf8,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `image` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `name`, `description`, `content`, `title_seo`, `description_seo`, `keyword_seo`, `lang`, `category_id`, `home`, `hot`, `focus`, `sort`, `view`, `image`, `active`, `alias`, `link`) VALUES
(1, 'album hè cắm trại 2018', '<p>nội dung m&ocirc; tả</p>\r\n', '<p>nội dung chi tiết</p>\r\n', 'Không gian nhà hàng', 'Không gian nhà hàng', 'Không gian nhà hàng', 'vi', 11, 1, NULL, NULL, 1, 0, 'upload/img/media/dia-diem-du-lich-54.jpg', 1, 'album-he-cam-trai-2018', ''),
(10, 'album anh  cam trại hè', '', '', '', '', '', 'vi', 11, 1, NULL, NULL, 2, 0, 'upload/img/media/dia-diem-du-lich-4.jpg', 1, 'album-anh-cam-trai-he', ''),
(11, 'up anh jpeg cha le khong duoc-12', '<p>m&ocirc;i tả</p>\r\n', '', '', '', '', 'vi', 1, NULL, NULL, 1, 3, 0, 'upload/img/media/1233.JPEG', 1, 'up-anh-jpeg-cha-le-khong-duoc-12', 't0WFOnwp3MM');

-- --------------------------------------------------------

--
-- Table structure for table `media_category`
--

CREATE TABLE `media_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `coupon` tinyint(1) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` text CHARACTER SET utf8,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `parent_id` int(11) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `left_right` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media_category`
--

INSERT INTO `media_category` (`id`, `name`, `alias`, `sort`, `home`, `focus`, `coupon`, `image`, `title_seo`, `description_seo`, `keyword_seo`, `lang`, `description`, `parent_id`, `hot`, `left_right`) VALUES
(1, 'Hình ảnh', 'hinh-anh', 2, NULL, NULL, NULL, 'upload/img/media/anh1.jpg', 'Hình ảnh', 'gdsagds', NULL, 'vi', '<p>noi dung m&ocirc; tả</p>\r\n', 0, NULL, 1),
(11, 'album anh nam 2019', 'album-anh-nam-2019', 5, 1, NULL, NULL, 'upload/img/media/dia-diem-du-lich-5.jpg', '', '', NULL, 'vi', '<p>m&ocirc; tả</p>\r\n', 1, NULL, 1),
(10, 'album nam 2018', 'album-nam-2018', 4, 1, NULL, NULL, 'upload/img/media/anh.jpg', '', '', NULL, 'vi', '<p>noi dung m&ocirc; tả cho album</p>\r\n', 1, NULL, 1),
(12, 'Hình ảnh hội nghị', 'hinh-anh-hoi-nghi', 6, 1, NULL, NULL, 'upload/img/media/dia-diem-du-lich-3.jpg', '', '', NULL, 'vi', '', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `media_images`
--

CREATE TABLE `media_images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` char(200) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media_images`
--

INSERT INTO `media_images` (`id`, `name`, `id_item`, `image`, `url`, `sort`) VALUES
(1, 'anh so 1', 1, 'upload/img/media_multi/ae20248dc61407525e7a96a1b002c72b.jpg', NULL, NULL),
(2, 'anh so 2', 1, 'upload/img/media_multi/67594498cb19b94e98cc1c2095c83c51.jpg', NULL, NULL),
(4, 'anh so 4', 1, 'upload/img/media_multi/44bb59baff034000b0f46258088bf8b8.jpg', NULL, NULL),
(5, 'anh so 5', 1, 'upload/img/media_multi/036d5e089f887f4687e3379500c8256d.jpg', NULL, NULL),
(6, 'anh so 6', 1, 'upload/img/media_multi/fa02a841c335c7566a42548fe1c0083d.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seturl` tinyint(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `module` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT '0',
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `view_type` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `style` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `image`, `alias`, `position`, `target`, `seturl`, `parent_id`, `description`, `module`, `cat_id`, `sort`, `home`, `lang`, `view_type`, `style`) VALUES
(1, 'Giới thiệu', 'page/gioi-thieu.html', NULL, 'gioi-thieu', 'main', '', NULL, 0, '<p>introduction</p>\r\n', 'pages', 0, 1, 0, 'vi', NULL, NULL),
(7, 'Liên hệ', 'contact', NULL, 'lien-he', 'main', '', NULL, 0, '', '0', 0, 4, 0, 'vi', NULL, NULL),
(39, 'Trang chủ', 'trang-chu', 'upload/img/menus/img_top1.png', 'trang-chu', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(40, 'Giới thiệu', 'gioi-thieu', 'upload/img/menus/img_top2.png', 'gioi-thieu', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(41, 'Thông báo', 'thong-bao', 'upload/img/menus/img_top3.png', 'thong-bao', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(42, 'Thanh toán mua hàng', 'thanh-toan-mua-hang', 'upload/img/menus/img_top4.png', 'thanh-toan-mua-hang', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(43, 'Khuyến mãi', 'khuyen-mai', NULL, 'khuyen-mai', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(44, 'Góp ý', 'gop-y', NULL, 'gop-y', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(45, 'Liên hệ', 'lien-he', NULL, 'lien-he', 'bottom_2', '', NULL, 0, '', '0', 0, NULL, 0, 'vi', NULL, NULL),
(46, 'Đặt hàng online', '#', 'upload/img/menus/a4.png', 'dat-hang-online', 'top', '', NULL, 0, '', '0', 0, 0, 0, 'vi', NULL, NULL),
(47, 'Giao hàng tận nơi', '#', 'upload/img/menus/a3.png', 'giao-hang-tan-noi', 'top', '', NULL, 0, '', '0', 0, 1, 0, 'vi', NULL, NULL),
(48, 'Hỗ trợ online', '#', 'upload/img/menus/a5.png', 'ho-tro-online', 'top', '', NULL, 0, '', '0', 0, 2, 0, 'vi', NULL, NULL),
(58, 'Tư vấn bán lẻ', 'tu-van-ban-le', 'upload/img/menus/img_box_footer4.png', 'tu-van-ban-le', 'bottom', '', NULL, 0, '<p>text demo sản phẩm</p>\r\n', '0', 0, 0, 0, 'vi', NULL, NULL),
(102, 'Sofa nỉ', 'danh-muc/sofa-ni.html', NULL, 'sofa-ni', 'main', '', NULL, 83, '', 'products', 8, 4, 0, 'vi', NULL, NULL),
(71, 'Home', 'home', NULL, 'home', 'main', '', NULL, 0, '', '0', 0, 0, 0, 'en', NULL, NULL),
(72, 'About', 'home-2', NULL, 'about', 'main', '', NULL, 0, '', '0', 0, 1, 0, 'en', NULL, NULL),
(76, 'Hỗ trợ 096 180 3334', 'ho-tro-096-180-3334', NULL, 'ho-tro-096-180-3334', 'bottom', '', NULL, 58, '<p>0961803334</p>\r\n', '0', 0, 3, 0, 'vi', NULL, NULL),
(107, 'Sofa da', 'danh-muc/sofa-da.html', NULL, 'sofa-da', 'main', '', NULL, 83, '', 'products', 13, 9, 0, 'vi', NULL, NULL),
(80, 'tv1', 'huong-dan-mua-tra-gop', NULL, 'tv1', 'bottom', '', NULL, 58, '<p><strong><span style=\"color:#000000\">MrB</span>: <span style=\"color:#c0392b\">091111</span></strong></p>\r\n', '0', 0, 0, 0, 'vi', NULL, NULL),
(83, 'Sản phẩm', 'danh-muc/sofa-vang.html', 'upload/img/menus/img-menu.png', 'san-pham', 'main', '', NULL, 0, '', 'products', 1, 2, 0, 'vi', NULL, NULL),
(90, 'Sofa gia đình', 'danh-muc/sofa-gia-dinh.html', NULL, 'sofa-gia-dinh', 'main', '', NULL, 83, '', 'products', 5, 1, 0, 'vi', NULL, NULL),
(100, 'Sofa khách sạn', 'danh-muc/sofa-khach-san.html', NULL, 'sofa-khach-san', 'main', '', NULL, 83, '', 'products', 6, 2, 0, 'vi', NULL, NULL),
(101, 'Sofa cafe', 'danh-muc/sofa-cafe.html', NULL, 'sofa-cafe', 'main', '', NULL, 83, '', 'products', 7, 3, 0, 'vi', NULL, NULL),
(92, 'Sofa văng', 'danh-muc/sofa-vang.html', NULL, 'sofa-vang', 'main', '', NULL, 83, '', 'products', 0, 0, 0, 'vi', NULL, NULL),
(106, 'Sofa giá rẻ', 'danh-muc/sofa-gia-re.html', NULL, 'sofa-gia-re', 'main', '', NULL, 83, '', 'products', 12, 8, 0, 'vi', NULL, NULL),
(112, 'Trang chủ', 'trang-chu', NULL, 'trang-chu', 'main', '', NULL, 0, '', '0', 0, 0, 0, 'vi', NULL, NULL),
(103, 'Sofa karaoke', 'danh-muc/sofa-karaoke.html', NULL, 'sofa-karaoke', 'main', '', NULL, 83, '', 'products', 9, 5, 0, 'vi', NULL, NULL),
(104, 'Sofa góc', 'danh-muc/sofa-goc.html', NULL, 'sofa-goc', 'main', '', NULL, 83, '', 'products', 10, 6, 0, 'vi', NULL, NULL),
(105, 'Sofa cao cấp', 'danh-muc/sofa-cao-cap.html', NULL, 'sofa-cao-cap', 'main', '', NULL, 83, '', 'products', 11, 7, 0, 'vi', NULL, NULL),
(98, 'Blog', 'danh-muc-tin/tin-tuc.html', NULL, 'blog', 'main', '', NULL, 0, '', 'news', 4, 3, 0, 'vi', NULL, NULL),
(108, 'Hỗ trợ 096 180 3334', 'ho-tro-096-180-3334', NULL, 'ho-tro-096-180-3334', 'bottom', '', NULL, 58, '<p>0961703335 Shinosuke</p>\r\n', '0', 0, 2, 0, 'vi', NULL, NULL),
(119, 'tv2', '0999999980', NULL, 'tv2', 'bottom', '', NULL, 58, '<p><strong><span style=\"font-size:14px\">MrA</span></strong>: <span style=\"font-size:14px\"><strong><span style=\"color:#f1c40f\">0922222</span></strong></span></p>\r\n', '0', 0, 1, 0, 'vi', NULL, NULL),
(118, 'baolong@gmail.com', 'baolonggmailcom', NULL, 'baolonggmailcom', 'bottom', '', NULL, 110, '<p>baolong@gmail.com</p>\r\n', '0', 0, 1, 0, 'vi', NULL, NULL),
(110, ' Email liên hệ', 'email-lien-he', NULL, 'email-lien-he', 'bottom', '', NULL, 0, '', '0', 0, 1, 0, 'vi', NULL, NULL),
(111, 'congtybaolong@gmail.com', 'congtybaolong', NULL, 'congtybaolonggmailcom', 'bottom', '', NULL, 110, '<p>congtybaolong@gmail.com</p>\r\n', '0', 0, 0, 0, 'vi', NULL, NULL),
(120, 'Product', 'product', NULL, 'product', 'main', '', NULL, 0, '', '0', 0, 2, 0, 'en', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `hot` int(11) DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `tag` text COLLATE utf8_unicode_ci,
  `time_update` int(8) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `focus` int(11) DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` text COLLATE utf8_unicode_ci,
  `video` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `button_1` int(11) NOT NULL,
  `sort` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `hot`, `image`, `content`, `alias`, `lang`, `tag`, `time_update`, `time`, `category_id`, `home`, `focus`, `title_seo`, `keyword_seo`, `description_seo`, `video`, `view`, `active`, `button_1`, `sort`) VALUES
(11, 'Lưu ý chọn ghế Sofa Cafe cho không gian quán Cafe của bạn', '<p>Qu&aacute;n Cafe với thiết kế v&agrave; c&aacute;ch b&agrave;i tr&iacute; độc đ&aacute;o chắc chắn sẽ tạo n&ecirc;n điểm mới lạ v&agrave; thu h&uacute;t c&agrave;ng nhiều kh&aacute;ch h&agrave;ng. V&agrave; nội thất l&agrave; yếu tố đ&oacute;ng vai tr&ograve; kh&ocirc;ng nhỏ tạo n&ecirc;n vẻ đẹp tổng thể của kh&ocirc;ng gian qu&aacute;n cafe. Ng&agrave;y nay, Sofa được sử dụng ng&agrave;y c&agrave;ng phổ biến khi trang tr&iacute; qu&aacute;n cafe. Bạn muốn l&agrave;m mới kh&ocirc;ng gian qu&aacute;n cafe của m&igrave;nh bằng ghế sofa. Tham khảo một số gợi &yacute; chọn&nbsp;<strong>ghế sofa cafe</strong>&nbsp;sau để c&oacute; chọn lựa đ&uacute;ng đắn nhất.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 'upload/img/news/sofacafe1.jpg', '<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>1. Chọn ghế sofa cafe h&agrave;i h&ograve;a với tổng thể của qu&aacute;n</strong></span></span></h2>\r\n\r\n<p>- Khi chọn ghế sofa cho qu&aacute;n cafe c&aacute;c bạn cần lưu &yacute; đến phong c&aacute;ch m&agrave; bạn hướng đến, nếu qu&aacute;n cafe của bạn c&oacute; phong c&aacute;ch năng động, trẻ trung th&igrave; n&ecirc;n chọn những loại ghế c&oacute; kiểu d&aacute;ng độc đ&aacute;o, m&agrave;u sắc tươi tắn. Hoặc nếu đối tượng kh&aacute;ch h&agrave;ng của bạn l&agrave; những người trưởng th&agrave;nh, thanh lịch th&igrave; những bộ ghế gam m&agrave;u trung t&iacute;nh, thiết kế đơn giản dẽ ph&ugrave; hợp hơn.</p>\r\n\r\n<p><img alt=\"Sofa cafe\" src=\"/upload/images/sofacafe6.jpg\" style=\"height:433px; width:650px\" /></p>\r\n\r\n<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>2. K&iacute;ch thước ghế sofa cafe</strong></span></span></h2>\r\n\r\n<p>- Bạn kh&ocirc;ng thể chọn lựa một mẫu ghế sofa k&iacute;ch thước lớn cho diện t&iacute;ch qu&aacute;n cafe nhỏ ch&iacute;nh v&igrave; thế bạn h&atilde;y lựa chọn những mẫu ghế vừa phải để c&acirc;n đối kh&ocirc;ng gian cho những đồ nội thất quan trọng kh&aacute;c của qu&aacute;n. Bạn n&ecirc;n c&oacute; những t&iacute;nh to&aacute;n , đo đạc cần thiết trước khi đặt mua để mang lại sự hợp l&iacute; nhất cho nội thất qu&aacute;n.</p>\r\n\r\n<p><img alt=\"Sofa2\" src=\"/upload/images/sofacafe3.jpg\" style=\"height:448px; width:746px\" /></p>\r\n\r\n<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>3. Thiết kế ghế sofa cafe</strong></span></span></h2>\r\n\r\n<p>- C&oacute; rất nhiều &yacute; tưởng để bạn lựa chọn với xu hướng thiết kế ghế sofa cho qu&aacute;n cafe hiện đại ng&agrave;y nay. Những yếu tố như chất liệu, m&agrave;u sắc n&ecirc;n được c&acirc;n nhắc kĩ c&agrave;ng để mang tới sự nổi bật cho nội thất qu&aacute;n cafe.</p>\r\n\r\n<p><img alt=\"Sofa cafe\" src=\"/upload/images/sofacafe7.jpg\" style=\"height:704px; width:564px\" /></p>\r\n\r\n<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>4. Ưu ti&ecirc;n chất lượng khi chọn ghế sofa cho qu&aacute;n cafe</strong></span></span></h2>\r\n\r\n<p>- Chọn những đồ nội thất chất lượng cho qu&aacute;n cafe l&agrave; điều rất quan trọng. Đầu ti&ecirc;n, một bộ sofa chất lượng chắc chắn sẽ g&acirc;y ấn tượng tốt với kh&aacute;ch h&agrave;ng v&agrave; sau đ&oacute; những bộ b&agrave;n ghế c&oacute; độ bền cao sẽ gi&uacute;p c&aacute;c bạn tiết kiệm được chi ph&iacute; ph&aacute;t sinh về sau.</p>\r\n\r\n<p><img alt=\"Sofacafe4\" src=\"/upload/images/sofacafe4.jpg\" style=\"height:539px; width:650px\" /></p>\r\n\r\n<h2><span style=\"font-size:18px\"><span style=\"color:#3498db\"><strong>5. H&agrave;i h&ograve;a với những đồ nội thất kh&aacute;c trong qu&aacute;n</strong></span></span></h2>\r\n\r\n<p>- Kh&aacute;ch h&agrave;ng sẽ v&ocirc; c&ugrave;ng h&agrave;i l&ograve;ng nếu c&aacute;c bạn biết c&aacute;ch kết hợp h&agrave;i h&ograve;a cho tổng thể kh&ocirc;ng gian của m&igrave;nh. C&aacute;c bạn c&oacute; thể chọn những bộ ghế sofa c&oacute; m&agrave;u sắc tươi s&aacute;ng cho những qu&aacute;n cafe c&oacute; kh&ocirc;ng gian mở, kh&ocirc;ng n&ecirc;n chọn những sản phẩm qu&aacute; l&ograve;e loẹt, sặc sỡ v&igrave; n&oacute; kh&ocirc;ng được tự nhi&ecirc;n, gần gũi. Ngo&agrave;i ra, c&aacute;c bạn c&oacute; thể tạo điểm nhấn bằng thảm sofa hoặc gối trang tr&iacute; độc đ&aacute;o, ấn tượng. N&ecirc;n đặc biệt lưu &yacute; phối kết hợp với những đồ nội thất kh&aacute;c để đạt được hiệu quả thẩm mĩ cao nhất.</p>\r\n\r\n<p><img alt=\"Sofacafe9\" src=\"/upload/images/sofacafe8.jpg\" style=\"height:518px; width:800px\" /></p>\r\n\r\n<h4><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></h4>\r\n\r\n<h2><span style=\"color:#e74c3c\"><strong>C&Ocirc;NG TY NỘI THẤT BẢO LONG</strong></span></h2>\r\n\r\n<p>&nbsp;Địa chỉ: Địa chỉ: 130 Ho&agrave;ng C&ocirc;ng Chất, P. Ph&uacute; Diễn, Q. Bắc Từ Li&ecirc;m, Tp. H&agrave; Nội</p>\r\n\r\n<p><a href=\"http://sofabaolam.com/#\" title=\"tel:0986199122\">&nbsp;Hotline: 0963761678</a></p>\r\n\r\n<p>&nbsp;Email: tranlinh.manh@gmail.com</p>\r\n', 'luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'vi', NULL, NULL, 1561111166, 4, 1, 1, 'Lưu ý chọn ghế Sofa Cafe cho không gian quán Cafe của bạn', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Lưu ý chọn ghế Sofa Cafe cho không gian quán Cafe của bạn đẹp và tinh tế', '', 55, 1, 0, 3),
(12, 'Những lưu ý cực quan trọng khi chọn Sofa cho gia đình', '<p><em>Để lựa chọn một bộ sofa đẹp&nbsp;về mẫu m&atilde;, đạt chuẩn chất lượng kh&ocirc;ng phải l&agrave; điều dễ d&agrave;ng. C&oacute; lẽ phải mất kh&aacute; nhiều thời gian t&igrave;m hiểu kĩ c&agrave;ng nhưng chi qua 8 lưu &yacute; cực k&igrave; quan trọng khi mua ghế sofa của Dcor chắc chắn gia đ&igrave;nh c&oacute; được lựa chọn tuyệt vời nhất.</em></p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 'upload/img/news/166sofa111.png', '<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>1. Phần khung</strong></span></span></h4>\r\n\r\n<p>- Khi chọn mua ghế sofa kh&aacute;ch h&agrave;ng cần quan t&acirc;m đầu ti&ecirc;n đến phần khung của bộ ghế. Trong 1 bộ ghế sofa th&igrave; phần khung l&agrave; phần quan trọng nhất. C&aacute;c phần khung chịu lực ch&iacute;nh được l&agrave;m bằng gỗ tự nhi&ecirc;n để tạo kết cấu vững chắc, kh&ocirc;ng biến dạng cong v&ecirc;nh mối mọt trong qu&aacute; tr&igrave;nh sử dụng. Li&ecirc;n kết giữa c&aacute;c kết cấu n&agrave;y bằng mộng, đinh hoặc v&iacute;t đen nhằm tạo n&ecirc;n kết cấu ổn định nhất. Để biết được khung gỗ c&oacute; đạt chất lượng vững chắc hay kh&ocirc;ng, bạn chỉ cần kiểm tra sự ổn định của bộ ghế như khi x&ocirc; vịn kh&ocirc;ng bị ọp ẹp lung lanh, b&ecirc; nặng tay, khung chắc chắn, c&oacute; thể đứng l&ecirc;n những phần tay, lưng để nh&uacute;n m&agrave; kh&ocirc;ng bị v&otilde;ng hay hỏng.</p>\r\n\r\n<p>- Bạn c&oacute; thể t&igrave;m những sản phẩm sofa c&oacute; phần khung l&agrave;m từ c&aacute;c loại gỗ cứng sấy kh&ocirc; như gỗ dầu, th&ocirc;ng, sồi, bạch dương.. kết hợp với c&aacute;c loại v&aacute;n &eacute;p chất lượng cao hoặc v&aacute;n &eacute;p chịu nước.L&ograve; xo dạng xoắn đứng cũng l&agrave; loại tốt, tuy nhi&ecirc;n loại l&ograve; xo dạng d&acirc;y cuộn sẽ thoải m&aacute;i v&agrave; &iacute;t tốn k&eacute;m hơn.</p>\r\n\r\n<p><img alt=\"Sofagiadinh1\" src=\"/upload/images/166sofa21%20(1).jpg\" style=\"height:612px; width:790px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>2. Phần nệm</strong></span></span></h4>\r\n\r\n<p>- Phần nệm ch&iacute;nh l&agrave; yếu tố ch&iacute;nh tạo cảm gi&aacute;c &ecirc;m &aacute;i, thoải m&aacute;i, dễ chịu khi sử dụng. Điều đ&oacute; đạt được khi chất lượng nệm đạt ti&ecirc;u chuẩn. Để thử chất lượng nệm bạn c&oacute; thể ngồi, nh&uacute;n l&ecirc;n xuống ngay khi lựa chọn mua sofa. Nếu cảm gi&aacute;c &ecirc;m &aacute;i, kh&ocirc;ng bị l&uacute;n tụt v&agrave; g&ugrave; lưng th&igrave; đ&oacute; l&agrave; mẫu nệm đạt chuẩn.</p>\r\n\r\n<p>- Phần nệm hầu hết được l&agrave;m từ c&aacute;c chất polyurethane tổng hợp, kh&aacute;c nhau về độ nặng, đặc sẽ ảnh hưởng đến độ mềm, đ&agrave;n hồi của phần ngồi. C&aacute;c d&ograve;ng nội thất gi&aacute; rẻ thường chỉ d&ugrave;ng 1 loại m&uacute;t cố định, hoặc l&agrave; loại rỗng cao (như D25), tạo cảo gi&aacute; mềm nhưng mau xẹp, hoặc l&agrave; loại đặc hẳn (như D40), tạo cảm gi&aacute;c bền nhưng ngồi lại kh&ocirc;ng thoải m&aacute;i. D&ograve;ng nội thất tốt hơn th&igrave; c&oacute; thể bọc th&ecirc;m một &iacute;t g&ograve;n. Loại nệm chất lượng cao l&agrave; loại nệm được kết hợp nhiều loại m&uacute;t với c&aacute;c độ đ&agrave;n hồi kh&aacute;c nhau, kết hợp với c&aacute;c lớp g&ograve;n, hoặc c&oacute; thể l&agrave; l&ocirc;ng vũ, để tạo độ phồng v&agrave; mềm mại - khi tiếp x&uacute;c</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình 2\" src=\"/upload/images/156sofa12445.png\" style=\"height:532px; width:632px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>3. Phần l&ograve; xo đ&agrave;n hồi</strong></span></span></h4>\r\n\r\n<p>- L&ograve; xo l&agrave; phần chịu lực ngồi của sofa n&ecirc;n khi mua ghế sofa cần lưu &yacute; sử dụng l&ograve; xo c&oacute; độ đ&agrave;n hồi tốt v&igrave; n&oacute; l&agrave; kết cấu gắn liền với khung ghế, chịu lực t&aacute;c động trực tiếp từ đệm ngồi xuống ghế. L&ograve; xo được l&agrave;m bằng th&eacute;p l&ograve; xo đặc biệt S60C, chịu đ&agrave;n hồi cao, &ecirc;m &aacute;i khi sử dụng, bền với thời gian. Kh&ocirc;ng bị gi&atilde;n lỏng trong suốt qu&aacute; tr&igrave;nh sử dụng, chịu được lực n&eacute;n lớn.</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/sofavang1.jpg\" style=\"height:534px; width:800px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>4. Ch&acirc;n ghế</strong></span></span></h4>\r\n\r\n<p>- Ch&acirc;n ghế lu&ocirc;n cần lưu &yacute; tới độ cao. Ch&acirc;n ghế cần cao vừa tầm người d&ugrave;ng mang lại cảm gi&aacute;c tiện nghi, c&acirc;n đối trong kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch. Hơn nữa, cũng cần lưu &yacute; tới chất liệu của ch&acirc;n ghế. Nếu ch&acirc;n gỗ kiểm tra độ cứng, chắc của ch&acirc;n tr&aacute;nh bị mối mọt.</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/Picture14-700x441.gif\" style=\"height:441px; width:700px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>5. Kiểu d&aacute;ng của ghế</strong></span></span></h4>\r\n\r\n<p>- Trước khi đi mua ghế sofa bạn h&atilde;y tham khảo một số kiểu d&aacute;ng ghế sofa tr&ecirc;n mạng hoặc tham khảo &yacute; kiến của kiến tr&uacute;c sư hoặc th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh . H&atilde;y chọn ghế sofa bổ sung cho phong c&aacute;ch trang tr&iacute; của cả ng&ocirc;i nh&agrave;. Với kh&ocirc;ng gian nội thất theo phong c&aacute;ch hiện đại việc lựa chọn một mẫu sofa hiện đại hoặc sofa H&agrave;n Quốc l&agrave; kh&ocirc;ng thể bỏ qua. C&aacute;c kiểu sofa hiện đại c&oacute; xu hướng đường n&eacute;t thẳng, r&otilde; r&agrave;ng, ph&oacute;ng kho&aacute;ng, bọc s&aacute;t đất hoặc ch&acirc;n inox cao hẳn.</p>\r\n\r\n<p><img alt=\"Sofa gia đình\" src=\"/upload/images/lua-chon-ban-ghe-sofa-cho-can-ho.jpg\" style=\"height:511px; width:766px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>6. K&iacute;ch cỡ</strong></span></span></h4>\r\n\r\n<p>- Cần phải c&acirc;n đối giữa k&iacute;ch thước bộ b&agrave;n ghế sofa với diện t&iacute;ch kh&ocirc;ng gian của ph&ograve;ng kh&aacute;ch. Với kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch nhỏ hẹp bộ b&agrave;n ghế sofa chữ L vừa gi&uacute;p tiết kiệm diện t&iacute;ch v&ugrave;a mang lại cảm gi&aacute;c gọn g&agrave;ng, sang trọng cho ph&ograve;ng kh&aacute;ch. Nếu c&oacute; lợi thế về diện t&iacute;ch gia đ&igrave;nh c&oacute; nhiều lựa chọn hơn như b&agrave;n ghế sofa văng, sofa chữ U&hellip;</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/156sofa111.png\" style=\"height:577px; width:631px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>7. Chất liệu bọc</strong></span></span></h4>\r\n\r\n<p>- Vải bọc l&agrave; phần b&ecirc;n ngo&agrave;i của chiếc ghế sofa, khi mua ghế sofa kh&aacute;ch h&agrave;ng n&ecirc;n lưu &yacute; đến vỏ bọc n&ecirc;n d&ugrave;ng chất liệu da hay nỉ, vải. Việc chọn chất liệu bọc sofa phụ thuộc v&agrave;o nhu cầu sử dụng v&agrave; sở th&iacute;ch của mỗi gia đ&igrave;nh.</p>\r\n\r\n<p>- Nếu bạn chọn đặt đ&oacute;ng sofa thay v&igrave; mua c&oacute; sẵn, h&atilde;y chắc rằng bạn đ&atilde; tưởng tượng được tổng thể sản phẩm của m&igrave;nh sẽ l&ecirc;n như thế n&agrave;o, c&aacute;c loại vải phối hợp với nhau ra sao. Nếu bạn kh&ocirc;ng tin tưởng về điều đ&oacute;, bạn c&oacute; thể chọn l&agrave;m theo mẫu của catalogue hoặc y&ecirc;u cầu với nh&acirc;n vi&ecirc;n tư vấn của rOsano cho bạn xem mẫu phối tr&ecirc;n m&aacute;y t&iacute;nh.</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/tim-hieu-ve-bao-duong-sofa-da-dung-chuan-nhu-chuyen-gia-1.jpg\" style=\"height:469px; width:660px\" /></p>\r\n\r\n<h4><span style=\"font-size:16px\"><span style=\"color:#3498db\"><strong>8. Gối trang tr&iacute; sofa</strong></span></span></h4>\r\n\r\n<p>- T&ugrave;y v&agrave;o nhu cầu sử dụng cũng như thẩm mĩ m&agrave; gia chủ lựa chọn gối trang tr&iacute; với họa tiết hoa văn kh&aacute;c nhau. C&aacute;c gối trang tr&iacute; tại Dcor đa dạng về m&agrave;u sắc, chủng loại v&agrave; kiểu c&aacute;ch phục vụ nhu cầu kh&aacute;c nhau của người sử dụng.</p>\r\n\r\n<p><img alt=\"Sofa Gia Đình\" src=\"/upload/images/sofani111.jpg\" style=\"height:480px; width:782px\" /></p>\r\n\r\n<h4><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></h4>\r\n\r\n<h2><span style=\"color:#e74c3c\"><strong>C&Ocirc;NG TY NỘI THẤT BẢO LONG</strong></span></h2>\r\n\r\n<p>&nbsp;Địa chỉ: Địa chỉ: 130 Ho&agrave;ng C&ocirc;ng Chất, P. Ph&uacute; Diễn, Q. Bắc Từ Li&ecirc;m, Tp. H&agrave; Nội</p>\r\n\r\n<p><a href=\"http://sofabaolam.com/#\" title=\"tel:0986199122\">&nbsp;Hotline: 0963761678</a></p>\r\n\r\n<p>&nbsp;Email: tranlinh.manh@gmail.com</p>\r\n', 'nhung-luu-y-cuc-quan-trong-khi-chon-sofa-cho-gia-dinh', 'vi', NULL, NULL, 1561111289, 4, 1, 1, 'Những lưu ý cực quan trọng khi chọn Sofa cho gia đình', 'Những lưu ý cực quan trọng khi chọn Sofa cho gia đình', 'Những lưu ý cực quan trọng khi chọn Sofa cho gia đình', '', 23, 1, 0, 1),
(13, 'Chọn Sofa cho phòng Karaoke sao cho đúng cách??', '<p><em>Để lựa chọn được bộ ghế sofa ph&ugrave; hợp th&igrave; ch&uacute;ng ta n&ecirc;n x&aacute;c định r&otilde; k&iacute;ch thước ph&ograve;ng h&aacute;t, sofa chỉ đẹp khi được &aacute;p dụng ph&ugrave; hợp, một qu&aacute;n karaoke thường c&oacute; nhiều ph&ograve;ng h&aacute;t với diện t&iacute;ch kh&aacute;c nhau n&ecirc;n những thiết kế sofa cũng c&oacute; đ&ocirc;i phần kh&aacute;c nhau về k&iacute;ch thước. Th&ocirc;ng thường, trong ph&ograve;ng h&aacute;t thường sử dụng thiết kế chữ L hay chữ U để c&oacute; sức chứa lớn v&agrave; tạo sự tương t&aacute;c giữa mọi người. Nếu ph&ograve;ng c&oacute; diện t&iacute;ch nhỏ,&nbsp; n&ecirc;n chọn loại sofa chỉ c&oacute; ghế trường kỷ v&agrave; một chiếc b&agrave;n nhỏ, tạo kh&ocirc;ng gian tho&aacute;ng đ&atilde;ng tr&aacute;nh chọn thiết kế sofa lớn cho&aacute;n đầy kh&ocirc;ng gian sẽ g&acirc;y n&ecirc;n cảm gi&aacute;c ngột ngạt, chật chội. Ngược lại, với ph&ograve;ng rộng th&igrave; bạn n&ecirc;n chọn những bộ sofa gồm đầy đủ b&agrave;n, ghế, đ&ocirc;n&hellip; để lấp đầy c&aacute;c khoảng trống, tr&aacute;nh cảm gi&aacute;c bộ sofa đơn độc, lạnh lẽo hay trống trải.</em><br />\r\n&nbsp;</p>\r\n', 1, 'upload/img/news/sofakaoke9.jpg', '<p><span style=\"color:#3498db\"><strong>PH&Ograve;NG&nbsp;KARAOKE THEO PHONG C&Aacute;CH CỔ ĐIỂN</strong></span></p>\r\n\r\n<p>- Đối với ph&ograve;ng h&aacute;t karaoke theo phong c&aacute;ch cổ điển th&igrave; việc lựa chọn một bộ ghế sofa ph&ugrave; hợp cũng cần l&agrave;m nổi bật được sự sang trọng, lịch l&atilde;m đ&uacute;ng như c&aacute;i t&ecirc;n gọi của n&oacute;. V&igrave; vậy n&ecirc;n chọn những bộ sofa cổ điển với kiểu d&aacute;ng cầu kỳ, thiết kế tinh tế sắc sảo, tạo điểm nhấn ở c&aacute;c g&oacute;c cạnh bằng hoa văn uốn lượn tạo cảm gi&aacute;c l&ocirc;i cuốn kh&aacute;ch h&agrave;ng từ c&aacute;i nh&igrave;n đầu ti&ecirc;n.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"color:#e74c3c\"><em>Về chất liệu</em></span></p>\r\n\r\n<p>- Mang đặc trưng cổ điển n&ecirc;n chất liệu được lựa chọn cho ph&ograve;ng h&aacute;t karaoke phải l&agrave; những loại chất liệu tạo n&ecirc;n sự sang trọng, qu&yacute; ph&aacute;i. Hiện nay tr&ecirc;n thị trường, mẫu ghế sofa ng&agrave;y c&agrave;ng đa dạng v&agrave; phong ph&uacute; về kiểu d&aacute;ng như: sofa vải, giả da, nỉ,...với kiểu d&aacute;ng một m&agrave;u đơn thuần, sofa cổ điển với kiểu d&aacute;ng cầu kỳ, thiết kế tinh tế sắc sảo, tạo điểm nhấn ở c&aacute;c g&oacute;c cạnh bằng hoa văn uốn lượn tinh tế kh&oacute; phai, nhằm tạo điểm nhấn cũng như l&ocirc;i cuốn kh&aacute;ch h&agrave;ng ngay từ c&aacute;i nh&igrave;n đầu ti&ecirc;n.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"color:#e74c3c\"><em>Về m&agrave;u sắc</em></span></p>\r\n\r\n<p><br />\r\n- M&agrave;u sắc được xem như l&agrave; linh hồn của bộ ghế sofa cho ph&ograve;ng h&aacute;t karaoke cổ điển. Sofa karaoke&nbsp; phong c&aacute;ch cổ điển thường tập trung v&agrave;o những gam m&agrave;u &aacute;nh v&agrave;ng, bạc, đỏ n&acirc;u, xanh, sữa&hellip;tạo cảm gi&aacute;c quyền lực v&agrave; l&ocirc;i cuốn . Tất cả bảng m&agrave;u kết hợp với nhau tạo th&agrave;nh một tổng thể v&ocirc; c&ugrave;ng ho&agrave;n mỹ. C&oacute; nhiều chủ đầu tư họ lu&ocirc;n chọn m&agrave;u ph&ugrave; hợp với phong thủy, vận, mệnh của m&igrave;nh . Ngo&agrave;i ra khi chọn m&agrave;u bạn n&ecirc;n ch&uacute; &yacute; đến m&agrave;u sơn tường v&agrave; m&agrave;u sắc s&agrave;n nh&agrave; để lựa chọn m&agrave;u cho bộ sofa được ph&ugrave; hợp hơn . Th&ocirc;ng thường sofa karaoke cổ điển sẽ thiết kế theo h&igrave;nh chữ L, chữ U, &hellip; . diện t&iacute;ch ph&ograve;ng karaoke cổ điển sẽ lớn, ch&iacute;nh v&igrave; thế m&agrave; bộ ghế sofa cũng phải ph&ugrave; hợp.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"Sofa Karaoke\" src=\"/upload/images/sofa-karaoke.jpg\" style=\"height:677px; width:1118px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"color:#3498db\"><strong>PH&Ograve;NG KARAOKE PHONG C&Aacute;CH HIỆN ĐẠI&nbsp;&nbsp;</strong></span></p>\r\n\r\n<p>- Kh&aacute;c với ph&ograve;ng h&aacute;t karaoke theo phong c&aacute;ch cổ điển, ph&ograve;ng h&aacute;t theo phong c&aacute;ch hiện đại c&oacute; lượng kh&aacute;ch h&agrave;ng tập trung chủ yếu ở lứa tuổi học sinh, sinh vi&ecirc;n, thanh ni&ecirc;n, người trẻ tuổi,... n&ecirc;n ghế thường được sử dụng l&agrave; loại sofa bọc da, sofa vải đ&iacute;nh hạt cườm hoặc đ&aacute; trang tr&iacute;, tạo n&ecirc;n&nbsp; phong c&aacute;ch mới mẻ, lạ mắt cũng như tạo kh&ocirc;ng gian hiện đại, vui vẻ v&agrave; năng động cho kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p><em>&nbsp;<span style=\"color:#e74c3c\">Về chất liệu</span></em><br />\r\n<br />\r\n- Với thiết kế hiện đại th&igrave; chất liệu được ưu ti&ecirc;n ở đ&acirc;y ch&iacute;nh l&agrave; sofa da v&agrave; sofa vải. Chất liệu ghế sẽ g&oacute;p phần quyết định đến việc kh&aacute;ch h&agrave;ng c&oacute; thoải m&aacute;i hay kh&ocirc;ng. Chắc chắn một điều rằng nếu thoải m&aacute;i th&igrave; họ sẽ n&aacute;n lại l&acirc;u hơn cũng như muốn quay trở lại qu&aacute;n của bạn th&ecirc;m v&agrave;i lần kh&aacute;c nữa. Hơn thế nữa, nếu bạn lựa chọn một chất liệu k&eacute;m hay dễ d&iacute;nh bẩn v&agrave; kh&oacute; vệ sinh th&igrave; việc bạn phải giặt hằng ng&agrave;y l&agrave; điều đương nhi&ecirc;n.V&igrave; vậy, chất liệu da sẽ được sử dụng nhiều nhất bởi ưu điểm kh&ocirc;ng b&aacute;m bụi, dễ lau ch&ugrave;i v&agrave; c&oacute; bẩn hay đổ nước l&ecirc;n th&igrave; cũng dễ lau v&agrave; kh&ocirc;ng mất nhiều c&ocirc;ng sức. C&ograve;n nếu như bạn mua chất nỉ hay vải th&igrave; n&ecirc;n mua th&ecirc;m vỏ bọc b&ecirc;n ngo&agrave;i,&nbsp; vừa tiết kiệm thời gian cũng như c&ocirc;ng sức. Kh&ocirc;ng giống như c&aacute;c loại ghế sofa d&ugrave;ng trong ph&ograve;ng h&aacute;t mang phong c&aacute;ch cổ điển, ghế sofa d&ugrave;ng cho ph&ograve;ng h&aacute;t karaoke hiện đại kh&ocirc;ng cầu k&igrave; về kiểu d&aacute;ng cũng như họa tiết trang tr&iacute;. N&oacute; được thiết kế theo h&igrave;nh thức đơn giản h&oacute;a, nhẹ nh&agrave;ng nhưng trẻ trung, năng động. C&aacute;c mẫu ghế thường được sử dụng đ&oacute; l&agrave; loại ghế chữ U, chữ L, vừa tiết kiệm được kh&ocirc;ng gian, vừa mang lại cảm gi&aacute;c thoải m&aacute;i, tho&aacute;ng m&aacute;t cho căn ph&ograve;ng của bạn.</p>\r\n\r\n<p><em><span style=\"color:#e74c3c\">Về m&agrave;u sắc</span>&nbsp;</em></p>\r\n\r\n<p>- Tr&aacute;i ngược với h&igrave;nh thức trang tr&iacute;, sofa karaoke hiện đại lại mang m&agrave;u sắc nổi bật, bắt mắt. Bạn c&oacute; thể kết hợp nhiều m&agrave;u sắc hay chỉ để c&oacute; một m&agrave;u trơn, nhưng chung quy lại tất cả phải c&oacute; sự h&agrave;i h&ograve;a, g&acirc;y cảm gi&aacute;c ấn tượng cho kh&aacute;ch h&agrave;ng, kh&ocirc;ng bị rối mắt. Những m&agrave;u sắc thường d&ugrave;ng cho ghế sofa karaoke hiện đại sẽ l&agrave; m&agrave;u v&agrave;ng, xanh, cam, t&iacute;m, đỏ.... Một lưu &yacute; nhỏ nữa l&agrave; bạn n&ecirc;n chọn m&agrave;u của ghế ph&ugrave; hợp với &aacute;nh điện, m&agrave;u sơn tường v&agrave; s&agrave;n nh&agrave; để tạo n&ecirc;n kh&ocirc;ng gian thoải m&aacute;i nhất cho người sử dụng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"sofa Karaoke\" src=\"/upload/images/sofakaoke77.jpg\" style=\"height:935px; width:1400px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"color:#3498db\"><strong>P</strong><strong>H&Ograve;NG KARAOKE B&Igrave;NH D&Acirc;N</strong>&nbsp;<strong>&amp; PH&Ograve;NG KARAOKE GIA Đ&Igrave;NH</strong></span></p>\r\n\r\n<p>- Đối với ph&ograve;ng karaoke b&igrave;nh d&acirc;n hay những ph&ograve;ng karaoke gia đ&igrave;nh th&igrave; đơn giản hơn về h&igrave;nh thức, c&oacute; thể lựa chọn những mẫu ghế sofa đơn giản, kh&ocirc;ng cầu k&igrave; về họa tiết trang tr&iacute; nhưng vẫn phải đảm bảo chất lượng, tạo cảm gi&aacute;c thư gi&atilde;n thoải m&aacute;i cho kh&aacute;ch h&agrave;ng cũng như c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh sau mỗi giờ học tập v&agrave; l&agrave;m việc căng thẳng. Đa phần chiếc ghế sofa&nbsp; n&agrave;y mới nh&igrave;n sẽ rất giản dị b&igrave;nh thường kh&ocirc;ng cầu k&igrave; nhưng vẫn to&aacute;t l&ecirc;n sự tinh tế, ấn tượng. Với những ph&ograve;ng karaoke với diện t&iacute;ch hẹp th&igrave; những mẫu ghế sofa b&igrave;nh d&acirc;n với k&iacute;ch thước nhỏ, kh&ocirc;ng cầu k&igrave; sẽ l&agrave; hợp l&iacute; nhất.. Điểm đ&aacute;ng lưu &yacute; ở đ&acirc;y ch&iacute;nh l&agrave; chất liệu, v&igrave; n&oacute; sử dụng ở nhiều nơi kh&aacute;c nhau, trong qu&aacute;n h&aacute;t hay cả ở gia đ&igrave;nh th&igrave; chất liệu n&oacute; cũng vẫn cần được lưu &yacute;. C&oacute; nhiều đơn vị thi c&ocirc;ng họ sẽ d&ugrave;ng chất liệu vải, nỉ, da nhưng v&igrave; chi ph&iacute; đắt hơn n&ecirc;n cũng sẽ được hạn chế. Thường th&igrave; k&iacute;ch thước của những bộ sofa gi&aacute; b&igrave;nh d&acirc;n sẽ nhỏ hơn sofa karaoke hiện đại v&agrave; cổ điển, do diện t&iacute;ch ph&ograve;ng cũng sẽ nhỏ hơn n&ecirc;n thường sẽ sử dụng sofa đ&ocirc;i, sofa ba hay sofa g&oacute;c chữ L với ch&acirc;n gỗ chắc chắn v&agrave; chất lượng tốt kh&ocirc;ng thua k&eacute;m những kiểu sofa kh&aacute;c.&nbsp;</p>\r\n\r\n<p><span style=\"color:#e74c3c\"><em>Về m&agrave;u sắc</em></span></p>\r\n\r\n<p>- Đối với kiểu sofa cho ph&ograve;ng h&aacute;t karaoke b&igrave;nh d&acirc;n sẽ được chọn lựa theo sở th&iacute;ch hay phong thủy nhưng vẫn phải ph&ugrave; hợp với m&agrave;u sơn tường, m&agrave;u s&agrave;n nh&agrave;. C&oacute; thể lựa chọn những m&agrave;u thường tập trung chủ yếu v&agrave;o những gam m&agrave;u n&oacute;ng m&agrave;u trầm như m&agrave;u v&agrave;ng, v&agrave;ng &aacute;nh kim, m&agrave;u bạc, n&acirc;u, đỏ,&hellip;.tạo cảm gi&aacute;c đơn giản m&agrave; l&ocirc;i cuốn. M&agrave;u sắc lu&ocirc;n l&agrave; thứ t&acirc;m điểm được kh&aacute;ch h&agrave;ng ch&uacute; &yacute; v&agrave; c&oacute; ấn tượng nhiều nhất, ch&iacute;nh v&igrave; vậy, để thiết kế được mẫu ph&ograve;ng h&aacute;t b&igrave;nh d&acirc;n, ph&ograve;ng h&aacute;t gia đ&igrave;nh vừa đẹp về thẩm mỹ vừa đẹp về chất lượng, ch&uacute;ng t&ocirc;i lu&ocirc;n nỗ lực t&igrave;m t&ograve;i, nghi&ecirc;n cứu những c&aacute;ch phối hợp ho&agrave;n hảo để mang lại một kh&ocirc;ng gian sinh động, đa sắc m&agrave;u.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"Sofa Karaoke\" src=\"/upload/images/ban-ghe-sofa-karaoke-dep.jpg\" style=\"height:762px; width:1290px\" /></p>\r\n\r\n<h4><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></h4>\r\n\r\n<h2><span style=\"color:#e74c3c\"><strong>C&Ocirc;NG TY NỘI THẤT BẢO LONG</strong></span></h2>\r\n\r\n<p>&nbsp;Địa chỉ: Địa chỉ: 130 Ho&agrave;ng C&ocirc;ng Chất, P. Ph&uacute; Diễn, Q. Bắc Từ Li&ecirc;m, Tp. H&agrave; Nội</p>\r\n\r\n<p><a href=\"http://sofabaolam.com/#\" title=\"tel:0986199122\">&nbsp;Hotline: 0963761678</a></p>\r\n\r\n<p>&nbsp;Email: tranlinh.manh@gmail.com</p>\r\n', 'chon-sofa-cho-phong-karaoke-sao-cho-dung-cach', 'vi', NULL, NULL, 1561110995, 4, 1, 1, 'Chọn Sofa cho phòng Karaoke sao cho đúng cách', 'Chọn Sofa cho phòng Karaoke sao cho đúng cách', 'Việc lựa chọn Sofa cho phòng Karaoke rất quan trọng đến đẳng cấp, thẩm mỹ và chất lượng của phòng Karaoke', '', 16, 1, 0, 2),
(14, 'Những lưu ý không thể bỏ qua khi bạn muốn mua sofa giá rẻ', '<p>&nbsp;</p>\r\n\r\n<p><em>Trước khi chọn mua ghế sofa gi&aacute; rẻ bạn cần phải nắm r&otilde; những ưu v&agrave; khuyết điểm của d&ograve;ng sản phẩm n&agrave;y để c&oacute; thể chọn được sản phẩm chất lượng nhất. Để gi&uacute;p qu&yacute; kh&aacute;ch h&agrave;ng c&oacute; thể tự m&igrave;nh chọn được bộ sofa gi&aacute; rẻ&nbsp;b&agrave;i viết dưới đ&acirc;y Nội thất Bảo L&acirc;m gửi tới qu&yacute; vị 5 lưu &yacute; trước khi chọn mua:</em></p>\r\n', 1, 'upload/img/news/166sofa21-1.jpg', '<p><img alt=\"Sofa Giá rẻ\" src=\"/upload/images/sofavang2.jpg\" style=\"height:720px; width:960px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\">1.&nbsp;<strong>Kiểm tra khung ghế</strong></span></span></p>\r\n\r\n<p>Khi chọn mua ghế sofa gi&aacute; rẻ điều quan trọng nhất m&agrave; bạn kh&ocirc;ng thể bỏ qua đ&oacute; l&agrave; phần khung ghế. Phần khung sofa l&agrave; bộ phận ch&iacute;nh quyết định đến chất lượng, độ bền cũng như kiểu d&aacute;ng của sản phẩm.</p>\r\n\r\n<p>Bạn c&oacute; thể kiểm tra sự chắc chắn của khung ghế c&oacute; được đảm bảo hay kh&ocirc;ng bằng c&aacute;ch ngồi l&ecirc;n ghế, x&ocirc; vịn ghế xem c&oacute; bị lung lay, ọp ẹp hay bị l&uacute;n, v&otilde;ng hay kh&ocirc;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\"><strong>2. Kiểm tra chất lượng đệm m&uacute;t</strong></span></span></p>\r\n\r\n<p>Đệm m&uacute;t l&agrave; phẩn ảnh hưởng trực tiếp đến sự &ecirc;m &aacute;i khi ngồi tr&ecirc;n sofa. Đ&acirc;y cũng l&agrave; phần ch&uacute;ng ta kh&ocirc;ng thể bỏ qua trước khi lựa chọn c&aacute;c sản phẩm sofa, đặc biệt l&agrave; sofa gi&aacute; rẻ. Bởi c&aacute;c sản phẩm gi&aacute; rẻ thường sử dụng c&aacute;c loại đệm m&uacute;t c&oacute; chất lượng thấp, nhanh ch&oacute;ng bị sụt l&uacute;n, biến dạng trong thời gian ngắn sử dụng. C&aacute;ch để bạn kiểm tra chất lượng đệm m&uacute;t c&oacute; được đảm bảo hay kh&ocirc;ng đơn giản l&agrave; bạn ngồi l&ecirc;n ghế trong khoảng thời gian từ 10 &ndash; 15 ph&uacute;t xem phần đệm c&oacute; đ&agrave;n hồi nhanh ch&oacute;ng trở về trạng th&aacute;i cũ hay kh&ocirc;ng. Nếu ch&uacute;ng mất nhiều thời gian để trở về trạng th&aacute;i ban đầu hay lu&ocirc;n hằn vết l&otilde;m th&igrave; chứng tỏ đ&acirc;y l&agrave; sản phẩm c&oacute; chất lượng đệm m&uacute;t k&eacute;m v&agrave; ch&uacute;ng ta kh&ocirc;ng n&ecirc;n chọn mua sản phẩm n&agrave;y.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\"><strong>3. Kiểm tra chất liệu vỏ bọc</strong></span></span></p>\r\n\r\n<p>Bọc vỏ ghế sofa gi&aacute; rẻ được lựa chọn chất liệu nỉ, vải, nhung hoặc chất liệu da simili. T&ugrave;y từng sản phẩm bạn muốn mua l&agrave;&nbsp;<a href=\"http://esofa.vn/sofa-ni\">sofa nỉ</a>, vải hay da m&agrave; n&ecirc;n kỹ c&agrave;ng, bề mặt kh&ocirc;ng được c&oacute; c&aacute;c vết r&aacute;ch, xước, sờn hay c&oacute; c&aacute;c vết bẩn tr&ecirc;n bề mặt.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\"><strong>4. Lựa chọn m&agrave;u sắc</strong></span></span></p>\r\n\r\n<p>Bạn n&ecirc;n lựa chọn m&agrave;u sắc bộ sofa tương đồng, ăn khớp với m&agrave;u sắc của ph&ograve;ng cần b&agrave;i tr&iacute; bộ sofa. Ph&ograve;ng kh&aacute;ch c&oacute; nhiều &aacute;nh s&aacute;ng th&igrave; ch&uacute;ng ta n&ecirc;n chọn c&aacute;c mẫu sofa c&oacute; gam m&agrave;u tươi s&aacute;ng như trắng, xanh, v&agrave;ng, be sẽ l&agrave;m cho kh&ocirc;ng gian ph&ograve;ng trở n&ecirc;n nổi bật v&agrave; ấn tượng hơn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><span style=\"color:#2ecc71\"><strong>5. Lựa chọn k&iacute;ch thước</strong></span></span></p>\r\n\r\n<p>Bạn cần x&aacute;c định k&iacute;ch thước của ph&ograve;ng đặt ghế sofa (chiều d&agrave;i, chiều rộng) l&agrave; bao nhi&ecirc;u để lựa chọn bộ ghế sofa c&oacute; k&iacute;ch thước ph&ugrave; hợp. Kh&ocirc;ng sử dụng bộ ghế sofa qu&aacute; nhỏ hay qu&aacute; lớn sẽ mang đến sự kh&ocirc;ng h&agrave;i h&ograve;a, lạc l&otilde;ng trong kh&ocirc;ng gian căn ph&ograve;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"Sofa giá rẻ\" src=\"/upload/images/sofacafe3(1).jpg\" style=\"height:448px; width:746px\" /></p>\r\n\r\n<h4><strong>TH&Ocirc;NG TIN LI&Ecirc;N HỆ</strong></h4>\r\n\r\n<h2><span style=\"color:#e74c3c\"><strong>C&Ocirc;NG TY NỘI THẤT BẢO LONG</strong></span></h2>\r\n\r\n<p>&nbsp;<span style=\"color:#1abc9c\">Địa chỉ:</span> Địa chỉ: 130 Ho&agrave;ng C&ocirc;ng Chất, P. Ph&uacute; Diễn, Q. Bắc Từ Li&ecirc;m, Tp. H&agrave; Nội</p>\r\n\r\n<p><a href=\"http://sofabaolam.com/#\" title=\"tel:0986199122\">&nbsp;Hotline: 0963761678</a></p>\r\n\r\n<p><span style=\"color:#e74c3c\">&nbsp;Email</span>: tranlinh.manh@gmail.com</p>\r\n\r\n<p>.</p>\r\n', 'nhung-luu-y-khong-the-bo-qua-khi-ban-muon-mua-sofa-gia-re', 'vi', NULL, NULL, 1561110874, 4, 1, 1, 'Những lưu ý không thể bỏ qua khi bạn muốn mua sofa giá rẻ', 'Những lưu ý không thể bỏ qua khi bạn muốn mua sofa giá rẻ', '5 lưu ý để chọn được Sofa giá rẻ', '', 22, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE `news_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `focus` int(11) DEFAULT NULL,
  `hot` int(11) DEFAULT NULL,
  `coupon` int(11) DEFAULT NULL,
  `time_update` int(11) DEFAULT NULL,
  `time_start` int(8) DEFAULT NULL,
  `sort` int(5) DEFAULT '1',
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `title_seo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` text COLLATE utf8_unicode_ci,
  `description_seo` text COLLATE utf8_unicode_ci,
  `button_view_left` int(11) NOT NULL,
  `button_view_right` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news_category`
--

INSERT INTO `news_category` (`id`, `name`, `alias`, `description`, `image`, `parent_id`, `home`, `focus`, `hot`, `coupon`, `time_update`, `time_start`, `sort`, `lang`, `title_seo`, `keyword`, `description_seo`, `button_view_left`, `button_view_right`) VALUES
(4, 'Tin tức', 'tin-tuc', '', NULL, 0, 1, NULL, 1, NULL, NULL, NULL, 1, 'vi', '', NULL, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_to_category`
--

CREATE TABLE `news_to_category` (
  `id` int(11) NOT NULL,
  `id_news` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news_to_category`
--

INSERT INTO `news_to_category` (`id`, `id_news`, `id_category`) VALUES
(15, 1, 4),
(14, 2, 4),
(13, 3, 4),
(12, 4, 4),
(11, 5, 4),
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 2),
(10, 10, 1),
(39, 11, 4),
(40, 12, 4),
(37, 13, 4),
(35, 14, 4);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `phone` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `note` text CHARACTER SET utf8,
  `item_order` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `show` tinyint(1) DEFAULT '0',
  `mark` tinyint(1) DEFAULT '0',
  `admin_note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `province` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
  `district` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `ward` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `user_id` decimal(21,0) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `view` tinyint(1) DEFAULT '1',
  `code` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `address2` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `startplaces` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `finishplace` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `mobile` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `startime` char(30) CHARACTER SET utf8 DEFAULT NULL,
  `endtime` char(30) CHARACTER SET utf8 DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `other_note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `code_sale` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `price_sale` int(10) DEFAULT NULL,
  `approved` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `price_ship` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `order_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(50) DEFAULT NULL,
  `price` int(100) DEFAULT NULL,
  `t_option` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `initierary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_start` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hotel` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `room` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tour_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_end` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `pro_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `description`, `lang`) VALUES
(3, 'Osaka', '', NULL),
(2, 'Tokyo', '', NULL),
(4, 'Kanazawa', '', NULL),
(5, 'Shirakawa-go', '', NULL),
(6, 'Nagano', '', NULL),
(7, 'Kobe', '', NULL),
(8, 'Hakuba', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `style` int(11) DEFAULT NULL,
  `id_value` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `coupon` tinyint(1) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `price` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `description_seo` text COLLATE utf8_unicode_ci,
  `location` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci,
  `like` int(11) DEFAULT '0',
  `order` decimal(21,0) DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  `caption_1` text COLLATE utf8_unicode_ci,
  `caption_2` text COLLATE utf8_unicode_ci,
  `locale` int(11) DEFAULT NULL,
  `bought` int(11) DEFAULT '0',
  `dksudung` text COLLATE utf8_unicode_ci,
  `sort` int(10) DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `counter` int(11) DEFAULT '0',
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT 'vi',
  `destination` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(8) DEFAULT NULL,
  `tags` text COLLATE utf8_unicode_ci,
  `pro_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multi_image` text COLLATE utf8_unicode_ci,
  `img_dir` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `quaranty` tinyint(3) DEFAULT NULL,
  `tinhtrang` tinyint(1) DEFAULT NULL,
  `group_attribute_id` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `button_color1` int(11) NOT NULL,
  `config_pro` text COLLATE utf8_unicode_ci NOT NULL,
  `config_pro_content` text COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `price_imp` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `style`, `id_value`, `brand`, `name`, `code`, `alias`, `image`, `hot`, `home`, `focus`, `coupon`, `view`, `active`, `price`, `price_sale`, `description`, `description_seo`, `location`, `title_seo`, `keyword_seo`, `detail`, `note`, `like`, `order`, `category_id`, `caption_1`, `caption_2`, `locale`, `bought`, `dksudung`, `sort`, `quantity`, `counter`, `lang`, `destination`, `time`, `tags`, `pro_dir`, `multi_image`, `img_dir`, `status`, `quaranty`, `tinhtrang`, `group_attribute_id`, `color`, `size`, `user_id`, `option_id`, `button_color1`, `config_pro`, `config_pro_content`, `weight`, `price_imp`) VALUES
(23, NULL, NULL, NULL, 'Sofa Gia Đình mã  GD215', NULL, 'sofa-gia-dinh-ma-gd215', '156soffa127.png', NULL, 1, 1, 1, 13, 1, 28500000, 25000000, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, '', '', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 'vi', NULL, 1560658235, NULL, '16062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(79, NULL, NULL, NULL, 'Sofa Karaoke mã SK811', NULL, 'sofa-karaoke-ma-sk811', 'sofakaok355.jpg', NULL, 1, 1, 1, 11, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 53, 0, NULL, 'vi', NULL, 1561023508, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(80, NULL, NULL, NULL, 'Sofa Karaoke mã SK812', NULL, 'sofa-karaoke-ma-sk812', 'sofakaoke4.jpg', NULL, 1, 1, 1, 8, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 54, 0, NULL, 'vi', NULL, 1561023584, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(27, NULL, NULL, NULL, 'Sofa Cao Cấp mã CP515', NULL, 'sofa-cao-cap-ma-cp515', '166sofa311.png', NULL, 1, 1, 1, 5, 1, 0, 145600000, '', '', NULL, 'Sofa cao cấp ', 'Sofa cao cấp ', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, 'vi', NULL, 1560657368, NULL, '16062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(59, NULL, NULL, NULL, 'Sofa Cao Cấp mã CP516', NULL, 'sofa-cao-cap-ma-cp516', '166sofa111.png', NULL, 1, 1, 1, 2, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa cao cấp ', 'Sofa cao cấp ', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 34, 0, NULL, 'vi', NULL, 1560657498, NULL, '16062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(28, NULL, NULL, NULL, 'Sofa Khách Sạn mã KS207', NULL, 'sofa-khach-san-ma-ks207', 'khachan3.jpg', NULL, 1, 1, 1, 9, 1, 0, 0, '', '', NULL, '', '', NULL, NULL, 0, '0', 6, NULL, NULL, NULL, NULL, NULL, 6, 0, NULL, 'vi', NULL, 1560526166, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(55, NULL, NULL, NULL, 'Sofa Khách Sạn mã KS208', NULL, 'sofa-khach-san-ma-ks208', 'ngam-can-ho-sang-trong-mang-phong-cach-co-dien-2.jpg', NULL, NULL, NULL, NULL, 3, 1, 0, 0, '', '', NULL, '', '', NULL, NULL, 0, '0', 6, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 'vi', NULL, 1560526221, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(29, NULL, NULL, NULL, 'Sofa Khách Sạn mã KS206', NULL, 'sofa-khach-san-ma-ks206', 'sofaroyal-bocghesofa123-07-mbyugzyxiarn.jpg', NULL, 1, 1, 1, 24, 1, 0, 0, '', '', NULL, '', '', NULL, NULL, 0, '0', 6, NULL, NULL, NULL, NULL, NULL, 7, 0, NULL, 'vi', NULL, 1560526082, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(30, NULL, NULL, NULL, 'Sofa Khách Sạn mã KS205', NULL, 'sofa-khach-san-ma-ks205', 'sofakhachsan4.jpg', NULL, 1, 1, 1, 9, 1, 0, 0, '', '', NULL, '', '', NULL, NULL, 0, '0', 6, NULL, NULL, NULL, NULL, NULL, 8, 0, NULL, 'vi', NULL, 1560525530, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(31, NULL, NULL, NULL, 'Sofa Gia Đình mã GD111', NULL, 'sofa-gia-dinh-ma-gd111', 'sofavang1.jpg', NULL, 1, 1, 1, 7, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, '', '', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 22, 0, NULL, 'vi', NULL, 1560523894, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(32, NULL, NULL, NULL, 'Sofa Gia Đình mã GD109', NULL, 'sofa-gia-dinh-ma-gd109', '1-1.jpg', NULL, 1, 1, 1, 17, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, '', '', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 23, 0, NULL, 'vi', NULL, 1560523867, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(81, NULL, NULL, NULL, 'Sofa Karaoke mã SK813', NULL, 'sofa-karaoke-ma-sk813', 'sofakaoke5.jpg', NULL, 1, 1, 1, 13, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 55, 0, NULL, 'vi', NULL, 1561023654, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(36, NULL, NULL, NULL, 'SOFIA', NULL, 'sofia', '4fcd01926a1d8843d10c.jpg', NULL, 1, 1, 1, 5, 1, 0, 0, '<p>Chất liệu: Nệm ngồi l&ograve; xo</p>\r\n\r\n<p>Kiểu d&aacute;ng được thiết kế theo xu hướng hiện đại, trang nh&atilde;. Sự kết hợp h&agrave;i h&ograve;a giữa kiểu d&aacute;ng, m&agrave;u sắc v&agrave; chất liệu vải cao cấp l&agrave;m tăng th&ecirc;m sự sang trọng, tiện nghi cho ph&ograve;ng kh&aacute;ch.&nbsp;</p>\r\n\r\n<p>Nệm ngồi sử dụng l&ograve; xo t&uacute;i kết hợp với lớp vải mềm mại tạo cảm gi&aacute;c &ecirc;m &aacute;i, tho&aacute;ng m&aacute;t. Đai thun đ&agrave;n hồi cao được thiết kế ri&ecirc;ng cho sofa kết hợp với l&ograve; xo zigzag tăng th&ecirc;m sự &ecirc;m &aacute;i v&agrave; chắc chắn khi ngồi.&nbsp; Phần khung gỗ dầu 100%, đ&atilde; qua xử l&yacute; sấy, chống ẩm v&agrave; mốc gi&uacute;p tăng độ bền cho Sofa</p>\r\n', '', NULL, '', '', NULL, NULL, 0, '0', 37, NULL, NULL, NULL, NULL, NULL, 12, 0, NULL, 'vi', NULL, 1558981182, NULL, '28052019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p>Ch\\u1ea5t li\\u1ec7u: N\\u1ec7m ng\\u1ed3i l&ograve; xo<\\/p>\\r\\n\\r\\n<p>Ki\\u1ec3u d&aacute;ng \\u0111\\u01b0\\u1ee3c thi\\u1ebft k\\u1ebf theo xu h\\u01b0\\u1edbng hi\\u1ec7n \\u0111\\u1ea1i, trang nh&atilde;. S\\u1ef1 k\\u1ebft h\\u1ee3p h&agrave;i h&ograve;a gi\\u1eefa ki\\u1ec3u d&aacute;ng, m&agrave;u s\\u1eafc v&agrave; ch\\u1ea5t li\\u1ec7u v\\u1ea3i cao c\\u1ea5p l&agrave;m t\\u0103ng th&ecirc;m s\\u1ef1 sang tr\\u1ecdng, ti\\u1ec7n nghi cho ph&ograve;ng kh&aacute;ch.&nbsp;<\\/p>\\r\\n\\r\\n<p>N\\u1ec7m ng\\u1ed3i s\\u1eed d\\u1ee5ng l&ograve; xo t&uacute;i k\\u1ebft h\\u1ee3p v\\u1edbi l\\u1edbp v\\u1ea3i m\\u1ec1m m\\u1ea1i t\\u1ea1o c\\u1ea3m gi&aacute;c &ecirc;m &aacute;i, tho&aacute;ng m&aacute;t. \\u0110ai thun \\u0111&agrave;n h\\u1ed3i cao \\u0111\\u01b0\\u1ee3c thi\\u1ebft k\\u1ebf ri&ecirc;ng cho sofa k\\u1ebft h\\u1ee3p v\\u1edbi l&ograve; xo zigzag t\\u0103ng th&ecirc;m s\\u1ef1 &ecirc;m &aacute;i v&agrave; ch\\u1eafc ch\\u1eafn khi ng\\u1ed3i.&nbsp; Ph\\u1ea7n khung g\\u1ed7 d\\u1ea7u 100%, \\u0111&atilde; qua x\\u1eed l&yacute; s\\u1ea5y, ch\\u1ed1ng \\u1ea9m v&agrave; m\\u1ed1c gi&uacute;p t\\u0103ng \\u0111\\u1ed9 b\\u1ec1n cho Sofa<img alt=\\\"\\\" src=\\\"\\/upload\\/images\\/4fcd01926a1d8843d10c.jpg\\\" style=\\\"height:675px; width:900px\\\" \\/><\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"\\u0110i\\u1ec7n \\u00e1p\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Tr\\u1ecdng l\\u01b0\\u1ee3ng\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"M\\u00e0n h\\u00ecnh\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(37, NULL, NULL, NULL, 'Sofa Khách Sạn mã KS203', NULL, 'sofa-khach-san-ma-ks203', 'sofa-khach-san.jpg', NULL, 1, 1, 1, 9, 1, 0, 0, '<p>Chất liệu: Nệm ngồi l&ograve; xo</p>\r\n\r\n<p>Kiểu d&aacute;ng được thiết kế theo xu hướng hiện đại, trang nh&atilde;. Sự kết hợp h&agrave;i h&ograve;a giữa kiểu d&aacute;ng, m&agrave;u sắc v&agrave; chất liệu vải cao cấp l&agrave;m tăng th&ecirc;m sự sang trọng, tiện nghi cho ph&ograve;ng kh&aacute;ch.&nbsp;</p>\r\n\r\n<p>Nệm ngồi sử dụng l&ograve; xo t&uacute;i kết hợp với lớp vải mềm mại tạo cảm gi&aacute;c &ecirc;m &aacute;i, tho&aacute;ng m&aacute;t. Đai thun đ&agrave;n hồi cao được thiết kế ri&ecirc;ng cho sofa kết hợp với l&ograve; xo zigzag tăng th&ecirc;m sự &ecirc;m &aacute;i v&agrave; chắc chắn khi ngồi.&nbsp; Phần khung gỗ dầu 100%, đ&atilde; qua xử l&yacute; sấy, chống ẩm v&agrave; mốc gi&uacute;p tăng độ bền cho Sofa</p>\r\n', '', NULL, '', '', NULL, NULL, 0, '0', 6, NULL, NULL, NULL, NULL, NULL, 13, 0, NULL, 'vi', NULL, 1560525297, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p>Ch\\u1ea5t li\\u1ec7u: N\\u1ec7m ng\\u1ed3i l&ograve; xo<\\/p>\\r\\n\\r\\n<p>Ki\\u1ec3u d&aacute;ng \\u0111\\u01b0\\u1ee3c thi\\u1ebft k\\u1ebf theo xu h\\u01b0\\u1edbng hi\\u1ec7n \\u0111\\u1ea1i, trang nh&atilde;. S\\u1ef1 k\\u1ebft h\\u1ee3p h&agrave;i h&ograve;a gi\\u1eefa ki\\u1ec3u d&aacute;ng, m&agrave;u s\\u1eafc v&agrave; ch\\u1ea5t li\\u1ec7u v\\u1ea3i cao c\\u1ea5p l&agrave;m t\\u0103ng th&ecirc;m s\\u1ef1 sang tr\\u1ecdng, ti\\u1ec7n nghi cho ph&ograve;ng kh&aacute;ch.&nbsp;<\\/p>\\r\\n\\r\\n<p>N\\u1ec7m ng\\u1ed3i s\\u1eed d\\u1ee5ng l&ograve; xo t&uacute;i k\\u1ebft h\\u1ee3p v\\u1edbi l\\u1edbp v\\u1ea3i m\\u1ec1m m\\u1ea1i t\\u1ea1o c\\u1ea3m gi&aacute;c &ecirc;m &aacute;i, tho&aacute;ng m&aacute;t. \\u0110ai thun \\u0111&agrave;n h\\u1ed3i cao \\u0111\\u01b0\\u1ee3c thi\\u1ebft k\\u1ebf ri&ecirc;ng cho sofa k\\u1ebft h\\u1ee3p v\\u1edbi l&ograve; xo zigzag t\\u0103ng th&ecirc;m s\\u1ef1 &ecirc;m &aacute;i v&agrave; ch\\u1eafc ch\\u1eafn khi ng\\u1ed3i.&nbsp; Ph\\u1ea7n khung g\\u1ed7 d\\u1ea7u 100%, \\u0111&atilde; qua x\\u1eed l&yacute; s\\u1ea5y, ch\\u1ed1ng \\u1ea9m v&agrave; m\\u1ed1c gi&uacute;p t\\u0103ng \\u0111\\u1ed9 b\\u1ec1n cho Sofa<\\/p>\\r\\n\\r\\n<p><img alt=\\\"\\\" src=\\\"\\/upload\\/images\\/f7b084cdef420d1c5453.jpg\\\" style=\\\"height:675px; width:900px\\\" \\/><\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(38, NULL, NULL, NULL, 'Sofa gia đinh ', NULL, 'sofa-gia-dinhs', 'thumbnail-1-ks2.jpg', NULL, 1, NULL, 1, 4, 1, 0, 23000000, '<p>T&ecirc;n sản phẩm: Sofa gia đ&igrave;nh&nbsp;<br />\r\nChất liệu:&nbsp;<br />\r\nM&agrave;u sắc : m&agrave;u sữa</p>\r\n\r\n<p>T&igrave;nh trạng&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n', '', NULL, '', '', NULL, NULL, 0, '0', 12, NULL, NULL, NULL, NULL, NULL, 14, 0, NULL, 'vi', NULL, 1560142662, NULL, '28052019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p>M\\u1ed9t b\\u1ed9 sofa c\\u1ee1 l\\u1edbn c\\u1ea7n \\u0111\\u1eb7t trong ph&ograve;ng di\\u1ec7n t&iacute;ch nh\\u1ecf h\\u1eb9p s\\u1ebd l&agrave;m c\\u1ea3n tr\\u1edf vi\\u1ec7c \\u0111i l\\u1ea1i trong ph&ograve;ng, trong t&igrave;nh hu\\u1ed1ng ng\\u01b0\\u1ee3c l\\u1ea1i, m\\u1ed9t b\\u1ed9 sofa n\\u1eb1m l\\u1ecdt th\\u1ecfm trong c\\u0103n ph&ograve;ng l\\u1edbn c\\u0169ng kh&ocirc;ng ph\\u1ea3i l&agrave; c&aacute;ch b&agrave;y tr&iacute; th&iacute;ch h\\u1ee3p.<br \\/>\\r\\n<img alt=\\\"\\\" src=\\\"\\/upload\\/images\\/thumbnail_1_ks2.jpg\\\" style=\\\"height:194px; width:280px\\\" \\/><br \\/>\\r\\n&nbsp;<\\/p>\\r\\n\\r\\n<h3 style=\\\"margin-left:0px; margin-right:0px; text-align:start\\\"><span style=\\\"font-size:16px\\\"><span style=\\\"color:#333333\\\"><span style=\\\"font-family:Arial\\\"><span style=\\\"font-size:16px\\\">\\u0110\\u1ecba ch\\u1ec9 cung c\\u1ea5p gh\\u1ebf sofa gia \\u0111&igrave;nh ph&ograve;ng kh&aacute;ch<\\/span><\\/span><\\/span><\\/span><\\/h3>\\r\\n\\r\\n<p style=\\\"text-align:justify\\\"><span style=\\\"font-size:12px\\\"><span style=\\\"color:#333333\\\"><span style=\\\"font-family:Arial\\\"><span style=\\\"font-family:Times New Roman\\\"><span style=\\\"font-size:16px\\\">V\\u1edbi kinh nghi\\u1ec7m nhi\\u1ec1u n\\u0103m ho\\u1ea1t \\u0111\\u1ed9ng trong l\\u0129nh v\\u1ef1c cung \\u1ee9ng, thi c&ocirc;ng c&aacute;c s\\u1ea3n ph\\u1ea9m, d\\u1ecbch v\\u1ee5 h&agrave;ng \\u0111\\u1ea7u t\\u1ea1o n&ecirc;n kh&ocirc;ng gian s\\u1ed1ng th\\u1ea9m m\\u1ef9 c&ocirc;ng n\\u0103ng v&agrave; l&agrave;m cho cu\\u1ed9c s\\u1ed1ng ng&agrave;y c&agrave;ng th&ecirc;m t\\u1ed1t \\u0111\\u1eb9p sofa&nbsp;<\\/span><\\/span><span style=\\\"font-family:Times New Roman\\\"><span style=\\\"font-size:16px\\\">ch\\u1eafc ch\\u1eafn s\\u1ebd l&agrave;m gia \\u0111&igrave;nh b\\u1ea1n h&agrave;i l&ograve;ng, \\u0111&aacute;p \\u1ee9ng \\u0111\\u01b0\\u1ee3c nh\\u1eefng y&ecirc;u c\\u1ea7u cao nh\\u1ea5t cu\\u1ea3 kh&aacute;ch h&agrave;ng.<\\/span><\\/span><\\/span><\\/span><\\/span><\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(39, NULL, NULL, NULL, 'Sofa Gia Đình mã GD106', NULL, 'sofa-gia-dinh-ma-gd106', 'tim-hieu-ve-bao-duong-sofa-da-dung-chuan-nhu-chuyen-gia-1.jpg', NULL, 1, 1, 1, 14, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n', '', NULL, '', '', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, 'vi', NULL, 1560523806, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p><span style=\\\"font-family:Arial,Helvetica,sans-serif\\\">M\\u1ed9t b\\u1ed9 sofa c\\u1ee1 l\\u1edbn c\\u1ea7n \\u0111\\u1eb7t trong ph&ograve;ng di\\u1ec7n t&iacute;ch nh\\u1ecf h\\u1eb9p s\\u1ebd l&agrave;m c\\u1ea3n tr\\u1edf vi\\u1ec7c \\u0111i l\\u1ea1i trong ph&ograve;ng, trong t&igrave;nh hu\\u1ed1ng ng\\u01b0\\u1ee3c l\\u1ea1i, m\\u1ed9t b\\u1ed9 sofa n\\u1eb1m l\\u1ecdt th\\u1ecfm trong c\\u0103n ph&ograve;ng l\\u1edbn c\\u0169ng kh&ocirc;ng ph\\u1ea3i l&agrave; c&aacute;ch b&agrave;y tr&iacute; th&iacute;ch h\\u1ee3p<\\/span><\\/p>\\r\\n\\r\\n<p><span style=\\\"font-family:Times New Roman\\\"><span style=\\\"font-size:16px\\\">V\\u1edbi kinh nghi\\u1ec7m nhi\\u1ec1u n\\u0103m ho\\u1ea1t \\u0111\\u1ed9ng trong l\\u0129nh v\\u1ef1c cung \\u1ee9ng, thi c&ocirc;ng c&aacute;c s\\u1ea3n ph\\u1ea9m, d\\u1ecbch v\\u1ee5 h&agrave;ng \\u0111\\u1ea7u t\\u1ea1o n&ecirc;n kh&ocirc;ng gian s\\u1ed1ng th\\u1ea9m m\\u1ef9 c&ocirc;ng n\\u0103ng v&agrave; l&agrave;m cho cu\\u1ed9c s\\u1ed1ng ng&agrave;y c&agrave;ng th&ecirc;m t\\u1ed1t \\u0111\\u1eb9p sofa B\\u1ea3o&nbsp;Long<\\/span><\\/span><span style=\\\"font-family:Times New Roman\\\"><span style=\\\"font-size:16px\\\">&nbsp;ch\\u1eafc ch\\u1eafn s\\u1ebd l&agrave;m gia \\u0111&igrave;nh b\\u1ea1n h&agrave;i l&ograve;ng, \\u0111&aacute;p \\u1ee9ng \\u0111\\u01b0\\u1ee3c nh\\u1eefng y&ecirc;u c\\u1ea7u cao nh\\u1ea5t cu\\u1ea3 kh&aacute;ch h&agrave;ng.<\\/span><\\/span><br \\/>\\r\\n&nbsp;<\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(40, NULL, NULL, NULL, 'Sofa Gia Đình mã GD105', NULL, 'sofa-gia-dinh-ma-gd105', '156sofa12445.png', NULL, 1, 1, 1, 28, 1, 0, 12000000, '<p><strong>Th&ocirc;ng tin sản phẩm</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa Gia đình, Sofa giá rẻ', 'Sofa Gia đình, Sofa giá rẻ', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 25, 0, NULL, 'vi', NULL, 1560657046, NULL, '16062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p><strong>Sofa Gia \\u0110&igrave;nh m&atilde; GD105<\\/strong><\\/p>\\r\\n\\r\\n<p>Sofa g&oacute;c t\\u1ea1o c\\u1ea3m gi&aacute;c g\\u1ecdn g&agrave;ng, ng\\u0103n n\\u1eafp v&agrave; kh&ocirc;ng chi\\u1ebfm nhi\\u1ec1u di\\u1ec7n t&iacute;ch cho ph&ograve;ng kh&aacute;ch c\\u1ee7a gia \\u0111&igrave;nh b\\u1ea1n!<\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(41, NULL, NULL, NULL, 'Sofa Khách Sạn mã KS201', NULL, 'sofa-khach-san-ma-ks201', 'sofa-khach-san2.jpg', NULL, 1, 1, 1, 6, 1, 25000000, 2300000, '', '', NULL, 'Sofa khách sạn Sofa giá rẻ', '', NULL, NULL, 0, '0', 6, NULL, NULL, NULL, NULL, NULL, 27, 0, NULL, 'vi', NULL, 1560525148, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p><iframe frameborder=\\\"0\\\" height=\\\"400\\\" scrolling=\\\"no\\\" src=\\\"https:\\/\\/www.youtube.com\\/embed\\/MFnB2HKgegM\\\" width=\\\"100%\\\"><\\/iframe><\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(42, NULL, NULL, NULL, 'Sofa văng 7', NULL, 'sofa-vang-7', '51571758-1206954472806407-6373434075138490368-n.jpg', NULL, 0, 1, NULL, 4, 1, 0, 0, '<p>K&iacute;ch thước<br />\r\nXuất xứ&nbsp;<br />\r\nGi&aacute; th&agrave;nh</p>\r\n', '', NULL, '', '', NULL, NULL, 0, '0', 12, NULL, NULL, NULL, NULL, NULL, 18, 0, NULL, 'vi', NULL, 1560142633, NULL, '30052019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p style=\\\"text-align:justify\\\"><span style=\\\"font-size:12px\\\"><span style=\\\"color:#000000\\\"><span style=\\\"font-family:Arial\\\"><span style=\\\"background-color:#f5f5f5\\\"><span style=\\\"font-size:14pt\\\"><strong><span style=\\\"font-family:Tahoma\\\"><span style=\\\"font-size:14px\\\">Sofa da th&acirc;\\u0323t<\\/span><\\/span><\\/strong><span style=\\\"font-family:Tahoma\\\"><span style=\\\"font-size:14px\\\">&nbsp;\\u0111\\u01b0\\u01a1\\u0323c sa\\u0309n xu&acirc;\\u0301t theo ti&ecirc;u chu&acirc;\\u0309n ch&acirc;u &Acirc;u x\\u01b0\\u0301ng \\u0111a\\u0301ng la\\u0300 s\\u01b0\\u0323 l\\u01b0\\u0323a cho\\u0323n s&ocirc;\\u0301 1 cho gia \\u0111i\\u0300nh ba\\u0323n. \\u0110\\u01b0\\u01a1\\u0323c sa\\u0309n xu&acirc;\\u0301t t\\u01b0\\u0300 ch&acirc;\\u0301t li&ecirc;\\u0323u cao c&acirc;\\u0301p nh&acirc;\\u0323p kh&acirc;\\u0309u, b&ocirc;\\u0323 sofa mang \\u0111&ecirc;\\u0301n gia\\u0301 tri\\u0323 s\\u01b0\\u0309 du\\u0323ng l&acirc;u da\\u0300i. Th\\u01a1\\u0300i gian ba\\u0309o ha\\u0300nh sa\\u0309n ph&acirc;\\u0309m ke\\u0301o da\\u0300i \\u0111&ecirc;\\u0301n 10 n\\u0103m ba\\u0309o ha\\u0300nh v&ecirc;\\u0300 ch&acirc;\\u0301t l\\u01b0\\u01a1\\u0323ng khung x\\u01b0\\u01a1ng, 6 n\\u0103m v&ecirc;\\u0300 ch&acirc;\\u0301t li&ecirc;\\u0323u bo\\u0323c.<\\/span><\\/span><\\/span><\\/span><\\/span><\\/span><\\/span><\\/p>\\r\\n\\r\\n<p style=\\\"text-align:justify\\\"><span style=\\\"font-size:12px\\\"><span style=\\\"color:#000000\\\"><span style=\\\"font-family:Arial\\\"><span style=\\\"background-color:#f5f5f5\\\"><span style=\\\"font-family:Tahoma\\\"><span style=\\\"font-size:14px\\\">\\u0110&ecirc;\\u0309 s\\u01a1\\u0309 h\\u01b0\\u0303u \\u0111\\u01b0\\u01a1\\u0323c nh\\u01b0\\u0303ng m&acirc;\\u0303u sofa hi&ecirc;\\u0323n \\u0111a\\u0323i nh&acirc;\\u0301t n\\u0103m 2019 xin quy\\u0301 kha\\u0301ch vui lo\\u0300ng truy c&acirc;\\u0323p va\\u0300o website thienduongsofa.com, chu\\u0301ng t&ocirc;i lu&ocirc;n c&acirc;\\u0323p nh&acirc;\\u0323t nh\\u01b0\\u0303ng m&acirc;\\u0303u sofa m\\u01a1\\u0301i nh&acirc;\\u0301t d&acirc;\\u0303n \\u0111&acirc;\\u0300u thi\\u0323 tr\\u01b0\\u01a1\\u0300ng v&ecirc;\\u0300 phong ca\\u0301ch thi&ecirc;\\u0301t k&ecirc;\\u0301. Sa\\u0309n ph&acirc;\\u0309m cu\\u0309a chu\\u0301ng t&ocirc;i kh&ocirc;ng chi\\u0309 h\\u01b0\\u01a1\\u0301ng t\\u01a1\\u0301i thi\\u0323 tr\\u01b0\\u01a1\\u0300ng trong n\\u01b0\\u01a1\\u0301c ma\\u0300 co\\u0300n h\\u01b0\\u01a1\\u0301ng \\u0111&ecirc;\\u0301n thi\\u0323 tr\\u01b0\\u01a1\\u0300ng xu&acirc;\\u0301t kh&acirc;\\u0309u. Sa\\u0309n ph&acirc;\\u0309m kh&ocirc;ng ng\\u01b0\\u0300ng \\u0111\\u01b0\\u01a1\\u0323c ca\\u0309i ti&ecirc;\\u0301n v&ecirc;\\u0300 ch&acirc;\\u0301t l\\u01b0\\u01a1\\u0323ng va\\u0300 m&acirc;\\u0303u ma\\u0303 \\u0111em \\u0111&ecirc;\\u0301n s\\u01b0\\u0323 ha\\u0300i lo\\u0300ng cho quy\\u0301 kha\\u0301ch ha\\u0300ng.<\\/span><\\/span><\\/span><\\/span><\\/span><\\/span><\\/p>\\r\\n\\r\\n<p style=\\\"text-align:justify\\\"><span style=\\\"font-size:12px\\\"><span style=\\\"color:#000000\\\"><span style=\\\"font-family:Arial\\\"><span style=\\\"background-color:#f5f5f5\\\"><span style=\\\"font-family:Tahoma\\\"><span style=\\\"font-size:14px\\\"><img alt=\\\"\\\" src=\\\"\\/upload\\/images\\/51571758_1206954472806407_6373434075138490368_n.jpg\\\" style=\\\"height:471px; width:536px\\\" \\/><\\/span><\\/span><\\/span><\\/span><\\/span><\\/span><\\/p>\\r\\n\\r\\n<p style=\\\"text-align:justify\\\"><strong>Sofa da th&acirc;\\u0323t<\\/strong>&nbsp;\\u0111a da\\u0323ng v&ecirc;\\u0300 ki&ecirc;\\u0309u da\\u0301ng va\\u0300 ki\\u0301ch th\\u01b0\\u01a1\\u0301c, co\\u0301 r&acirc;\\u0301t nhi&ecirc;\\u0300u loa\\u0323i cho kha\\u0301ch ha\\u0300ng l\\u01b0\\u0323a cho\\u0323n. Tu\\u0300y thu&ocirc;\\u0323c va\\u0300o di&ecirc;\\u0323n ti\\u0301ch nha\\u0300 ba\\u0323n \\u01a1\\u0309 va\\u0300 \\u0111i&ecirc;\\u0300u ki&ecirc;\\u0323n kinh t&ecirc;\\u0301 ma\\u0300 l\\u01b0\\u0323a cho\\u0323n sofa cu\\u0303ng kha\\u0301c nhau. Co\\u0301 ca\\u0301c loa\\u0323i sofa nh\\u01b0 sofa go\\u0301c, sofa ch\\u01b0\\u0303 U, sofa v\\u0103ng, sofa b&ocirc;\\u0323, sofa \\u0111\\u01a1n. N&ecirc;\\u0301u nha\\u0300 ba\\u0323n r&ocirc;\\u0323ng ra\\u0303i thi\\u0300 n&ecirc;n cho\\u0323n sofa hi\\u0300nh ch\\u01b0\\u0303 U ho\\u0103\\u0323c sofa b&ocirc;\\u0323 se\\u0303 giu\\u0301p t&ocirc;n l&ecirc;n ve\\u0309 \\u0111e\\u0323p hoa\\u0300nh tra\\u0301ng cu\\u0309a ng&ocirc;i nha\\u0300, co\\u0300n n&ecirc;\\u0301u nha\\u0300 ba\\u0323n co\\u0301 di&ecirc;\\u0323n ti\\u0301ch khi&ecirc;m t&ocirc;\\u0301n thi\\u0300 n&ecirc;n l\\u01b0\\u0323a cho\\u0323n sofa go\\u0301c hi\\u0300nh ch\\u01b0\\u0303 L c\\u01a1\\u0303 nho\\u0309 ho\\u0103\\u0323c sofa v\\u0103ng, sofa \\u0111\\u01a1n se\\u0303 giu\\u0301p ti&ecirc;\\u0301t ki&ecirc;\\u0323m t&ocirc;\\u0301i \\u0111a di&ecirc;\\u0323n ti\\u0301ch cu\\u0303ng nh\\u01b0 kinh t&ecirc;\\u0301.<\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(47, NULL, NULL, NULL, 'Sofa nỉ Gia đình mã GD103', NULL, 'sofa-ni-gia-dinh-ma-gd103', 'sofa-vai-ma-ntx1833-pniixv.jpg', NULL, 1, 1, 1, 39, 1, 0, 0, '<p><strong>Sofa Gia Đ&igrave;nh m&atilde; GD103</strong><br />\r\nSofa nỉ gia đ&igrave;nh mang cảm gi&aacute;c đơn giản, tiết kiệm diện t&iacute;ch cho căn ph&ograve;ng của gia đ&igrave;nh bạn</p>\r\n', '', NULL, 'Sofa nỉ gia đình Sofa giá rẻ Sofa Hà Nội', 'Sofa nỉ gia đình Sofa giá rẻ Sofa Hà Nội', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 27, 0, NULL, 'vi', NULL, 1560520705, NULL, '13062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p><strong>Th&ocirc;ng tin s\\u1ea3n ph\\u1ea9m<\\/strong><\\/p>\\r\\n\\r\\n<p>- L&ograve; xo nh\\u1eadp kh\\u1ea9u c&oacute; \\u0111\\u1ed9 \\u0111&agrave;n h\\u1ed3i cao<br \\/>\\r\\n- \\u0110\\u1ec7m m&uacute;t c\\u1ea5u t\\u1ea1o m\\u1edbi g\\u1ed3m 3 l\\u1edbp &ecirc;m &aacute;i, ch\\u1ed1ng l&uacute;n<br \\/>\\r\\n- Ch&acirc;n gh\\u1ebf l&agrave; ch&acirc;n g\\u1ed7 ch\\u1eafc ch\\u1eafn<br \\/>\\r\\n- Tay v\\u1ecbn th\\u1ea5p, t\\u1ef1a l\\u01b0ng &ecirc;m &aacute;i<br \\/>\\r\\n- K&iacute;ch th\\u01b0\\u1edbc: Theo y&ecirc;u c\\u1ea7u<br \\/>\\r\\n- Ch\\u1ea5t li\\u1ec7u b\\u1ecdc sofa \\u0111\\u1eb9p h&agrave; n\\u1ed9i : Da Malaysia<br \\/>\\r\\n- B\\u1ea3o h&agrave;nh 6 n\\u0103m khung x\\u01b0\\u01a1ng, 3 n\\u0103m \\u0111\\u1ec7m m&uacute;t, 1 n\\u0103m ch\\u1ea5t li\\u1ec7u da<br \\/>\\r\\n- Giao h&agrave;ng nhanh ch&oacute;ng<\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"Sofa B\\u1ea3o L\\u00e2m\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau C\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Vi\\u1ec7t Nam\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(46, NULL, NULL, NULL, 'Sofa Gia Đình mã GD104', NULL, 'sofa-gia-dinh-ma-gd104', 'sofavang1.jpg', NULL, 1, 1, 1, 22, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', NULL, 'Sofa Sofa văng Sofa văng gia đình', 'Sofa Sofa văng Sofa văng gia đình', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 26, 0, NULL, 'vi', NULL, 1560522285, NULL, '13062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p><strong>Sofa Gia \\u0110&igrave;nh m&atilde; 104<\\/strong><\\/p>\\r\\n\\r\\n<p>Sofa v\\u0103ng \\u0111\\u01a1n gi\\u1ea3n nh\\u1ecf g\\u1ecdn ph&ugrave; h\\u1ee3p v\\u1edbi ph&ograve;ng kh&aacute;ch c\\u1ee7a nh\\u1eefng c\\u0103n nh&agrave; nh\\u1ecf xinh<\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"Sofa B\\u1ea3o L\\u00e2m\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u \",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Vi\\u1ec7t Nam\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(44, NULL, NULL, NULL, 'Sofa Karaoke mã Sk823', NULL, 'sofa-karaoke-ma-sk823', 'sofakaoke9.jpg', NULL, 1, 1, 1, 7, 1, 15000000, 12000000, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 20, 0, NULL, 'vi', NULL, 1561025243, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Vi\\u1ec7t Nam\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(48, NULL, NULL, NULL, 'Sofa nỉ Gia Đình mã GD102', NULL, 'sofa-ni-gia-dinh-ma-gd102', 'sofani111.jpg', NULL, 1, 1, 1, 11, 1, 0, 0, '<p><strong>Sofa gia đ&igrave;nh m&atilde; GD102</strong><br />\r\nMang phong c&aacute;ch t&acirc;n cổ điển, với kiểu d&aacute;ng sang trọng tinh tế đến từng đường kim mũi chỉ! Bộ sofa đem đến cảm gi&aacute;c sang trọng v&agrave; nổi bật ph&ograve;ng kh&aacute;ch của gi&aacute; đ&igrave;nh bạn</p>\r\n', '', NULL, 'Sofa Gia đình, Sofa giá rẻ', 'Sofa Gia đình, Sofa giá rẻ', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 28, 0, NULL, 'vi', NULL, 1560520518, NULL, '13062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p><strong>Th&ocirc;ng tin s\\u1ea3n ph\\u1ea9m&nbsp;<\\/strong><\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li>L&ograve; xo nh\\u1eadp kh\\u1ea9u c&oacute; \\u0111\\u1ed9 \\u0111&agrave;n h\\u1ed3i cao<\\/li>\\r\\n\\t<li>\\u0110\\u1ec7m m&uacute;t c\\u1ea5u t\\u1ea1o m\\u1edbi g\\u1ed3m 3 l\\u1edbp &ecirc;m &aacute;i, ch\\u1ed1ng l&uacute;n<\\/li>\\r\\n\\t<li>Ch&acirc;n gh\\u1ebf l&agrave; ch&acirc;n g\\u1ed7 ch\\u1eafc ch\\u1eafn<\\/li>\\r\\n\\t<li>Tay v\\u1ecbn th\\u1ea5p, t\\u1ef1a l\\u01b0ng &ecirc;m &aacute;i<\\/li>\\r\\n\\t<li>K&iacute;ch th\\u01b0\\u1edbc: Theo y&ecirc;u c\\u1ea7u<\\/li>\\r\\n\\t<li>Ch\\u1ea5t li\\u1ec7u b\\u1ecdc sofa \\u0111\\u1eb9p h&agrave; n\\u1ed9i : Da Malaysia<\\/li>\\r\\n\\t<li>B\\u1ea3o h&agrave;nh 6 n\\u0103m khung x\\u01b0\\u01a1ng, 3 n\\u0103m \\u0111\\u1ec7m m&uacute;t, 1 n\\u0103m ch\\u1ea5t li\\u1ec7u da<\\/li>\\r\\n\\t<li>Giao h&agrave;ng nhanh ch&oacute;ng<\\/li>\\r\\n<\\/ul>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"Sofa B\\u1ea3o L\\u00e2m\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u \",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Vi\\u1ec7t Nam\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(49, NULL, NULL, NULL, 'Sofa Da mã SD311', NULL, 'sofa-da-ma-sd311', 'sofada7.jpg', NULL, 1, 1, 1, 7, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox&nbsp;chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội', NULL, 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội', NULL, NULL, 0, '0', 13, NULL, NULL, NULL, NULL, NULL, 25, 0, NULL, 'vi', NULL, 1560939408, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(50, NULL, NULL, NULL, 'Sofa Karaoke mã SK822', NULL, 'sofa-karaoke-ma-sk822', 'sofa-karaoke.jpg', NULL, 1, 1, 1, 6, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa karaoke sofa giá rẻ', '', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 26, 0, NULL, 'vi', NULL, 1561025141, NULL, '13062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p>K&iacute;ch th\\u01b0\\u1edbc: theo y&ecirc;u c\\u1ea7u<br \\/>\\r\\nM&agrave;u s\\u1eafc: theo y&ecirc;u c\\u1ea7u<\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"Sofa B\\u1ea3o L\\u00e2m\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(51, NULL, NULL, NULL, 'Sofa Khách Sạn mã KS202', NULL, 'sofa-khach-san-ma-ks202', 'sofa-nha-hang-khach-san-tinh-te-sofa-nguyen-a-3.jpg', NULL, 0, 1, 1, 8, 1, 0, 0, '<p>Sofa cực sang trọng cho kh&aacute;ch sạn&nbsp;</p>\r\n', '', NULL, 'Sofa khách sạn Sofa giá rẻ', '', NULL, NULL, 0, '0', 6, NULL, NULL, NULL, NULL, NULL, 16, 0, NULL, 'vi', NULL, 1560525216, NULL, '13062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p>K&iacute;ch th\\u01b0\\u1edbc: theo y&ecirc;u c\\u1ea7u<br \\/>\\r\\nM&agrave;u s\\u1eafc: theo y&ecirc;u c\\u1ea7u<\\/p>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"Sofa B\\u1ea3o L\\u00e2m\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL);
INSERT INTO `product` (`id`, `style`, `id_value`, `brand`, `name`, `code`, `alias`, `image`, `hot`, `home`, `focus`, `coupon`, `view`, `active`, `price`, `price_sale`, `description`, `description_seo`, `location`, `title_seo`, `keyword_seo`, `detail`, `note`, `like`, `order`, `category_id`, `caption_1`, `caption_2`, `locale`, `bought`, `dksudung`, `sort`, `quantity`, `counter`, `lang`, `destination`, `time`, `tags`, `pro_dir`, `multi_image`, `img_dir`, `status`, `quaranty`, `tinhtrang`, `group_attribute_id`, `color`, `size`, `user_id`, `option_id`, `button_color1`, `config_pro`, `config_pro_content`, `weight`, `price_imp`) VALUES
(52, NULL, NULL, NULL, 'Sofa Gia Đình mã GD101', NULL, 'sofa-gia-dinh-ma-gd101', '156soffa1233.png', NULL, 1, 1, 1, 32, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao</li>\r\n	<li>Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n</li>\r\n	<li>Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn</li>\r\n	<li>Tay vịn thấp, tựa lưng &ecirc;m &aacute;i</li>\r\n	<li>K&iacute;ch thước: Theo y&ecirc;u cầu</li>\r\n	<li>Khung xương: &nbsp;gỗ sồi tự nhi&ecirc;n</li>\r\n	<li>Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia</li>\r\n	<li>Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da</li>\r\n	<li>Giao h&agrave;ng nhanh ch&oacute;ng</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '', NULL, 'Sofa Gia đình, Sofa giá rẻ', 'Sofa nỉ gia đình Sofa giá rẻ Sofa Hà Nội', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 29, 0, NULL, 'vi', NULL, 1560657119, NULL, '16062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"<p><strong>Sofa gia \\u0111&igrave;nh m&atilde; GD101<\\/strong><br \\/>\\r\\nNh\\u1eefng gam m&agrave;u tr\\u1eafng s&aacute;ng th\\u01b0\\u1eddng r\\u1ea5t \\u0111\\u01b0\\u1ee3c \\u01b0a chu\\u1ed9ng s\\u1eed d\\u1ee5ng trong nh\\u1eefng kh&ocirc;ng gian n\\u1ed9i th\\u1ea5t \\u0111\\u01b0\\u1ee3c thi\\u1ebft k\\u1ebf theo phong c&aacute;ch hi\\u1ec7n \\u0111\\u1ea1i, \\u0111\\u1eb7c bi\\u1ec7t l&agrave; trong nh\\u1eefng c\\u0103n ph&ograve;ng c&oacute; di\\u1ec7n t&iacute;ch nh\\u1ecf.&nbsp;<\\/p>\\r\\n\\r\\n<ul>\\r\\n<\\/ul>\\r\\n\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(53, NULL, NULL, NULL, 'Sofa Gia Đình Mã GD 112', NULL, 'sofa-gia-dinh-ma-gd-112', 'lua-chon-ban-ghe-sofa-cho-can-ho.jpg', NULL, 1, 1, 1, 12, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa Gia đình, Sofa giá rẻ', '', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 21, 0, NULL, 'vi', NULL, 1560522833, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(54, NULL, NULL, NULL, 'Sofa Gia Đình mã GD108', NULL, 'sofa-gia-dinh-ma-gd108', 'c4.jpg', NULL, 1, 1, 1, 8, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa Gia đình, Sofa giá rẻ', 'Sofa nỉ gia đình Sofa giá rẻ Sofa Hà Nội', NULL, NULL, 0, '0', 5, NULL, NULL, NULL, NULL, NULL, 22, 0, NULL, 'vi', NULL, 1560916161, NULL, '14062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(56, NULL, NULL, NULL, 'Sofa Cao Cấp mã CP511', NULL, 'sofa-cao-cap-ma-cp511', '156sofa125.png', NULL, 0, 1, 1, 7, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa cao cấp ', 'Sofa cao cấp ', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 31, 0, NULL, 'vi', NULL, 1560572731, NULL, '15062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(57, NULL, NULL, NULL, 'Sofa Cao Cấp mã CP512', NULL, 'sofa-cao-cap-ma-cp512', '156soffa122.png', NULL, 0, 1, 1, 4, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa cao cấp ', 'Sofa cao cấp ', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 32, 0, NULL, 'vi', NULL, 1560572853, NULL, '15062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(58, NULL, NULL, NULL, 'Sofa Cao Cấp mã CP 513', NULL, 'sofa-cao-cap-ma-cp-513', '156soffa124.png', NULL, 1, 1, 1, 4, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong></p>\r\n\r\n<p>- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa cao cấp ', 'Sofa cao cấp ', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 33, 0, NULL, 'vi', NULL, 1560572997, NULL, '15062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(60, NULL, NULL, NULL, 'Sofa', NULL, 'sofa', '166sofa911.png', NULL, 1, 1, 1, 1, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa cao cấp ', 'Sofa cao cấp ', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 35, 0, NULL, 'vi', NULL, 1560751486, NULL, '16062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(61, NULL, NULL, NULL, 'Sofa Cao Cấp mã CP518', NULL, 'sofa-cao-cap-ma-cp518', '166sofa411.png', NULL, 1, 1, 1, 5, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa cao cấp ', 'Sofa cao cấp ', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 36, 0, NULL, 'vi', NULL, 1560935765, NULL, '16062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(62, NULL, NULL, NULL, 'Sofa Cao Cấp mã CP519', NULL, 'sofa-cao-cap-ma-cp519', '166sofa2111.png', NULL, 1, 1, 1, 5, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n gỗ chắc chắn<br />\r\n- Tay vịn thấp, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- Chất liệu bọc sofa đẹp h&agrave; nội : Da Malaysia<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', '', NULL, 'Sofa cao cấp ', 'Sofa cao cấp ', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 37, 0, NULL, 'vi', NULL, 1560935745, NULL, '16062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(65, NULL, NULL, NULL, 'Sofa Da mã SD313', NULL, 'sofa-da-ma-sd313', 'sofada1.jpg', NULL, 1, 1, 1, 4, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox&nbsp;chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', NULL, 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', NULL, NULL, 0, '0', 13, NULL, NULL, NULL, NULL, NULL, 39, 0, NULL, 'vi', NULL, 1560939303, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(64, NULL, NULL, NULL, 'Sofa Da mã SD312', NULL, 'sofa-da-ma-sd312', 'sofada6.jpg', NULL, 1, 1, 1, 7, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội', NULL, 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội', NULL, NULL, 0, '0', 11, NULL, NULL, NULL, NULL, NULL, 38, 0, NULL, 'vi', NULL, 1560939312, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(66, NULL, NULL, NULL, 'Sofa Da mã SD314', NULL, 'sofa-da-ma-sd314', 'sofada4.jpg', NULL, 1, 1, 1, 4, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', NULL, 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', NULL, NULL, 0, '0', 13, NULL, NULL, NULL, NULL, NULL, 40, 0, NULL, 'vi', NULL, 1560939294, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(67, NULL, NULL, NULL, 'Sofa Da mã SD315', NULL, 'sofa-da-ma-sd315', 'sofada8.jpg', NULL, 1, 1, 1, 9, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', NULL, 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', NULL, NULL, 0, '0', 13, NULL, NULL, NULL, NULL, NULL, 41, 0, NULL, 'vi', NULL, 1560939283, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(68, NULL, NULL, NULL, 'Sofa Da mã SD316', NULL, 'sofa-da-ma-sd316', 'sofada9.jpg', NULL, 1, 1, 1, 4, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', NULL, 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', 'Sofa da sofa giá rẻ, Sofa da, Sofa Hà Nội, Sofa da Hà Nội, Thanh lý sofa Hà Nội', NULL, NULL, 0, '0', 13, NULL, NULL, NULL, NULL, NULL, 42, 0, NULL, 'vi', NULL, 1560939213, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(69, NULL, NULL, NULL, 'Sofa Cafe mã SC611', NULL, 'sofa-cafe-ma-sc611', 'sofacafe1.jpg', NULL, 1, 1, 1, 9, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 43, 0, NULL, 'vi', NULL, 1560939202, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(70, NULL, NULL, NULL, 'Sofa Cafe mã SC612', NULL, 'sofa-cafe-ma-sc612', 'sofacafe9.jpg', NULL, 1, 1, 1, 6, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 44, 0, NULL, 'vi', NULL, 1560939189, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(71, NULL, NULL, NULL, 'Sofa Cafe mã SD613', NULL, 'sofa-cafe-ma-sd613', 'sofacafe2.jpg', NULL, 1, 1, 1, 6, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 45, 0, NULL, 'vi', NULL, 1560939179, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(72, NULL, NULL, NULL, 'Sofa Cafe mã SC614', NULL, 'sofa-cafe-ma-sc614', 'sofacafe3.jpg', NULL, 1, 1, 1, 8, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 46, 0, NULL, 'vi', NULL, 1560939169, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(73, NULL, NULL, NULL, 'Sofa Cafe mã SC615', NULL, 'sofa-cafe-ma-sc615', 'sofacafe4.jpg', NULL, 1, 1, 1, 4, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 47, 0, NULL, 'vi', NULL, 1560939156, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(74, NULL, NULL, NULL, 'Sofa Cafe mã SC616', NULL, 'sofa-cafe-ma-sc616', 'sofacafe5.jpg', NULL, 1, 1, 1, 7, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 48, 0, NULL, 'vi', NULL, 1560939145, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(75, NULL, NULL, NULL, 'Sofa Cafe mã SC617', NULL, 'sofa-cafe-ma-sc617', 'sofacafe6.jpg', NULL, 1, 1, 1, 9, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 49, 0, NULL, 'vi', NULL, 1560939134, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(76, NULL, NULL, NULL, 'Sofa Cafe mã SC618', NULL, 'sofa-cafe-ma-sc618', 'sofacafe7.jpg', NULL, 1, 1, 1, 4, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 50, 0, NULL, 'vi', NULL, 1560939123, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(77, NULL, NULL, NULL, 'Sofa Cafe mã SC619', NULL, 'sofa-cafe-ma-sc619', 'sofacafe8.jpg', NULL, 1, 1, 1, 8, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 51, 0, NULL, 'vi', NULL, 1560939116, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(78, NULL, NULL, NULL, 'Sofa Cafe mã SC621', NULL, 'sofa-cafe-ma-sc621', 'sofacafe211.jpg', NULL, 1, 1, 1, 6, 1, 0, 0, '<p><strong>Th&ocirc;ng tin sản phẩm&nbsp;</strong><br />\r\n- L&ograve; xo nhập khẩu c&oacute; độ đ&agrave;n hồi cao<br />\r\n- Đệm m&uacute;t cấu tạo mới gồm 3 lớp &ecirc;m &aacute;i, chống l&uacute;n<br />\r\n- Ch&acirc;n ghế l&agrave; ch&acirc;n innox hoặc gỗ chắc chắn<br />\r\n- Tay vịn chắc chắn, tựa lưng &ecirc;m &aacute;i<br />\r\n- K&iacute;ch thước: Theo y&ecirc;u cầu<br />\r\n- M&agrave;u sắc:Theo thiết kế&nbsp;<br />\r\n- Chất liệu bọc sofa đẹp H&agrave; Nội<br />\r\n- Bảo h&agrave;nh 6 năm khung xương, 3 năm đệm m&uacute;t, 1 năm chất liệu da<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', 'Sofa Cafe, Sofa Hà Nội, Sofa giá rẻ Hà Nội', NULL, NULL, 0, '0', 7, NULL, NULL, NULL, NULL, NULL, 52, 0, NULL, 'vi', NULL, 1560939106, NULL, '19062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo  y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(82, NULL, NULL, NULL, 'Sofa Karaoke mã Sk814', NULL, 'sofa-karaoke-ma-sk814', 'sofakaoke55.jpg', NULL, 1, 1, 1, 6, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 56, 0, NULL, 'vi', NULL, 1561023728, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(83, NULL, NULL, NULL, 'Sofa Karaoke mã SK815', NULL, 'sofa-karaoke-ma-sk815', 'sofakaoke77.jpg', NULL, 1, 1, 1, 19, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 57, 0, NULL, 'vi', NULL, 1561023813, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(84, NULL, NULL, NULL, 'Sofa Karaoke mã SK816', NULL, 'sofa-karaoke-ma-sk816', 'sofakaraoke3.png', NULL, 1, 1, 1, 17, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm</strong>&nbsp;<br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 58, 0, NULL, 'vi', NULL, 1561024363, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(85, NULL, NULL, NULL, 'Sofa Karaoke mã SK817', NULL, 'sofa-karaoke-ma-sk817', 'sofakaraoke2.jpg', NULL, 1, 1, 1, 13, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng<br />\r\n&nbsp;</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 59, 0, NULL, 'vi', NULL, 1561024444, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(86, NULL, NULL, NULL, 'Sofa Karaoke mã SK818', NULL, 'sofa-karaoke-ma-sk818', 'sofakaraoke4.jpg', NULL, 1, 1, 1, 17, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 'vi', NULL, 1561024548, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL),
(87, NULL, NULL, NULL, 'Sofa Karaoke mã SK819', NULL, 'sofa-karaoke-ma-sk819', 'sofakaraoke5.jpg', NULL, 1, 1, 1, 30, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 61, 0, NULL, 'vi', NULL, 1561024617, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL);
INSERT INTO `product` (`id`, `style`, `id_value`, `brand`, `name`, `code`, `alias`, `image`, `hot`, `home`, `focus`, `coupon`, `view`, `active`, `price`, `price_sale`, `description`, `description_seo`, `location`, `title_seo`, `keyword_seo`, `detail`, `note`, `like`, `order`, `category_id`, `caption_1`, `caption_2`, `locale`, `bought`, `dksudung`, `sort`, `quantity`, `counter`, `lang`, `destination`, `time`, `tags`, `pro_dir`, `multi_image`, `img_dir`, `status`, `quaranty`, `tinhtrang`, `group_attribute_id`, `color`, `size`, `user_id`, `option_id`, `button_color1`, `config_pro`, `config_pro_content`, `weight`, `price_imp`) VALUES
(88, NULL, NULL, NULL, 'Sofa Karaoke mã Sk821', NULL, 'sofa-karaoke-ma-sk821', 'sofakaraoke6.jpg', NULL, 1, 1, 1, 28, 1, 0, 0, '<p><strong>Th&ocirc;ng Tin Sản Phẩm&nbsp;</strong><br />\r\n-Khung gỗ dầu đ&atilde; qua sử l&yacute; chống mối mọt cong v&ecirc;nh đảm bảo độ bền v&agrave; khả năng chịu nhiệt tốt ( Bảo H&agrave;nh 36 th&aacute;ng )<br />\r\n-Bọc vải nỉ , simili &nbsp;nhập khẩu cao cấp với đa dạng chất liệu vải bọc đa dạng như : vải ,nỉ ,bố ,nhung ,gấm cao cấp &hellip; đa dạng m&agrave;u sắc v&ocirc; c&ugrave;ng ấn tượng ,hiện đại cho bạn tha hồ lựa chọn khi đến với Việt Ph&aacute;t ch&uacute;ng t&ocirc;i .<br />\r\n- Nệm D40, m&uacute;t &eacute;p &nbsp;kh&ocirc;ng n&uacute;n, xẹp c&oacute; độ bền cao ( Bảo H&agrave;nh &nbsp;36 th&aacute;ng )<br />\r\n-Ch&acirc;n ghế sofa tự chọn thường được d&ugrave;ng từ gỗ ,nhựa ,inox cao cấp ..<br />\r\n**Đặc Biệt :<br />\r\n- K&iacute;ch Thước , M&agrave;u Sắc ,Chất Liệu t&ugrave;y chọn theo kh&ocirc;ng gian nh&agrave; .<br />\r\n- Giao h&agrave;ng nhanh ch&oacute;ng</p>\r\n', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', 'Sofa karaoke, sofa giá rẻ, Sofa giá rẻ, Sofa Karaoke Hà Nội', NULL, NULL, 0, '0', 9, NULL, NULL, NULL, NULL, NULL, 62, 0, NULL, 'vi', NULL, 1561024745, NULL, '20062019', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"Theo y\\u00eau c\\u1ea7u\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_brand`
--

CREATE TABLE `product_brand` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `women` tinyint(1) DEFAULT NULL,
  `men` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `title_seo` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `description_seo` text CHARACTER SET latin1,
  `keyword` text CHARACTER SET latin1,
  `sort` int(11) DEFAULT NULL,
  `lang` char(10) CHARACTER SET latin1 DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '1',
  `view` tinyint(1) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_brand`
--

INSERT INTO `product_brand` (`id`, `parent_id`, `name`, `image`, `alias`, `description`, `women`, `men`, `focus`, `title_seo`, `description_seo`, `keyword`, `sort`, `lang`, `gender`, `view`, `home`, `hot`) VALUES
(10, NULL, 'Chanel', 'upload/img/tải_xuống_(1).png', 'chanel', '', 0, 1, 0, NULL, NULL, NULL, 1, 'vi', 1, NULL, NULL, NULL),
(11, NULL, 'puma', 'upload/img/images_(13).jpg', 'puma', '', NULL, NULL, 1, NULL, NULL, NULL, 16, 'vi', 1, NULL, NULL, NULL),
(13, NULL, 'Lanvin', 'upload/img/th17.png', 'lanvin', '', 0, 0, 0, NULL, NULL, NULL, 4, 'vi', 1, NULL, NULL, NULL),
(14, NULL, 'H&M', 'upload/img/tải_xuống_(2).png', 'hm', '', 0, 1, 0, NULL, NULL, NULL, 5, 'vi', 1, NULL, NULL, NULL),
(15, NULL, 'Nike', 'upload/img/tải_xuống_(1).jpg', 'nike', '', 0, 0, 1, NULL, NULL, NULL, 6, 'vi', 1, NULL, NULL, NULL),
(20, NULL, 'Valentino', 'upload/img/tải_xuống_(6).png', 'valentino', '', 0, 0, 0, NULL, NULL, NULL, 7, 'vi', 1, NULL, NULL, NULL),
(21, NULL, 'Zaza', 'upload/img/tải_xuống_(4).png', 'zaza', '', 0, 0, 0, NULL, NULL, NULL, 8, 'vi', 1, NULL, NULL, NULL),
(22, NULL, 'Gucci', 'upload/img/images_(4).jpg', 'gucci', '', NULL, NULL, NULL, NULL, NULL, NULL, 15, 'vi', 1, NULL, NULL, NULL),
(23, NULL, 'Armani', 'upload/img/th4.png', 'armani', '', 0, 0, 0, NULL, NULL, NULL, 1, 'vi', 1, NULL, NULL, NULL),
(24, NULL, 'Bebe', 'upload/img/8307969_orig.jpg', 'bebe', '', 0, 0, 0, NULL, NULL, NULL, 11, 'vi', 1, NULL, NULL, NULL),
(32, NULL, 'Dior', 'upload/img/images_(14).jpg', 'dior', '', 0, 0, 0, NULL, NULL, NULL, 12, 'vi', 1, NULL, NULL, NULL),
(33, NULL, 'Mango', 'upload/img/th7.png', 'mango', '', NULL, NULL, NULL, NULL, NULL, NULL, 14, 'vi', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `user` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` text CHARACTER SET utf8,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `home` tinyint(1) DEFAULT NULL,
  `sort` int(3) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `coupon` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT '1',
  `gender` tinyint(1) DEFAULT NULL,
  `banner` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `user`, `name`, `title_seo`, `keyword_seo`, `description_seo`, `image`, `alias`, `parent_id`, `description`, `home`, `sort`, `hot`, `coupon`, `focus`, `lang`, `gender`, `banner`) VALUES
(6, NULL, 'Sofa khách sạn', '', NULL, '', NULL, 'sofa-khach-san', 0, '', 0, 5, NULL, NULL, NULL, 'vi', NULL, NULL),
(5, NULL, 'Sofa gia đình', '', NULL, '', NULL, 'sofa-gia-dinh', 0, '', 1, 8, NULL, NULL, NULL, 'vi', NULL, NULL),
(7, NULL, 'Sofa cafe', '', NULL, '', NULL, 'sofa-cafe', 0, '', 0, 7, NULL, NULL, NULL, 'vi', NULL, NULL),
(9, NULL, 'Thanh lý Sofa Karaoke', '', NULL, '', NULL, 'thanh-ly-sofa-karaoke', 0, '', 1, 9, NULL, NULL, NULL, 'vi', NULL, NULL),
(11, NULL, 'Sofa cao cấp', '', NULL, '', NULL, 'sofa-cao-cap', 0, '', 0, 3, NULL, NULL, NULL, 'vi', NULL, NULL),
(12, NULL, 'Sofa giá rẻ', '', NULL, '', NULL, 'sofa-gia-re', 0, '', 0, 1, NULL, NULL, NULL, 'vi', NULL, NULL),
(13, NULL, 'Sofa da', '', NULL, '', NULL, 'sofa-da', 0, '', 0, 2, NULL, NULL, NULL, 'vi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE `product_color` (
  `id` int(11) NOT NULL,
  `color` varchar(255) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lang` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_color`
--

INSERT INTO `product_color` (`id`, `color`, `name`, `description`, `lang`, `image`, `sort`, `parent_id`) VALUES
(2, '#31859b', 'Màu xanh lam', 'màu xanh lam', 'vi', NULL, 3, NULL),
(3, '#000000', 'Màu đen', 'màu đen', 'vi', NULL, 4, NULL),
(4, '#ff0000', 'Màu đỏ', 'màu đỏ', 'vi', NULL, 5, NULL),
(5, '#7030a0', 'Màu tím', 'màu tím', 'vi', NULL, 6, NULL),
(6, '#f79646', 'Màu cam', 'màu cam', 'vi', NULL, 7, NULL),
(7, '#ffffff', 'Màu Trắng', 'màu trắng', 'vi', NULL, 8, NULL),
(8, '#d99694', 'màu hồng', '', 'vi', NULL, 9, NULL),
(9, '#7f7f7f', 'màu ghi', 'màu ghi', 'vi', NULL, 10, NULL),
(10, '#ffc000', 'màu ánh vàng', 'màu ánh vàng', 'vi', NULL, 11, NULL),
(11, '#974806', 'màu nâu', 'màu nâu', 'vi', NULL, 12, NULL),
(12, '#4f6128', 'màu xanh xám', 'màu xanh xám', 'vi', NULL, 13, NULL),
(13, '#d8d8d8', 'Màu ánh bạc', 'màu ánh bạc', 'vi', NULL, 14, NULL),
(16, '#5f497a', 'tím', '', 'vi', NULL, 15, NULL),
(17, '#fdeada', 'Màu nude', '<p>m&agrave;u nude</p>\r\n', 'vi', NULL, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_img`
--

CREATE TABLE `product_img` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `multi_image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `img_dir` varchar(255) CHARACTER SET latin1 NOT NULL,
  `id_color` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_locale`
--

CREATE TABLE `product_locale` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(70) CHARACTER SET latin1 DEFAULT NULL,
  `sort` tinyint(1) DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `lang` char(10) CHARACTER SET latin1 DEFAULT NULL,
  `alias` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_locale`
--

INSERT INTO `product_locale` (`id`, `name`, `image`, `sort`, `description`, `lang`, `alias`, `title_seo`, `description_seo`, `keyword`, `parent_id`) VALUES
(4, 'Ấn Độ', NULL, 2, '', 'vi', 'an-do', NULL, NULL, NULL, NULL),
(5, 'Thái Lan', NULL, 3, '', 'vi', 'thai-lan', NULL, NULL, NULL, NULL),
(6, 'Đài Loan', NULL, 4, '', 'vi', 'dai-loan', NULL, NULL, NULL, NULL),
(7, 'Trung Quốc', NULL, 5, '', 'vi', 'trung-quoc', NULL, NULL, NULL, NULL),
(8, 'Anh', NULL, 6, '', 'vi', 'anh', NULL, NULL, NULL, NULL),
(9, 'Pháp', NULL, 7, '', 'vi', 'phap', NULL, NULL, NULL, NULL),
(10, 'Mỹ', NULL, 8, '', 'vi', 'my', NULL, NULL, NULL, NULL),
(11, 'Nhật', NULL, 10, '', 'vi', 'nhat', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_old`
--

CREATE TABLE `product_old` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hot` tinyint(1) NOT NULL,
  `home` tinyint(1) NOT NULL,
  `focus` tinyint(1) NOT NULL,
  `coupon` tinyint(1) NOT NULL,
  `mostview` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `price_sale` int(11) NOT NULL,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_seo` text COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `like` int(11) NOT NULL DEFAULT '0',
  `origin` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `color` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `size` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `caption_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `from_price` int(11) DEFAULT NULL,
  `to_price` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `lang` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_price`
--

INSERT INTO `product_price` (`id`, `name`, `from_price`, `to_price`, `sort`, `lang`) VALUES
(1, 'Dưới 100.000 đ', 0, 100000, 1, 'vi'),
(3, '200.000 - 400.000 đ', 200000, 400000, 2, 'vi'),
(4, '400.000 - 500.000 đ', 400000, 500000, 3, 'vi'),
(5, '500.000 - 1000.000 đ', 500000, 1000000, 4, 'vi'),
(6, '1000000 - 2000000đ', 1000000, 2000000, 5, 'vi'),
(9, 'Trên 2000000đ', 2000000, 3000000, 6, 'vi');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `size` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sort` int(11) NOT NULL,
  `lang` varchar(100) CHARACTER SET latin1 NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`id`, `name`, `size`, `sort`, `lang`, `parent_id`) VALUES
(1, 'XL', '', 15, 'vi', NULL),
(2, 'M', '', 13, 'vi', NULL),
(3, 'XS', '', 12, 'vi', NULL),
(4, 'L', '', 14, 'vi', NULL),
(5, 'S', '', 11, 'vi', NULL),
(6, 'XXL', '', 16, 'vi', NULL),
(7, '34', '', 1, 'vi', NULL),
(8, '35', '', 2, 'vi', NULL),
(9, '36', '', 3, 'vi', NULL),
(10, '37', '', 4, 'vi', NULL),
(11, '38', '', 5, 'vi', NULL),
(12, '39', '', 6, 'vi', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_tag`
--

CREATE TABLE `product_tag` (
  `product_tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lang` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tag` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_brand`
--

CREATE TABLE `product_to_brand` (
  `brand_id` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_category`
--

CREATE TABLE `product_to_category` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_to_category`
--

INSERT INTO `product_to_category` (`id`, `id_product`, `id_category`) VALUES
(600, 85, 9),
(599, 84, 9),
(604, 88, 9),
(576, 74, 7),
(467, 32, 5),
(495, 54, 5),
(462, 53, 5),
(468, 31, 5),
(472, 30, 6),
(602, 87, 9),
(601, 86, 9),
(578, 73, 7),
(580, 72, 7),
(579, 72, 12),
(582, 71, 7),
(581, 71, 12),
(584, 70, 7),
(583, 70, 12),
(586, 69, 7),
(585, 69, 12),
(587, 68, 13),
(588, 67, 13),
(589, 66, 13),
(590, 65, 13),
(592, 64, 11),
(591, 64, 13),
(594, 79, 9),
(492, 23, 5),
(569, 78, 7),
(570, 77, 7),
(572, 76, 7),
(571, 76, 12),
(574, 75, 7),
(573, 75, 12),
(473, 29, 6),
(575, 74, 12),
(479, 58, 11),
(477, 57, 11),
(476, 56, 11),
(475, 55, 6),
(474, 28, 6),
(577, 73, 12),
(496, 62, 11),
(497, 61, 11),
(493, 60, 11),
(485, 59, 11),
(484, 27, 11),
(598, 83, 9),
(597, 82, 9),
(596, 81, 9),
(595, 80, 9),
(349, 36, 1),
(350, 36, 14),
(351, 36, 15),
(352, 36, 16),
(353, 36, 36),
(354, 36, 37),
(605, 50, 9),
(593, 49, 13),
(431, 48, 5),
(432, 47, 5),
(454, 46, 5),
(471, 37, 6),
(481, 52, 5),
(470, 51, 6),
(417, 38, 12),
(465, 39, 5),
(480, 40, 5),
(469, 41, 6),
(415, 42, 12),
(606, 44, 9);

-- --------------------------------------------------------

--
-- Table structure for table `product_to_color`
--

CREATE TABLE `product_to_color` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_color` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_to_color`
--

INSERT INTO `product_to_color` (`id`, `id_product`, `id_color`) VALUES
(67, 5, 2),
(68, 5, 3),
(69, 5, 4),
(70, 5, 5),
(71, 5, 6),
(72, 5, 7),
(75, 3, 2),
(76, 3, 3),
(82, 4, 2),
(81, 8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_to_option`
--

CREATE TABLE `product_to_option` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_season`
--

CREATE TABLE `product_to_season` (
  `id` int(11) NOT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_season` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_size`
--

CREATE TABLE `product_to_size` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_size` int(11) NOT NULL,
  `note` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_to_size`
--

INSERT INTO `product_to_size` (`id`, `id_product`, `id_size`, `note`) VALUES
(1, 120, 1, ''),
(5, 101, 1, ''),
(6, 100, 1, ''),
(7, 99, 1, ''),
(8, 98, 1, ''),
(9, 97, 1, ''),
(10, 96, 1, ''),
(11, 95, 1, ''),
(12, 93, 1, ''),
(13, 94, 1, ''),
(14, 16, 1, ''),
(16, 2, 1, ''),
(17, 3, 1, ''),
(19, 4, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `lat` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `lng` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `districtid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `lat`, `lng`, `districtid`) VALUES
(1, '13B Conic Phong Phú', '10.71240234375', '106.64177703857', 1),
(2, '13D Asia Phú Mỹ', '10.705533027649', '106.64806365967', 1);

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `price` int(11) DEFAULT '0',
  `sort` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `name`, `code`, `price`, `sort`) VALUES
(1, 'Hồ Chí Minh', 'SG', 20000, NULL),
(2, 'Hà Nội', 'HN', 30000, NULL),
(3, 'Đà Nẵng', 'DDN', 0, NULL),
(4, 'Bình Dương', 'BD', 0, NULL),
(5, 'Đồng Nai', 'DNA', 0, NULL),
(6, 'Khánh Hòa', 'KH', 0, NULL),
(7, 'Hải Phòng', 'HP', 0, NULL),
(8, 'Long An', 'LA', 0, NULL),
(9, 'Quảng Nam', 'QNA', 0, NULL),
(10, 'Bà Rịa Vũng Tàu', 'VT', 0, NULL),
(11, 'Đắk Lắk', 'DDL', 0, NULL),
(12, 'Cần Thơ', 'CT', 0, NULL),
(13, 'Bình Thuận  ', 'BTH', 0, NULL),
(14, 'Lâm Đồng', 'LDD', 0, NULL),
(15, 'Thừa Thiên Huế', 'TTH', 0, NULL),
(16, 'Kiên Giang', 'KG', 0, NULL),
(17, 'Bắc Ninh', 'BN', 0, NULL),
(18, 'Quảng Ninh', 'QNI', 0, NULL),
(19, 'Thanh Hóa', 'TH', 0, NULL),
(20, 'Nghệ An', 'NA', 0, NULL),
(21, 'Hải Dương', 'HD', 0, NULL),
(22, 'Gia Lai', 'GL', 0, NULL),
(23, 'Bình Phước', 'BP', 0, NULL),
(24, 'Hưng Yên', 'HY', 0, NULL),
(25, 'Bình Định', 'BDD', 0, NULL),
(26, 'Tiền Giang', 'TG', 0, NULL),
(27, 'Thái Bình', 'TB', 0, NULL),
(28, 'Bắc Giang', 'BG', 0, NULL),
(29, 'Hòa Bình', 'HB', 0, NULL),
(30, 'An Giang', 'AG', 0, NULL),
(31, 'Vĩnh Phúc', 'VP', 0, NULL),
(32, 'Tây Ninh', 'TNI', 0, NULL),
(33, 'Thái Nguyên', 'TN', 0, NULL),
(34, 'Lào Cai', 'LCA', 0, NULL),
(35, 'Nam Định', 'NDD', 0, NULL),
(36, 'Quảng Ngãi', 'QNG', 0, NULL),
(37, 'Bến Tre', 'BTR', 0, NULL),
(38, 'Đắk Nông', 'DNO', 0, NULL),
(39, 'Cà Mau', 'CM', 120000, NULL),
(40, 'Vĩnh Long', 'VL', 3, NULL),
(41, 'Ninh Bình', 'NB', 320, NULL),
(42, 'Phú Thọ', 'PT', 25, NULL),
(43, 'Ninh Thuận', 'NT', 120000, NULL),
(44, 'Phú Yên', 'PY', 123456, NULL),
(45, 'Hà Nam', 'HNA', 40000, NULL),
(46, 'Hà Tĩnh', 'HT', 12000, NULL),
(47, 'Đồng Tháp', 'DDT', 0, NULL),
(48, 'Sóc Trăng', 'ST', 0, NULL),
(49, 'Kon Tum', 'KT', 0, NULL),
(50, 'Quảng Bình', 'QB', 0, NULL),
(51, 'Quảng Trị', 'QT', 0, NULL),
(52, 'Trà Vinh', 'TV', 0, NULL),
(53, 'Hậu Giang', 'HGI', 0, NULL),
(54, 'Sơn La', 'SL', 0, NULL),
(55, 'Bạc Liêu', 'BL', 0, NULL),
(56, 'Yên Bái', 'YB', 0, NULL),
(57, 'Tuyên Quang', 'TQ', 0, NULL),
(58, 'Điện Biên', 'DDB', 0, NULL),
(59, 'Lai Châu', 'LCH', 0, NULL),
(60, 'Lạng Sơn', 'LS', 0, NULL),
(61, 'Hà Giang', 'HG', 0, NULL),
(62, 'Bắc Kạn', 'BK', 0, NULL),
(63, 'Cao Bằng', 'CB', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pro_size`
--

CREATE TABLE `pro_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pro_values`
--

CREATE TABLE `pro_values` (
  `pro_id` int(11) DEFAULT NULL,
  `attr_id` int(11) DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `p_images`
--

CREATE TABLE `p_images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` char(200) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `p_images`
--

INSERT INTO `p_images` (`id`, `name`, `id_item`, `image`, `url`, `link`, `sort`) VALUES
(37, NULL, 42, 'upload/img/products_multi/2.png', NULL, NULL, NULL),
(35, NULL, 38, 'upload/img/products_multi/13-1.jpg', NULL, NULL, NULL),
(66, NULL, 37, 'upload/img/products_multi/sofa_khách_sạn.jpg', NULL, NULL, NULL),
(11, 'anh so 1', 93, 'upload/img/products_multi/web.png', NULL, '', NULL),
(13, 'anh 2', 92, 'upload/img/products_multi/logo1.png', NULL, '', NULL),
(14, '', 92, 'upload/img/products_multi/logo-thep.jpg', NULL, '', NULL),
(16, 'anh so 2', NULL, 'upload/img/products_multi/logo-thep1.jpg', NULL, '', NULL),
(17, 'anh so 2', NULL, NULL, NULL, '', NULL),
(18, 'anh so 123', NULL, 'upload/img/products_multi/logo.png', NULL, '', NULL),
(19, 'anh cho 91', NULL, NULL, NULL, '', NULL),
(20, 'anh cho 91', NULL, 'upload/img/products_multi/logo1.png', NULL, '', NULL),
(22, 'anh so 1', 15, 'upload/img/products_multi/golf.png', NULL, '', NULL),
(24, NULL, 21, 'upload/img/products_multi/01.jpg', NULL, NULL, NULL),
(25, NULL, 21, 'upload/img/products_multi/2.jpg', NULL, NULL, NULL),
(26, NULL, 21, 'upload/img/products_multi/3.jpg', NULL, NULL, NULL),
(68, NULL, 28, 'upload/img/products_multi/kháchan3.jpg', NULL, NULL, NULL),
(69, NULL, 55, 'upload/img/products_multi/ngam-can-ho-sang-trong-mang-phong-cach-co-dien-2.jpg', NULL, NULL, NULL),
(60, NULL, 39, 'upload/img/products_multi/tim-hieu-ve-bao-duong-sofa-da-dung-chuan-nhu-chuyen-gia-1.jpg', NULL, NULL, NULL),
(36, NULL, 38, 'upload/img/products_multi/13-2.png', NULL, NULL, NULL),
(54, NULL, 47, 'upload/img/products_multi/sofani.jpg', NULL, NULL, NULL),
(40, NULL, 45, 'upload/img/products_multi/5.jpg', NULL, NULL, NULL),
(41, 'Sofagiadinh1', 46, 'upload/img/products_multi/4.jpg', NULL, NULL, NULL),
(42, NULL, 47, 'upload/img/products_multi/1.png', NULL, NULL, NULL),
(43, NULL, 19, 'upload/img/products_multi/15.png', NULL, NULL, NULL),
(44, NULL, 20, 'upload/img/products_multi/16-11.png', NULL, NULL, NULL),
(80, NULL, 23, 'upload/img/products_multi/156soffa127.png', NULL, NULL, NULL),
(50, NULL, 42, 'upload/img/products_multi/51571758_1206954472806407_6373434075138490368_n.jpg', NULL, NULL, NULL),
(107, NULL, 44, 'upload/img/products_multi/sofakaoke9.jpg', NULL, NULL, NULL),
(52, NULL, 45, 'upload/img/products_multi/sofavang1.jpg', NULL, NULL, NULL),
(53, 'Sofagiadinh1', 46, 'upload/img/products_multi/sofavang11.jpg', NULL, NULL, NULL),
(55, NULL, 48, 'upload/img/products_multi/sofani111.jpg', NULL, NULL, NULL),
(81, NULL, 49, 'upload/img/products_multi/sofada7.jpg', NULL, NULL, NULL),
(57, NULL, 51, 'upload/img/products_multi/sofa-nha-hang-khach-san-tinh-te-sofa-nguyen-a-3.jpg', NULL, NULL, NULL),
(74, NULL, 52, 'upload/img/products_multi/156soffa1233.png', NULL, NULL, NULL),
(59, NULL, 39, 'upload/img/products_multi/sofagd1011.jpg', NULL, NULL, NULL),
(62, NULL, 32, 'upload/img/products_multi/1_(1).jpg', NULL, NULL, NULL),
(63, NULL, 31, 'upload/img/products_multi/sofavang1.jpg', NULL, NULL, NULL),
(64, NULL, 53, 'upload/img/products_multi/lua-chon-ban-ghe-sofa-cho-can-ho.jpg', NULL, NULL, NULL),
(65, NULL, 54, 'upload/img/products_multi/c4.jpg', NULL, NULL, NULL),
(67, NULL, 30, 'upload/img/products_multi/sofakhachsan4.jpg', NULL, NULL, NULL),
(70, NULL, 56, 'upload/img/products_multi/156sofa125.png', NULL, NULL, NULL),
(71, NULL, 57, 'upload/img/products_multi/156soffa122.png', NULL, NULL, NULL),
(72, NULL, 58, 'upload/img/products_multi/156soffa124.png', NULL, NULL, NULL),
(73, NULL, 40, 'upload/img/products_multi/156sofa12445.png', NULL, NULL, NULL),
(75, NULL, 27, 'upload/img/products_multi/166sofa311.png', NULL, NULL, NULL),
(76, NULL, 59, 'upload/img/products_multi/166sofa111.png', NULL, NULL, NULL),
(77, NULL, 60, 'upload/img/products_multi/166sofa911.png', NULL, NULL, NULL),
(78, NULL, 61, 'upload/img/products_multi/166sofa411.png', NULL, NULL, NULL),
(79, NULL, 62, 'upload/img/products_multi/166sofa2111.png', NULL, NULL, NULL),
(83, NULL, 64, 'upload/img/products_multi/sofada6.jpg', NULL, NULL, NULL),
(84, NULL, 65, 'upload/img/products_multi/sofada1.jpg', NULL, NULL, NULL),
(85, NULL, 66, 'upload/img/products_multi/sofada4.jpg', NULL, NULL, NULL),
(86, NULL, 67, 'upload/img/products_multi/sofada8.jpg', NULL, NULL, NULL),
(87, NULL, 68, 'upload/img/products_multi/sofada9.jpg', NULL, NULL, NULL),
(88, NULL, 69, 'upload/img/products_multi/sofacafe1.jpg', NULL, NULL, NULL),
(89, NULL, 70, 'upload/img/products_multi/sofacafe9.jpg', NULL, NULL, NULL),
(90, NULL, 71, 'upload/img/products_multi/sofacafe2.jpg', NULL, NULL, NULL),
(91, NULL, 72, 'upload/img/products_multi/sofacafe3.jpg', NULL, NULL, NULL),
(92, NULL, 73, 'upload/img/products_multi/sofacafe4.jpg', NULL, NULL, NULL),
(93, NULL, 75, 'upload/img/products_multi/sofacafe6.jpg', NULL, NULL, NULL),
(94, NULL, 76, 'upload/img/products_multi/sofacafe7.jpg', NULL, NULL, NULL),
(95, NULL, 77, 'upload/img/products_multi/sofacafe8.jpg', NULL, NULL, NULL),
(96, NULL, 78, 'upload/img/products_multi/sofacafe211.jpg', NULL, NULL, NULL),
(97, NULL, 79, 'upload/img/products_multi/sofakaok355.jpg', NULL, NULL, NULL),
(98, NULL, 80, 'upload/img/products_multi/sofakaoke4.jpg', NULL, NULL, NULL),
(99, NULL, 81, 'upload/img/products_multi/sofakaoke5.jpg', NULL, NULL, NULL),
(100, NULL, 83, 'upload/img/products_multi/sofakaoke77.jpg', NULL, NULL, NULL),
(101, NULL, 84, 'upload/img/products_multi/sofakaraoke3.png', NULL, NULL, NULL),
(102, NULL, 85, 'upload/img/products_multi/sofakaraoke2.jpg', NULL, NULL, NULL),
(103, NULL, 86, 'upload/img/products_multi/sofakaraoke4.jpg', NULL, NULL, NULL),
(104, NULL, 87, 'upload/img/products_multi/sofakaraoke5.jpg', NULL, NULL, NULL),
(106, NULL, 88, 'upload/img/products_multi/sofakaraoke6.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `id_sanpham` int(11) DEFAULT NULL,
  `comment` text CHARACTER SET utf8,
  `flg` int(11) DEFAULT NULL,
  `reply` int(11) DEFAULT NULL,
  `review` tinyint(1) DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `id_sanpham`, `comment`, `flg`, `reply`, `review`, `user_name`, `user_email`, `time`, `date`) VALUES
(13, 5, 'hhhhggg', NULL, 0, NULL, 'sieuwebqt', 'dangtranmanh@gmail.com', 1505724581, NULL),
(14, 5, 'hhhhggg', NULL, 0, NULL, 'sieuwebqt', 'dangtranmanh@gmail.com', 1505724675, NULL),
(15, 5, 'noi dung', NULL, 0, NULL, 'nguyen đát', 'dat@gmail.com', 1505725003, NULL),
(16, 5, 'noi dung câu hỏi', NULL, 0, NULL, 'tran manh', 'tranmanh@gmail.com', 1505725440, NULL),
(17, 5, 'noi dung cua toi', NULL, 0, NULL, 'khowebqts', 'tranmanh@gmail.com', 1505725631, NULL),
(18, 5, 'noi dung', NULL, 0, 1, 'tranmanh', 'tranmanh@gmail.com', 1505725689, NULL),
(19, 5, 'noi dung', NULL, 0, 1, 'sieuwebqt', 'tranmanh@gmail.com', 1505725843, NULL),
(20, 5, 'noi dung', NULL, 0, 1, 'sieuwebqt', 'tranmanh@gmail.com', 1505725878, NULL),
(21, 5, 'noi dung', NULL, 0, 1, 'sieuwebqt', 'tranmanh@gmail.com', 1505725928, NULL),
(22, 5, 'noi dung câu hỏi', NULL, 0, 1, 'tranmanh', 'dangtranmanh@gmail.com', 1505726276, NULL),
(23, 5, 'noi dung cau tra loi', NULL, 21, 1, 'van đạt', 'dat@gmail.com', 1505726568, NULL),
(24, 4, 'sâssa', NULL, 0, 1, 'Vân', 'buivananh.th@gmail.com', 1505981779, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `raovat`
--

CREATE TABLE `raovat` (
  `home` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `price` int(11) DEFAULT NULL,
  `price_sale` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `description_seo` text COLLATE utf8_unicode_ci,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `caption_1` text COLLATE utf8_unicode_ci,
  `caption_2` text COLLATE utf8_unicode_ci,
  `locale` int(11) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT 'vi',
  `caption_3` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(8) DEFAULT NULL,
  `tags` text COLLATE utf8_unicode_ci,
  `raovat_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multi_image` text COLLATE utf8_unicode_ci,
  `img_dir` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `tinhtrang` tinyint(1) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `id` int(11) NOT NULL,
  `style` int(11) DEFAULT NULL,
  `id_value` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `raovat`
--

INSERT INTO `raovat` (`home`, `focus`, `view`, `active`, `price`, `price_sale`, `description`, `description_seo`, `title_seo`, `keyword_seo`, `detail`, `note`, `category_id`, `caption_1`, `caption_2`, `locale`, `sort`, `quantity`, `lang`, `caption_3`, `time`, `tags`, `raovat_dir`, `multi_image`, `img_dir`, `status`, `tinhtrang`, `user_id`, `id`, `style`, `id_value`, `brand`, `name`, `code`, `alias`, `image`, `hot`) VALUES
(1, NULL, 0, 1, 0, 0, '<p>n&ocirc;i dung m&ocirc; ta</p>\r\n', '', '', '', '<p>noi dung chi tiet</p>\r\n', NULL, NULL, NULL, '<p>noi dung phu</p>\r\n', 0, 1, 0, 'vi', NULL, 1504065201, NULL, NULL, NULL, NULL, 0, NULL, 620, 1, NULL, NULL, 0, 'bán nhà tai hà nội', '', 'ban-nha-tai-ha-noi', NULL, NULL),
(1, NULL, 0, 1, 12424334, 12332342, '<p>n&ocirc;i dung m&ocirc; ta</p>\r\n', '', '', '', '<p>noi dung chi tiet</p>\r\n', NULL, 29, NULL, '<p>noi dung phu</p>\r\n', 6, 2, 0, 'vi', NULL, 1504068779, NULL, '30082017', NULL, NULL, 0, NULL, 620, 2, NULL, NULL, 14, 'bán nhà tai hà nội đường số 237', '', 'ban-nha-tai-ha-noi-duong-so-237', 'db652781fa07e94e75c9023c9de373cf.jpg', NULL),
(1, 1, 12, 1, 1234566, 1234333, '<p>n&ocirc;i dung m&ocirc; ta</p>\r\n', '', '', '', '<p>noi dung chi tiet</p>\r\n', NULL, 28, NULL, '<p>noi dung phu</p>\r\n', 5, 3, 0, 'vi', NULL, 1516353599, NULL, '30082017', NULL, NULL, 0, NULL, NULL, 3, NULL, NULL, 10, 'bán nhà tai hà nội viet nam', '', 'ban-nha-tai-ha-noi-viet-nam', '766564be313697c3bdae612b28a89d0a.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `raovat_category`
--

CREATE TABLE `raovat_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `home` tinyint(1) DEFAULT '0',
  `sort` int(3) DEFAULT '0',
  `hot` tinyint(1) DEFAULT '0',
  `focus` tinyint(1) DEFAULT '0',
  `lang` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` text CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `raovat_category`
--

INSERT INTO `raovat_category` (`id`, `name`, `image`, `alias`, `parent_id`, `description`, `home`, `sort`, `hot`, `focus`, `lang`, `title_seo`, `keyword_seo`, `description_seo`) VALUES
(20, 'Điện thoại, viễn thông ', 'upload/img/phone.png', 'dien-thoai-vien-thong', 0, '                                                                                                                                                                                                            ', 0, 1, 0, 0, 'vi', NULL, NULL, NULL),
(27, 'Ô tô, xe máy, xe đạp', 'upload/img/oto.png', 'o-to-xe-may-xe-dap', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(28, 'Xây dựng, công nghiệp', 'upload/img/connghiep.png', 'xay-dung-cong-nghiep', 0, '                                                                                                                                        ', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(29, 'Thời trang, phụ kiện', 'upload/img/thoitrang.png', 'thoi-trang-phu-kien', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(30, 'Mẹ & Bé', 'upload/img/me_be.png', 'me-be', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(31, 'Sức khỏe, sắc đẹp', 'upload/img/suckhoe.png', 'suc-khoe-sac-dep', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(33, 'Nội thất, ngoại thất', 'upload/img/noithat.png', 'noi-that-ngoai-that', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(34, 'Sách, đồ văn phòng', 'upload/img/sach.png', 'sach-do-van-phong', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(35, 'Hoa, quà tặng, đồ chơi', 'upload/img/qua_tang.png', 'hoa-qua-tang-do-choi', 0, '', 0, 0, 0, 0, 'vi', NULL, NULL, NULL),
(42, 'Khác', '', 'khac', 0, '', 0, 2, 1, 1, 'vi', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `raovat_images`
--

CREATE TABLE `raovat_images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` char(200) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `raovat_images`
--

INSERT INTO `raovat_images` (`id`, `name`, `id_item`, `image`, `url`, `link`, `sort`) VALUES
(1, NULL, 3, 'upload/img/raovats_multi/f46482c87ab814e5d5ea59819e568564.jpg', NULL, NULL, NULL),
(2, NULL, 3, 'upload/img/raovats_multi/f4b467b6d383eb5d6062b2fa9c9c0708.jpg', NULL, NULL, NULL),
(3, NULL, 3, 'upload/img/raovats_multi/e86f742e7d986de26413443600fa8535.jpg', NULL, NULL, NULL),
(4, NULL, 3, 'upload/img/raovats_multi/d640c2db815fbba330306bdbdc9e9326.jpg', NULL, NULL, NULL),
(5, NULL, 2, 'upload/img/raovats_multi/3915f302b19fa28fc4001d6a66238681.jpg', NULL, NULL, NULL),
(6, NULL, 2, 'upload/img/raovats_multi/866917b6bab0b8c3eeb0f52f45efd867.jpg', NULL, NULL, NULL),
(7, NULL, 2, 'upload/img/raovats_multi/a8f9dbaa6c627b3a47a0f442cbe0c1ab.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `raovat_tag`
--

CREATE TABLE `raovat_tag` (
  `raovat_tag_id` int(11) NOT NULL,
  `raovat_id` int(11) NOT NULL,
  `lang` varchar(11) CHARACTER SET utf8 NOT NULL,
  `tag` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `alias` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raovat_to_category`
--

CREATE TABLE `raovat_to_category` (
  `id` int(11) NOT NULL,
  `id_raovat` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `raovat_to_category`
--

INSERT INTO `raovat_to_category` (`id`, `id_raovat`, `id_category`) VALUES
(18, 3, 27),
(19, 3, 28),
(26, 2, 27),
(27, 2, 28),
(28, 2, 29);

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `resource` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `icon` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active` int(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `parent_id`, `resource`, `name`, `description`, `sort`, `icon`, `alias`, `active`) VALUES
(10, 0, 'product', 'Quản lý sản phẩm', NULL, 3, 'fa-bars', '', 1),
(11, 10, 'products', 'Danh sách sản phẩm', NULL, 1, 'fa-files-o', 'vnadmin/product/products', 1),
(13, 116, 'listColor', 'Màu sắc', NULL, 3, 'fa-circle-o', 'vnadmin/attribute/listColor', 0),
(14, 116, 'listprice', 'Khoảng giá', NULL, 4, 'fa-circle-o', 'vnadmin/attribute/listprice', 0),
(15, 116, 'listOption', 'Kích thước', NULL, 5, 'fa-circle-o', 'vnadmin/attribute/listOption', 0),
(17, 0, 'menu', 'Quản lý menu', NULL, 7, 'fa-bars', 'vnadmin/menu/menulist', 1),
(18, 10, 'category_pro', 'Danh mục sản phẩm', NULL, 2, 'fa-files-o', 'vnadmin/product/categories', 1),
(19, 173, 'comments', 'Đánh giá bình luận', NULL, 3, 'fa-comments-o', 'vnadmin/comment/comments', 0),
(20, 173, 'questions', 'Danh sách hỏi đáp', NULL, 4, 'fa-question-circle', 'vnadmin/comment/questions', 0),
(22, 174, 'tag', 'Thẻ tags sản phẩm', NULL, 5, 'fa-tag', 'vnadmin/tag/listtagpro', 0),
(25, 0, 'news', 'Quản lý tin bài', NULL, 6, 'fa-newspaper-o', '', 1),
(26, 25, 'newslist', 'Danh sách tin', NULL, 1, 'fa-file-text-o', 'vnadmin/news/newslist', 1),
(28, 25, 'tagsnews', 'Tags tin tức', NULL, 3, 'fa fa-tag', '', 0),
(29, 0, 'media', 'Quản lý media', NULL, 1, 'fa-picture-o', '', 0),
(30, 29, 'listAll', 'Danh sách media', NULL, 1, 'fa-file-image-o', 'vnadmin/media/listAll', 1),
(31, 25, 'categories', 'Danh mục tin', NULL, 2, 'fa-newspaper-o', 'vnadmin/news/categories', 1),
(32, 29, 'categories', 'Danh mục media', NULL, 2, 'fa-file-image-o', 'vnadmin/media/categories', 1),
(33, 0, 'users', 'Quản lý thành viên', NULL, 16, 'fa-users', '', 1),
(34, 33, 'smslist', 'Tin Nhắn SMS', NULL, 6, 'fa-commenting-o', 'vnadmin/users/smslist', 1),
(39, 0, 'pages', 'Quản lý nội dung', NULL, 9, 'fa-file-o', 'vnadmin/pages/pagelist', 1),
(40, 0, 'video', 'Quản Lý Video', NULL, 2, 'fa-video-camera', '', 1),
(42, 40, 'listAll', 'Danh sách video', NULL, 1, 'fa-file-video-o', 'vnadmin/video/listAll', 1),
(43, 40, 'category_video', 'Danh mục video', NULL, 2, 'fa-video-camera', 'vnadmin/video/categories', 1),
(44, 107, 'listraovat', 'Danh sách rao vặt', NULL, 1, 'fa-files-o', 'vnadmin/raovat/listraovat', 1),
(49, 107, 'categories', 'Danh mục rao vặt', NULL, 2, 'fa-files-o', 'vnadmin/raovat/categories', 1),
(53, 0, 'imageupload', 'Quản lý banner', NULL, 8, 'fa-file-image-o', 'vnadmin/imageupload/banners', 1),
(54, 162, 'listWard', 'Quản lý phường xã', NULL, 3, 'fa-map-signs', 'vnadmin/province/listWard', 1),
(56, 162, 'listDistric', 'Quản lý quận huyện', NULL, 2, 'fa-map-marker', 'vnadmin/province/listDistric', 1),
(57, 162, 'street', 'Quản lý đường phố', NULL, 4, 'fa-road', 'vnadmin/province/listStreet', 1),
(58, 97, 'soldout', 'danh sách hết hàng', NULL, 1, 'fa-circle-o', 'admin/report/soldout', 0),
(63, 10, 'cat_add', 'Thêm - Sửa danh mục sp', NULL, 9, '', '', 0),
(64, 95, 'maps', 'Cấu hình bản đồ Maps', NULL, 1, ' fa-map-o', 'vnadmin/admin/bando_map', 1),
(65, 10, 'add', 'Thêm -Sửa sản phẩm', NULL, 7, '', '', 0),
(66, 17, 'delete', 'Xóa menu', NULL, 2, '', '', 0),
(67, 10, 'delete_once', 'Xóa sản phẩm', NULL, 8, '', '', 0),
(95, 0, 'admin', 'Hệ thống', NULL, 17, 'fa-gears text-red', '', 1),
(96, 95, 'site_option', 'Cấu hình hệ thống', NULL, 0, 'fa-circle-o text-red', 'vnadmin/admin/site_option', 1),
(97, 0, 'report', 'Báo cáo-Thống kê', NULL, 19, '', '', 0),
(98, 104, 'listProvince', 'Phí vận chuyển', NULL, 3, 'fa-truck', 'vnadmin/order/listProvince', 0),
(99, 90, 'categories', 'Danh mục share', NULL, 0, NULL, NULL, NULL),
(100, 90, 'cat_add', 'Thêm danh mục share', NULL, 0, NULL, NULL, NULL),
(101, 90, 'cat_edit', 'Sửa danh mục share', NULL, 0, NULL, NULL, NULL),
(102, 90, 'delete_cat', 'Xóa danh mục share', NULL, 0, NULL, NULL, NULL),
(103, 97, 'bestsellers', 'Hàng bán chạy', NULL, 2, 'fa-circle-o', 'admin/report/bestsellers', 0),
(104, 0, 'order', 'Quản lý giỏ hàng', NULL, 4, 'fa-shopping-cart', '', 0),
(105, 104, 'orders', 'Danh sách đặt hàng', NULL, 1, 'fa-cart-arrow-down', 'vnadmin/order/orders', 1),
(106, 104, 'listSale', 'Mã giảm giá', NULL, 2, 'fa-files-o', 'vnadmin/order/listSale', 0),
(107, 0, 'raovat', 'Quản lý rao vặt', NULL, 13, 'fa-bars', '', 1),
(108, 0, 'inuser', 'Ý kiến khách hàng', NULL, 0, 'fa-user-plus', 'vnadmin/inuser/categories', 1),
(109, 107, 'tagtinrao', 'Tags tin rao', NULL, 3, 'fa-tag', '', 0),
(110, 0, 'email', 'Quản lý email', NULL, 14, ' fa-envelope-o ', 'vnadmin/email/emails', 0),
(111, 0, 'support', 'Hỗ trợ  trực tuyến', NULL, 15, 'fa-life-ring', 'vnadmin/support/listSuport', 1),
(112, 95, 'store_shopping', 'Chuỗi cửa hàng', NULL, 5, 'fa-files-o', 'vnadmin/store/Ds_shopping', 0),
(113, 116, 'listBrand', 'Thương hiệu', NULL, 1, 'fa-circle-o', 'vnadmin/attribute/listBrand', 0),
(114, 116, 'listLocale', 'Xuất sứ', NULL, 2, 'fa-circle-o', 'vnadmin/attribute/listLocale', 0),
(115, 0, 'contact', 'Quản lý liên hệ', NULL, 10, 'fa-bars', 'vnadmin/contact/contacts', 1),
(116, 0, 'attribute', 'Thuộc tính sản phẩm', NULL, 5, 'fa-bars', '', 0),
(117, 108, 'cate_add', 'Thêm và Sửa', NULL, 2, '', '', 1),
(118, 108, 'delete_cat_once', 'Xóa', NULL, 3, '', '', 0),
(119, 108, 'categories', 'ý kiến khách hàng', NULL, 1, 'fa-files-o', 'vnadmin/inuser/categories', 1),
(120, 17, 'addmenu', 'Thêm - Sửa menu', NULL, 0, '', '', 0),
(121, 10, 'del_cat_once', 'Xóa danh mục sp', NULL, 10, '', '', 0),
(122, 29, 'add', 'Thêm -Sửa media', NULL, 3, '', '', 0),
(123, 29, 'delete_once', 'Xóa media', NULL, 4, '', '', 0),
(124, 29, 'cat_add', 'Thêm - Sửa danh mục media', NULL, 5, '', '', 0),
(125, 29, 'del_cat_once', 'Xóa danh mục media', NULL, 6, '', '', 0),
(126, 40, 'add', 'Thêm sửa video', NULL, 3, '', '', 0),
(127, 40, 'delete_once', 'Xóa video', NULL, 4, '', '', 0),
(128, 40, 'cat_add', 'Thêm danh mục video', NULL, 5, '', '', 0),
(129, 40, 'del_cat_once', 'Xóa danh mục video', NULL, 6, '', '', 0),
(130, 10, 'delete_once_question', 'Xóa hỏi đáp', NULL, 12, '', '', 0),
(131, 10, 'delete_once_comment', 'Xóa bình luận', NULL, 11, '', '', 0),
(132, 104, 'delete_once_orders', 'Xóa đơn hàng', NULL, 4, '', '', 0),
(133, 104, 'addSale', 'Thêm - Sửa mã giảm giá', NULL, 5, '', '', 0),
(134, 104, 'del_once_sale', 'Xóa mã giảm giá', NULL, 6, '', '', 0),
(135, 116, 'addbrand', 'Thêm - Sửa thương hiệu', NULL, 6, '', '', 0),
(136, 116, 'delete_brand_once', 'Xóa thương hiệu', NULL, 7, '', '', 0),
(137, 116, 'addlocale', 'Thêm - Sửa xuất sứ', NULL, 7, '', '', 0),
(138, 116, 'delete_locale_once', 'Xóa xuất sứ', NULL, 8, '', '', 0),
(139, 116, 'addcolor', 'Thêm - Sửa màu sắc', NULL, 9, '', '', 0),
(140, 116, 'delete_color_once', 'Xóa màu sắc', NULL, 10, '', '', 0),
(141, 116, 'addprice', 'Thêm - Sửa khoản giá', NULL, 11, '', '', 0),
(142, 116, 'delete_price_once', 'Xóa khoảng giá', NULL, 12, '', '', 0),
(143, 116, 'addoption', 'Thêm - Sửa kích thước', NULL, 12, '', '', 0),
(144, 116, 'delete_option_once', 'Xóa kích thước', NULL, 13, '', '', 0),
(145, 25, 'addnews', 'Thêm - Sửa tin tức', NULL, 4, '', '', 0),
(146, 25, 'delete_once_news', 'Xóa tin tức', NULL, 5, '', '', 0),
(147, 25, 'cat_add_news', 'Thêm - Sửa danh mục tin', NULL, 6, '', '', 0),
(148, 25, 'del_catnews_once', 'Xóa danh mục tin', NULL, 7, '', '', 0),
(149, 53, 'addbanner', 'Thêm - Sửa banner', NULL, 1, '', '', 0),
(150, 53, 'delete_Banner_once', 'Xóa banner', NULL, 2, '', '', 0),
(151, 39, 'addpage', 'Thêm - Sửa nội dung', NULL, 1, '', '', 0),
(152, 39, 'delete_page_once', 'Xóa nội dung', NULL, 2, '', '', 0),
(153, 115, 'delete', 'Xóa liên hệ', NULL, 1, '', '', 0),
(154, 107, 'add', 'Thêm - Sửa rao vặt', NULL, 4, '', '', 0),
(155, 107, 'delete_raovat_once', 'Xóa tin rao', NULL, 5, '', '', 0),
(156, 107, 'cat_add', 'Thêm - Sửa danh mục tin rao', NULL, 6, '', '', 0),
(157, 107, 'del_cattinrao_once', 'Xóa danh mục tin rao', NULL, 7, '', '', 0),
(158, 110, 'delete', 'Xóa email', NULL, 1, '', '', 0),
(159, 111, 'add', 'Thêm - Sửa hỗ trợ trực tuyến', NULL, 1, '', '', 0),
(160, 111, 'delete_support_once', 'Xóa hỗ trợ trực tuyến', NULL, 2, '', '', 0),
(161, 33, 'delete_users_once', 'Xóa thành viên', NULL, 1, '', '', 1),
(162, 0, 'province', 'Danh sách quan huyện', NULL, 18, '', '', 1),
(163, 33, 'add_users', 'Thêm thành viên quan trị', NULL, 1, '', 'vnadmin/users/add_users', 1),
(164, 33, 'delete_users_once', 'Xóa thành viên quản trị', NULL, 10, '', '', 1),
(165, 33, 'listuser_admin', 'Danh sách tài khoản quản trị', NULL, 0, '', 'vnadmin/users/listuser_admin', 1),
(166, 33, 'listusers', 'Danh sách thành viên', NULL, 0, '', 'vnadmin/users/listusers', 1),
(167, 17, 'menulist', 'Danh sách menu', NULL, 1, '', 'vnadmin/menu/menulist', 0),
(168, 53, 'banners', 'Danh sách banner', NULL, 0, '', 'vnadmin/imageupload/banners', 0),
(169, 39, 'pagelist', 'Danh sách nội dung', NULL, 0, '', 'vnadmin/pages/pagelist', 0),
(170, 110, 'emails', 'Danh sách email', NULL, 0, '', 'vnadmin/email/emails', 0),
(171, 115, 'contacts', 'Danh sách liên hệ', NULL, 0, '', 'vnadmin/contact/contacts', 0),
(172, 111, 'listSuport', 'Danh sách support', NULL, 0, '', 'vnadmin/support/listSuport', 1),
(173, 0, 'comment', 'Quản lý bình luận', NULL, 7, 'fa-comments-o', '', 0),
(174, 0, 'tag', 'Quản lý thẻ tag', NULL, 6, 'fa-tags', '', 0),
(175, 174, 'tag', 'Thẻ tags tin tức', NULL, 2, 'fa-tag', 'vnadmin/tag/listnew', 0),
(177, 95, 'setup_product', ' Cấu hình sản phẩm', NULL, 20, 'fa-gears', 'vnadmin/admin/setup_product', 1),
(178, 107, 'raovat_bds', 'Danh mục rao vặt BĐS', NULL, 1, 'fa-user-plus', 'vnadmin/raovat_bds/cat_raovat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_log`
--

CREATE TABLE `site_log` (
  `site_log_id` int(10) UNSIGNED NOT NULL,
  `no_of_visits` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `requested_url` tinytext CHARACTER SET utf8,
  `referer_page` tinytext CHARACTER SET utf8,
  `page_name` tinytext CHARACTER SET utf8,
  `query_string` tinytext CHARACTER SET utf8,
  `user_agent` tinytext CHARACTER SET utf8,
  `is_unique` tinyint(1) DEFAULT '0',
  `access_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `visits_count` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_log`
--

INSERT INTO `site_log` (`site_log_id`, `no_of_visits`, `ip_address`, `requested_url`, `referer_page`, `page_name`, `query_string`, `user_agent`, `is_unique`, `access_date`, `visits_count`) VALUES
(63378, NULL, '42.112.239.147', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 0, '2019-10-03 15:22:05', 1),
(63377, NULL, '64.202.188.205', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-03 14:32:08', 1),
(63376, NULL, '64.202.188.205', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-03 14:32:07', 1),
(63374, NULL, '149.202.161.210', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-03 05:31:38', 1),
(63375, NULL, '69.171.251.30', '/?fbclid=IwAR0yKD1a49iA1Dxpv-l9Z5cWkbZiJuMbck7tYhw6tjxENyK02j1qSjH7PbY', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-10-03 10:07:49', 1),
(63373, NULL, '149.202.161.210', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-03 05:31:36', 1),
(63372, NULL, '79.191.198.98', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 0, '2019-10-03 05:15:42', 1),
(63370, NULL, '89.108.87.179', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-02 22:53:13', 1),
(63371, NULL, '89.108.87.179', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-02 22:53:17', 1),
(63369, NULL, '162.144.34.147', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-02 13:51:37', 1),
(63368, NULL, '162.144.34.147', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-02 13:51:32', 1),
(63367, NULL, '111.230.97.36', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-10-02 12:33:24', 1),
(63366, NULL, '173.252.127.19', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-10-02 11:55:52', 1),
(63365, NULL, '173.252.127.9', '/san-pham/sofa-karaoke-ma-sk819.html', '', 'products/detail', 'raoke-ma-sk819', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-10-02 11:52:58', 1),
(63363, NULL, '72.167.190.2', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-02 06:09:58', 1),
(63364, NULL, '173.252.127.37', '/san-pham/sofa-karaoke-ma-sk819.html', '', 'products/detail', 'raoke-ma-sk819', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-10-02 11:52:56', 1),
(63361, NULL, '52.37.155.154', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-10-02 04:16:28', 1),
(63362, NULL, '72.167.190.2', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-02 06:09:56', 1),
(63360, NULL, '178.62.82.141', '/san-pham/sofa-gia-dinh-ma-gd215.html', '', 'products/detail', 'a-dinh-ma-gd215', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-10-02 03:27:26', 1),
(63359, NULL, '178.62.82.141', '/danh-muc/sofa-gia-dinh.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-10-02 03:27:25', 1),
(63358, NULL, '178.62.82.141', '/san-pham/sofa-gia-dinh-ma-gd111.html', '', 'products/detail', 'a-dinh-ma-gd111', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-10-02 03:27:24', 1),
(63357, NULL, '178.62.82.141', '/san-pham/sofa-gia-dinh-ma-gd109.html', '', 'products/detail', 'a-dinh-ma-gd109', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-10-02 03:27:23', 1),
(63356, NULL, '178.62.82.141', '/san-pham/sofa-gia-dinh-ma-gd106.html', '', 'products/detail', 'a-dinh-ma-gd106', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-10-02 03:27:22', 1),
(63354, NULL, '178.62.82.141', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-10-02 03:27:18', 1),
(63355, NULL, '178.62.82.141', '/san-pham/sofa-gia-dinh-ma-gd105.html', '', 'products/detail', 'a-dinh-ma-gd105', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-10-02 03:27:20', 1),
(63353, NULL, '148.72.232.141', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-02 02:50:50', 1),
(63351, NULL, '185.65.137.208', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-01 16:23:47', 1),
(63352, NULL, '148.72.232.141', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-02 02:50:47', 1),
(63350, NULL, '185.65.137.208', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-01 16:23:45', 1),
(63349, NULL, '173.252.95.46', '/?fbclid=IwAR00Dkq2s23M17ooEJsAmxuYqF4-LHwq1_8f8aYKzI4gLMQFfFEAZz28SHs', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-10-01 11:30:02', 1),
(63348, NULL, '94.21.92.42', '/', 'https://brandnewblogs.com/?domain=sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', 0, '2019-10-01 11:05:43', 1),
(63347, NULL, '89.46.106.107', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-01 06:55:40', 1),
(63345, 1, '188.247.75.135', '/wp-login.php/wp-login.php', '', 'wp-login.php/wp-login.php', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0', 0, '2019-10-01 05:37:02', 0),
(63346, NULL, '89.46.106.107', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-10-01 06:55:34', 1),
(63344, NULL, '188.247.75.135', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0', 0, '2019-10-01 05:37:00', 1),
(63343, NULL, '198.187.29.24', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-30 23:08:44', 1),
(63341, NULL, '181.66.52.183', '/wp-login.php/wp-login.php', '', 'wp-login.php/wp-login.php', '', 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0', 0, '2019-09-30 17:39:26', 1),
(63342, NULL, '198.187.29.24', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-30 23:08:41', 1),
(63340, NULL, '173.252.95.30', '/?fbclid=IwAR0ySsVot7VQ5QIKKLTsQZ_DOP2qadfWd38tpifywKxbivSve-V0LXGM6xg', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-09-30 14:36:59', 1),
(63339, NULL, '68.66.224.30', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-30 13:42:28', 1),
(63338, NULL, '68.66.224.30', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-30 13:42:26', 1),
(63336, NULL, '95.179.154.158', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 0, '2019-09-30 11:55:48', 1),
(63337, NULL, '62.210.89.11', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', 0, '2019-09-30 12:08:39', 1),
(63334, NULL, '148.72.232.63', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-30 01:27:27', 1),
(63335, NULL, '34.208.240.167', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-30 06:28:40', 1),
(63333, NULL, '148.72.232.63', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-30 01:27:25', 1),
(63332, NULL, '150.109.204.109', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-29 17:28:27', 1),
(63331, NULL, '150.109.204.109', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-29 17:28:24', 1),
(63330, NULL, '66.220.149.31', '/?fbclid=IwAR3jNBMPjL1fe_86y5S1HxNEwtxpRnO0kZtdFWkki-7sV0zTLetNVgBaZew', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-09-29 11:53:29', 1),
(63328, NULL, '166.62.117.176', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-29 07:55:59', 1),
(63329, NULL, '166.62.117.176', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-29 07:56:02', 1),
(63327, NULL, '89.42.217.11', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-28 23:07:34', 1),
(63325, NULL, '93.159.230.28', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 0, '2019-09-28 14:20:03', 1),
(63326, NULL, '89.42.217.11', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-28 23:07:31', 1),
(63324, NULL, '182.50.135.85', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-28 13:59:59', 1),
(63323, NULL, '182.50.135.85', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-28 13:59:57', 1),
(63322, NULL, '69.171.251.52', '/?fbclid=IwAR3pwdfib1NsCWrpRAdZ0i74vjtc8DX3yN7ffLM1isOkAC-yjm5X9ydMzBs', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 0, '2019-09-28 08:12:26', 1),
(63320, NULL, '187.17.106.62', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-28 04:14:34', 1),
(63321, NULL, '187.17.106.62', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-28 04:14:47', 1),
(63319, 1, '84.17.62.153', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 0, '2019-09-28 01:31:35', 0),
(63318, NULL, '84.17.62.153', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 0, '2019-09-28 01:31:33', 1),
(63317, NULL, '167.99.168.27', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-27 18:58:14', 1),
(63316, NULL, '149.210.155.74', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-27 11:32:26', 1),
(63314, 1, '85.91.211.124', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 08:21:37', 0),
(63315, NULL, '149.210.155.74', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-27 11:32:12', 1),
(63313, NULL, '85.91.211.124', '/admin', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 08:21:37', 1),
(63311, 1, '85.91.211.124', '/wp-admin/', 'http://sofabaolam.com/wp-login.php', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 08:18:36', 0),
(63312, 1, '85.91.211.124', '/404_override', 'http://sofabaolam.com/wp-login.php', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 08:18:37', 0),
(63310, NULL, '85.91.211.124', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 08:18:36', 1),
(63308, NULL, '85.91.211.124', '/administrator/', '', 'home/index', 'or', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 08:11:44', 1),
(63309, 1, '85.91.211.124', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 08:11:44', 0),
(63307, NULL, '173.252.79.21', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-27 08:03:43', 1),
(63306, 1, '195.98.71.193', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:53:39', 0),
(63304, 1, '195.98.71.193', '/404_override', 'http://sofabaolam.com/wp-login.php', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:50:41', 0),
(63305, NULL, '195.98.71.193', '/admin', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:53:39', 1),
(63303, 1, '195.98.71.193', '/wp-admin/', 'http://sofabaolam.com/wp-login.php', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:50:41', 0),
(63301, 1, '195.98.71.193', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:42:18', 0),
(63302, NULL, '195.98.71.193', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:50:40', 1),
(63299, 1, '188.163.81.233', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:28:37', 0),
(63300, NULL, '195.98.71.193', '/administrator/', '', 'home/index', 'or', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:42:18', 1),
(63298, NULL, '188.163.81.233', '/admin', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:28:37', 1),
(63297, 1, '188.163.81.233', '/404_override', 'http://sofabaolam.com/wp-login.php', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:26:24', 0),
(63295, NULL, '188.163.81.233', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:26:23', 1),
(63296, 1, '188.163.81.233', '/wp-admin/', 'http://sofabaolam.com/wp-login.php', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:26:24', 0),
(63294, 1, '188.163.81.233', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:21:16', 0),
(63292, 1, '94.231.116.22', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:01:29', 0),
(63293, NULL, '188.163.81.233', '/administrator/', '', 'home/index', 'or', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:21:16', 1),
(63290, 1, '94.231.116.22', '/404_override', 'http://sofabaolam.com/wp-login.php', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:58:16', 0),
(63291, NULL, '94.231.116.22', '/admin', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 07:01:29', 1),
(63289, 1, '94.231.116.22', '/wp-admin/', 'http://sofabaolam.com/wp-login.php', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:58:16', 0),
(63288, NULL, '94.231.116.22', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:58:15', 1),
(63286, NULL, '94.231.116.22', '/administrator/', '', 'home/index', 'or', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:49:23', 1),
(63287, 1, '94.231.116.22', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:49:23', 0),
(63285, 1, '188.162.166.13', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:42:56', 0),
(63284, NULL, '188.162.166.13', '/admin', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:42:55', 1),
(63283, 1, '188.162.166.13', '/404_override', 'http://sofabaolam.com/wp-login.php', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:40:41', 0),
(63281, NULL, '188.162.166.13', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:40:40', 1),
(63282, 1, '188.162.166.13', '/wp-admin/', 'http://sofabaolam.com/wp-login.php', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:40:41', 0),
(63280, 1, '188.162.166.13', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:36:17', 0),
(63279, NULL, '188.162.166.13', '/administrator/', '', 'home/index', 'or', 'Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0', 0, '2019-09-27 06:36:17', 1),
(63278, NULL, '107.178.200.217', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7', 0, '2019-09-27 04:49:32', 1),
(63277, NULL, '158.69.241.225', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', 0, '2019-09-26 19:38:44', 1),
(63275, NULL, '37.120.151.62', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36', 0, '2019-09-26 15:06:16', 1),
(63276, 1, '37.120.151.62', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36', 0, '2019-09-26 15:06:17', 0),
(63274, NULL, '47.88.90.204', '/vendor/phpunit/phpunit/src/Util/PHP/kill.php', '', 'vendor/phpunit', 'phpunit/src/Util/PHP/kill.php', NULL, 0, '2019-09-26 14:37:15', 1),
(63273, NULL, '47.88.90.204', '/sites/all/libraries/mailchimp/vendor/phpunit/phpunit/src/Util/PHP/kill.php', '', 'sites/all', 'libraries/mailchimp/vendor/phpunit/phpunit/src/Util/PHP/kill.php', NULL, 0, '2019-09-26 14:33:25', 1),
(63271, NULL, '79.191.125.26', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 0, '2019-09-26 13:10:45', 1),
(63272, NULL, '162.144.60.165', '/website/wp-login.php', '', 'website/wp-login.php', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-26 14:25:25', 1),
(63270, NULL, '212.56.221.195', '/cms/wp-login.php', '', 'cms/wp-login.php', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-26 12:45:06', 1),
(63269, NULL, '18.237.61.93', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 0, '2019-09-26 11:54:02', 1),
(63268, NULL, '151.1.254.220', '/web/wp-login.php', '', 'web/wp-login.php', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-26 03:25:33', 1),
(63266, NULL, '35.231.148.103', '/site/wp-login.php', '', 'site/wp-login.php', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-25 23:19:03', 1),
(63267, NULL, '35.240.23.37', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 0, '2019-09-26 02:36:44', 1),
(63265, NULL, '45.118.145.84', '/backup/wp-login.php', '', 'backup/wp-login.php', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-25 19:03:15', 1),
(63263, NULL, '185.67.45.160', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 18:55:52', 1),
(63264, NULL, '185.67.45.160', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 18:55:54', 1),
(63262, NULL, '148.251.151.248', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208', 0, '2019-09-25 18:53:43', 1),
(63261, NULL, '18.237.201.129', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 0, '2019-09-25 18:51:31', 1),
(63260, NULL, '171.232.181.86', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24.', 0, '2019-09-25 18:26:42', 1),
(63258, NULL, '51.15.129.45', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 16:47:24', 1),
(63259, NULL, '51.15.129.45', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 16:47:39', 1),
(63257, NULL, '100.16.255.31', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 16:46:27', 1),
(63255, NULL, '221.133.7.121', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 14:32:50', 1),
(63256, NULL, '100.16.255.31', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 16:46:22', 1),
(63254, NULL, '221.133.7.121', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 14:32:48', 1),
(63253, NULL, '192.162.102.230', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36', 0, '2019-09-25 10:10:38', 1),
(63251, NULL, '69.162.107.34', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 09:00:39', 1),
(63252, NULL, '69.162.107.34', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 09:00:40', 1),
(63250, NULL, '104.41.5.236', '//', '', 'home/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-25 06:25:40', 1),
(63248, NULL, '89.46.106.107', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 06:20:30', 1),
(63249, NULL, '89.46.106.107', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 06:20:31', 1),
(63247, NULL, '51.15.78.133', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0', 0, '2019-09-25 05:49:55', 1),
(63246, NULL, '91.134.248.235', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 03:44:35', 1),
(63244, NULL, '139.59.92.2', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-25 01:05:34', 1),
(63245, NULL, '91.134.248.235', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 03:44:14', 1),
(63243, NULL, '3.112.18.136', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 01:01:31', 1),
(63241, NULL, '173.230.252.250', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-24 22:08:05', 1),
(63242, NULL, '3.112.18.136', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-25 01:01:27', 1),
(63240, NULL, '173.230.252.250', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-24 22:08:03', 1),
(63239, NULL, '132.148.42.109', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-24 19:07:35', 1),
(63237, NULL, '96.125.164.243', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-24 14:56:57', 1),
(63238, NULL, '132.148.42.109', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-24 19:07:32', 1),
(63236, NULL, '96.125.164.243', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-24 14:56:54', 1),
(63235, NULL, '5.188.62.5', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36', 0, '2019-09-24 14:54:07', 1),
(63233, NULL, '34.212.133.61', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-24 11:08:56', 1),
(63234, NULL, '187.143.83.178', '/wp-login.php/wp-login.php', '', 'wp-login.php/wp-login.php', '', 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0', 0, '2019-09-24 13:37:20', 1),
(63232, NULL, '51.77.230.131', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-24 03:54:47', 1),
(63230, NULL, '171.244.142.202', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-23 23:26:36', 1),
(63231, NULL, '51.77.230.131', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-24 03:54:44', 1),
(63229, NULL, '171.244.142.202', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-23 23:26:35', 1),
(63228, NULL, '45.119.81.158', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-23 18:42:32', 1),
(63226, NULL, '173.211.77.148', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 0, '2019-09-23 18:37:21', 1),
(63227, NULL, '45.119.81.158', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-23 18:42:31', 1),
(63225, NULL, '185.2.4.144', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-23 04:02:44', 1),
(63224, NULL, '185.2.4.144', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-23 04:02:42', 1),
(63222, NULL, '5.101.156.145', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-23 00:03:28', 1),
(63223, NULL, '5.101.156.145', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-23 00:03:30', 1),
(63221, NULL, '72.167.190.2', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-22 19:53:48', 1),
(63220, NULL, '72.167.190.2', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-22 19:53:45', 1),
(63218, NULL, '34.223.6.13', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-22 15:13:47', 1),
(63219, NULL, '37.187.132.107', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-22 17:41:42', 1),
(63217, NULL, '66.220.149.45', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-22 15:02:40', 1),
(63215, NULL, '132.148.42.109', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-22 12:51:09', 1),
(63216, NULL, '132.148.42.109', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-22 12:51:11', 1),
(63214, NULL, '92.222.167.201', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-22 08:12:26', 1),
(63212, NULL, '148.66.145.135', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-22 03:04:15', 1),
(63213, NULL, '92.222.167.201', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-22 08:12:24', 1),
(63211, NULL, '148.66.145.135', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-22 03:04:13', 1),
(63210, 1, '92.119.160.72', '/sito/wp-includes/wlwmanifest.xml', '', 'sito/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:46', 0),
(63209, 1, '92.119.160.72', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:45', 0),
(63208, 1, '92.119.160.72', '/site/wp-includes/wlwmanifest.xml', '', 'site/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:44', 0),
(63207, 1, '92.119.160.72', '/wp2/wp-includes/wlwmanifest.xml', '', 'wp2/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:44', 0),
(63206, 1, '92.119.160.72', '/media/wp-includes/wlwmanifest.xml', '', 'media/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:43', 0),
(63205, 1, '92.119.160.72', '/test/wp-includes/wlwmanifest.xml', '', 'test/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:42', 0),
(63204, 1, '92.119.160.72', '/wp1/wp-includes/wlwmanifest.xml', '', 'wp1/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:41', 0),
(63203, 1, '92.119.160.72', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:41', 0),
(63202, 1, '92.119.160.72', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:40', 0),
(63201, 1, '92.119.160.72', '/2017/wp-includes/wlwmanifest.xml', '', '2017/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:32:39', 0),
(63200, 1, '92.119.160.72', '/news/wp-includes/wlwmanifest.xml', '', 'news/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:30:37', 0),
(63199, 1, '92.119.160.72', '/wordpress/wp-includes/wlwmanifest.xml', '', 'wordpress/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:29:55', 0),
(63197, 1, '92.119.160.72', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:29:22', 0),
(63198, 1, '92.119.160.72', '/web/wp-includes/wlwmanifest.xml', '', 'web/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:29:33', 0),
(63196, 1, '92.119.160.72', '/xmlrpc.php?rsd', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:29:21', 0),
(63195, 1, '92.119.160.72', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:29:20', 0),
(63194, NULL, '92.119.160.72', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 0, '2019-09-21 23:29:12', 1),
(63193, NULL, '82.202.161.133', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 0, '2019-09-21 22:15:23', 1),
(63191, NULL, '72.167.190.155', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-21 20:46:35', 1),
(63192, NULL, '72.167.190.155', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-21 20:46:51', 1),
(63190, NULL, '52.12.77.57', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-21 12:14:10', 1),
(63188, NULL, '198.37.116.25', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24.', 0, '2019-09-21 02:43:11', 1),
(63189, NULL, '173.252.79.17', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-21 04:42:11', 1),
(63187, NULL, '51.105.124.35', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', 0, '2019-09-20 17:19:24', 1),
(63185, NULL, '163.172.76.38', '/fuel/pages/select/', '', 'fuel/pages', 'select', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36', 0, '2019-09-20 07:32:44', 1),
(63186, NULL, '34.222.184.234', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-20 14:35:34', 1),
(63184, NULL, '163.172.76.38', '/fuel/modules/fuel/assets/css/fuel.min.css', '', 'fuel/modules', 'fuel/assets/css/fuel.min.css', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36', 0, '2019-09-20 07:32:43', 1),
(63183, NULL, '163.172.76.38', '/README.md', '', 'README.md/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36', 0, '2019-09-20 07:32:37', 1),
(63181, NULL, '54.202.166.173', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-19 10:28:09', 1),
(63182, NULL, '173.252.127.38', '/?fbclid=IwAR0HndRGoGwuzcfgd2-1CRRtQWm04FuqFYBqZBI8h_CfH0OzcTu92SeDMiU', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-09-19 21:19:46', 1),
(63180, NULL, '173.252.87.20', '/san-pham/sofa-karaoke-ma-sk821.html', '', 'products/detail', 'raoke-ma-sk821', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-19 08:41:43', 1),
(63179, 1, '14.248.85.156', '/danh-muc/sofa-gia-re.html', 'http://sofabaolam.com/danh-muc/sofa-da.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36', 0, '2019-09-19 07:37:51', 0),
(63178, 1, '14.248.85.156', '/danh-muc/sofa-da.html', 'http://sofabaolam.com/danh-muc/sofa-cao-cap.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36', 0, '2019-09-19 07:37:50', 0),
(63177, 1, '14.248.85.156', '/danh-muc/sofa-cao-cap.html', 'http://sofabaolam.com/danh-muc/sofa-khach-san.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36', 0, '2019-09-19 07:37:49', 0),
(63176, 1, '14.248.85.156', '/danh-muc/sofa-khach-san.html', 'http://sofabaolam.com/danh-muc/sofa-cafe.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36', 0, '2019-09-19 07:37:47', 0),
(63175, 1, '14.248.85.156', '/danh-muc/sofa-cafe.html', 'http://sofabaolam.com/danh-muc/sofa-gia-dinh.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36', 0, '2019-09-19 07:37:44', 0),
(63174, 1, '14.248.85.156', '/danh-muc/sofa-gia-dinh.html', 'http://sofabaolam.com/danh-muc/thanh-ly-sofa-karaoke.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36', 0, '2019-09-19 07:37:43', 0),
(63173, 1, '14.248.85.156', '/danh-muc/thanh-ly-sofa-karaoke.html', 'http://sofabaolam.com/', 'products/pro_bycategory', 'araoke', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36', 0, '2019-09-19 07:37:39', 0),
(63171, NULL, '14.231.158.27', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/80.0.182 Chrome/74.0.3729.182 Safari/537.36', 0, '2019-09-19 01:22:28', 1),
(63172, NULL, '14.248.85.156', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36', 0, '2019-09-19 07:37:35', 1),
(63169, NULL, '34.220.116.26', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-18 14:16:31', 1),
(63170, NULL, '181.177.108.137', '/contact', '', 'contact/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15', 0, '2019-09-18 14:42:09', 1),
(63168, NULL, '69.171.251.47', '/san-pham/sofa-karaoke-ma-sk816.html', '', 'products/detail', 'raoke-ma-sk816', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-18 14:02:36', 1),
(63167, NULL, '173.252.127.11', '/san-pham/sofa-gia-dinh-ma-gd105.html', '', 'products/detail', 'a-dinh-ma-gd105', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-18 10:21:55', 1),
(63165, 1, '212.103.50.78', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 0, '2019-09-18 07:04:47', 0),
(63166, NULL, '173.252.127.20', '/san-pham/sofa-gia-dinh-ma-gd105.html', '', 'products/detail', 'a-dinh-ma-gd105', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-18 10:21:54', 1),
(63164, NULL, '212.103.50.78', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 0, '2019-09-18 07:04:46', 1),
(63163, NULL, '173.252.127.52', '/san-pham/sofa-ni-gia-dinh-ma-gd102.html', '', 'products/detail', '-gia-dinh-ma-gd102', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-18 02:28:14', 1),
(63162, NULL, '173.252.127.50', '/san-pham/sofa-ni-gia-dinh-ma-gd102.html', '', 'products/detail', '-gia-dinh-ma-gd102', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-18 02:28:13', 1),
(63161, NULL, '107.178.232.226', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/69.0.3494.0 Safari/537.36', 0, '2019-09-17 22:50:58', 1),
(63160, NULL, '173.252.95.43', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'cortex/1.0', 0, '2019-09-17 17:57:28', 1),
(63159, NULL, '173.252.95.8', '/san-pham/sofa-karaoke-ma-sk814.html', '', 'products/detail', 'raoke-ma-sk814', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-17 17:57:26', 1),
(63158, NULL, '173.252.95.22', '/san-pham/sofa-karaoke-ma-sk814.html', '', 'products/detail', 'raoke-ma-sk814', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-17 17:57:25', 1),
(63157, NULL, '173.252.95.8', '/san-pham/sofa-karaoke-ma-sk818.html', '', 'products/detail', 'raoke-ma-sk818', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-17 15:32:42', 1),
(63156, NULL, '173.252.95.12', '/san-pham/sofa-karaoke-ma-sk818.html', '', 'products/detail', 'raoke-ma-sk818', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-17 15:32:42', 1),
(63155, NULL, '173.252.79.10', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-17 09:38:37', 1),
(63154, NULL, '69.171.251.11', '/san-pham/sofa-karaoke-ma-sk815.html', '', 'products/detail', 'raoke-ma-sk815', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-17 05:23:46', 1),
(63153, NULL, '69.171.251.11', '/san-pham/sofa-karaoke-ma-sk815.html', '', 'products/detail', 'raoke-ma-sk815', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-17 05:23:45', 1),
(63152, NULL, '37.1.218.99', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 (.NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)', 0, '2019-09-16 19:57:57', 1),
(63151, NULL, '69.171.251.29', '/san-pham/sofa-karaoke-ma-sk816.html', '', 'products/detail', 'raoke-ma-sk816', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-16 12:15:51', 1),
(63150, NULL, '69.171.251.52', '/san-pham/sofa-karaoke-ma-sk816.html', '', 'products/detail', 'raoke-ma-sk816', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-16 12:15:50', 1),
(63149, 1, '35.224.153.36', '/test/wp-includes/wlwmanifest.xml', '', 'test/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 0, '2019-09-16 10:33:48', 0),
(63148, 1, '35.224.153.36', '/wp1/wp-includes/wlwmanifest.xml', '', 'wp1/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 0, '2019-09-16 10:33:47', 0),
(63147, 1, '35.224.153.36', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 0, '2019-09-16 10:33:46', 0),
(63146, 1, '35.224.153.36', '/2019/wp-includes/wlwmanifest.xml', '', '2019/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 0, '2019-09-16 10:33:46', 0),
(63145, 1, '35.224.153.36', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 0, '2019-09-16 10:33:45', 0),
(63144, NULL, '35.224.153.36', '/news/wp-includes/wlwmanifest.xml', '', 'news/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 0, '2019-09-16 10:33:43', 1),
(63142, NULL, '173.252.87.17', '/san-pham/sofa-karaoke-ma-sk821.html', '', 'products/detail', 'raoke-ma-sk821', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-16 08:51:38', 1),
(63143, NULL, '18.237.175.141', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-16 10:08:58', 1),
(63141, NULL, '64.246.165.50', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:59.0) Gecko/20100101 Firefox/59.0', 0, '2019-09-16 07:18:42', 1),
(63140, NULL, '31.184.215.241', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36', 0, '2019-09-15 10:02:36', 1),
(63138, NULL, '79.144.249.239', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 0, '2019-09-15 07:20:15', 1),
(63139, NULL, '191.102.143.118', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:68.0) Gecko/20100101 Firefox/68.0', 0, '2019-09-15 09:52:25', 1),
(63137, NULL, '31.184.215.241', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36', 0, '2019-09-15 02:47:36', 1);
INSERT INTO `site_log` (`site_log_id`, `no_of_visits`, `ip_address`, `requested_url`, `referer_page`, `page_name`, `query_string`, `user_agent`, `is_unique`, `access_date`, `visits_count`) VALUES
(63136, NULL, '18.139.2.187', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 0, '2019-09-14 23:22:07', 1),
(63133, NULL, '52.10.40.0', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-14 10:04:29', 1),
(63134, NULL, '118.25.154.158', '/wp/wp-admin/', '', 'wp/wp-admin', '', NULL, 0, '2019-09-14 10:16:35', 1),
(63135, NULL, '167.114.226.133', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', 0, '2019-09-14 13:40:35', 1),
(63132, 1, '84.17.47.135', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 0, '2019-09-14 03:22:23', 0),
(63131, NULL, '84.17.47.135', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 0, '2019-09-14 03:22:21', 1),
(63130, NULL, '51.77.129.159', '/', '', 'home/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-14 03:04:57', 1),
(63128, NULL, '31.184.215.241', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36', 0, '2019-09-13 12:07:30', 1),
(63129, NULL, '75.149.221.170', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36', 0, '2019-09-13 20:48:24', 1),
(63126, NULL, '167.114.65.240', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 5.1.1; SM-G925F Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.94 Mobile Safari/537.36', 0, '2019-09-13 06:04:14', 1),
(63127, NULL, '18.237.8.204', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-13 10:28:29', 1),
(63125, NULL, '167.114.65.240', '/ads.txt', '', 'ads.txt/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:04:13', 1),
(63123, 1, '167.114.65.240', '/danh-muc/sofa-khach-san.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:04:09', 0),
(63124, 1, '167.114.65.240', '/san-pham/sofa-karaoke-ma-sk812.html', '', 'products/detail', 'raoke-ma-sk812', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:04:11', 0),
(63121, 1, '167.114.65.240', '/danh-muc/sofa-cafe.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:04:06', 0),
(63122, 1, '167.114.65.240', '/zalo:0972.050.202', '', 'zalo:0972.050.202/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:04:08', 0),
(63120, 1, '167.114.65.240', '/san-pham/sofa-karaoke-ma-sk817.html', '', 'products/detail', 'raoke-ma-sk817', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:04:04', 0),
(63119, 1, '167.114.65.240', '/nhung-luu-y-cuc-quan-trong-khi-chon-sofa-cho-gia-dinh.html', '', 'home/index', '-cuc-quan-trong-khi-chon-sofa-cho-gia-dinh', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:04:02', 0),
(63118, 1, '167.114.65.240', '/chon-sofa-cho-phong-karaoke-sao-cho-dung-cach.html', '', 'home/index', 'ho-phong-karaoke-sao-cho-dung-cach', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:04:00', 0),
(63116, 1, '167.114.65.240', '/contact', '', 'contact/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:03:56', 0),
(63117, 1, '167.114.65.240', '/san-pham/sofa-ni-gia-dinh-ma-gd103.html', '', 'products/detail', '-gia-dinh-ma-gd103', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:03:58', 0),
(63115, NULL, '167.114.65.240', '/', '', 'home/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-09-13 06:03:46', 1),
(63113, NULL, '52.88.144.91', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-12 12:01:21', 1),
(63114, NULL, '173.252.95.38', '/san-pham/sofa-karaoke-ma-sk815.html?fbclid=IwAR16jx6KAae0EgNaBCapVGvi8YxBNbHEsDBDwDGGMO4rlc3pes8PMcevbos', '', 'products/detail', 'raoke-ma-sk815', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-12 18:50:42', 1),
(63112, NULL, '173.252.79.12', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-12 09:31:35', 1),
(63111, NULL, '18.237.139.88', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-11 15:22:09', 1),
(63109, NULL, '185.139.68.142', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-09-11 14:30:44', 1),
(63110, 1, '185.139.68.142', '/license.php', '', 'license.php/index', '', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-09-11 14:30:44', 0),
(63108, 1, '37.120.142.154', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 0, '2019-09-11 09:55:19', 0),
(63107, NULL, '37.120.142.154', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 0, '2019-09-11 09:55:16', 1),
(63106, NULL, '35.224.153.36', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 OPR/36.0.2130.32', 0, '2019-09-11 05:15:53', 1),
(63104, NULL, '51.75.243.74', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-10 20:15:16', 1),
(63105, NULL, '173.252.95.22', '/san-pham/sofa-karaoke-ma-sk815.html?fbclid=IwAR16jx6KAae0EgNaBCapVGvi8YxBNbHEsDBDwDGGMO4rlc3pes8PMcevbos', '', 'products/detail', 'raoke-ma-sk815', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-10 23:22:34', 1),
(63102, 1, '69.58.178.59', '/contact', '', 'contact/index', '', 'Mozilla/5.0 (compatible; ips-agent)', 0, '2019-09-10 17:58:49', 0),
(63103, 1, '69.58.178.59', '/trang-chu', '', 'home/index', '', 'Mozilla/5.0 (compatible; ips-agent)', 0, '2019-09-10 17:58:50', 0),
(63100, NULL, '69.58.178.59', '/', '', 'home/index', '', 'Mozilla/5.0 (compatible; ips-agent)', 0, '2019-09-10 17:58:45', 1),
(63101, 1, '69.58.178.59', '/danh-muc-tin/tin-tuc.html', '', 'news/new_bycategory', '', 'Mozilla/5.0 (compatible; ips-agent)', 0, '2019-09-10 17:58:47', 0),
(63098, 1, '185.93.3.114', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36 Kinza/4.7.2', 0, '2019-09-10 12:03:15', 0),
(63099, NULL, '34.222.90.189', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-10 17:10:01', 1),
(63097, NULL, '185.93.3.114', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36 Kinza/4.7.2', 0, '2019-09-10 12:03:13', 1),
(63096, NULL, '51.75.192.117', '/.env', '', '.env/index', '', 'Mozlila/5.0 (Linux; Android 7.0; SM-G892A Bulid/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Moblie Safari/537.36', 0, '2019-09-10 07:53:15', 1),
(63094, NULL, '54.244.18.242', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-09 10:28:22', 1),
(63095, NULL, '51.75.192.117', '/.env', '', '.env/index', '', 'Mozlila/5.0 (Linux; Android 7.0; SM-G892A Bulid/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Moblie Safari/537.36', 0, '2019-09-09 22:11:06', 1),
(63092, NULL, '5.188.62.5', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36', 0, '2019-09-09 00:13:23', 1),
(63093, NULL, '173.252.79.16', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-09 08:55:05', 1),
(63090, NULL, '52.40.52.223', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-07 11:50:08', 1),
(63091, NULL, '18.237.35.243', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-08 15:52:30', 1),
(63088, NULL, '213.14.116.235', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-06 13:54:36', 1),
(63089, NULL, '52.90.24.75', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 0, '2019-09-07 03:05:25', 1),
(63086, NULL, '92.63.111.27', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 0, '2019-09-05 17:49:48', 1),
(63087, NULL, '54.201.6.52', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-09-06 08:54:06', 1),
(63085, NULL, '66.249.82.84', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Mobile Safari/537.36', 0, '2019-09-05 10:59:48', 1),
(63083, NULL, '42.118.11.4', '/vnadmin', '', 'defaults/index', '', 'ZaloPC-win32-24v357', 0, '2019-09-05 06:29:30', 1),
(63084, NULL, '77.74.177.113', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 0, '2019-09-05 08:56:57', 1),
(63082, NULL, '173.252.127.1', '/san-pham/sofa-khach-san-ma-ks207.html', '', 'products/detail', 'ach-san-ma-ks207', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-04 15:02:23', 1),
(63081, NULL, '173.252.127.35', '/san-pham/sofa-khach-san-ma-ks207.html', '', 'products/detail', 'ach-san-ma-ks207', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-04 15:02:22', 1),
(63080, NULL, '69.171.251.43', '/san-pham/sofa-cao-cap-ma-cp-513.html', '', 'products/detail', 'o-cap-ma-cp-513', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-04 13:34:11', 1),
(63079, NULL, '69.171.251.43', '/san-pham/sofa-cao-cap-ma-cp-513.html', '', 'products/detail', 'o-cap-ma-cp-513', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-04 13:34:10', 1),
(63078, NULL, '173.252.127.16', '/san-pham/sofa-cao-cap-ma-cp511.html', '', 'products/detail', 'o-cap-ma-cp511', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-04 13:33:17', 1),
(63077, NULL, '173.252.127.42', '/san-pham/sofa-cao-cap-ma-cp511.html', '', 'products/detail', 'o-cap-ma-cp511', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-04 13:33:16', 1),
(63076, 1, '37.120.142.164', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 0, '2019-09-04 11:43:44', 0),
(63075, NULL, '37.120.142.164', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 0, '2019-09-04 11:43:43', 1),
(63074, 1, '37.115.185.176', '/sito/wp-includes/wlwmanifest.xml', '', 'sito/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:41', 0),
(63073, 1, '37.115.185.176', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:41', 0),
(63072, 1, '37.115.185.176', '/site/wp-includes/wlwmanifest.xml', '', 'site/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:41', 0),
(63071, 1, '37.115.185.176', '/wp2/wp-includes/wlwmanifest.xml', '', 'wp2/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:40', 0),
(63070, 1, '37.115.185.176', '/media/wp-includes/wlwmanifest.xml', '', 'media/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:40', 0),
(63069, 1, '37.115.185.176', '/test/wp-includes/wlwmanifest.xml', '', 'test/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:40', 0),
(63068, 1, '37.115.185.176', '/wp1/wp-includes/wlwmanifest.xml', '', 'wp1/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:40', 0),
(63067, 1, '37.115.185.176', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:39', 0),
(63066, 1, '37.115.185.176', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:39', 0),
(63065, 1, '37.115.185.176', '/2017/wp-includes/wlwmanifest.xml', '', '2017/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:39', 0),
(63064, 1, '37.115.185.176', '/2016/wp-includes/wlwmanifest.xml', '', '2016/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:39', 0),
(63063, 1, '37.115.185.176', '/2015/wp-includes/wlwmanifest.xml', '', '2015/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:38', 0),
(63062, 1, '37.115.185.176', '/news/wp-includes/wlwmanifest.xml', '', 'news/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:38', 0),
(63061, 1, '37.115.185.176', '/wp/wp-includes/wlwmanifest.xml', '', 'wp/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:38', 0),
(63060, 1, '37.115.185.176', '/website/wp-includes/wlwmanifest.xml', '', 'website/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:38', 0),
(63059, 1, '37.115.185.176', '/wordpress/wp-includes/wlwmanifest.xml', '', 'wordpress/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:37', 0),
(63058, 1, '37.115.185.176', '/web/wp-includes/wlwmanifest.xml', '', 'web/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:37', 0),
(63057, 1, '37.115.185.176', '/blog/wp-includes/wlwmanifest.xml', '', 'blog/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:37', 0),
(63056, 1, '37.115.185.176', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:36', 0),
(63055, 1, '37.115.185.176', '/xmlrpc.php?rsd', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:36', 0),
(63054, 1, '37.115.185.176', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:36', 0),
(63053, NULL, '37.115.185.176', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-09-04 10:40:35', 1),
(63051, NULL, '84.17.58.77', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36 Kinza/4.8.2', 0, '2019-09-04 08:43:39', 1),
(63052, 1, '84.17.58.77', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36 Kinza/4.8.2', 0, '2019-09-04 08:43:42', 0),
(63050, NULL, '148.251.151.248', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208', 0, '2019-09-03 17:03:38', 1),
(63049, NULL, '173.252.95.23', '/san-pham/sofa-gia-dinh-ma-gd108.html', '', 'products/detail', 'a-dinh-ma-gd108', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-03 16:18:37', 1),
(63048, NULL, '173.252.95.42', '/san-pham/sofa-gia-dinh-ma-gd108.html', '', 'products/detail', 'a-dinh-ma-gd108', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-03 16:18:36', 1),
(63046, NULL, '173.252.127.14', '/san-pham/sofa-gia-dinh-ma-gd-112.html', '', 'products/detail', 'a-dinh-ma-gd-112', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 16:59:41', 1),
(63047, NULL, '188.165.192.220', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', 0, '2019-09-03 05:47:28', 1),
(63045, NULL, '173.252.127.43', '/san-pham/sofa-gia-dinh-ma-gd-112.html', '', 'products/detail', 'a-dinh-ma-gd-112', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 16:59:40', 1),
(63044, NULL, '173.252.95.25', '/san-pham/sofa-gia-dinh-ma-gd215.html', '', 'products/detail', 'a-dinh-ma-gd215', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 16:27:09', 1),
(63042, NULL, '173.252.95.37', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'cortex/1.0', 0, '2019-09-02 16:20:57', 1),
(63043, NULL, '173.252.95.13', '/san-pham/sofa-gia-dinh-ma-gd215.html', '', 'products/detail', 'a-dinh-ma-gd215', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 16:27:08', 1),
(63041, NULL, '173.252.95.12', '/san-pham/sofa-ni-gia-dinh-ma-gd102.html', '', 'products/detail', '-gia-dinh-ma-gd102', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 16:20:55', 1),
(63040, NULL, '173.252.95.42', '/san-pham/sofa-ni-gia-dinh-ma-gd102.html', '', 'products/detail', '-gia-dinh-ma-gd102', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 16:20:54', 1),
(63039, NULL, '173.252.127.62', '/san-pham/sofa-ni-gia-dinh-ma-gd103.html', '', 'products/detail', '-gia-dinh-ma-gd103', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 15:59:17', 1),
(63038, NULL, '173.252.127.12', '/san-pham/sofa-ni-gia-dinh-ma-gd103.html', '', 'products/detail', '-gia-dinh-ma-gd103', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 15:59:16', 1),
(63036, NULL, '190.169.30.46', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-02 15:45:02', 1),
(63037, NULL, '190.169.30.46', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-02 15:45:04', 1),
(63035, NULL, '173.252.95.29', '/san-pham/sofa-karaoke-ma-sk816.html', '', 'products/detail', 'raoke-ma-sk816', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 14:32:18', 1),
(63034, NULL, '173.252.95.50', '/san-pham/sofa-karaoke-ma-sk816.html', '', 'products/detail', 'raoke-ma-sk816', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 14:32:17', 1),
(63033, NULL, '69.171.251.40', '/san-pham/sofa-karaoke-ma-sk813.html', '', 'products/detail', 'raoke-ma-sk813', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 14:03:16', 1),
(63032, NULL, '69.171.251.28', '/san-pham/sofa-karaoke-ma-sk813.html', '', 'products/detail', 'raoke-ma-sk813', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 14:03:15', 1),
(63031, NULL, '173.252.127.20', '/san-pham/sofa-karaoke-ma-sk821.html', '', 'products/detail', 'raoke-ma-sk821', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 11:59:49', 1),
(63029, NULL, '173.252.87.40', '/san-pham/sofa-karaoke-ma-sk818.html', '', 'products/detail', 'raoke-ma-sk818', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 11:48:52', 1),
(63030, NULL, '173.252.127.7', '/san-pham/sofa-karaoke-ma-sk821.html', '', 'products/detail', 'raoke-ma-sk821', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 11:59:48', 1),
(63028, NULL, '173.252.87.8', '/san-pham/sofa-karaoke-ma-sk818.html', '', 'products/detail', 'raoke-ma-sk818', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 11:48:51', 1),
(63027, NULL, '173.252.87.5', '/san-pham/sofa-cafe-ma-sc619.html', '', 'products/detail', 'fe-ma-sc619', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 11:21:55', 1),
(63026, NULL, '173.252.127.44', '/san-pham/sofa-karaoke-ma-sk811.html', '', 'products/detail', 'raoke-ma-sk811', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-09-02 11:08:27', 1),
(63024, NULL, '95.110.160.27', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-02 09:30:27', 1),
(63025, NULL, '95.110.160.27', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-02 09:30:29', 1),
(63023, NULL, '50.62.177.191', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-02 02:47:00', 1),
(63022, NULL, '50.62.177.191', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-02 02:46:58', 1),
(63020, NULL, '185.2.4.121', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-01 14:56:03', 1),
(63021, NULL, '185.2.4.121', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-09-01 14:56:05', 1),
(63019, NULL, '171.244.4.122', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-09-01 05:49:28', 1),
(63018, NULL, '66.84.89.109', '/contact', '', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-31 15:26:06', 1),
(63016, NULL, '173.252.87.4', '/san-pham/sofa-cafe-ma-sc621.html', '', 'products/detail', 'fe-ma-sc621', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-31 15:03:12', 1),
(63017, NULL, '173.252.87.31', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'cortex/1.0', 0, '2019-08-31 15:03:13', 1),
(63014, NULL, '34.221.251.33', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-31 11:19:04', 1),
(63015, NULL, '173.252.79.5', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-31 12:49:44', 1),
(63013, NULL, '173.252.87.10', '/san-pham/sofa-gia-dinh-ma-gd109.html', '', 'products/detail', 'a-dinh-ma-gd109', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-31 10:00:49', 1),
(63011, NULL, '34.201.153.108', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 0, '2019-08-31 05:46:35', 1),
(63012, NULL, '173.252.87.5', '/san-pham/sofa-gia-dinh-ma-gd109.html', '', 'products/detail', 'a-dinh-ma-gd109', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-31 10:00:48', 1),
(63010, NULL, '185.234.219.246', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.15', 0, '2019-08-30 23:58:56', 1),
(63008, 1, '89.187.178.140', '/wp/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', 0, '2019-08-30 20:21:32', 0),
(63009, NULL, '62.210.83.78', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 0, '2019-08-30 23:15:32', 1),
(63007, 1, '89.187.178.140', '/wordpress/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', 0, '2019-08-30 20:21:32', 0),
(63005, 1, '89.187.178.140', '/blog/robots.txt', '', 'blog/robots.txt', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', 0, '2019-08-30 20:21:31', 0),
(63006, 1, '89.187.178.140', '/blog/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', 0, '2019-08-30 20:21:31', 0),
(63004, 1, '89.187.178.140', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', 0, '2019-08-30 20:21:29', 0),
(63003, NULL, '89.187.178.140', '/xmlrpc.php?rsd', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0', 0, '2019-08-30 20:21:27', 1),
(63002, 1, '104.140.22.147', '/contact', 'http://sofabaolam.com', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-30 05:14:08', 0),
(63000, NULL, '34.213.111.12', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-29 16:02:21', 1),
(63001, NULL, '104.140.22.147', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-30 05:14:06', 1),
(62998, NULL, '89.108.99.6', '/', '', 'home/index', '', NULL, 0, '2019-08-29 04:26:37', 1),
(62999, NULL, '173.252.79.8', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-29 07:29:12', 1),
(62996, NULL, '18.231.106.124', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 0, '2019-08-28 03:30:41', 1),
(62997, NULL, '54.212.131.177', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-28 10:46:29', 1),
(62995, NULL, '5.188.62.5', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36', 0, '2019-08-27 23:29:30', 1),
(62993, NULL, '193.32.161.130', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0', 0, '2019-08-27 17:53:19', 1),
(62994, NULL, '54.244.60.220', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-27 21:11:17', 1),
(62992, NULL, '35.167.54.162', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 0, '2019-08-27 14:24:54', 1),
(62990, NULL, '89.22.101.69', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 0, '2019-08-26 20:41:36', 1),
(62991, NULL, '181.177.97.164', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0', 0, '2019-08-27 12:05:12', 1),
(62989, NULL, '89.22.101.69', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 0, '2019-08-26 20:40:17', 1),
(62987, 1, '66.249.82.84', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 13:32:30', 0),
(62988, NULL, '54.245.171.241', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-26 18:31:23', 1),
(62986, 1, '66.249.82.84', '/404_override', 'http://sofabaolam.com/', 'error/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 13:32:30', 0),
(62985, 1, '66.249.82.86', '/danh-muc/sofa-khach-san.html', 'http://sofabaolam.com/danh-muc/sofa-khach-san.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 13:32:30', 0),
(62984, 1, '66.249.82.84', '/trang-chu', 'http://sofabaolam.com/danh-muc/sofa-khach-san.html', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 13:32:12', 0),
(62983, 1, '66.249.82.86', '/danh-muc/sofa-khach-san.html', 'http://sofabaolam.com/404_override', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 13:31:51', 0),
(62982, 1, '66.249.82.86', '/404_override', 'http://sofabaolam.com/', 'error/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 13:31:38', 0),
(62981, 1, '66.249.82.86', '/danh-muc/sofa-vang.html', 'http://sofabaolam.com/', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 13:31:37', 0),
(62980, 1, '66.249.82.84', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 13:31:27', 0),
(62979, NULL, '173.252.79.6', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-26 12:52:06', 1),
(62978, NULL, '89.22.101.69', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 0, '2019-08-26 00:42:34', 1),
(62977, 1, '123.24.220.131', '/danh-muc/sofa-cafe.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 00:25:59', 0),
(62976, 1, '123.24.220.131', '/trang-chu', 'http://sofabaolam.com/danh-muc/sofa-cafe.html', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 00:25:40', 0),
(62974, NULL, '34.221.46.162', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-25 14:36:08', 1),
(62975, 1, '123.24.220.131', '/danh-muc/sofa-cafe.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-26 00:25:31', 0),
(62973, 1, '66.249.82.82', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:32:41', 0),
(62972, 1, '66.249.82.82', '/danh-muc/sofa-gia-dinh.html', 'http://sofabaolam.com/', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:32:40', 0),
(62971, 1, '66.249.82.86', '/danh-muc/sofa-cafe.html', 'http://sofabaolam.com/danh-muc/sofa-gia-dinh.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:32:29', 0),
(62970, 1, '66.249.82.82', '/san-pham/sofa-cafe-ma-sc617.html', 'http://sofabaolam.com/danh-muc/sofa-cafe.html', 'products/detail', 'fe-ma-sc617', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:32:25', 0),
(62969, 1, '66.249.82.86', '/danh-muc/sofa-cafe.html', 'http://sofabaolam.com/danh-muc/sofa-gia-dinh.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:32:23', 0),
(62968, 1, '66.249.82.86', '/san-pham/sofa-cafe-ma-sc617.html', 'http://sofabaolam.com/danh-muc/sofa-cafe.html', 'products/detail', 'fe-ma-sc617', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:32:19', 0),
(62967, 1, '66.249.82.84', '/danh-muc/sofa-cafe.html', 'http://sofabaolam.com/danh-muc/sofa-gia-dinh.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:32:16', 0),
(62966, NULL, '66.220.149.42', '/san-pham/sofa-cafe-ma-sc617.html', '', 'products/detail', 'fe-ma-sc617', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-25 12:32:15', 1),
(62965, NULL, '66.220.149.50', '/san-pham/sofa-cafe-ma-sc617.html', '', 'products/detail', 'fe-ma-sc617', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-25 12:32:14', 1),
(62964, 1, '66.249.82.82', '/san-pham/sofa-cafe-ma-sc617.html', 'http://sofabaolam.com/danh-muc/sofa-cafe.html', 'products/detail', 'fe-ma-sc617', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:32:11', 0),
(62963, 1, '66.249.82.84', '/danh-muc/sofa-cafe.html', 'http://sofabaolam.com/danh-muc/sofa-gia-dinh.html', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:31:58', 0),
(62962, 1, '66.249.82.86', '/danh-muc/sofa-gia-dinh.html', 'http://sofabaolam.com/', 'products/pro_bycategory', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-25 12:31:55', 0),
(62960, NULL, '64.246.165.140', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:59.0) Gecko/20100101 Firefox/59.0', 0, '2019-08-25 02:57:27', 1),
(62961, NULL, '51.75.192.117', '/.env', '', '.env/index', '', 'Mozlila/5.0 (Linux; Android 7.0; SM-G892A Bulid/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Moblie Safari/537.36', 0, '2019-08-25 12:21:48', 1),
(62958, NULL, '34.222.45.150', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-24 15:20:55', 1),
(62959, NULL, '42.114.196.6', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-25 00:48:16', 1),
(62957, NULL, '173.252.79.4', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-24 03:31:17', 1),
(62956, 1, '37.115.184.193', '/sito/wp-includes/wlwmanifest.xml', '', 'sito/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:13', 0),
(62955, 1, '37.115.184.193', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:13', 0),
(62954, 1, '37.115.184.193', '/site/wp-includes/wlwmanifest.xml', '', 'site/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:13', 0),
(62953, 1, '37.115.184.193', '/wp2/wp-includes/wlwmanifest.xml', '', 'wp2/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:12', 0),
(62952, 1, '37.115.184.193', '/media/wp-includes/wlwmanifest.xml', '', 'media/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:12', 0),
(62950, 1, '37.115.184.193', '/wp1/wp-includes/wlwmanifest.xml', '', 'wp1/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:11', 0),
(62951, 1, '37.115.184.193', '/test/wp-includes/wlwmanifest.xml', '', 'test/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:12', 0),
(62949, 1, '37.115.184.193', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:11', 0),
(62948, 1, '37.115.184.193', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:11', 0),
(62947, 1, '37.115.184.193', '/2017/wp-includes/wlwmanifest.xml', '', '2017/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:10', 0),
(62946, 1, '37.115.184.193', '/2016/wp-includes/wlwmanifest.xml', '', '2016/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:09', 0),
(62945, 1, '37.115.184.193', '/2015/wp-includes/wlwmanifest.xml', '', '2015/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:09', 0),
(62944, 1, '37.115.184.193', '/news/wp-includes/wlwmanifest.xml', '', 'news/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:08', 0),
(62943, 1, '37.115.184.193', '/wp/wp-includes/wlwmanifest.xml', '', 'wp/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:07', 0),
(62942, 1, '37.115.184.193', '/website/wp-includes/wlwmanifest.xml', '', 'website/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:07', 0),
(62941, 1, '37.115.184.193', '/wordpress/wp-includes/wlwmanifest.xml', '', 'wordpress/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:06', 0),
(62940, 1, '37.115.184.193', '/web/wp-includes/wlwmanifest.xml', '', 'web/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:06', 0),
(62939, 1, '37.115.184.193', '/blog/wp-includes/wlwmanifest.xml', '', 'blog/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:05', 0),
(62938, 1, '37.115.184.193', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:03', 0),
(62937, 1, '37.115.184.193', '/xmlrpc.php?rsd', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:01', 0),
(62935, NULL, '37.115.184.193', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:00', 1),
(62936, 1, '37.115.184.193', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-23 20:19:01', 0),
(62934, NULL, '54.213.211.214', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-23 18:57:59', 1),
(62933, NULL, '1.55.18.162', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G973F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36', 0, '2019-08-23 08:47:45', 1),
(62932, NULL, '1.55.18.162', '/new/luu-y-chon-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban.html', '', 'news/detail', 'on-ghe-sofa-cafe-cho-khong-gian-quan-cafe-cua-ban', 'Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G973F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36', 0, '2019-08-23 08:28:20', 1),
(62931, 1, '66.249.82.82', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:47:43', 0),
(62930, 1, '66.249.82.86', '/san-pham/sofa-karaoke-ma-sk819.html', 'http://sofabaolam.com/', 'products/detail', 'raoke-ma-sk819', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:47:40', 0),
(62929, 1, '66.249.82.84', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:46:28', 0),
(62928, NULL, '66.220.149.18', '/san-pham/sofa-karaoke-ma-sk813.html', '', 'products/detail', 'raoke-ma-sk813', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 05:46:17', 1),
(62927, NULL, '66.220.149.25', '/san-pham/sofa-karaoke-ma-sk813.html', '', 'products/detail', 'raoke-ma-sk813', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 05:46:17', 1),
(62926, 1, '66.249.82.86', '/san-pham/sofa-karaoke-ma-sk813.html', 'http://sofabaolam.com/', 'products/detail', 'raoke-ma-sk813', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:46:14', 0),
(62924, 1, '66.249.82.82', '/trang-chu', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:46:08', 0),
(62925, 1, '66.249.82.84', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:46:11', 0),
(62923, 1, '66.249.82.86', '/', 'http://sofabaolam.com/trang-chu', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:46:07', 0),
(62922, 1, '66.249.82.86', '/trang-chu', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:45:55', 0),
(62921, 1, '66.249.82.84', '/san-pham/sofa-karaoke-ma-sk819.html', 'http://sofabaolam.com/trang-chu', 'products/detail', 'raoke-ma-sk819', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:45:51', 0),
(62920, 1, '66.249.82.82', '/trang-chu', 'http://sofabaolam.com/san-pham/sofa-karaoke-ma-sk819.html', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:45:50', 0),
(62919, 1, '66.249.82.82', '/san-pham/sofa-karaoke-ma-sk817.html', 'http://sofabaolam.com/trang-chu', 'products/detail', 'raoke-ma-sk817', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:45:23', 0),
(62917, NULL, '66.220.149.12', '/san-pham/sofa-karaoke-ma-sk817.html', '', 'products/detail', 'raoke-ma-sk817', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 05:45:22', 1),
(62918, NULL, '66.220.149.7', '/san-pham/sofa-karaoke-ma-sk817.html', '', 'products/detail', 'raoke-ma-sk817', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 05:45:23', 1),
(62916, 1, '66.249.82.86', '/trang-chu', 'http://sofabaolam.com/san-pham/sofa-karaoke-ma-sk819.html', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:45:21', 0),
(62915, 1, '66.249.82.82', '/san-pham/sofa-karaoke-ma-sk817.html', 'http://sofabaolam.com/trang-chu', 'products/detail', 'raoke-ma-sk817', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:45:19', 0),
(62914, 1, '66.249.82.82', '/trang-chu', 'http://sofabaolam.com/san-pham/sofa-karaoke-ma-sk819.html', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:45:12', 0),
(62912, NULL, '66.220.149.6', '/san-pham/sofa-karaoke-ma-sk819.html', '', 'products/detail', 'raoke-ma-sk819', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 05:45:07', 1),
(62913, NULL, '66.220.149.18', '/san-pham/sofa-karaoke-ma-sk819.html', '', 'products/detail', 'raoke-ma-sk819', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 05:45:08', 1),
(62910, 1, '66.249.82.82', '/trang-chu', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:44:09', 0),
(62911, 1, '66.249.82.86', '/san-pham/sofa-karaoke-ma-sk819.html', 'http://sofabaolam.com/trang-chu', 'products/detail', 'raoke-ma-sk819', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:45:02', 0),
(62909, 1, '66.249.82.82', '/', 'http://sofabaolam.com/trang-chu', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:44:06', 0),
(62908, 1, '66.249.82.84', '/trang-chu', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-23 05:44:04', 0);
INSERT INTO `site_log` (`site_log_id`, `no_of_visits`, `ip_address`, `requested_url`, `referer_page`, `page_name`, `query_string`, `user_agent`, `is_unique`, `access_date`, `visits_count`) VALUES
(62907, NULL, '173.252.127.3', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 04:35:13', 1),
(62906, NULL, '173.252.127.16', '/san-pham/sofa-cafe-ma-sc611.html', '', 'products/detail', 'fe-ma-sc611', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 04:35:11', 1),
(62904, NULL, '34.222.93.123', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-22 15:51:08', 1),
(62905, NULL, '173.252.127.16', '/san-pham/sofa-cafe-ma-sc611.html', '', 'products/detail', 'fe-ma-sc611', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-23 04:35:09', 1),
(62903, NULL, '77.74.177.114', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 0, '2019-08-22 08:57:13', 1),
(62901, NULL, '35.167.230.24', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-21 18:01:16', 1),
(62902, NULL, '171.236.63.203', '/', 'https://www.google.com/', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 0, '2019-08-22 01:43:32', 1),
(62900, NULL, '78.85.163.19', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 (.NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)', 0, '2019-08-21 02:35:23', 1),
(62899, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-21 01:14:27', 1),
(62898, 1, '37.115.184.193', '/sito/wp-includes/wlwmanifest.xml', '', 'sito/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:23', 0),
(62897, 1, '37.115.184.193', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:22', 0),
(62896, 1, '37.115.184.193', '/site/wp-includes/wlwmanifest.xml', '', 'site/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:22', 0),
(62895, 1, '37.115.184.193', '/wp2/wp-includes/wlwmanifest.xml', '', 'wp2/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:22', 0),
(62894, 1, '37.115.184.193', '/media/wp-includes/wlwmanifest.xml', '', 'media/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:21', 0),
(62893, 1, '37.115.184.193', '/test/wp-includes/wlwmanifest.xml', '', 'test/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:21', 0),
(62892, 1, '37.115.184.193', '/wp1/wp-includes/wlwmanifest.xml', '', 'wp1/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:21', 0),
(62891, 1, '37.115.184.193', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:20', 0),
(62890, 1, '37.115.184.193', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:20', 0),
(62889, 1, '37.115.184.193', '/2017/wp-includes/wlwmanifest.xml', '', '2017/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:20', 0),
(62888, 1, '37.115.184.193', '/2016/wp-includes/wlwmanifest.xml', '', '2016/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:19', 0),
(62887, 1, '37.115.184.193', '/2015/wp-includes/wlwmanifest.xml', '', '2015/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:19', 0),
(62886, 1, '37.115.184.193', '/news/wp-includes/wlwmanifest.xml', '', 'news/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:19', 0),
(62885, 1, '37.115.184.193', '/wp/wp-includes/wlwmanifest.xml', '', 'wp/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:18', 0),
(62884, 1, '37.115.184.193', '/website/wp-includes/wlwmanifest.xml', '', 'website/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:18', 0),
(62883, 1, '37.115.184.193', '/wordpress/wp-includes/wlwmanifest.xml', '', 'wordpress/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:18', 0),
(62882, 1, '37.115.184.193', '/web/wp-includes/wlwmanifest.xml', '', 'web/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:17', 0),
(62881, 1, '37.115.184.193', '/blog/wp-includes/wlwmanifest.xml', '', 'blog/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:17', 0),
(62880, 1, '37.115.184.193', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:17', 0),
(62879, 1, '37.115.184.193', '/xmlrpc.php?rsd', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:16', 0),
(62878, 1, '37.115.184.193', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:16', 0),
(62876, NULL, '54.185.195.222', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-20 16:49:38', 1),
(62877, NULL, '37.115.184.193', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-20 17:50:14', 1),
(62875, NULL, '185.253.96.198', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 0, '2019-08-20 12:57:37', 1),
(62874, 1, '24.116.202.31', '/contact', 'http://sofabaolam.com', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-20 09:30:12', 0),
(62872, NULL, '54.186.166.33', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-19 18:03:25', 1),
(62873, NULL, '24.116.202.31', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-20 09:30:11', 1),
(62871, NULL, '14.166.93.29', '/?fbclid=IwAR0Rr1Nl9L-3rgVCU2NroAIxCq0JcdXr_CjAK3tuPjMBVvs-nZfSoJO5cyw', 'https://l.facebook.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 0, '2019-08-19 15:49:12', 1),
(62869, NULL, '173.252.79.5', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-19 11:38:47', 1),
(62870, NULL, '14.162.136.33', '/?fbclid=IwAR0Ss12UmgT0tq7SqXQnotXrA8oxobSgfdpgLenfSN6vWOpdBaFmMfzh3e4', 'https://l.facebook.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/80.0.180 Chrome/74.0.3729.180 Safari/537.36', 0, '2019-08-19 13:27:20', 1),
(62868, NULL, '173.252.79.10', '/', '', 'home/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-19 11:38:31', 1),
(62866, NULL, '173.252.79.11', '/', '', 'home/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-19 11:38:29', 1),
(62867, NULL, '66.220.149.49', '/', '', 'home/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-19 11:38:29', 1),
(62865, NULL, '91.210.147.105', '/404_override', 'http://sofabaolam.com/blog/', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36', 0, '2019-08-18 16:43:38', 1),
(62863, NULL, '91.210.147.105', '/', 'http://sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36', 0, '2019-08-18 16:43:20', 1),
(62864, NULL, '91.210.147.105', '/blog/', 'http://sofabaolam.com/blog/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36', 0, '2019-08-18 16:43:36', 1),
(62861, NULL, '92.53.96.125', '/?author=49', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:39', 1),
(62862, NULL, '34.215.74.220', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-17 17:59:55', 1),
(62859, NULL, '92.53.96.125', '/?author=47', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:38', 1),
(62860, NULL, '92.53.96.125', '/?author=48', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:39', 1),
(62858, NULL, '92.53.96.125', '/?author=46', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:37', 1),
(62856, NULL, '92.53.96.125', '/?author=44', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:36', 1),
(62857, NULL, '92.53.96.125', '/?author=45', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:37', 1),
(62854, NULL, '92.53.96.125', '/?author=42', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:35', 1),
(62855, NULL, '92.53.96.125', '/?author=43', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:35', 1),
(62853, NULL, '92.53.96.125', '/?author=41', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:34', 1),
(62851, NULL, '92.53.96.125', '/?author=39', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:33', 1),
(62852, NULL, '92.53.96.125', '/?author=40', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:33', 1),
(62850, NULL, '92.53.96.125', '/?author=38', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:32', 1),
(62849, NULL, '92.53.96.125', '/?author=37', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:32', 1),
(62847, NULL, '92.53.96.125', '/?author=35', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:30', 1),
(62848, NULL, '92.53.96.125', '/?author=36', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:31', 1),
(62846, NULL, '92.53.96.125', '/?author=34', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:30', 1),
(62844, NULL, '92.53.96.125', '/?author=32', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:28', 1),
(62845, NULL, '92.53.96.125', '/?author=33', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:29', 1),
(62843, NULL, '92.53.96.125', '/?author=31', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:28', 1),
(62842, NULL, '92.53.96.125', '/?author=30', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:27', 1),
(62840, NULL, '92.53.96.125', '/?author=28', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:26', 1),
(62841, NULL, '92.53.96.125', '/?author=29', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:26', 1),
(62838, NULL, '92.53.96.125', '/?author=26', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:24', 1),
(62839, NULL, '92.53.96.125', '/?author=27', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:25', 1),
(62837, NULL, '92.53.96.125', '/?author=25', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:24', 1),
(62836, NULL, '92.53.96.125', '/?author=24', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:23', 1),
(62834, NULL, '92.53.96.125', '/?author=22', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:22', 1),
(62835, NULL, '92.53.96.125', '/?author=23', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:22', 1),
(62833, NULL, '92.53.96.125', '/?author=21', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:21', 1),
(62831, NULL, '92.53.96.125', '/?author=19', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:20', 1),
(62832, NULL, '92.53.96.125', '/?author=20', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:21', 1),
(62830, NULL, '92.53.96.125', '/?author=18', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:19', 1),
(62829, NULL, '92.53.96.125', '/?author=17', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:19', 1),
(62827, NULL, '92.53.96.125', '/?author=15', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:17', 1),
(62828, NULL, '92.53.96.125', '/?author=16', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:18', 1),
(62826, NULL, '92.53.96.125', '/?author=14', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:17', 1),
(62824, NULL, '92.53.96.125', '/?author=12', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:15', 1),
(62825, NULL, '92.53.96.125', '/?author=13', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:16', 1),
(62823, NULL, '92.53.96.125', '/?author=11', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:15', 1),
(62821, NULL, '92.53.96.125', '/?author=9', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:13', 1),
(62822, NULL, '92.53.96.125', '/?author=10', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:14', 1),
(62820, NULL, '92.53.96.125', '/?author=8', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:13', 1),
(62818, NULL, '92.53.96.125', '/?author=6', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:11', 1),
(62819, NULL, '92.53.96.125', '/?author=7', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:12', 1),
(62817, NULL, '92.53.96.125', '/?author=5', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:11', 1),
(62815, NULL, '92.53.96.125', '/?author=3', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:09', 1),
(62816, NULL, '92.53.96.125', '/?author=4', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:10', 1),
(62814, NULL, '92.53.96.125', '/?author=2', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:08', 1),
(62813, NULL, '92.53.96.125', '/?author=1', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:08', 1),
(62811, NULL, '92.53.96.125', '/xmlrpc.php', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:07', 1),
(62812, NULL, '92.53.96.125', '/?feed=rss2', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:07', 1),
(62810, NULL, '92.53.96.125', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:06', 1),
(62807, NULL, '34.215.246.141', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-16 19:06:11', 1),
(62808, NULL, '52.15.212.3', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0', 0, '2019-08-16 23:13:09', 1),
(62809, NULL, '92.53.96.125', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1', 0, '2019-08-17 12:39:04', 1),
(62805, NULL, '49.234.233.211', '/wordpress/wp-admin/', '', 'wordpress/wp-admin', '', NULL, 0, '2019-08-15 17:32:51', 1),
(62806, NULL, '52.15.212.3', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0', 0, '2019-08-15 21:37:18', 1),
(62803, NULL, '38.145.112.248', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-14 19:03:36', 1),
(62804, NULL, '66.249.82.84', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 9; SM-A705F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 0, '2019-08-15 08:25:45', 1),
(62802, 1, '42.113.129.142', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-14 09:20:21', 0),
(62800, NULL, '42.113.129.142', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-14 09:19:20', 1),
(62801, 1, '42.113.129.142', '/san-pham/sofa-karaoke-ma-sk813.html', 'http://sofabaolam.com/', 'products/detail', 'raoke-ma-sk813', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-14 09:20:00', 0),
(62799, NULL, '138.68.180.18', '/danh-muc/sofa-gia-dinh.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-08-14 06:48:04', 1),
(62798, NULL, '138.68.180.18', '/san-pham/sofa-gia-dinh-ma-gd111.html', '', 'products/detail', 'a-dinh-ma-gd111', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-08-14 06:48:03', 1),
(62797, NULL, '138.68.180.18', '/san-pham/sofa-gia-dinh-ma-gd109.html', '', 'products/detail', 'a-dinh-ma-gd109', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-08-14 06:48:02', 1),
(62796, NULL, '138.68.180.18', '/san-pham/sofa-gia-dinh-ma-gd106.html', '', 'products/detail', 'a-dinh-ma-gd106', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-08-14 06:48:01', 1),
(62795, NULL, '138.68.180.18', '/san-pham/sofa-gia-dinh-ma-gd105.html', '', 'products/detail', 'a-dinh-ma-gd105', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-08-14 06:48:00', 1),
(62794, NULL, '138.68.180.18', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 0, '2019-08-14 06:47:58', 1),
(62792, NULL, '52.64.20.252', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0', 0, '2019-08-13 20:19:39', 1),
(62793, NULL, '61.160.213.146', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 0, '2019-08-14 00:44:11', 1),
(62790, NULL, '198.27.121.92', '//vendor/phpunit/phpunit/phpunit.xsd', '', 'vendor/phpunit', 'phpunit/phpunit.xsd', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 0, '2019-08-12 19:54:33', 1),
(62791, NULL, '38.145.107.234', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-13 14:45:46', 1),
(62788, 1, '172.93.192.118', '/contact', 'http://sofabaolam.com', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-12 02:51:47', 0),
(62789, NULL, '42.113.129.24', '/?gidzl=lDY4FQuUmtMon8mxeGN4SBF7YpguLiHs_fACFUC5ooEamjSxjmUOTAV0tZhX0S5syi2AR3abz7vxgnl8SW', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/66.4.104 Chrome/60.4.3112.104 Safari/537.36', 0, '2019-08-12 10:41:48', 1),
(62787, NULL, '172.93.192.118', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-12 02:51:42', 1),
(62785, NULL, '198.27.121.92', '//vendor/phpunit/phpunit/phpunit.xsd', '', 'vendor/phpunit', 'phpunit/phpunit.xsd', 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 0, '2019-08-11 08:38:04', 1),
(62786, NULL, '5.188.62.5', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36', 0, '2019-08-11 09:15:52', 1),
(62784, NULL, '94.21.145.90', '/', 'https://brandnewblogs.com/?domain=sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', 0, '2019-08-10 21:01:36', 1),
(62782, NULL, '191.96.241.204', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-10 17:57:13', 1),
(62783, NULL, '64.202.185.111', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-08-10 18:40:06', 1),
(62781, NULL, '173.252.79.3', '/san-pham/sofa-gia-dinh-ma-gd104.html', '', 'products/detail', 'a-dinh-ma-gd104', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-10 09:51:31', 1),
(62780, NULL, '173.252.79.2', '/san-pham/sofa-gia-dinh-ma-gd104.html', '', 'products/detail', 'a-dinh-ma-gd104', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-10 09:51:30', 1),
(62779, 1, '42.112.126.87', '/san-pham/sofa-gia-dinh-ma-gd104.html', 'http://sofabaolam.com/', 'products/detail', 'a-dinh-ma-gd104', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 0, '2019-08-10 09:51:25', 0),
(62778, NULL, '42.112.126.87', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 0, '2019-08-10 09:50:51', 1),
(62776, NULL, '181.214.189.142', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-09 15:20:12', 1),
(62777, NULL, '185.234.219.246', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99', 0, '2019-08-10 00:33:39', 1),
(62775, NULL, '173.252.127.37', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-09 10:27:18', 1),
(62773, NULL, '69.171.251.27', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-09 04:47:36', 1),
(62774, NULL, '173.252.79.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-09 07:16:17', 1),
(62772, NULL, '185.242.87.40', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36 OPR/62.0.3331.99', 0, '2019-08-09 02:09:56', 1),
(62770, NULL, '79.191.146.68', '/', 'http://sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36', 0, '2019-08-09 00:45:02', 1),
(62771, NULL, '173.252.95.29', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-09 02:02:00', 1),
(62769, NULL, '173.252.79.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 23:59:34', 1),
(62767, NULL, '173.252.79.3', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 23:59:34', 1),
(62768, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 23:59:34', 1),
(62766, NULL, '5.188.84.9', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 0, '2019-08-08 20:59:40', 1),
(62764, NULL, '66.220.149.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 17:38:41', 1),
(62765, NULL, '94.21.145.90', '/', 'https://brandnewblogs.com/?domain=sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', 0, '2019-08-08 19:36:32', 1),
(62762, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 14:59:36', 1),
(62763, NULL, '191.96.241.196', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-08 16:18:31', 1),
(62761, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 14:59:35', 1),
(62759, NULL, '173.252.79.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 11:55:31', 1),
(62760, NULL, '173.252.79.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 11:55:32', 1),
(62757, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 11:55:31', 1),
(62758, NULL, '173.252.79.12', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 11:55:31', 1),
(62756, NULL, '173.252.79.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 09:45:13', 1),
(62755, NULL, '77.74.177.114', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 0, '2019-08-08 08:57:29', 1),
(62752, NULL, '49.235.198.166', '/wp/wp-admin/', '', 'wp/wp-admin', '', NULL, 0, '2019-08-08 06:20:01', 1),
(62753, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 07:25:54', 1),
(62754, NULL, '173.252.79.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 07:25:55', 1),
(62750, NULL, '173.252.127.29', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 01:47:28', 1),
(62751, NULL, '173.252.79.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 03:50:05', 1),
(62749, NULL, '173.252.127.12', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-08 01:47:26', 1),
(62747, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 23:33:57', 1),
(62748, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 23:33:57', 1),
(62745, NULL, '173.252.79.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 15:02:55', 1),
(62746, NULL, '181.214.189.182', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-07 16:15:22', 1),
(62744, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 12:45:51', 1),
(62742, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 12:45:50', 1),
(62743, NULL, '173.252.79.5', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 12:45:51', 1),
(62740, NULL, '173.252.87.5', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 07:47:51', 1),
(62741, NULL, '173.252.95.34', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 10:13:25', 1),
(62739, NULL, '173.252.79.6', '/san-pham/sofa-karaoke-ma-sk821.html', '', 'products/detail', 'raoke-ma-sk821', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 07:04:18', 1),
(62738, NULL, '173.252.79.9', '/san-pham/sofa-karaoke-ma-sk821.html', '', 'products/detail', 'raoke-ma-sk821', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 07:04:17', 1),
(62737, 1, '14.248.85.156', '/assets/css/front_end/bootstrap.min.css.map', '', 'assets/css', 'front_end/bootstrap.min.css.map', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-07 07:04:13', 0),
(62736, 1, '14.248.85.156', '/san-pham/sofa-karaoke-ma-sk821.html', 'http://sofabaolam.com/', 'products/detail', 'raoke-ma-sk821', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-07 07:04:13', 0),
(62735, NULL, '173.252.79.8', '/san-pham/sofa-karaoke-ma-sk818.html', '', 'products/detail', 'raoke-ma-sk818', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 07:03:01', 1),
(62734, NULL, '173.252.79.8', '/san-pham/sofa-karaoke-ma-sk818.html', '', 'products/detail', 'raoke-ma-sk818', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 07:03:01', 1),
(62733, 1, '14.248.85.156', '/san-pham/sofa-karaoke-ma-sk818.html', '', 'products/detail', 'raoke-ma-sk818', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-07 07:02:57', 0),
(62732, 1, '14.248.85.156', '/assets/css/front_end/bootstrap.min.css.map', '', 'assets/css', 'front_end/bootstrap.min.css.map', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-07 07:02:55', 0),
(62731, NULL, '207.241.229.160', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-07 07:02:52', 1),
(62730, NULL, '14.248.85.156', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-07 07:02:49', 1),
(62728, NULL, '173.252.87.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 05:42:29', 1),
(62729, NULL, '173.252.87.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 05:42:29', 1),
(62726, NULL, '66.220.149.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 02:01:46', 1),
(62727, NULL, '66.249.137.89', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-07 05:38:35', 1),
(62725, NULL, '66.220.149.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-07 02:01:46', 1),
(62723, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 23:41:55', 1),
(62724, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 23:41:55', 1),
(62721, NULL, '94.21.145.90', '/', 'https://brandnewblogs.com/?domain=sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', 0, '2019-08-06 18:45:33', 1),
(62722, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 23:41:54', 1),
(62720, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 16:14:09', 1),
(62718, NULL, '173.252.79.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 16:14:09', 1),
(62719, NULL, '173.252.79.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 16:14:09', 1),
(62717, NULL, '173.252.79.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 16:14:09', 1),
(62715, NULL, '173.252.79.13', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 12:42:34', 1),
(62716, NULL, '173.252.79.12', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 12:42:34', 1),
(62714, NULL, '173.252.127.37', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 10:41:46', 1),
(62712, NULL, '14.248.85.156', '/', '', 'home/index', '', 'ZaloPC-win32-24v353', 0, '2019-08-06 10:06:05', 1),
(62713, NULL, '173.252.127.47', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 10:41:45', 1),
(62710, NULL, '106.52.166.143', '/wp/wp-admin/', '', 'wp/wp-admin', '', NULL, 0, '2019-08-06 08:53:03', 1),
(62711, NULL, '31.13.115.1', '/?fbclid=IwAR2qoCrCXUMtd6iH3phsup3puEFWiJknhk9JYX9iydC1D-NMkeU2UwgLD9M', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-08-06 09:06:43', 1),
(62709, NULL, '173.252.95.5', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 08:28:43', 1),
(62707, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 06:27:27', 1),
(62708, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 06:27:27', 1),
(62705, NULL, '69.171.251.20', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 03:44:18', 1),
(62706, NULL, '69.171.251.43', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 03:44:18', 1),
(62704, NULL, '65.49.1.26', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36 OPR/54.0.2952.54', 0, '2019-08-06 01:27:23', 1),
(62703, NULL, '173.252.127.32', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-06 01:17:18', 1),
(62702, NULL, '185.234.217.42', '/.env', '', '.env/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', 0, '2019-08-06 00:30:48', 1),
(62699, NULL, '173.252.79.15', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-05 16:31:46', 1),
(62700, NULL, '89.108.99.6', '/', '', 'home/index', '', NULL, 0, '2019-08-05 20:06:18', 1),
(62701, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-05 22:38:10', 1),
(62698, NULL, '173.252.79.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-05 13:56:15', 1),
(62696, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-05 11:36:54', 1),
(62697, NULL, '104.198.196.151', '//', '', 'home/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-08-05 13:47:02', 1),
(62695, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-05 09:21:40', 1),
(62693, NULL, '173.252.95.22', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-05 04:17:12', 1),
(62694, NULL, '173.252.95.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-05 06:22:09', 1),
(62691, NULL, '66.220.149.16', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-05 01:44:46', 1),
(62692, NULL, '89.108.99.6', '/', '', 'home/index', '', NULL, 0, '2019-08-05 02:24:05', 1),
(62688, NULL, '181.214.178.250', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-04 16:15:31', 1),
(62689, NULL, '173.252.79.17', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 16:17:51', 1),
(62690, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 23:00:41', 1),
(62687, NULL, '173.252.79.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 11:56:24', 1),
(62685, NULL, '173.252.127.46', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 09:13:53', 1),
(62686, NULL, '173.252.79.12', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 11:56:24', 1),
(62683, NULL, '66.220.149.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 07:05:48', 1),
(62684, NULL, '173.252.127.19', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 09:13:53', 1),
(62682, NULL, '66.220.149.25', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 07:05:48', 1),
(62680, NULL, '173.252.95.31', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 04:11:09', 1),
(62681, NULL, '66.220.149.20', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 07:05:48', 1),
(62679, NULL, '173.252.95.22', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 04:11:09', 1),
(62677, NULL, '181.214.189.65', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-04 01:45:27', 1),
(62678, NULL, '66.220.149.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-04 01:45:32', 1),
(62675, NULL, '51.77.111.48', '/vendor/phpunit/phpunit/src/Util/PHP/kill.php', '', 'vendor/phpunit', 'phpunit/src/Util/PHP/kill.php', NULL, 0, '2019-08-03 21:23:57', 1),
(62676, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 23:34:07', 1),
(62674, NULL, '66.220.149.20', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 14:03:39', 1),
(62672, NULL, '66.220.149.15', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 14:03:39', 1),
(62673, NULL, '66.220.149.47', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 14:03:39', 1),
(62670, NULL, '66.220.149.41', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 14:03:38', 1),
(62671, NULL, '66.220.149.17', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 14:03:38', 1),
(62668, NULL, '173.252.127.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 10:23:54', 1),
(62669, NULL, '46.101.79.108', '//', '', 'home/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-08-03 12:55:57', 1),
(62666, NULL, '173.252.87.16', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 08:16:32', 1),
(62667, NULL, '45.86.244.0', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-03 08:31:02', 1),
(62664, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 05:50:35', 1),
(62665, NULL, '167.71.81.189', '/', '', 'home/index', '', 'Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; +info@netcraft.com)', 0, '2019-08-03 06:01:22', 1),
(62662, NULL, '66.220.149.45', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 02:38:07', 1),
(62663, NULL, '64.246.165.170', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:59.0) Gecko/20100101 Firefox/59.0', 0, '2019-08-03 03:22:36', 1),
(62660, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 00:19:56', 1),
(62661, NULL, '66.220.149.18', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 02:38:06', 1),
(62659, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 00:19:56', 1),
(62656, NULL, '103.28.37.16', '/wp2/wp-includes/wlwmanifest.xml', '', 'wp2/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:52:54', 1),
(62657, NULL, '160.153.154.7', '/blogs/wp-includes/wlwmanifest.xml', '', 'blogs/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:53:11', 1),
(62658, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-03 00:19:56', 1),
(62654, NULL, '184.168.193.120', '/1/wp-includes/wlwmanifest.xml', '', '1/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:52:28', 1),
(62655, NULL, '45.7.229.77', '/store/wp-includes/wlwmanifest.xml', '', 'store/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:52:35', 1),
(62652, NULL, '104.238.120.2', '/v1/wp-includes/wlwmanifest.xml', '', 'v1/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:51:55', 1),
(62653, NULL, '171.244.16.85', '/1/wp-includes/wlwmanifest.xml', '', '1/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:52:03', 1),
(62650, NULL, '160.153.147.37', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:50:40', 1),
(62651, NULL, '50.63.194.53', '/2019/wp-includes/wlwmanifest.xml', '', '2019/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:50:49', 1),
(62647, NULL, '97.74.24.224', '/newsite/wp-includes/wlwmanifest.xml', '', 'newsite/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:48:20', 1),
(62648, NULL, '184.168.193.148', '/v2/wp-includes/wlwmanifest.xml', '', 'v2/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:48:28', 1),
(62649, NULL, '167.114.84.225', '/oldsite/wp-includes/wlwmanifest.xml', '', 'oldsite/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:49:08', 1),
(62645, NULL, '101.0.93.194', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:47:33', 1),
(62646, NULL, '50.62.177.231', '/newsite/wp-includes/wlwmanifest.xml', '', 'newsite/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:47:55', 1),
(62643, NULL, '160.153.147.155', '/staging/wp-includes/wlwmanifest.xml', '', 'staging/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:46:36', 1),
(62644, NULL, '97.74.4.42', '/beta/wp-includes/wlwmanifest.xml', '', 'beta/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:46:52', 1);
INSERT INTO `site_log` (`site_log_id`, `no_of_visits`, `ip_address`, `requested_url`, `referer_page`, `page_name`, `query_string`, `user_agent`, `is_unique`, `access_date`, `visits_count`) VALUES
(62641, NULL, '160.153.153.29', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:45:55', 1),
(62642, NULL, '185.51.202.228', '/www/wp-includes/wlwmanifest.xml', '', 'www/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:46:13', 1),
(62639, NULL, '50.63.196.79', '/home/wp-includes/wlwmanifest.xml', '', 'home/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:45:14', 1),
(62640, NULL, '97.74.4.42', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:45:29', 1),
(62637, NULL, '184.168.46.216', '/web/wp-includes/wlwmanifest.xml', '', 'web/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:44:16', 1),
(62638, NULL, '89.46.105.173', '/dev/wp-includes/wlwmanifest.xml', '', 'dev/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:44:56', 1),
(62635, NULL, '185.67.103.2', '/new/wp-includes/wlwmanifest.xml', '', 'new/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:43:03', 1),
(62636, NULL, '67.227.153.5', '/en/wp-includes/wlwmanifest.xml', '', 'en/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:43:25', 1),
(62633, NULL, '89.46.106.125', '/old/wp-includes/wlwmanifest.xml', '', 'old/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:41:40', 1),
(62634, NULL, '89.46.105.235', '/backup/wp-includes/wlwmanifest.xml', '', 'backup/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:42:46', 1),
(62631, NULL, '200.98.190.62', '/blog/wp-includes/wlwmanifest.xml', '', 'blog/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:40:51', 1),
(62632, NULL, '184.168.152.208', '/site/wp-includes/wlwmanifest.xml', '', 'site/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:41:03', 1),
(62630, NULL, '198.71.239.26', '/wordpress/wp-includes/wlwmanifest.xml', '', 'wordpress/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:40:38', 1),
(62627, NULL, '107.167.82.131', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', NULL, 0, '2019-08-02 21:39:38', 1),
(62628, NULL, '185.51.202.228', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', NULL, 0, '2019-08-02 21:40:04', 1),
(62629, NULL, '45.40.164.157', '/wp/wp-includes/wlwmanifest.xml', '', 'wp/wp-includes', 'wlwmanifest.xml', NULL, 0, '2019-08-02 21:40:22', 1),
(62626, 1, '46.118.152.141', '/sito/wp-includes/wlwmanifest.xml', '', 'sito/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:04', 0),
(62625, 1, '46.118.152.141', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:03', 0),
(62624, 1, '46.118.152.141', '/site/wp-includes/wlwmanifest.xml', '', 'site/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:03', 0),
(62623, 1, '46.118.152.141', '/wp2/wp-includes/wlwmanifest.xml', '', 'wp2/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:03', 0),
(62622, 1, '46.118.152.141', '/media/wp-includes/wlwmanifest.xml', '', 'media/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:03', 0),
(62621, 1, '46.118.152.141', '/test/wp-includes/wlwmanifest.xml', '', 'test/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:02', 0),
(62620, 1, '46.118.152.141', '/wp1/wp-includes/wlwmanifest.xml', '', 'wp1/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:02', 0),
(62619, 1, '46.118.152.141', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:02', 0),
(62618, 1, '46.118.152.141', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:01', 0),
(62617, 1, '46.118.152.141', '/2017/wp-includes/wlwmanifest.xml', '', '2017/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:01', 0),
(62616, 1, '46.118.152.141', '/2016/wp-includes/wlwmanifest.xml', '', '2016/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:01', 0),
(62615, 1, '46.118.152.141', '/2015/wp-includes/wlwmanifest.xml', '', '2015/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:01', 0),
(62614, 1, '46.118.152.141', '/news/wp-includes/wlwmanifest.xml', '', 'news/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:00', 0),
(62613, 1, '46.118.152.141', '/wp/wp-includes/wlwmanifest.xml', '', 'wp/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:00', 0),
(62612, 1, '46.118.152.141', '/website/wp-includes/wlwmanifest.xml', '', 'website/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:57:00', 0),
(62611, 1, '46.118.152.141', '/wordpress/wp-includes/wlwmanifest.xml', '', 'wordpress/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:56:59', 0),
(62610, 1, '46.118.152.141', '/web/wp-includes/wlwmanifest.xml', '', 'web/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:56:59', 0),
(62608, 1, '46.118.152.141', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:56:58', 0),
(62609, 1, '46.118.152.141', '/blog/wp-includes/wlwmanifest.xml', '', 'blog/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:56:59', 0),
(62607, 1, '46.118.152.141', '/xmlrpc.php?rsd', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:56:58', 0),
(62606, 1, '46.118.152.141', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:56:58', 0),
(62604, NULL, '181.215.151.23', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-08-02 15:16:14', 1),
(62605, NULL, '46.118.152.141', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-08-02 16:56:57', 1),
(62602, NULL, '66.220.149.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 14:26:47', 1),
(62603, NULL, '173.252.79.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 14:40:28', 1),
(62601, NULL, '185.234.217.42', '/.env', '', '.env/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', 0, '2019-08-02 14:16:26', 1),
(62600, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 12:46:21', 1),
(62598, NULL, '69.171.251.31', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 10:40:10', 1),
(62599, NULL, '69.171.251.35', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 10:40:11', 1),
(62597, 1, '190.95.145.11', '/contact', 'http://sofabaolam.com', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-02 09:33:03', 0),
(62595, NULL, '69.171.251.41', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 08:17:56', 1),
(62596, NULL, '190.95.145.11', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36', 0, '2019-08-02 09:33:02', 1),
(62594, NULL, '173.252.127.38', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 07:22:11', 1),
(62593, NULL, '173.252.79.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 06:14:43', 1),
(62591, NULL, '173.252.87.17', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 03:22:32', 1),
(62592, NULL, '66.220.149.47', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 04:05:16', 1),
(62284, 1, '14.248.85.156', '/vnadmin', 'http://sofabaolam.com/vnadmin', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:09', 0),
(62283, 1, '14.248.85.156', '/favicon.ico', 'http://sofabaolam.com/vnadmin', 'favicon.ico/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:04', 0),
(62282, 1, '14.248.85.156', '/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:03', 0),
(62281, NULL, '14.248.85.156', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:25:51', 1),
(62279, NULL, '45.32.224.28', '/mysql.sql', '', 'mysql.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-23 07:05:52', 1),
(62280, NULL, '27.72.29.144', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:12:59', 1),
(62278, NULL, '66.220.149.35', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 06:54:20', 1),
(62276, NULL, '66.220.149.31', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 06:54:19', 1),
(62277, NULL, '66.220.149.46', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 06:54:20', 1),
(62275, NULL, '104.248.68.88', '/base.dump.sql', '', 'base.dump.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-23 04:23:55', 1),
(62273, NULL, '66.220.149.21', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 02:20:23', 1),
(62274, NULL, '66.220.149.15', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 02:20:23', 1),
(62272, NULL, '110.49.47.242', '/sofabaolam.com_dump.sql', '', 'sofabaolam.com_dump.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-22 23:04:58', 1),
(62269, NULL, '34.210.4.96', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-07-22 16:15:10', 1),
(62270, NULL, '45.77.119.62', '/dump.sql', '', 'dump.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-22 17:48:30', 1),
(62271, NULL, '3.105.229.76', '/sofabaolam_dump.sql', '', 'sofabaolam_dump.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-22 20:27:01', 1),
(62268, NULL, '5.187.3.155', '/copy.sql', '', 'copy.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-22 15:08:04', 1),
(62266, NULL, '35.203.245.211', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7', 0, '2019-07-22 10:47:11', 1),
(62267, NULL, '104.131.115.50', '/base.sql', '', 'base.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-22 12:28:24', 1),
(62265, NULL, '188.166.183.202', '/backup.sql', '', 'backup.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-22 09:57:23', 1),
(62264, NULL, '64.233.172.144', '/favicon.ico', '', 'favicon.ico/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-22 07:21:47', 1),
(62263, NULL, '64.233.172.146', '/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-22 07:21:46', 1),
(62261, NULL, '207.241.230.164', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-22 07:21:27', 1),
(62262, NULL, '64.233.172.148', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-22 07:21:46', 1),
(62260, NULL, '134.209.30.155', '/sofabaolam.com_backup.sql', '', 'sofabaolam.com_backup.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-22 04:54:05', 1),
(62258, NULL, '122.152.227.144', '/', '', 'home/index', '', NULL, 0, '2019-07-22 00:49:50', 1),
(62259, NULL, '110.49.47.242', '/sofabaolam_backup.sql', '', 'sofabaolam_backup.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-22 02:25:51', 1),
(62257, NULL, '104.131.103.14', '/sofabaolam.com.sql', '', 'sofabaolam.com.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-21 21:51:25', 1),
(62255, NULL, '193.124.206.194', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-21 16:21:20', 1),
(62256, NULL, '61.126.47.234', '/backup/sofabaolam.sql', '', 'backup/sofabaolam.sql', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-21 17:13:10', 1),
(62253, NULL, '190.210.9.25', '//', '', 'home/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-21 08:02:34', 1),
(62254, NULL, '192.232.225.12', '/sofabaolam.sql', '', 'sofabaolam.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-21 15:26:44', 1),
(62252, NULL, '79.191.151.114', '/', 'http://sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134', 0, '2019-07-21 06:35:28', 1),
(62589, NULL, '173.252.127.40', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 02:00:22', 1),
(62590, NULL, '173.252.87.22', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 03:22:31', 1),
(62588, NULL, '173.252.127.35', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-02 02:00:20', 1),
(62585, NULL, '66.220.149.31', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 16:56:43', 1),
(62586, NULL, '66.220.149.3', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 20:02:35', 1),
(62587, NULL, '173.252.79.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 22:45:01', 1),
(62584, NULL, '173.252.127.45', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 15:20:11', 1),
(62583, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 15:10:46', 1),
(62580, NULL, '173.252.79.3', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 14:17:30', 1),
(62581, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 15:10:44', 1),
(62582, NULL, '173.252.79.13', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 15:10:45', 1),
(62579, NULL, '66.220.149.44', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 13:54:37', 1),
(62578, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 13:31:27', 1),
(62576, NULL, '173.252.79.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 12:12:44', 1),
(62577, NULL, '173.252.79.5', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 13:31:27', 1),
(62575, NULL, '173.252.127.24', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 11:55:31', 1),
(62574, NULL, '91.197.132.69', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 0, '2019-08-01 11:26:56', 1),
(62573, NULL, '173.252.127.38', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 10:32:14', 1),
(62569, NULL, '66.220.149.21', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 09:49:40', 1),
(62570, NULL, '69.171.251.34', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 10:27:04', 1),
(62571, NULL, '173.252.95.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 10:27:05', 1),
(62572, NULL, '173.252.127.27', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 10:32:14', 1),
(62568, NULL, '66.220.149.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 09:49:40', 1),
(62566, NULL, '173.252.79.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 08:36:49', 1),
(62567, NULL, '173.252.79.3', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 09:37:28', 1),
(62564, NULL, '69.171.251.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 08:00:41', 1),
(62565, NULL, '173.252.79.3', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 08:36:49', 1),
(62563, NULL, '69.171.251.46', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 08:00:41', 1),
(62562, NULL, '173.252.127.21', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 07:29:19', 1),
(62560, NULL, '173.252.79.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 06:34:11', 1),
(62561, NULL, '64.233.172.60', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-08-01 06:52:33', 1),
(62558, NULL, '54.80.153.128', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 0, '2019-08-01 05:10:19', 1),
(62559, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 06:34:11', 1),
(62557, NULL, '178.128.233.240', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, like Gecko) Chrome/4.0.219.4 Safari/532.1', 0, '2019-08-01 04:30:38', 1),
(62554, NULL, '173.252.95.20', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 02:07:25', 1),
(62555, NULL, '173.252.127.39', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 04:29:56', 1),
(62556, NULL, '173.252.127.28', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 04:29:56', 1),
(62551, NULL, '64.233.172.62', '/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-08-01 00:30:10', 1),
(62552, NULL, '64.233.172.62', '/favicon.ico', '', 'favicon.ico/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-08-01 00:30:10', 1),
(62553, NULL, '173.252.95.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-08-01 02:07:25', 1),
(62549, NULL, '54.218.63.39', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-07-31 18:52:44', 1),
(62550, NULL, '173.252.79.3', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 23:45:23', 1),
(62548, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 15:04:29', 1),
(62546, NULL, '173.252.79.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 10:19:15', 1),
(62547, NULL, '173.252.127.13', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 11:17:13', 1),
(62541, NULL, '173.252.79.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 07:07:43', 1),
(62542, NULL, '27.72.29.144', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-31 08:50:09', 1),
(62543, NULL, '27.72.29.144', '/', '', 'home/index', '', 'ZaloPC-win32-24v352', 0, '2019-07-31 08:50:36', 1),
(62544, NULL, '120.138.68.219', '/', '', 'home/index', '', 'HTMLParser/1.6', 0, '2019-07-31 08:50:36', 1),
(62545, NULL, '173.252.79.5', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 10:19:14', 1),
(62539, NULL, '173.252.95.28', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 04:28:47', 1),
(62540, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 07:07:43', 1),
(62538, NULL, '69.171.251.27', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-31 02:23:09', 1),
(62536, NULL, '27.72.29.144', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-31 02:13:26', 1),
(62537, 1, '27.72.29.144', '/trang-chu', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-31 02:13:31', 0),
(62535, NULL, '64.233.172.58', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-31 01:12:06', 1),
(62534, NULL, '64.233.172.62', '/favicon.ico', '', 'favicon.ico/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-31 00:34:56', 1),
(62533, NULL, '64.233.172.62', '/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-31 00:34:55', 1),
(62531, NULL, '173.252.79.18', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 17:13:02', 1),
(62532, NULL, '173.252.79.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 23:52:19', 1),
(62527, NULL, '84.236.87.184', '/', 'https://brandnewblogs.com/?domain=sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', 0, '2019-07-30 12:33:07', 1),
(62528, NULL, '185.234.219.246', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36', 0, '2019-07-30 13:13:06', 1),
(62529, NULL, '173.252.79.5', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 14:38:27', 1),
(62530, NULL, '173.252.79.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 17:13:02', 1),
(62525, NULL, '173.252.127.53', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 11:44:48', 1),
(62526, NULL, '173.252.127.17', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 11:44:48', 1),
(62523, NULL, '91.210.146.194', '/404_override', 'http://sofabaolam.com/blog/', 'error/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36', 0, '2019-07-30 08:45:35', 1),
(62524, NULL, '173.252.79.13', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 09:32:47', 1),
(62522, NULL, '91.210.146.194', '/blog/', 'http://sofabaolam.com/blog/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36', 0, '2019-07-30 08:45:34', 1),
(62518, NULL, '66.220.149.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 04:25:30', 1),
(62519, NULL, '173.252.79.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 07:01:44', 1),
(62520, NULL, '173.252.79.16', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 07:01:44', 1),
(62521, NULL, '91.210.146.194', '/', 'http://sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36', 0, '2019-07-30 08:45:18', 1),
(62516, NULL, '51.77.246.201', '/', '', 'home/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-30 04:05:11', 1),
(62517, NULL, '66.220.149.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 04:25:30', 1),
(62515, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 02:19:21', 1),
(62514, NULL, '64.233.172.58', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-30 00:50:02', 1),
(62512, NULL, '173.252.79.14', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 00:08:03', 1),
(62513, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-30 00:08:03', 1),
(62510, NULL, '66.220.149.13', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 20:27:22', 1),
(62511, NULL, '66.220.149.18', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 20:27:22', 1),
(62506, NULL, '66.220.149.34', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 14:16:41', 1),
(62507, NULL, '173.252.127.38', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 16:54:31', 1),
(62508, NULL, '66.220.149.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 20:27:22', 1),
(62509, NULL, '66.220.149.37', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 20:27:22', 1),
(62505, NULL, '66.220.149.30', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 14:16:40', 1),
(62504, NULL, '66.220.149.34', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 11:55:12', 1),
(62502, NULL, '66.220.149.3', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 07:11:48', 1),
(62503, NULL, '69.171.251.17', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 09:52:42', 1),
(62501, NULL, '66.220.149.40', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 07:11:47', 1),
(62499, NULL, '158.69.26.193', '/ads.txt', '', 'ads.txt/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:24', 1),
(62500, NULL, '158.69.26.193', '/', '', 'home/index', '', 'Mozilla/5.0 (Linux; Android 5.1.1; SM-G925F Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.94 Mobile Safari/537.36', 0, '2019-07-29 05:13:25', 1),
(62497, 1, '158.69.26.193', '/danh-muc/sofa-khach-san.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:20', 0),
(62498, 1, '158.69.26.193', '/san-pham/sofa-karaoke-ma-sk812.html', '', 'products/detail', 'raoke-ma-sk812', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:22', 0),
(62496, 1, '158.69.26.193', '/zalo:0972.050.202', '', 'zalo:0972.050.202/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:19', 0),
(62493, 1, '158.69.26.193', '/nhung-luu-y-cuc-quan-trong-khi-chon-sofa-cho-gia-dinh.html', '', 'home/index', '-cuc-quan-trong-khi-chon-sofa-cho-gia-dinh', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:12', 0),
(62494, 1, '158.69.26.193', '/san-pham/sofa-karaoke-ma-sk817.html', '', 'products/detail', 'raoke-ma-sk817', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:14', 0),
(62495, 1, '158.69.26.193', '/danh-muc/sofa-cafe.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:15', 0),
(62492, 1, '158.69.26.193', '/chon-sofa-cho-phong-karaoke-sao-cho-dung-cach.html', '', 'home/index', 'ho-phong-karaoke-sao-cho-dung-cach', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:11', 0),
(62491, 1, '158.69.26.193', '/san-pham/sofa-ni-gia-dinh-ma-gd103.html', '', 'products/detail', '-gia-dinh-ma-gd103', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:08', 0),
(62485, NULL, '173.252.79.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 01:53:07', 1),
(62486, NULL, '173.252.79.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 01:53:08', 1),
(62487, NULL, '173.252.95.52', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 04:01:09', 1),
(62488, NULL, '173.252.95.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-29 04:01:10', 1),
(62489, NULL, '158.69.26.193', '/', '', 'home/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:12:59', 1),
(62490, 1, '158.69.26.193', '/contact', '', 'contact/index', '', 'Mozilla/5.0 (compatible; Dataprovider.com)', 0, '2019-07-29 05:13:07', 0),
(62483, NULL, '64.233.172.60', '/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-29 00:42:18', 1),
(62484, NULL, '64.233.172.60', '/favicon.ico', '', 'favicon.ico/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-29 00:42:18', 1),
(62482, NULL, '64.233.172.62', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-29 00:42:17', 1),
(62481, NULL, '66.220.149.27', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 22:18:01', 1),
(62479, NULL, '66.220.149.23', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 22:18:01', 1),
(62480, NULL, '66.220.149.44', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 22:18:01', 1),
(62478, NULL, '51.68.47.222', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-28 15:26:45', 1),
(62477, NULL, '173.252.95.29', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 15:23:39', 1),
(62474, NULL, '173.252.79.9', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 10:45:07', 1),
(62475, NULL, '173.252.79.15', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 10:45:07', 1),
(62476, NULL, '66.220.149.18', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 13:03:55', 1),
(62471, NULL, '173.252.95.26', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 04:21:37', 1),
(62472, NULL, '185.234.219.246', '/contact', '', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0', 0, '2019-07-28 04:37:15', 1),
(62473, NULL, '173.252.95.17', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 07:31:40', 1),
(62469, NULL, '69.171.251.50', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 01:13:38', 1),
(62470, NULL, '69.171.251.20', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-28 01:13:38', 1),
(62466, NULL, '173.252.79.16', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-27 13:24:46', 1),
(62467, NULL, '66.220.149.46', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-27 15:28:33', 1),
(62468, NULL, '173.252.79.14', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-27 22:59:54', 1),
(62465, NULL, '173.252.127.34', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-27 10:24:54', 1),
(62464, NULL, '173.252.127.13', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-27 10:24:53', 1),
(62463, NULL, '64.233.172.58', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-27 08:13:22', 1),
(62461, NULL, '173.252.127.36', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-27 07:51:55', 1),
(62462, NULL, '173.252.127.35', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-27 07:51:55', 1),
(62459, NULL, '46.4.33.48', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208', 0, '2019-07-27 00:38:00', 1),
(62460, NULL, '66.220.149.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-27 02:10:14', 1),
(62458, 1, '185.93.3.114', '/contact', 'http://sofabaolam.com/contact', 'contact/index', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36 Kinza/4.7.2', 0, '2019-07-27 00:36:29', 0),
(62457, NULL, '185.93.3.114', '/', 'http://sofabaolam.com/', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36 Kinza/4.7.2', 0, '2019-07-27 00:36:28', 1),
(62456, NULL, '173.252.79.14', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 23:26:01', 1),
(62452, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 16:20:09', 1),
(62453, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 16:20:10', 1),
(62454, NULL, '173.252.79.3', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 23:26:01', 1),
(62455, NULL, '173.252.79.1', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 23:26:01', 1),
(62450, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 12:17:40', 1),
(62451, NULL, '54.203.16.82', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-07-26 15:22:53', 1),
(62447, NULL, '52.89.222.143', '/', '', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 0, '2019-07-26 10:20:39', 1),
(62448, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 12:17:40', 1),
(62449, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 12:17:40', 1),
(62445, NULL, '173.252.127.38', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 07:14:25', 1),
(62446, NULL, '173.252.95.50', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 09:36:18', 1),
(62444, NULL, '173.252.127.46', '/?fbclid=IwAR1mMbkxM0fnDhTINboOtgpy78oNiAe6G1T5QOEeZtrx_EpcKrZ7BnbOpu8', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134', 0, '2019-07-26 07:02:02', 1),
(62442, NULL, '64.233.172.60', '/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-26 06:50:51', 1),
(62443, NULL, '64.233.172.62', '/favicon.ico', '', 'favicon.ico/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-26 06:50:52', 1),
(62440, NULL, '173.252.95.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 02:06:45', 1),
(62441, NULL, '173.252.127.19', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 04:34:44', 1),
(62438, NULL, '173.252.79.15', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 23:47:29', 1),
(62439, NULL, '173.252.95.20', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-26 02:06:45', 1),
(62436, NULL, '173.252.79.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 12:00:15', 1),
(62437, NULL, '173.252.79.4', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 15:06:21', 1),
(62433, NULL, '93.159.230.28', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 0, '2019-07-25 08:54:56', 1),
(62434, NULL, '69.171.251.50', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 09:26:06', 1),
(62435, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 12:00:15', 1),
(62432, NULL, '64.233.172.60', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-25 07:19:20', 1),
(62431, NULL, '173.252.79.7', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 06:45:49', 1),
(62429, NULL, '173.252.79.8', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 06:45:49', 1),
(62430, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 06:45:49', 1),
(62427, 1, '180.169.182.50', '/sito/wp-includes/wlwmanifest.xml', '', 'sito/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:43', 0),
(62428, NULL, '173.252.79.12', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 06:45:49', 1),
(62425, 1, '180.169.182.50', '/site/wp-includes/wlwmanifest.xml', '', 'site/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:43', 0),
(62426, 1, '180.169.182.50', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:43', 0),
(62423, 1, '180.169.182.50', '/media/wp-includes/wlwmanifest.xml', '', 'media/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:42', 0),
(62424, 1, '180.169.182.50', '/wp2/wp-includes/wlwmanifest.xml', '', 'wp2/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:43', 0),
(62422, 1, '180.169.182.50', '/test/wp-includes/wlwmanifest.xml', '', 'test/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:42', 0),
(62421, 1, '180.169.182.50', '/wp1/wp-includes/wlwmanifest.xml', '', 'wp1/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:42', 0),
(62420, 1, '180.169.182.50', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:42', 0),
(62418, 1, '180.169.182.50', '/2017/wp-includes/wlwmanifest.xml', '', '2017/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:42', 0),
(62419, 1, '180.169.182.50', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:42', 0),
(62417, 1, '180.169.182.50', '/2016/wp-includes/wlwmanifest.xml', '', '2016/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:42', 0),
(62416, 1, '180.169.182.50', '/2015/wp-includes/wlwmanifest.xml', '', '2015/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:41', 0),
(62415, 1, '180.169.182.50', '/news/wp-includes/wlwmanifest.xml', '', 'news/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:41', 0),
(62414, 1, '180.169.182.50', '/wp/wp-includes/wlwmanifest.xml', '', 'wp/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:41', 0),
(62413, 1, '180.169.182.50', '/website/wp-includes/wlwmanifest.xml', '', 'website/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:41', 0);
INSERT INTO `site_log` (`site_log_id`, `no_of_visits`, `ip_address`, `requested_url`, `referer_page`, `page_name`, `query_string`, `user_agent`, `is_unique`, `access_date`, `visits_count`) VALUES
(62412, 1, '180.169.182.50', '/wordpress/wp-includes/wlwmanifest.xml', '', 'wordpress/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:41', 0),
(62410, 1, '180.169.182.50', '/blog/wp-includes/wlwmanifest.xml', '', 'blog/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:40', 0),
(62411, 1, '180.169.182.50', '/web/wp-includes/wlwmanifest.xml', '', 'web/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:41', 0),
(62409, 1, '180.169.182.50', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:40', 0),
(62406, NULL, '180.169.182.50', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:40', 1),
(62407, 1, '180.169.182.50', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:40', 0),
(62408, 1, '180.169.182.50', '/xmlrpc.php?rsd', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-25 06:45:40', 0),
(62405, NULL, '129.211.98.123', '/?xxnew2018_url2=x&xxnew2018_url1=x', '', 'home/index', '', NULL, 0, '2019-07-25 04:06:44', 1),
(62403, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 01:07:44', 1),
(62404, NULL, '66.220.149.30', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-25 03:27:46', 1),
(62402, 1, '104.192.74.231', '/page/gioi-thieu', '', 'pages/page_content', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 0, '2019-07-24 23:43:03', 0),
(62401, 1, '104.192.74.231', '/404_override', '', 'error/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 0, '2019-07-24 23:42:48', 0),
(62400, 1, '104.192.74.231', '/danh-muc/sofa-ni.html', '', 'products/pro_bycategory', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 0, '2019-07-24 23:42:47', 0),
(62399, 1, '104.192.74.231', '/san-pham/sofa-karaoke-ma-sk811.html', '', 'products/detail', 'raoke-ma-sk811', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 0, '2019-07-24 23:42:14', 0),
(62397, NULL, '104.192.74.231', '/contact', '', 'contact/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 0, '2019-07-24 23:41:53', 1),
(62398, NULL, '104.192.74.231', '/trang-chu', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 0, '2019-07-24 23:41:54', 1),
(62396, NULL, '104.192.74.231', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 0, '2019-07-24 23:41:08', 1),
(62395, NULL, '104.192.74.231', '/', 'http://sofabaolam.com', 'home/index', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', 0, '2019-07-24 23:41:05', 1),
(62394, NULL, '173.252.83.6', '/san-pham/sofa-khach-san-ma-ks208.html', '', 'products/detail', 'ach-san-ma-ks208', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 23:17:39', 1),
(62393, NULL, '173.252.83.14', '/san-pham/sofa-khach-san-ma-ks208.html', '', 'products/detail', 'ach-san-ma-ks208', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 23:17:38', 1),
(62392, NULL, '173.252.79.2', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 23:04:48', 1),
(62390, 1, '180.169.182.50', '/cms/wp-includes/wlwmanifest.xml', '', 'cms/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:12', 0),
(62391, 1, '180.169.182.50', '/sito/wp-includes/wlwmanifest.xml', '', 'sito/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:12', 0),
(62389, 1, '180.169.182.50', '/site/wp-includes/wlwmanifest.xml', '', 'site/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:12', 0),
(62388, 1, '180.169.182.50', '/wp2/wp-includes/wlwmanifest.xml', '', 'wp2/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:11', 0),
(62387, 1, '180.169.182.50', '/media/wp-includes/wlwmanifest.xml', '', 'media/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:11', 0),
(62385, 1, '180.169.182.50', '/wp1/wp-includes/wlwmanifest.xml', '', 'wp1/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:11', 0),
(62386, 1, '180.169.182.50', '/test/wp-includes/wlwmanifest.xml', '', 'test/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:11', 0),
(62384, 1, '180.169.182.50', '/shop/wp-includes/wlwmanifest.xml', '', 'shop/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:11', 0),
(62383, 1, '180.169.182.50', '/2018/wp-includes/wlwmanifest.xml', '', '2018/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:11', 0),
(62382, 1, '180.169.182.50', '/2017/wp-includes/wlwmanifest.xml', '', '2017/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:11', 0),
(62381, 1, '180.169.182.50', '/2016/wp-includes/wlwmanifest.xml', '', '2016/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:11', 0),
(62380, 1, '180.169.182.50', '/2015/wp-includes/wlwmanifest.xml', '', '2015/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:10', 0),
(62379, 1, '180.169.182.50', '/news/wp-includes/wlwmanifest.xml', '', 'news/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:10', 0),
(62378, 1, '180.169.182.50', '/wp/wp-includes/wlwmanifest.xml', '', 'wp/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:10', 0),
(62376, 1, '180.169.182.50', '/wordpress/wp-includes/wlwmanifest.xml', '', 'wordpress/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:10', 0),
(62377, 1, '180.169.182.50', '/website/wp-includes/wlwmanifest.xml', '', 'website/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:10', 0),
(62375, 1, '180.169.182.50', '/web/wp-includes/wlwmanifest.xml', '', 'web/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:10', 0),
(62374, 1, '180.169.182.50', '/blog/wp-includes/wlwmanifest.xml', '', 'blog/wp-includes', 'wlwmanifest.xml', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:10', 0),
(62372, 1, '180.169.182.50', '/xmlrpc.php?rsd', '', 'xmlrpc.php/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:09', 0),
(62373, 1, '180.169.182.50', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:09', 0),
(62370, NULL, '180.169.182.50', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:09', 1),
(62371, 1, '180.169.182.50', '/wp-includes/wlwmanifest.xml', '', 'wp-includes/wlwmanifest.xml', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 0, '2019-07-24 22:48:09', 0),
(62369, NULL, '173.252.127.55', '/san-pham/sofa-cafe-ma-sc616.html', '', 'products/detail', 'fe-ma-sc616', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:33:22', 1),
(62368, NULL, '173.252.127.44', '/san-pham/sofa-cafe-ma-sc616.html', '', 'products/detail', 'fe-ma-sc616', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:33:21', 1),
(62366, NULL, '173.252.95.9', '/san-pham/sofa-khach-san-ma-ks207.html', '', 'products/detail', 'ach-san-ma-ks207', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:30:07', 1),
(62367, NULL, '173.252.95.22', '/san-pham/sofa-khach-san-ma-ks207.html', '', 'products/detail', 'ach-san-ma-ks207', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:30:08', 1),
(62365, NULL, '173.252.95.34', '/san-pham/sofa-gia-dinh-ma-gd106.html', '', 'products/detail', 'a-dinh-ma-gd106', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:26:48', 1),
(62362, NULL, '173.252.83.2', '/san-pham/sofa-da-ma-sd312.html', '', 'products/detail', '-ma-sd312', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:26:46', 1),
(62363, NULL, '173.252.95.10', '/san-pham/sofa-gia-dinh-ma-gd106.html', '', 'products/detail', 'a-dinh-ma-gd106', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:26:46', 1),
(62364, NULL, '173.252.83.1', '/san-pham/sofa-da-ma-sd312.html', '', 'products/detail', '-ma-sd312', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:26:47', 1),
(62361, NULL, '173.252.79.6', '/san-pham/sofa-khach-san-ma-ks202.html', '', 'products/detail', 'ach-san-ma-ks202', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:26:36', 1),
(62360, NULL, '173.252.79.8', '/san-pham/sofa-khach-san-ma-ks202.html', '', 'products/detail', 'ach-san-ma-ks202', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:26:29', 1),
(62358, NULL, '173.252.87.21', '/san-pham/sofa-khach-san-ma-ks203.html', '', 'products/detail', 'ach-san-ma-ks203', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:49', 1),
(62359, NULL, '173.252.87.12', '/san-pham/sofa-khach-san-ma-ks203.html', '', 'products/detail', 'ach-san-ma-ks203', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:50', 1),
(62355, NULL, '173.252.127.20', '/san-pham/sofa-karaoke-ma-sk816.html', '', 'products/detail', 'raoke-ma-sk816', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:46', 1),
(62356, NULL, '173.252.127.20', '/san-pham/sofa-karaoke-ma-sk816.html', '', 'products/detail', 'raoke-ma-sk816', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:47', 1),
(62357, NULL, '173.252.127.55', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'cortex/1.0', 0, '2019-07-24 20:23:49', 1),
(62352, NULL, '173.252.87.4', '/san-pham/sofa-karaoke-ma-sk813.html', '', 'products/detail', 'raoke-ma-sk813', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:25', 1),
(62354, NULL, '69.171.251.9', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'cortex/1.0', 0, '2019-07-24 20:23:30', 1),
(62353, NULL, '69.171.251.1', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'cortex/1.0', 0, '2019-07-24 20:23:28', 1),
(62351, NULL, '173.252.95.40', '/san-pham/sofa-karaoke-ma-sk819.html', '', 'products/detail', 'raoke-ma-sk819', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:18', 1),
(62350, NULL, '173.252.95.41', '/san-pham/sofa-karaoke-ma-sk819.html', '', 'products/detail', 'raoke-ma-sk819', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:17', 1),
(62349, NULL, '173.252.83.9', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'cortex/1.0', 0, '2019-07-24 20:23:15', 1),
(62347, NULL, '173.252.87.3', '/san-pham/sofa-cao-cap-ma-cp518.html', '', 'products/detail', 'o-cap-ma-cp518', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:12', 1),
(62348, NULL, '173.252.83.5', '/san-pham/sofa-cao-cap-ma-cp519.html', '', 'products/detail', 'o-cap-ma-cp519', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:12', 1),
(62345, NULL, '173.252.87.2', '/san-pham/sofa-cao-cap-ma-cp518.html', '', 'products/detail', 'o-cap-ma-cp518', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:11', 1),
(62346, NULL, '173.252.83.5', '/san-pham/sofa-cao-cap-ma-cp519.html', '', 'products/detail', 'o-cap-ma-cp519', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:12', 1),
(62344, NULL, '173.252.79.3', '/san-pham/upload/img/logo/logo_left21.png', '', 'san-pham/upload', 'img/logo/logo_left21.png', 'cortex/1.0', 0, '2019-07-24 20:23:08', 1),
(62342, NULL, '173.252.127.17', '/san-pham/sofa-cafe-ma-sc614.html', '', 'products/detail', 'fe-ma-sc614', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:02', 1),
(62343, NULL, '173.252.79.11', '/san-pham/sofa-cao-cap-ma-cp512.html', '', 'products/detail', 'o-cap-ma-cp512', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:05', 1),
(62341, NULL, '173.252.79.4', '/san-pham/sofa-cao-cap-ma-cp512.html', '', 'products/detail', 'o-cap-ma-cp512', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:23:01', 1),
(62340, NULL, '173.252.83.11', '/san-pham/sofa-cafe-ma-sc619.html', '', 'products/detail', 'fe-ma-sc619', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:54', 1),
(62338, NULL, '173.252.87.40', '/san-pham/sofa-da-ma-sd315.html', '', 'products/detail', '-ma-sd315', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:49', 1),
(62339, NULL, '173.252.83.7', '/san-pham/sofa-cafe-ma-sc619.html', '', 'products/detail', 'fe-ma-sc619', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:53', 1),
(62337, NULL, '173.252.87.22', '/san-pham/sofa-da-ma-sd315.html', '', 'products/detail', '-ma-sd315', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:48', 1),
(62336, NULL, '173.252.95.27', '/san-pham/sofa-da-ma-sd311.html', '', 'products/detail', '-ma-sd311', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:45', 1),
(62335, NULL, '173.252.95.27', '/san-pham/sofa-da-ma-sd311.html', '', 'products/detail', '-ma-sd311', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:45', 1),
(62334, NULL, '173.252.87.34', '/san-pham/sofa-cafe-ma-sc611.html', '', 'products/detail', 'fe-ma-sc611', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:29', 1),
(62333, NULL, '173.252.87.30', '/san-pham/sofa-cafe-ma-sc611.html', '', 'products/detail', 'fe-ma-sc611', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:28', 1),
(62332, NULL, '173.252.127.33', '/san-pham/sofa-gia-dinh-ma-gd-112.html', '', 'products/detail', 'a-dinh-ma-gd-112', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:21', 1),
(62331, NULL, '173.252.127.40', '/san-pham/sofa-gia-dinh-ma-gd-112.html', '', 'products/detail', 'a-dinh-ma-gd-112', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 20:22:20', 1),
(62330, NULL, '66.220.149.31', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 18:54:55', 1),
(62329, NULL, '18.237.45.55', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-07-24 18:47:35', 1),
(62328, NULL, '173.252.79.10', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 12:51:23', 1),
(62327, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 10:42:44', 1),
(62326, NULL, '173.252.79.5', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 07:54:33', 1),
(62325, NULL, '64.233.172.60', '/favicon.ico', '', 'favicon.ico/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-24 06:44:23', 1),
(62324, NULL, '64.233.172.60', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-24 06:44:22', 1),
(62323, NULL, '64.233.172.62', '/vnadmin', '', 'defaults/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-24 06:44:22', 1),
(62322, NULL, '173.252.95.23', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 04:25:16', 1),
(62320, NULL, '173.252.87.31', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 02:11:03', 1),
(62321, NULL, '173.252.95.20', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 04:25:16', 1),
(62319, NULL, '173.252.87.16', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-24 02:11:03', 1),
(62318, NULL, '14.248.85.156', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-24 01:40:06', 1),
(62317, NULL, '66.220.149.41', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 22:16:56', 1),
(62316, NULL, '54.202.51.217', '/', '', 'home/index', '', 'Go-http-client/1.1', 0, '2019-07-23 19:25:25', 1),
(62315, NULL, '173.252.79.11', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 17:09:02', 1),
(62314, NULL, '66.220.149.44', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 14:30:37', 1),
(62313, NULL, '66.220.149.33', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 14:30:37', 1),
(62312, NULL, '66.220.149.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 14:30:37', 1),
(62311, NULL, '37.187.158.132', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-23 12:52:24', 1),
(62310, NULL, '66.220.149.6', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 11:39:28', 1),
(62309, NULL, '66.220.149.18', '/contact', '', 'contact/index', '', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 0, '2019-07-23 11:39:28', 1),
(62308, NULL, '70.32.28.60', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5', 0, '2019-07-23 10:13:27', 1),
(62307, NULL, '216.172.183.202', '/wp-login.php', '', 'wp-login.php/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-23 10:12:01', 1),
(62306, NULL, '112.74.177.254', '/copy.sql', '', 'copy.sql/index', '', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0', 0, '2019-07-23 09:53:47', 1),
(62304, 1, '14.248.85.156', '/', '', 'home/index', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:31:36', 0),
(62305, NULL, '64.233.172.148', '/', '', 'home/index', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 0, '2019-07-23 08:17:13', 1),
(62303, 1, '14.248.85.156', '/img/noimage.png', 'http://sofabaolam.com/vnadmin/support/listSuport', 'img/noimage.png', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:31:32', 0),
(62302, 1, '14.248.85.156', '/img/en.gif', 'http://sofabaolam.com/vnadmin/support/listSuport', 'img/en.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:31:32', 0),
(62301, 1, '14.248.85.156', '/img/vi.gif', 'http://sofabaolam.com/vnadmin/support/listSuport', 'img/vi.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:31:32', 0),
(62300, 1, '14.248.85.156', '/vnadmin/support/listSuport', 'http://sofabaolam.com/vnadmin/menu/edit/80?p=bottom', 'support/listSuport', 'tSuport', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:31:31', 0),
(62299, 1, '14.248.85.156', '/img/noimage.png', 'http://sofabaolam.com/vnadmin/support/listSuport', 'img/noimage.png', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:55', 0),
(62298, 1, '14.248.85.156', '/img/vi.gif', 'http://sofabaolam.com/vnadmin/support/listSuport', 'img/vi.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:55', 0),
(62297, 1, '14.248.85.156', '/img/en.gif', 'http://sofabaolam.com/vnadmin/support/listSuport', 'img/en.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:55', 0),
(62296, 1, '14.248.85.156', '/vnadmin/support/listSuport', 'http://sofabaolam.com/vnadmin/menu/edit/80?p=bottom', 'support/listSuport', 'tSuport', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:55', 0),
(62295, 1, '14.248.85.156', '/img/noimage.png', 'http://sofabaolam.com/vnadmin/menu/edit/80?p=bottom', 'img/noimage.png', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:46', 0),
(62294, 1, '14.248.85.156', '/img/en.gif', 'http://sofabaolam.com/vnadmin/menu/edit/80?p=bottom', 'img/en.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:46', 0),
(62293, 1, '14.248.85.156', '/img/vi.gif', 'http://sofabaolam.com/vnadmin/menu/edit/80?p=bottom', 'img/vi.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:46', 0),
(62292, 1, '14.248.85.156', '/vnadmin/menu/edit/80?p=bottom', 'http://sofabaolam.com/vnadmin/menu/menulist', 'menu/edit', 'nu/edit/80', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:46', 0),
(62291, 1, '14.248.85.156', '/vnadmin/menu/active_tab', 'http://sofabaolam.com/vnadmin/menu/menulist', 'menu/active_tab', 'ive_tab', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:15', 0),
(62290, 1, '14.248.85.156', '/vnadmin/menu/savelist', 'http://sofabaolam.com/vnadmin/menu/menulist', 'menu/savelist', 'avelist', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:14', 0),
(62289, 1, '14.248.85.156', '/img/en.gif', 'http://sofabaolam.com/vnadmin/menu/menulist', 'img/en.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:14', 0),
(62288, 1, '14.248.85.156', '/img/vi.gif', 'http://sofabaolam.com/vnadmin/menu/menulist', 'img/vi.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:14', 0),
(62287, 1, '14.248.85.156', '/vnadmin/menu/menulist', 'http://sofabaolam.com/vnadmin', 'menu/menulist', 'enulist', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:14', 0),
(62286, 1, '14.248.85.156', '/img/en.gif', 'http://sofabaolam.com/vnadmin', 'img/en.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:10', 0),
(62285, 1, '14.248.85.156', '/img/vi.gif', 'http://sofabaolam.com/vnadmin', 'img/vi.gif', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 0, '2019-07-23 07:26:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_option`
--

CREATE TABLE `site_option` (
  `id` int(11) NOT NULL,
  `coppy_right` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `site_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `slogan` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `link_instagram` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `site_logo` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `site_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name_language` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `site_keyword` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_keyword_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link_sky` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link_printer` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `link_linkedin` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `site_email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `site_fanpage` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_video` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `WM_text` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `WM_color` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `WM_size` int(10) DEFAULT NULL,
  `hotline1` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `hotline2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `hotline3` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `link_tt` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `favicon` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `company_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `shipping` text CHARACTER SET utf8,
  `site_promo` text CHARACTER SET utf8,
  `thanhtoan_tienmat` text CHARACTER SET utf8,
  `thanhtoan_chuyenkhoan` text CHARACTER SET utf8,
  `hdfMap` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `map_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `map_adrdress` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `map_phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `dia_chi_timkiem` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT '1',
  `link_gg` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_youtube` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `face_id` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timeopen` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `chat` text CHARACTER SET utf8,
  `site_logo_footer` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `map_iframe` text CHARACTER SET utf8,
  `input_text_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_footer` text COLLATE utf8_unicode_ci,
  `icon_language` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `config_pro` text COLLATE utf8_unicode_ci NOT NULL,
  `config_pro_content` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(3) UNSIGNED DEFAULT '1',
  `bando` text COLLATE utf8_unicode_ci,
  `watermark` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_option`
--

INSERT INTO `site_option` (`id`, `coppy_right`, `site_name`, `slogan`, `link_instagram`, `site_logo`, `site_title`, `name_language`, `site_keyword`, `site_keyword_en`, `site_description`, `link_sky`, `link_printer`, `link_linkedin`, `site_email`, `site_fanpage`, `site_video`, `WM_text`, `WM_color`, `WM_size`, `hotline1`, `hotline2`, `hotline3`, `address`, `link_tt`, `favicon`, `company_name`, `shipping`, `site_promo`, `thanhtoan_tienmat`, `thanhtoan_chuyenkhoan`, `hdfMap`, `map_title`, `map_adrdress`, `map_phone`, `dia_chi_timkiem`, `lang`, `link_gg`, `link_youtube`, `face_id`, `timeopen`, `chat`, `site_logo_footer`, `map_iframe`, `input_text_1`, `domain`, `map_footer`, `icon_language`, `config_pro`, `config_pro_content`, `active`, `bando`, `watermark`) VALUES
(1, NULL, 'CÔNG TY NỘI THẤT BẢO LONG', '', 'pqcuong1995', 'upload/img/logo/logo_left21.png', 'CÔNG TY NỘI THẤT BẢO LONG', 'Việt Nam', '', '0', '', '', NULL, NULL, 'tranlinh.manh@gmail.com', 'BaoLongDesigner', 'lU02XJYXgK8', NULL, NULL, NULL, '0963761678', '0972050202', NULL, ' Địa chỉ: 130 Hoàng Công Chất, P. Phú Diễn, Q. Bắc Từ Liêm, Tp. Hà Nội', '', 'upload/img/logo/logo_left22.png', NULL, '<p>&nbsp;</p>\r\n\r\n<p>Tế b&agrave;o gốc l&agrave; tế b&agrave;o mầm hay tế b&agrave;o nền m&oacute;ng m&agrave; từ đ&oacute; c&aacute;c loại tế b&agrave;o của cơ thể con người được tạo ra. Mọi tế b&agrave;o trong cơ thể người đều được tạo ra từ tế b&agrave;o nền m&oacute;ng m&agrave; từ đ&oacute; c&aacute;c loại tế b&agrave;o của cơ thể con người được tạo ra.</p>\r\n', NULL, NULL, NULL, '(,)', 'Công ty nội thất Bảo Lâm', '130 Hoàng Công Chất, Cầu Diễn, Bắc Từ Liêm, Hà Nội', '0963761678', '130 Hoàng Công Chất, Cầu Diễn, Bắc Từ Liêm, Hà Nội', 'vi', '', NULL, NULL, NULL, NULL, 'upload/img/logo/logo1.png', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.4672661068253!2d105.76712161501895!3d21.013981586006356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3134535816bfae5d%3A0xc352638ab10225d4!2zQ-G7lSBwaOG6p24gQ8O0bmcgbmdo4buHIFFUUw!5e0!3m2!1svi!2s!4v1529901490962\" width=\"100%\" height=\"200\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', NULL, '', NULL, 'img/vi.gif', '[]', '[{\"content\":\"\",\"name\":\"Chi ti\\u1ebft s\\u1ea3n ph\\u1ea9m\",\"type\":\"textarea\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Nh\\u00e0 s\\u1ea3n xu\\u1ea5t\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"K\\u00edch th\\u01b0\\u1edbc\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Xu\\u1ea5t x\\u1ee9\",\"type\":\"text\",\"sort\":\"\"},{\"content\":\"\",\"name\":\"Ch\\u1ee9ng nh\\u1eadn\",\"type\":\"text\",\"sort\":\"\"}]', 1, NULL, '0'),
(2, NULL, 'JSC polygon media', '', NULL, 'upload/img/logo4.png', '', 'English', '', '0', '', '0', NULL, NULL, 'hanhnh@polygonmedia.vn', '', 'uI2wcf05wq0', '', '', 0, '', '', '0', '', '', '0', NULL, '', '', NULL, NULL, '(21.0218044, 105.79087200000004)', 'Công ty', '', '', 'Yên hòa', 'en', '', '', '', '', '', NULL, '', NULL, '', '', 'img/en.gif', '[]', '', 1, NULL, '0'),
(3, '0', '1', '1', '1', '1', '1', '', '1', '1', '1', '1', '0', '0', '1', '1', '1', '0', '0', 0, '1', '1', '0', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1', 'config', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '', '1', 0, NULL, '0'),
(4, 'Coppy right', 'tên đơn vị', 'Slogan', 'Zalo', NULL, 'Tiêu đề website', NULL, NULL, NULL, NULL, 'Link skype', 'link printer', 'Link linkedin', NULL, 'Fanpage Facebook', 'Video (Youtube)', 'Chữ Nổi Warter Mark', 'Màu Chữ (Hex Color VD : #ed1c2', 1, 'Hotline', 'Hotline 2', 'điên thoại bàn', 'Địa chỉ', 'Link twitter', NULL, NULL, 'Thông tin chân trang', 'khuyến mại', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'conf_text', 'Link google', 'Link youtube', 'id ap facebook', 'Thời gian mở cửa', 'mã chát online', 'logo chân trang', 'Mã nhúng bản đồ chân trang', 'padding text', 'Tên miền', 'mã nhúng javascript', '', '', '', 0, NULL, 'Đóng dấu logo'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, 'japanese', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ja', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', '', 'upload/img/logo/ja4.jpg', '[]', '', 0, NULL, '0'),
(6, NULL, NULL, NULL, NULL, NULL, NULL, 'Korean', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ko', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', '', 'upload/img/logo/lag21.png', '[]', '', 0, NULL, '0'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, 'hungary', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hu', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', '', 'upload/img/logo/hungary.png', '[]', '', 0, NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `staticpage`
--

CREATE TABLE `staticpage` (
  `id` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` char(10) COLLATE utf8_unicode_ci DEFAULT '1',
  `home` tinyint(1) DEFAULT '0',
  `hot` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `contact_page` tinyint(1) DEFAULT '0',
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `page_footer` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staticpage`
--

INSERT INTO `staticpage` (`id`, `name`, `alias`, `description`, `content`, `image`, `lang`, `home`, `hot`, `focus`, `contact_page`, `title_seo`, `keyword_seo`, `description_seo`, `parent_id`, `page_footer`) VALUES
(31, 'Giới Thiệu', 'gioi-thieu', '<p style=\"text-align:justify\">Lấy cảm hứng từ nguồn năng lượng &aacute;nh s&aacute;ng của vũ trụ. BẢO LONG đ&atilde; thổi v&agrave;o c&aacute;c thiết kế sự huyền diệu, lung linh bằng hệ thống LED,MOVING hiện đại, kỹ xảo ưu việt, cho thượng đế cảm gi&aacute;c mỗi kh&aacute;n ph&ograve;ng như l&agrave; 1 s&acirc;n khấu &aacute;nh s&aacute;ng thực sự.&nbsp;</p>\r\n', '<p>Lấy cảm hứng từ nguồn năng lượng &aacute;nh s&aacute;ng của vũ trụ. BẢO LONG đ&atilde; thổi v&agrave;o c&aacute;c thiết kế sự huyền diệu, lung linh bằng hệ thống LED,MOVING hiện đại, kỹ xảo ưu việt, cho thượng đế cảm gi&aacute;c mỗi kh&aacute;n ph&ograve;ng như l&agrave; 1 s&acirc;n khấu &aacute;nh s&aacute;ng thực sự.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>L&agrave; một trong những đơn vị h&agrave;ng đầu trong ngh&agrave;nh&nbsp;SẢN XUẤT, GIA C&Ocirc;NG SẢN PHẨM INOX&nbsp;về quy m&ocirc;, nh&acirc;n sự, trang thiết bị m&aacute;y m&oacute;c hiện đại. Inox Bảo Long c&oacute; khả năng đ&aacute;p ứng đa dạng nhu cầu của kh&aacute;ch h&agrave;ng, tại Tp. Hồ Ch&iacute; Minh v&agrave; c&aacute;c tỉnh l&acirc;n cận khu vực ph&iacute;a Nam!&nbsp;<br />\r\nSản phẩm ti&ecirc;u biểu:<br />\r\n✎ B&agrave;n ghế inox<br />\r\n✎ Thiết bị bếp c&ocirc;ng nghiệp inox<br />\r\n✎ Thiết bị y tế inox<br />\r\n✎ Đồ gia dụng inox,..<br />\r\nVới cam kết: Thẩm mỹ đẹp, độ bền cao, gi&aacute; th&agrave;nh tốt.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\nĐặc biệt, ch&uacute;ng t&ocirc;i&nbsp;Nhận Gia C&ocirc;ng Sản Phẩm Inox Theo Y&ecirc;u Cầu.<br />\r\nH&atilde;y đến với Inox Bảo Long để sở hữu những sản phẩm chất lượng!</p>\r\n', 'upload/img/pages/1.jpg', 'vi', 1, 1, 1, 0, '', '', '', 0, 1),
(40, 'Công nghệ thầm mỹ', 'cong-nghe-tham-my', '<p>Ti&ecirc;n tiến h&agrave;ng đầu</p>\r\n', '', 'upload/img/pages/gt.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(42, 'Dịch vụ khách hàng', 'dich-vu-khach-hang', '<p>Ch&uacute;ng t&ocirc;i sử dụng c&aacute;c c&ocirc;ng nghệ in ấn v&agrave; th&aacute;nh phẩm ti&ecirc;n tiến nhất Việt Nam để cho ra sản phẩm chất lượng&nbsp;</p>\r\n', '', 'upload/img/pages/img_dv_kh.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(43, 'In mẫu miễn phí', 'in-mau-mien-phi', '<p>Ch&uacute;ng t&ocirc;i sử dụng c&aacute;c c&ocirc;ng nghệ in ấn v&agrave; th&aacute;nh phẩm ti&ecirc;n tiến nhất Việt Nam để cho ra sản phẩm chất lượng&nbsp;</p>\r\n', '', 'upload/img/pages/img_im_mp.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(44, 'Giao hàng tận nơi', 'giao-hang-tan-noi', '<p>Ch&uacute;ng t&ocirc;i sử dụng c&aacute;c c&ocirc;ng nghệ in ấn v&agrave; th&aacute;nh phẩm ti&ecirc;n tiến nhất Việt Nam để cho ra sản phẩm chất lượng&nbsp;</p>\r\n', '', 'upload/img/pages/img_gh_tn.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(45, 'Giá luôn ổn định', 'gia-luon-on-dinh', '<p>Ch&uacute;ng t&ocirc;i sử dụng c&aacute;c c&ocirc;ng nghệ in ấn v&agrave; th&aacute;nh phẩm ti&ecirc;n tiến nhất Việt Nam để cho ra sản phẩm chất lượng&nbsp;</p>\r\n', '', 'upload/img/pages/img_gh_tn1.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0),
(47, 'Lịch sử hình thành', 'lich-su-hinh-thanh', '', '', 'upload/img/pages/img_box.png', 'vi', 0, NULL, NULL, 0, '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `street`
--

CREATE TABLE `street` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `pre` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `districtid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `support_online`
--

CREATE TABLE `support_online` (
  `id` int(11) NOT NULL,
  `yahoo` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `skype` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `image` varchar(70) CHARACTER SET utf8 DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `support_online`
--

INSERT INTO `support_online` (`id`, `yahoo`, `phone`, `skype`, `email`, `name`, `active`, `image`, `type`, `address`) VALUES
(19, 'https://id.zalo.me/account/login?continue=https%3A', '0963.671.678', '', 'tranlinh.manh@gmail.com', 'Mr. LINH', 1, NULL, 2, '130 Hoàng Công Chất, P. Phú Diễn, Q. Bắc Từ Liêm, Tp. Hà Nội'),
(22, '', '0972.050.202', '', 'sofabaolam1@gmail.com', 'Ms. Gấm', 1, NULL, 0, '130 Hoàng Công Chất, Từ Liêm Hà Nội');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `alias`, `lang`, `time`, `title_seo`, `keyword_seo`, `description_seo`, `sort`) VALUES
(1, 'manh', 'manh', 'vi', 1526529820, NULL, NULL, NULL, 0),
(2, 'tuyen', 'tuyen', 'vi', 1526529820, NULL, NULL, NULL, 0),
(3, 'tin tức', 'tin-tuc', 'vi', 1526530190, '', '', '', 1),
(4, 'Kem bb', 'kem-bb', 'vi', 1526530223, '', '', '', 2),
(5, 'kem', 'kem', 'vi', 1526530670, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tags_news`
--

CREATE TABLE `tags_news` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `title_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_seo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags_to_news`
--

CREATE TABLE `tags_to_news` (
  `id` int(11) NOT NULL,
  `id_raovat` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags_to_product`
--

CREATE TABLE `tags_to_product` (
  `id` int(11) NOT NULL,
  `id_tags` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags_to_product`
--

INSERT INTO `tags_to_product` (`id`, `id_tags`, `id_product`) VALUES
(1, 1, 101),
(2, 2, 101),
(3, 5, 100),
(4, 4, 100),
(5, 3, 100),
(6, 1, 100),
(7, 2, 100);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_xnt`
--

CREATE TABLE `tbl_xnt` (
  `id` int(11) NOT NULL,
  `date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_time` int(11) DEFAULT NULL,
  `mahh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sltd` int(11) DEFAULT NULL COMMENT 'Số lượng tồn đầu ngày',
  `sln` int(11) DEFAULT NULL COMMENT 'Số lượng hàng nhập trong ngày',
  `slx` int(11) DEFAULT NULL COMMENT 'Số lượng hàng xuất trong ngày',
  `sltc` int(11) DEFAULT NULL COMMENT 'Số lượng hàng tồn cuối ngày',
  `sltt` int(11) DEFAULT NULL COMMENT 'Số lượng hàng tồn tối thiểu',
  `product_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL COMMENT 'Giá Hiện Tại',
  `price_export` int(11) DEFAULT NULL COMMENT 'Giá Xuất Hàng',
  `price_import` int(11) DEFAULT NULL COMMENT 'Giá Nhập Hàng'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_xnt`
--

INSERT INTO `tbl_xnt` (`id`, `date`, `date_time`, `mahh`, `sltd`, `sln`, `slx`, `sltc`, `sltt`, `product_id`, `price`, `price_export`, `price_import`) VALUES
(356, '1526490000', 1526551961, NULL, 0, 0, 1, 0, 1, 97, 129000, NULL, NULL),
(357, '1526490000', 1526551961, NULL, 0, 0, 1, 0, 1, 95, 229000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `thong_ke_online`
--

CREATE TABLE `thong_ke_online` (
  `id` int(11) NOT NULL,
  `access_date` int(11) NOT NULL,
  `today` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thong_ke_online`
--

INSERT INTO `thong_ke_online` (`id`, `access_date`, `today`) VALUES
(37, 1517850000, 0),
(38, 1517936400, 1),
(39, 1518022800, 29),
(40, 1518109200, 20),
(41, 1519578000, 9),
(42, 1519664400, 15),
(43, 1519750800, 5),
(44, 1521046800, 55),
(45, 1521133200, 9),
(46, 1521219600, 233),
(47, 1526317200, 332),
(48, 1526403600, 258),
(49, 1526490000, 541),
(50, 1526835600, 251),
(51, 1526922000, 245),
(52, 1527008400, 95),
(53, 1527094800, 114),
(54, 1527181200, 51),
(55, 1527267600, 378),
(56, 1527440400, 265),
(57, 1527699600, 427),
(58, 1527786000, 20),
(59, 1528045200, 41),
(60, 1528131600, 43),
(61, 1528218000, 120),
(62, 1528390800, 182),
(63, 1528477200, 37),
(64, 1528736400, 336),
(65, 1528822800, 128),
(66, 1528995600, 435),
(67, 1529341200, 61),
(68, 1529427600, 161),
(69, 1529600400, 80),
(70, 1529686800, 64),
(71, 1529859600, 3391),
(72, 1529946000, 1457),
(73, 1530032400, 746),
(74, 1530118800, 522),
(75, 1530205200, 792),
(76, 1530464400, 800),
(77, 1530550800, 603),
(78, 1530637200, 519),
(79, 1530723600, 95),
(80, 1530810000, 29),
(81, 1530896400, 217),
(82, 1531069200, 83),
(83, 1531155600, 8),
(84, 1531242000, 6),
(85, 1531328400, 5),
(86, 1531414800, 113),
(87, 1531760400, 40),
(88, 1531846800, 10),
(89, 1531933200, 60),
(90, 1532019600, 69),
(91, 1532106000, 299),
(92, 1532278800, 346),
(93, 1532365200, 555),
(94, 1532451600, 122),
(95, 1532538000, 219),
(96, 1532624400, 41),
(97, 1532883600, 463),
(98, 1532970000, 172),
(99, 1533056400, 96),
(100, 1533142800, 46),
(101, 1533229200, 8),
(102, 1533315600, 142),
(103, 1533488400, 567),
(104, 1533574800, 24),
(105, 1533661200, 851),
(106, 1533747600, 460),
(107, 1533834000, 217),
(108, 1534179600, 276),
(109, 1534266000, 389),
(110, 1534352400, 68),
(111, 1534784400, 18),
(112, 1534870800, 64),
(113, 1534957200, 6),
(114, 1535302800, 9),
(115, 1535389200, 36),
(116, 1535475600, 21),
(117, 1535562000, 718),
(118, 1535994000, 49),
(119, 1536166800, 8),
(120, 1536253200, 26),
(121, 1536339600, 166),
(122, 1536512400, 40),
(123, 1536685200, 185),
(124, 1536771600, 11),
(125, 1536858000, 74),
(126, 1536944400, 9),
(127, 1537117200, 17),
(128, 1537203600, 99),
(129, 1537290000, 202),
(130, 1537376400, 22),
(131, 1537549200, 31),
(132, 1537722000, 9),
(133, 1537894800, 42),
(134, 1538067600, 0),
(135, 1538154000, 0),
(136, 1538413200, 21),
(137, 1538499600, 204),
(138, 1538672400, 237),
(139, 1538931600, 4),
(140, 1539104400, 0),
(141, 1539190800, 0),
(142, 1539277200, 1),
(143, 1539363600, 1),
(144, 1539709200, 0),
(145, 1539882000, 0),
(146, 1540141200, 0),
(147, 1540227600, 8),
(148, 1540314000, 0),
(149, 1540573200, 0),
(150, 1540832400, 127),
(151, 1540918800, 74),
(152, 1541005200, 0),
(153, 1199120400, 407),
(154, 1199552400, 823),
(155, 1199638800, 46),
(156, 1199725200, 127),
(157, 1541782800, 102),
(158, 1542042000, 1),
(159, 1542128400, 13),
(160, 1542646800, 180),
(161, 1543338000, 60),
(162, 1543424400, 62),
(163, 1548694800, 18),
(164, 1550422800, 66),
(165, 1550509200, 118),
(166, 1550595600, 596),
(167, 1550682000, 2096),
(168, 1550768400, 1666),
(169, 1551027600, 2011),
(170, 1551114000, 60),
(171, 1551286800, 16),
(172, 1551978000, 238),
(173, 1552237200, 74),
(174, 1554138000, 9),
(175, 1555347600, 102),
(176, 1555434000, 8),
(177, 1556125200, 4),
(178, 1556730000, 10),
(179, 1557680400, 71),
(180, 1557853200, 10),
(181, 1558026000, 109),
(182, 1558890000, 1282),
(183, 1558976400, 1428),
(184, 1559062800, 807),
(185, 1559149200, 203),
(186, 1559235600, 99),
(187, 1559494800, 2),
(188, 1559581200, 189),
(189, 1559840400, 84),
(190, 1559926800, 492),
(191, 1560099600, 387),
(192, 1560186000, 214),
(193, 1560358800, 162),
(194, 1560445200, 8),
(195, 1560531600, 239),
(196, 1560704400, 621),
(197, 1560790800, 124),
(198, 1560877200, 73),
(199, 1560963600, 555),
(200, 1561050000, 20),
(201, 1562173200, 3),
(202, 1563814800, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `md5_id` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `username` varchar(35) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(35) CHARACTER SET utf8 NOT NULL,
  `fullname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `use_salt` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `shop_name` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `avt_dir` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `avatar` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `use_logo` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `block` tinyint(3) UNSIGNED DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` int(1) UNSIGNED DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address_province` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `address_district` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `address_ward` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `use_mobile` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `use_face` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `use_yahoo` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `use_skype` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `use_group` int(3) UNSIGNED DEFAULT NULL,
  `active` int(1) UNSIGNED DEFAULT NULL,
  `use_key` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `smskey` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `token` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `deleted` tinyint(3) UNSIGNED DEFAULT NULL,
  `use_regisdate` int(11) UNSIGNED DEFAULT NULL,
  `use_enddate` int(11) UNSIGNED DEFAULT NULL,
  `lastest_login` int(11) UNSIGNED DEFAULT NULL,
  `signup_date` int(11) DEFAULT NULL,
  `lever` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `md5_id`, `username`, `phone`, `email`, `password`, `fullname`, `use_salt`, `shop_name`, `avt_dir`, `avatar`, `use_logo`, `block`, `birthday`, `sex`, `address`, `address_province`, `address_district`, `address_ward`, `use_mobile`, `use_face`, `use_yahoo`, `use_skype`, `use_group`, `active`, `use_key`, `smskey`, `token`, `deleted`, `use_regisdate`, `use_enddate`, `lastest_login`, `signup_date`, `lever`) VALUES
(2, NULL, 'admin', 'admin', 'daibkz@gmail.com', '2dea47c85f72a6ee3703facec5b18c2a', 'Admin', 'Wm8KT06E', NULL, NULL, NULL, NULL, NULL, '0000-00-00', 1, 'Ninh Binh', '66', NULL, NULL, '0986839102', NULL, 'dainguyen', '', 4, 1, '9671508f22c9982fbac60ffc130f9b7811ec2b4d7f6e9f253679a3b950a3f5c8', NULL, NULL, NULL, 1498496400, 1814029200, 1540432578, NULL, 2),
(617, '5d44ee6f2c3f71b73125876103c8f6c4', 'taikhoan', '01649962597', 'cauhai.1297@gmail.com', 'ab77a83b110f3517f746938bf49d0ae3', 'Nguyễn Văn Hải', NULL, NULL, '04072017', '986bc2226881542276ecf99e72443fc7.jpg', NULL, 0, NULL, NULL, 'Số 38 - Đường Dương Khuê ', '01', '005', '00163', NULL, NULL, NULL, NULL, NULL, 1, NULL, '595ae9294eb32', '2d9228de1d6c18ad3ab56b2a0c6d2def', 0, 1499130153, NULL, 1500969769, NULL, 0),
(620, 'b73dfe25b4b8714c029b37a6ad3006fa', 'taikhoan', '0986126561', 'hungvu258@gmail.com', 'a9f1ea798b9bcdcf0573dad7af97cbe0', 'Vũ Văn Hùng', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '595f3520d9e2e', '86054560b15b889346283a906596eaa6', 0, 1499411744, NULL, 1499411806, NULL, 0),
(612, 'f76a89f0cb91bc419542ce9fa43902dc', 'ĐẶNG VĂN ĐIỀN', '0965986385', 'cauvan1995@gmail.com', 'c26be8aaf53b15054896983b43eb6a65', 'ĐẶNG VĂN ĐIỀN', '-h01K8w3', NULL, '03072017', 'ad29f13d8e28e7cabeaf257192385ba6.png', NULL, 0, NULL, 1, 'Số 36 Dương Khuê', '01', '005', '00163', NULL, NULL, NULL, NULL, 4, 1, 'c51519f1ba3de1da58ef5bd2850861e5bf233a4b55eec27fdef32357a98b7205', '5954b39739ebb', '36fb0bab89277945551398212d0c1d8e', 0, 1499619600, 2067613200, 1504604112, 2017, 1),
(619, 'cdc0d6e63aa8e41c89689f54970bb35f', 'taikhoan', '0985088848', 'ngoc.dbsk@gmail.com', 'acb4798109c61257851f53f7521d8a4f', 'Đỗ Thị Ngọc', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '595c4381c1481', 'cd3c498d71a8889eebe96ed5946df7a3', 0, 1499218817, NULL, 1499503366, NULL, 0),
(616, '7750ca3559e5b8e1f44210283368fc16', 'taikhoan', '0915460000', 'ktviet.com.vn@gmail.com', '6140c8871dd9df0c091760c83d3562a7', 'Kỹ thuật việt', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Số 38 Đường Dương Khuê ', '01', '005', '00163', NULL, NULL, NULL, NULL, NULL, 1, NULL, '595a22e73caf4', 'd04eedd402adbee246d22bd05a16d82f', 0, 1499079399, NULL, 1501031009, NULL, 0),
(621, '85fc37b18c57097425b52fc7afbb6969', 'taikhoan', '0987999947', 'ktviet.com.vn@gmail.com', '6140c8871dd9df0c091760c83d3562a7', 'aalo.vn', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '5960999273327', '9652a76f8510d397d571651a98234986', 0, 1499502994, NULL, 1500945384, NULL, 0),
(622, '3871bd64012152bfb53fdf04b401193f', 'taikhoan', '0869118060', 'Sales@maytinhtruongson.com.vn', '29ac98cd17193f4ce1fe80017bff7cb8', 'Phan Văn Trường', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '59638b308df68', 'f082409b697ee95fbd373f4078ade2e3', 0, 1499695920, NULL, NULL, NULL, 0),
(623, 'a733fa9b25f33689e2adbe72199f0e62', 'taikhoan', '0983003484', 'cunhue@gmail.com', '3c31d5cf8058f39ef8ed267658fcae11', 'Nguyễn Trọng Hiền', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '59661988955c0', 'd89f5465c4496ea3cfe6a7f3b57c365a', 0, 1499863432, NULL, 1499863576, NULL, 0),
(629, '051e4e127b92f5d98d3c79b195f2b291', 'taikhoan', '0975279573', 'vietbk193@gmail.com', 'f1160b722eceefca344715db03d1c66b', 'Ma Thế Việt', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '5972f6b2ed53b', '4cd25c877db0de884d1dcf85f1211fc6', 0, 1500706482, NULL, 1500706576, NULL, 0),
(628, '42e77b63637ab381e8be5f8318cc28a2', 'taikhoan', '0964278201', 'nguyenvantrisahara@gmail.com', 'ef9468922149cf75765bab2d348d64aa', 'Nguyễn Văn Trí', NULL, NULL, '21072017', '6c92927ea9071ce920efcc34f6f732c2.jpg', NULL, 0, NULL, NULL, '52 Đường Lê Quang Đạo Quận Nam Từ Liêm', '01', '019', '00592', NULL, NULL, NULL, NULL, NULL, 0, NULL, '5969ae9b73f4e', '878cbe26fbc949c65aaf15d3ba3019b9', 0, 1500098203, NULL, 1500686349, NULL, 0),
(638, '4c27cea8526af8cfee3be5e183ac9605', 'taikhoan', '0982255552', 'buivananh.th@gmail.com', '01b080fe7398c4c669be0be9cd78792d', 'Vân', '9SZDFmt3', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '09c7375321d2ce9a405e4c1606850ccdb7413aed9db60ec941a374a31c42f129', NULL, '553048f16cca9be3bbd6cf0ea897dd39', NULL, 1505926800, NULL, 1506331171, NULL, 0),
(639, '0f96613235062963ccde717b18f97592', 'taikhoan', '0982255552', 'Van@gmail.com', 'c26be8aaf53b15054896983b43eb6a65', 'Vân anh', 'S3phkf4r', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '6760ca72cfe94cd737b7a804b6f415f2d28ed2339429656e2fb086e47312517d', NULL, 'aec76ec422606554a14edd7ff28cee3f', NULL, 1505926800, NULL, NULL, NULL, 0),
(646, '0ff39bbbf981ac0151d340c9aa40e63e', 'huyen1', '0384932462', 'danghuyen6297@gmail.com', 'cdb6962bc528e37a4b44d77bba500f71', 'Đặng Thị Huyền', 'aH0rJ8uE', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '7d4da39237a0759ce86dc9e59c2010ace331ce980fb3dead5233a072c7105417', NULL, '3ca0ab12f6a1a634fa7f53cea8a91ff5', NULL, 1559149200, 1559149200, 1559149200, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_sms`
--

CREATE TABLE `user_sms` (
  `id` int(11) NOT NULL,
  `smsid` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8,
  `result` int(11) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `error` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `comment` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `create_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_sms`
--

INSERT INTO `user_sms` (`id`, `smsid`, `userid`, `phone`, `content`, `result`, `count`, `error`, `comment`, `create_at`) VALUES
(5, '1130b1db-ffcb-477b-862b-040e60174a888', 76, '0974901590', 'Mã Kích Hoạt SMS : 5950ac70440c6', 100, 0, NULL, NULL, '2017-06-26 13:40:48'),
(6, '3141f19d-e25d-46fb-9cff-9c1cdd3371fb8', 76, '0974901590', 'abc test gửi lại', 100, 0, NULL, 'gửi lại', '2017-06-26 13:41:39'),
(7, NULL, 610, '0986839102', 'Mã Kích Hoạt SMS : 5954a8af5779f', 103, 0, 'Balance not enough to send message', NULL, '2017-06-29 14:13:53'),
(8, NULL, 611, '0986839102', 'Mã Kích Hoạt SMS : 5954a9ed7f497', 103, 0, 'Balance not enough to send message', NULL, '2017-06-29 14:19:09'),
(9, NULL, 612, '0965986385', 'Mã Kích Hoạt SMS : 5954b39739ebb', 103, 0, 'Balance not enough to send message', NULL, '2017-06-29 15:00:23'),
(10, NULL, 613, '01649962597', 'Mã Kích Hoạt SMS : 5955bbaedda8d', 103, 0, 'Balance not enough to send message', NULL, '2017-06-30 09:47:11'),
(11, NULL, 614, '987654321', 'Mã Kích Hoạt SMS : 595606e747183', 103, 0, 'Balance not enough to send message', NULL, '2017-06-30 15:08:07'),
(12, NULL, 615, '324234234', 'Mã Kích Hoạt SMS : 5956074367a46', 99, 0, 'Phone not valid:324234234', NULL, '2017-06-30 15:09:39'),
(13, NULL, 616, '0915460000', 'Mã Kích Hoạt SMS : 595a22e73caf4', 103, 0, 'Balance not enough to send message', NULL, '2017-07-03 17:56:39'),
(14, NULL, 617, '01649962597', 'Mã Kích Hoạt SMS : 595ae9294eb32', 103, 0, 'Balance not enough to send message', NULL, '2017-07-04 08:02:33'),
(15, NULL, 618, '0985088848', 'Mã Kích Hoạt SMS : 595b3b0287471', 103, 0, 'Balance not enough to send message', NULL, '2017-07-04 13:51:46'),
(16, NULL, 619, '0985088848', 'Mã Kích Hoạt SMS : 595c4381c1481', 103, 0, 'Balance not enough to send message', NULL, '2017-07-05 08:40:19'),
(17, NULL, 620, '0986126561', 'Mã Kích Hoạt SMS : 595f3520d9e2e', 103, 0, 'Balance not enough to send message', NULL, '2017-07-07 14:15:45'),
(18, NULL, 621, '0987999947', 'Mã Kích Hoạt SMS : 5960999273327', 103, 0, 'Balance not enough to send message', NULL, '2017-07-08 15:36:34'),
(19, NULL, 622, '0869118060', 'Mã Kích Hoạt SMS : 59638b308df68', 103, 0, 'Balance not enough to send message', NULL, '2017-07-10 21:12:00'),
(20, NULL, 623, '0983003484', 'Mã Kích Hoạt SMS : 59661988955c0', 103, 0, 'Balance not enough to send message', NULL, '2017-07-12 19:43:52'),
(21, NULL, 624, '01652724972', 'Mã Kích Hoạt SMS : 5966e56f21617', 103, 0, 'Balance not enough to send message', NULL, '2017-07-13 10:13:51'),
(22, NULL, 625, '09164278201', 'Mã Kích Hoạt SMS : 59697ab70dbfb', 99, 0, 'Phone not valid:09164278201', NULL, '2017-07-15 09:15:19'),
(23, NULL, 626, '0964278201', 'Mã Kích Hoạt SMS : 59697b7e356e4', 103, 0, 'Balance not enough to send message', NULL, '2017-07-15 09:18:38'),
(24, NULL, 627, '09642728201', 'Mã Kích Hoạt SMS : 59697cba3fe16', 99, 0, 'Phone not valid:09642728201', NULL, '2017-07-15 09:23:54'),
(25, NULL, 628, '0964278201', 'Mã Kích Hoạt SMS : 5969ae9b73f4e', 103, 0, 'Balance not enough to send message', NULL, '2017-07-15 12:56:43'),
(26, NULL, 629, '0975279573', 'Mã Kích Hoạt SMS : 5972f6b2ed53b', 103, 0, 'Balance not enough to send message', NULL, '2017-07-22 13:54:43'),
(27, NULL, 630, '01648464081', 'Mã Kích Hoạt SMS : 5974f19ddd13a', 103, 0, 'Balance not enough to send message', NULL, '2017-07-24 01:57:34');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link_video` text CHARACTER SET utf8,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `image` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `name`, `description`, `link_video`, `title_seo`, `description_seo`, `keyword_seo`, `lang`, `category_id`, `home`, `hot`, `focus`, `sort`, `image`, `active`, `alias`) VALUES
(1, 'SKIN5 - OCEANIA', '<p>n&ocirc;i dung video h&aacute;t rất hay nh&eacute;</p>\r\n', 'gf22lsI2oT0', '', '', '', 'vi', 3, 1, NULL, NULL, 1, 'upload/img/video/img_news_video1.png', 1, 'skin5-oceania'),
(3, 'IVE500 - OCEANIA', '', '3GaoeXv2T90', '', '', '', 'vi', 3, NULL, NULL, NULL, 3, 'upload/img/video/img_news_video2.png', 0, 'ive500-oceania'),
(2, 'Raffine - Auto MTS - OCEANIA', '<p>nội dung m&ocirc; tả</p>\r\n', '1mx44MNB4M4', '', '', '', 'vi', 3, 1, NULL, 1, 2, 'upload/img/video/img_news_video.png', 1, 'raffine-auto-mts-oceania'),
(4, 'R1 Face - OCEANIA', '', 'mDZGJWIifW8', '', '', '', 'vi', 3, NULL, NULL, NULL, 4, 'upload/img/video/img_news_video3.png', 0, 'r1-face-oceania'),
(5, 'Smart Sonic - OCEANIA', '', 'Bm6u9zHpp0w', '', '', '', 'vi', 3, NULL, NULL, NULL, 5, 'upload/img/video/img_news_video4.png', 0, 'smart-sonic-oceania');

-- --------------------------------------------------------

--
-- Table structure for table `video_category`
--

CREATE TABLE `video_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `hot` tinyint(1) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_seo` text CHARACTER SET utf8,
  `keyword_seo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lang` char(10) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video_category`
--

INSERT INTO `video_category` (`id`, `name`, `alias`, `sort`, `home`, `hot`, `focus`, `image`, `title_seo`, `description_seo`, `keyword_seo`, `lang`, `description`, `parent_id`) VALUES
(3, 'Video', 'video', 1, 1, NULL, NULL, 'upload/img/video/dia-diem-du-lich-4.jpg', '', '', NULL, 'vi', '<p>nội dung m&ocirc; tả</p>\r\n', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ward`
--

CREATE TABLE `ward` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `pre` varchar(50) CHARACTER SET utf8 NOT NULL,
  `districtid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `pro_id` decimal(21,0) DEFAULT NULL,
  `user_id` decimal(21,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alias`
--
ALTER TABLE `alias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `code_sale`
--
ALTER TABLE `code_sale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments_binhluan`
--
ALTER TABLE `comments_binhluan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_banner`
--
ALTER TABLE `config_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_checkpro`
--
ALTER TABLE `config_checkpro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_menu`
--
ALTER TABLE `config_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_wiget`
--
ALTER TABLE `config_wiget`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_debt`
--
ALTER TABLE `customer_debt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inuser`
--
ALTER TABLE `inuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inuser_category`
--
ALTER TABLE `inuser_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inuser_to_category`
--
ALTER TABLE `inuser_to_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices_detail`
--
ALTER TABLE `invoices_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_shopping`
--
ALTER TABLE `map_shopping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_category`
--
ALTER TABLE `media_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_images`
--
ALTER TABLE `media_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_to_category`
--
ALTER TABLE `news_to_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_brand`
--
ALTER TABLE `product_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_img`
--
ALTER TABLE `product_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_locale`
--
ALTER TABLE `product_locale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_old`
--
ALTER TABLE `product_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tag`
--
ALTER TABLE `product_tag`
  ADD PRIMARY KEY (`product_tag_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `language_id` (`lang`),
  ADD KEY `tag` (`tag`);

--
-- Indexes for table `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_to_color`
--
ALTER TABLE `product_to_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_to_option`
--
ALTER TABLE `product_to_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_to_season`
--
ALTER TABLE `product_to_season`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_to_size`
--
ALTER TABLE `product_to_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pro_size`
--
ALTER TABLE `pro_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_images`
--
ALTER TABLE `p_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raovat`
--
ALTER TABLE `raovat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raovat_category`
--
ALTER TABLE `raovat_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raovat_images`
--
ALTER TABLE `raovat_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raovat_tag`
--
ALTER TABLE `raovat_tag`
  ADD PRIMARY KEY (`raovat_tag_id`);

--
-- Indexes for table `raovat_to_category`
--
ALTER TABLE `raovat_to_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_log`
--
ALTER TABLE `site_log`
  ADD PRIMARY KEY (`site_log_id`);

--
-- Indexes for table `site_option`
--
ALTER TABLE `site_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staticpage`
--
ALTER TABLE `staticpage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `street`
--
ALTER TABLE `street`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_online`
--
ALTER TABLE `support_online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags_news`
--
ALTER TABLE `tags_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags_to_news`
--
ALTER TABLE `tags_to_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags_to_product`
--
ALTER TABLE `tags_to_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_xnt`
--
ALTER TABLE `tbl_xnt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thong_ke_online`
--
ALTER TABLE `thong_ke_online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sms`
--
ALTER TABLE `user_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_category`
--
ALTER TABLE `video_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ward`
--
ALTER TABLE `ward`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `alias`
--
ALTER TABLE `alias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2369;

--
-- AUTO_INCREMENT for table `code_sale`
--
ALTER TABLE `code_sale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `comments_binhluan`
--
ALTER TABLE `comments_binhluan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `config_banner`
--
ALTER TABLE `config_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `config_checkpro`
--
ALTER TABLE `config_checkpro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `config_menu`
--
ALTER TABLE `config_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `config_wiget`
--
ALTER TABLE `config_wiget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `customer_debt`
--
ALTER TABLE `customer_debt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=698;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;

--
-- AUTO_INCREMENT for table `inuser`
--
ALTER TABLE `inuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `inuser_category`
--
ALTER TABLE `inuser_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `inuser_to_category`
--
ALTER TABLE `inuser_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `invoices_detail`
--
ALTER TABLE `invoices_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `map_shopping`
--
ALTER TABLE `map_shopping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `media_category`
--
ALTER TABLE `media_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `media_images`
--
ALTER TABLE `media_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `news_to_category`
--
ALTER TABLE `news_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `product_brand`
--
ALTER TABLE `product_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `product_img`
--
ALTER TABLE `product_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_locale`
--
ALTER TABLE `product_locale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_old`
--
ALTER TABLE `product_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_price`
--
ALTER TABLE `product_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_tag`
--
ALTER TABLE `product_tag`
  MODIFY `product_tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_to_category`
--
ALTER TABLE `product_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=607;

--
-- AUTO_INCREMENT for table `product_to_color`
--
ALTER TABLE `product_to_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `product_to_option`
--
ALTER TABLE `product_to_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_to_season`
--
ALTER TABLE `product_to_season`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_to_size`
--
ALTER TABLE `product_to_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `pro_size`
--
ALTER TABLE `pro_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `p_images`
--
ALTER TABLE `p_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `raovat`
--
ALTER TABLE `raovat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `raovat_category`
--
ALTER TABLE `raovat_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `raovat_images`
--
ALTER TABLE `raovat_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `raovat_tag`
--
ALTER TABLE `raovat_tag`
  MODIFY `raovat_tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `raovat_to_category`
--
ALTER TABLE `raovat_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `site_log`
--
ALTER TABLE `site_log`
  MODIFY `site_log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63379;

--
-- AUTO_INCREMENT for table `site_option`
--
ALTER TABLE `site_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staticpage`
--
ALTER TABLE `staticpage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `street`
--
ALTER TABLE `street`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `support_online`
--
ALTER TABLE `support_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tags_news`
--
ALTER TABLE `tags_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags_to_news`
--
ALTER TABLE `tags_to_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags_to_product`
--
ALTER TABLE `tags_to_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_xnt`
--
ALTER TABLE `tbl_xnt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=358;

--
-- AUTO_INCREMENT for table `thong_ke_online`
--
ALTER TABLE `thong_ke_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=647;

--
-- AUTO_INCREMENT for table `user_sms`
--
ALTER TABLE `user_sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `video_category`
--
ALTER TABLE `video_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ward`
--
ALTER TABLE `ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
