$(document).ready(function() {
    $('.btn-show-chid').click(function(e) {
        $(this).find('.toggle-icon').attr('data-icon', function(index, attr) {
            return attr == 'chevron-down' ? 'chevron-up' : 'chevron-down';
        });
        $(this).parent().toggleClass('mobile-child-active');
    });
    $('.mobile-button').click(function(e) {
        $(this).parent().find('#mobile-menu-list').addClass('mobile-menu-active');
        $(this).find('.mobile-button-click').toggleClass('menu-change');
    });
    $('.mobile-button-close').click(function(e) {
        $(this).parent().parent().removeClass('mobile-menu-active');
        $('#mobile-button-visible').find('.mobile-button-click').removeClass('menu-change');
    });
    $('.banner-slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: !0,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 7000,
        draggable: false,
        arrows: false,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    draggable: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });
    $('.top-category-slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        slidesToShow: 7,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 7000,
        nextArrow: '<img src="img/redo.png" class="btn-category-next" alt="Image">',
        prevArrow: '<img src="img/undo.png" class="btn-category-prev" alt="Image">',
        responsive: [{
                breakpoint: 1980,
                settings: {
                    slidesToShow: 7,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 1680,
                settings: {
                    slidesToShow: 6,
                    draggable: true,
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 5,
                    draggable: true,
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 4,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });


    $('.product-sale-slide').slick({
        dots: false,
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,

        autoplaySpeed: 7000,
        nextArrow: '<img src="img/redo.png" class="btn-product-next btn-slide" alt="Image">',
        prevArrow: '<img src="img/undo.png" class="btn-product-prev btn-slide" alt="Image">',
        responsive: [{
                breakpoint: 1980,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 1680,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });

    $('.product-best-seller').slick({
        dots: false,
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        slidesToShow: 5,
        slidesToScroll: 1,
        margin: 10,
        autoplay: true,
        autoplaySpeed: 7000,
        nextArrow: "<div class='btn-slick-slide'><button class='btn-slide-next'><i class='fa fa-chevron-right   btn-seller-prev'></i></button></div>",
        prevArrow: "<div class='btn-slick-slide'><button class='btn-slide-prev'><i class='fa fa-chevron-left   btn-seller-prev'></i></button></div>",
        responsive: [{
                breakpoint: 1980,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 1680,
                settings: {
                    slidesToShow: 5,
                    draggable: true,
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 4,
                    draggable: true,
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 3,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });
    $('.electronic-product-slide').slick({
        dots: false,
        infinite: true,
        speed: 500,
        margin: 10,
        cssEase: 'linear',
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 7000,
        nextArrow: "<div class='btn-slick-slide'><button class='btn-slide-next'><i class='fa fa-chevron-right   btn-seller-prev'></i></button></div>",
        prevArrow: "<div class='btn-slick-slide'><button class='btn-slide-prev'><i class='fa fa-chevron-left   btn-seller-prev'></i></button></div>",
        responsive: [{
                breakpoint: 1980,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1680,
                settings: {
                    slidesToShow: 4,
                    draggable: true,
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 3,
                    draggable: true,
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 3,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });
    $('.partner-slide').slick({
        dots: false,
        infinite: true,
        speed: 500,
        margin: 10,
        cssEase: 'linear',
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 7000,
        nextArrow: "<img src='img/next_arrow.png' class='btn-product-electronic btn-electronic-next' >",
        prevArrow: "<img src='img/back_arrow.png' class='btn-product-electronic btn-electronic-prev' >",
        responsive: [{
                breakpoint: 1980,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1680,
                settings: {
                    slidesToShow: 5,
                    draggable: true,
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 4,
                    draggable: true,
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 3,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });
    // $('.partner-slide').owlCarousel({
    //     items: 5,
    //     autoplay: false,
    //     pagination: false,
    //     navigation: true,
    //     navigationText: [
    //         "<img src='img/back_arrow.png' class='btn-product-electronic btn-electronic-prev' >",
    //         "<img src='img/next_arrow.png' class='btn-product-electronic btn-electronic-next' >"
    //     ],
    //     responsive: {
    //         1980: {
    //             items: 5,
    //             nav: true
    //         },
    //         1680: {
    //             items: 4,
    //             nav: false
    //         },
    //         1366: {
    //             items: 3,
    //             nav: true,
    //             loop: false
    //         },
    //         981: {
    //             items: 2,
    //             nav: true,
    //             loop: false
    //         },
    //         768: {
    //             items: 1,
    //             nav: true,
    //             loop: false
    //         },
    //         480: {
    //             items: 1,
    //             nav: true,
    //             loop: false
    //         },
    //         400: {
    //             items: 1,
    //             nav: true,
    //             loop: false
    //         }
    //     }
    // });
    $('.product-similar-list').slick({
        dots: false,
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 7000,
        nextArrow: "<div class='btn-slick-slide'><button class='btn-slide-next'><i class='fa fa-chevron-right   btn-seller-prev'></i></button></div>",
        prevArrow: "<div class='btn-slick-slide'><button class='btn-slide-prev'><i class='fa fa-chevron-left   btn-seller-prev'></i></button></div>",
        responsive: [{
                breakpoint: 1980,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 1680,
                settings: {
                    slidesToShow: 5,
                    draggable: true,
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 4,
                    draggable: true,
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 3,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });

    $('.recently-slide').slick({
        dots: false,
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 7000,
        nextArrow: "<div class='btn-slick-slide'><button class='btn-slide-next'><i class='fa fa-chevron-right   btn-seller-prev'></i></button></div>",
        prevArrow: "<div class='btn-slick-slide'><button class='btn-slide-prev'><i class='fa fa-chevron-left   btn-seller-prev'></i></button></div>",
        responsive: [{
                breakpoint: 1980,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 1680,
                settings: {
                    slidesToShow: 5,
                    draggable: true,
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 4,
                    draggable: true,
                }
            },
            {
                breakpoint: 981,
                settings: {
                    slidesToShow: 3,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    draggable: true,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    draggable: true,
                    slidesToScroll: 1
                }
            }

        ]
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        vertical: true,
        verticalSwiping: true,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        focusOnSelect: true
    });

    // ZOOM
    $('.ex1').zoom();

    // STYLE GRAB
    $('.ex2').zoom({ on: 'grab' });

    // STYLE CLICK
    $('.ex3').zoom({ on: 'click' });

    // STYLE TOGGLE
    $('.ex4').zoom({ on: 'toggle' });

});